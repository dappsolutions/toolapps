var Anomali = {
 module: function () {
  return 'anomali';
 },

 add: function () {
  window.location.href = url.base_url(Anomali.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Anomali.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Anomali.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Anomali.module()) + "index";
   }
  }
 },

 getPostDetailItem: function () {
  var data = [];
  var tr = $('tbody.table_anomali').find('tr');
  $.each(tr, function () {
   var id_alat = $(this).find('td:eq(0)').attr('id_alat');
   data.push({
    'id_alat': id_alat,
    'id': $(this).attr('id'),
    'status': $(this).find('select#status').val(),
    'keterangan_anomali': $(this).find('textarea#keterangan_anomali').val(),
   });
  });

  return data;
 },

 getPostDetailItemUbah: function () {
  var data = [];
  var tr = $('tbody.table_anomali').find('tr');
  $.each(tr, function () {
   if ($(this).hasClass('ubah')) {
    var id_alat = $(this).find('td:eq(0)').attr('id_alat');
    data.push({
     'id': $(this).attr('id'),
     'id_alat': id_alat,
     'status': $(this).find('select#status').val(),
     'keterangan_anomali': $(this).find('textarea#keterangan_anomali').val(),
    });
   }
  });

  return data;
 },

 getPostDetailItemHapus: function () {
  var data = [];
  var tr = $('tbody.table_anomali').find('tr');
  $.each(tr, function () {
   if ($(this).hasClass('hapus')) {
    data.push({
     'id': $(this).attr('id'),
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'tanggal_anomali': $('#tanggal_anomali').val(),
   'detail_item': Anomali.getPostDetailItem(),
   'detail_item_ubah': Anomali.getPostDetailItemUbah(),
   'detail_item_hapus': Anomali.getPostDetailItemHapus()
  };

  return data;
 },

 simpan: function (id) {
  var data = Anomali.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Anomali.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Anomali.module()) + "detail" + '/' + resp.anomali;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Anomali.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Anomali.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Anomali.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Anomali.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal_anomali').pickadate({
   format: 'yyyy-mm-dd',
   formatSubmit: 'yyyy-mm-dd',
  });
 },

 showDataAlat: function (elm) {
  var index_tr = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   data: {
    index_tr: index_tr
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Anomali.module()) + "showDataAlat",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },
 pilihAlat: function (elm, idAlat) {
  var index_tr = $('input#index_tr').val();
  var kode_alat = $(elm).closest('tr').attr('kode_alat');
  var nama_alat = $.trim($(elm).closest('tr').find('td:eq(4)').text());
  var table_anomali = $('tbody.table_anomali').find('tr:eq(' + index_tr + ')');
  var no_seri = $.trim($(elm).closest('tr').find('td:eq(3)').text());
  var data = "<b>" + nama_alat + " <label class='text-success'>(" + no_seri + ")</label></b>";
  table_anomali.find('td:eq(0)').html(data);
  table_anomali.find('td:eq(0)').attr('id_alat', idAlat);
  table_anomali.find('td:eq(1)').html('<b>' + kode_alat + '</b>');
  message.closeDialog();
 },

 addDetail: function (elm) {
  var button = '<button id="" class="btn btn-info" onclick="Anomali.showDataAlat(this)">Pilih Alat</button>';
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(0)').html(button);
  newTr.find('textarea').val('');
  newTr.find('td:eq(4)').html('<i class="mdi mdi-minus mdi-24px hover" onclick="Anomali.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 hapusAnomali: function (elm) {
  $(elm).closest('tr').addClass('hapus');
  $(elm).closest('tr').addClass('display-none');
 },

 ubahAnomali: function (elm) {
  var tr = $(elm).closest('tr');
  var id = tr.attr('id');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Anomali.module()) + "ubahAnomali/" + id,
   success: function (resp) {
    tr.html(resp);
    if (!tr.hasClass('ubah')) {
     tr.addClass('ubah');
    }
   }
  });
 },
 
 cariAlat: function(elm, e){
  var value = $(elm).val();
  Anomali.searchInTable(value, "#tb_alat_detail");
 },
 
 searchInTable: function (value, elm) {
  $("" + elm + " tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },
};

$(function () {
 Anomali.setDate();
});