var GarduInduk = {
 module: function () {
  return 'gardu_induk';
 },

 add: function () {
  window.location.href = url.base_url(GarduInduk.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(GarduInduk.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(GarduInduk.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(GarduInduk.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'upt': $('#upt').val(),
   'gardu': $('#gardu_induk').val(),
   'parent': $('#parent').val(),
   'alamat': $('#alamat').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = GarduInduk.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(GarduInduk.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(GarduInduk.module()) + "detail" + '/' + resp.gardu;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(GarduInduk.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(GarduInduk.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(GarduInduk.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(GarduInduk.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 }
};


$(function(){
 $('select#parent').select2();
});