var Grafik = {
 randomData: function () {
  barChartData.datasets.forEach(function (dataset) {
   dataset.data = dataset.data.map(function () {
    return randomScalingFactor();
   });
  });
  window.myBar.update();
 },

 getDataAlatRusak: function () {
  var data_alat_rusak = $('#data_total_rusak').val();
  var alat = data_alat_rusak.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 12; i++) {
   var value = parseFloat(alat[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

  //  console.log(data_all);
  return data_all;
 },

 getDataAlatNormal: function () {
  var data_total_normal = $('#data_total_normal').val();
  var alat = data_total_normal.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 12; i++) {
   var value = parseFloat(alat[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

  //  console.log(data_all);
  return data_all;
 },

 getDataNotValid: function () {
  var data_total_not_valid = $('#data_total_not_valid').val();

  var anomali = data_total_not_valid.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(anomali[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

//  console.log(data_all);
  return data_all;
 },

 getDataValid: function () {
  var data_total_valid = $('#data_total_valid').val();

  var anomali = data_total_valid.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(anomali[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

//  console.log(data_all);
  return data_all;
 },

 getDataPembelianResep: function () {
  // var data_pembelian = $('#data_pembelian_resep').val();
  //  var pembelian = data_pembelian.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   // var value = parseFloat(pembelian[i]).toFixed(2);
   var value = i * 30;
   data_all.push(value);
  }

//  console.log(data_all);
  return data_all;
 },

 getDataLabelTotalAlat: function () {
  var data = [];

  data.push("Jan");
  data.push("Feb");
  data.push("Mar");
  data.push("Apr");
  data.push("Mei");
  data.push("Jun");
  data.push("Jul");
  data.push("Ags");
  data.push("Sep");
  data.push("Okt");
  data.push("Nov");
  data.push("Des");

  return data;
 },

 getDataLabelAnomali: function () {
  var data = [];
  data.push("Jan");
  data.push("Feb");
  data.push("Mar");
  data.push("Apr");
  data.push("Mei");
  data.push("Jun");
  data.push("Jul");
  data.push("Ags");
  data.push("Sep");
  data.push("Okt");
  data.push("Nov");
  data.push("Des");


  return data;
 },

 setGrafikTotalAlat: function () {
  var ctx = document.getElementById('canvas_alat').getContext('2d');
  var barChartData = {
   labels: Grafik.getDataLabelTotalAlat(),
   datasets: [{
     label: 'Alat Rusak',
     backgroundColor: window.chartColors.red,
     yAxisID: 'rusak',
     data: Grafik.getDataAlatRusak()
    }, {
     label: 'Alat Normal',
     backgroundColor: window.chartColors.green,
     yAxisID: 'normal',
     data: Grafik.getDataAlatNormal()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Total Alat Rusak dan Normal'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'rusak',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_rusak').val()) > parseInt($('#total_data_normal').val()) ? parseInt($('#total_data_rusak').val()) : parseInt($('#total_data_normal').val())
       }
      }, {
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'normal',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_rusak').val()) > parseInt($('#total_data_normal').val()) ? parseInt($('#total_data_rusak').val()) : parseInt($('#total_data_normal').val())
       }
      }]
    }
   }
  });
 },

 setGrafikAnomali: function () {
  var ctx = document.getElementById('canvas_anomali').getContext('2d');
  var barChartData = {
   labels: Grafik.getDataLabelAnomali(),
   datasets: [{
     label: 'Belum Validasi',
     backgroundColor: window.chartColors.yellow,
     yAxisID: 'not_valid',
     data: Grafik.getDataNotValid()
    }, {
     label: 'Sudah Validasi',
     backgroundColor: window.chartColors.green,
     yAxisID: 'valid',
     data: Grafik.getDataValid()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Pengajuan Anomali'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'not_valid',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_not_valid').val()) > parseInt($('#total_data_valid').val()) ? parseInt($('#total_data_not_valid').val()) : parseInt($('#total_data_valid').val())
       }
      }, {
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'valid',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_not_valid').val()) > parseInt($('#total_data_valid').val()) ? parseInt($('#total_data_not_valid').val()) : parseInt($('#total_data_valid').val())
       }
      }]
    }
   }
  });
 }
};


$(function () {
// Grafik.setGrafikTotalAlat();
// Grafik.setGrafikAnomali();
});
