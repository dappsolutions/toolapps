var ValidasiAnomali = {
 module: function () {
  return 'validasi_anomali';
 },

 add: function () {
  window.location.href = url.base_url(ValidasiAnomali.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(ValidasiAnomali.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(ValidasiAnomali.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(ValidasiAnomali.module()) + "index";
   }
  }
 },

 getPostDetailItem: function () {
  var data = [];
  var tr = $('tbody.table_anomali').find('tr');
  $.each(tr, function () {
   var id_alat = $(this).find('td:eq(0)').attr('id_alat');
   data.push({
    'id_alat': id_alat,
    'status': $(this).find('select#status').val(),
    'keterangan_anomali': $(this).find('textarea#keterangan_anomali').val(),
   });
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'tanggal_anomali': $('#tanggal_anomali').val(),
   'detail_item': ValidasiAnomali.getPostDetailItem()
  };

  return data;
 },

 simpan: function (id) {
  var data = ValidasiAnomali.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(ValidasiAnomali.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(ValidasiAnomali.module()) + "detail" + '/' + resp.anomali;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 detail: function (id) {
  window.location.href = url.base_url(ValidasiAnomali.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(ValidasiAnomali.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(ValidasiAnomali.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal_anomali').pickadate({
   format: 'yyyy-mm-dd',
   formatSubmit: 'yyyy-mm-dd',
  });
 },

 showDataAlat: function (elm) {
  var index_tr = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   data: {
    index_tr: index_tr
   },
   dataType: 'html',
   async: false,
   url: url.base_url(ValidasiAnomali.module()) + "showDataAlat",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },
 pilihAlat: function (elm, idAlat) {
  var index_tr = $('input#index_tr').val();
  var nama_alat = $.trim($(elm).closest('tr').find('td:eq(4)').text());
  var table_anomali = $('tbody.table_anomali').find('tr:eq(' + index_tr + ')');
  var no_seri = $.trim($(elm).closest('tr').find('td:eq(3)').text());
  var data = "<b>" + nama_alat + " <label class='text-success'>(" + no_seri + ")</label></b>";
  table_anomali.find('td:eq(0)').html(data);
  table_anomali.find('td:eq(0)').attr('id_alat', idAlat);
  message.closeDialog();
 },

 addDetail: function (elm) {
  var button = '<button id="" class="btn btn-info" onclick="ValidasiAnomali.showDataAlat(this)">Pilih Alat</button>';
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(0)').html(button);
  newTr.find('textarea').val('');
  newTr.find('td:eq(3)').html('<i class="mdi mdi-minus mdi-24px hover" onclick="ValidasiAnomali.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 approve: function (id) {
  $.ajax({
   type: 'POST',
   data: {},
   dataType: 'json',
   async: false,
   url: url.base_url(ValidasiAnomali.module()) + "approve/" + id,
   beforeSend: function () {
    message.loadingProses("Proses Validasi ...");
   },

   error: function () {
    message.closeLoading();
    toastr.error("Gagal Validasi");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Validasi Berhasil");
     var reload = function () {
      window.location.href = url.base_url(ValidasiAnomali.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Validasi Gagal");
    }

    message.closeLoading();
   }
  });
 }
};

$(function () {
 ValidasiAnomali.setDate();
});