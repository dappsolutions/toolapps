var ExportAlat = {
 module: function () {
  return "export_alat";
 },

 getByKategori: function (elm) {
  var kategori = $(elm).val();
  if (kategori != '') {
   window.location.href = url.base_url(ExportAlat.module()) + "index/" + kategori;
  } else {
   window.location.href = url.base_url(ExportAlat.module()) + "index";
  }
 }
};