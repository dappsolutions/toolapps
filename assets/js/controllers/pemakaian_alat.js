var PemakaianAlat = {
 module: function () {
  return 'pemakaian_alat';
 },

 add: function () {
  window.location.href = url.base_url(PemakaianAlat.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(PemakaianAlat.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(PemakaianAlat.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(PemakaianAlat.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'kode_alat': $('#kode_alat').val(),
   'tgl_pemakaian': $('#tanggal_pemakaian').val(),
   'keterangan': $('#keterangan').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = PemakaianAlat.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(PemakaianAlat.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(PemakaianAlat.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(PemakaianAlat.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(PemakaianAlat.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(PemakaianAlat.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(PemakaianAlat.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal_pemakaian').pickadate({
   format: 'yyyy-mm-dd',
   formatSubmit: 'yyyy-mm-dd',
  });
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  PemakaianAlat.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'pdf') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('input#filename').val($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Pdf');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showSertifikat: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(PemakaianAlat.module()) + "showSertifikat",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<label class='badge badge-success label-change'>Manual Book Diubah</label>&nbsp<label class='badge badge-danger hover label-change' onclick='PemakaianAlat.cancelChangeManual(this)'>Batalkan</label>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('label.label-change').remove();

  var inputFile = '<div class="input-group col-xs-12">';
  inputFile += '<input id="filename" class="form-control file-upload-info" disabled="" placeholder="Upload File Pdf" type="text">';
  inputFile += '<span class="input-group-append">';
  inputFile += '<button class="file-upload-browse btn btn-warning" type="button" onclick="PemakaianAlat.upload(this)">Upload</button>';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="PemakaianAlat.getFilename(this)"/>';
  inputFile += '</span>';
  inputFile += '</div>';
  $('div.manual_upload').html(inputFile);
 }
};

$(function () {
 PemakaianAlat.setDate();
 $('select#kode_alat').select2();
});