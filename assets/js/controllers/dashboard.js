var BarAlat;

var Dashboard = {
    module: function () {
        return 'dashboard';
    },

    setGrafik: function () {
        BarAlat = Morris.Bar({
            element: 'canvas_alat',
            data: JSON.parse($('label#json_alat').text()),
            xkey: ['alat'],
            ykeys: ['jumlah', 'jumlah_normal', 'jumlah_pengajuan', 'jumlah_kembali'],
            ymax: parseInt($.trim($('label#max_alat').text())),
            ymin: 0,
            labels: ['Jumlah Rusak', 'Jumlah Normal', 'Jumlah Keluar', 'Jumlah Kembali'],
            resize: true,
            lineWidth: '3px',
            redraw: true,
            barColors: ["#e74a25", "#2ecc71", '#0283cc', '#ffb136']
        });

        BarAlat.redraw();

        BarAlat.on('click', function (i, row) {
            var jumlah = 0;
            switch (i) {
                case 0:
                    jumlah = row.jumlah;
                    break;
                case 1:
                    jumlah = row.jumlah_normal;
                    break;
                case 2:
                    jumlah = row.jumlah_pengajuan;
                    break;
                case 3:
                    jumlah = row.jumlah_kembali;
                    break;
                default:

                    break;
            }

            var alat = row.alat;

            Dashboard.displayDataAlat(jumlah, alat);
        });
    },

    displayDataAlat: function (jumlah, alat) {
        console.log('alat', alat);
        
        var upt = $('input#upt').val();
        
        $.ajax({
            type: 'POST',
            data: {
                upt: upt,
                alat:alat
            },
            dataType: 'json',
            async: false,
            url: url.base_url(Dashboard.module()) + "displayDataAlat",
            error: function () {
                toastr.error("Gagal");
                message.closeLoading();
            },
            
            beforeSend: function () {
                message.loadingProses("Proses Retrieving Data...");
            },
            
            success: function (resp) {
                message.closeLoading();
                
                $('div#content_data_alat').closest('div.white-box').removeClass('display-none');
                $('div#content_data_alat').html(resp.view);
            }
        });
    },
    
    getDisplayAlat: function (elm) {
        var alat = $.trim($(elm).closest('div.row').find('label#jenis_alat').text());
        
        var upt = $('input#upt').val();
        
        $.ajax({
            type: 'POST',
            data: {
                upt: upt,
                alat:alat
            },
            dataType: 'json',
            async: false,
            url: url.base_url(Dashboard.module()) + "displayDataAlat",
            error: function () {
                toastr.error("Gagal");
                message.closeLoading();
            },
            
            beforeSend: function () {
                message.loadingProses("Proses Retrieving Data...");
            },
            
            success: function (resp) {
                message.closeLoading();
                
                $('div#content_data_alat').closest('div.white-box').removeClass('display-none');
                $('div#content_data_alat').html(resp.view);
            }
        });
    },

    reDrawGrafikAlat: function (elm) {
        var upt = $.trim($(elm).closest('div.row').find('div#title_upt').text());
        $('h4#title_dashboard').text('Grafik Alat ' + upt);

        var upt_id = $.trim($(elm).closest('div.row').find('div#title_upt').attr('data_id'));

        $('input#upt').val(upt_id);
        $.ajax({
            type: 'POST',
            data: {
                upt: upt_id
            },
            dataType: 'json',
            async: false,
            url: url.base_url(Dashboard.module()) + "getTotalDataGrafikAlat",
            error: function () {
                toastr.error("Gagal");
                message.closeLoading();
            },

            beforeSend: function () {
                message.loadingProses("Proses Retrieving Data...");
            },

            success: function (resp) {
                console.log(resp.data_json);
                message.closeLoading();
                BarAlat.setData(JSON.parse(resp.data_json));
                
                //show
                $('label#total_kalibrasi').text(resp.total_alat_dikalibrasi);
                $('label#total_servis').text(resp.total_alat_diservis);
                $('label#total_rusak').text(resp.total_alat_rusak);
                $('label#total_keluar').text(resp.total_alat_keluar);
                $('label#total_normal').text(resp.total_alat_normal);
                $('label#total_kembali').text(resp.total_alat_kembali);
            }
        });
    }
};


$(function () {
    Dashboard.setGrafik();
});
