var Alat = {
 module: function () {
  return 'alat';
 },

 add: function (baru = '') {
  if (baru != '') {
   window.location.href = url.base_url(Alat.module()) + "add/" + baru;
  } else {
   window.location.href = url.base_url(Alat.module()) + "add";
 }
 },

 exportAlat: function () {
  window.location.href = url.base_url("export_alat") + "index";
 },

 back: function () {
  window.location.href = url.base_url(Alat.module()) + "index";
 },

 search: function (elm, e) {
  e.preventDefault();
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   
   if (keyWord != '') {
    keyWord = decodeURI(keyWord.toString().replace(/%/g, 'persen'));
//    console.log(keyWord.toString());
//    return;
//    var url_link = url.base_url(Alat.module()) + "search" + '/' + keyWord;
//    url_link = decodeURIComponent(url_link);
//    console.log(url_link);
//    return;
    window.location.href = url.base_url(Alat.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Alat.module()) + "index";
   }
  }
 },

 getDetailImagPost: function () {
  var images = $('div.images');
  var data = [];
  $.each(images, function () {
   var text_image = $.trim($(this).find('input.text_image').val());
   var keterangan = $.trim($(this).find('input#keterangan_foto').val());
   if (text_image != '') {
    data.push({
     'keterangan_foto': keterangan,
     'img': text_image
    });
   }
  });

  return data;
 },

 getDetailFotoRemove: function () {
  var image = $('div.data_image');
  var data = [];
  $.each(image, function () {
   if ($(this).hasClass('remove')) {
    data.push({
     'id': $(this).attr('id')
    });
   }
  });

  return data;
 },

 getDetailFotoUpdate: function () {
  var image = $('div.data_image');
  var data = [];
  $.each(image, function () {
   if (!$(this).hasClass('remove')) {
    data.push({
     'id': $(this).attr('id'),
     'title': $.trim($(this).find('input#title').val())
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'kategori_alat': $('#kategori_alat').val(),
   'tipe_alat': $('#tipe_alat').val(),
   'upt': $('#upt').val(),
   'alat': $('select#alat').val(),
   'nama_alat': $('input#nama_alat').val(),
   'merk': $('input#merk').val(),
   'tahun': $('#tahun').val(),
   'status_alat': $('#status_alat').val(),
   'tgl_kalibrasi': $('#tgl_kalibrasi').val(),
   'gardu_induk': $('#gardu_induk').val(),
   'nomer_seri': $('input#nomer_seri').val(),
   'keterangan': $('textarea#keterangan').val(),
   'kelengkapan': $('input#kelengkapan').val(),
   'tonase': $('input#tonase').val(),
   'tahun_produksi': $('select#tahun_produksi').val(),
   'nilai_perolehan': $('input#nilai_perolehan').val(),
   'detail_image': Alat.getDetailImagPost(),
   'foto_remove': Alat.getDetailFotoRemove(),
   'foto_update': Alat.getDetailFotoUpdate(),
  };

  return data;
 },

 simpan: function (id) {
  var data = Alat.getPostData();
//  console.log(data);
//  return;
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Alat.module()) + "simpan",
    error: function () {
     toastr.error("Gagal Simpan");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Alat.module()) + "detail" + '/' + resp.alat;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id, baru = '') {
  if(baru != ''){
   window.location.href = url.base_url(Alat.module()) + "ubah/" + id + "/" + baru;
  }else{
   window.location.href = url.base_url(Alat.module()) + "ubah/" + id;
  }
 },

 detail: function (id) {
  window.location.href = url.base_url(Alat.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Alat.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Alat.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 approveAlatBaru: function (id, status) {
  $.ajax({
   type: 'POST',
   data: {
    status: status
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Alat.module()) + "approveAlatBaru/" + id,

   error: function () {
    toastr.error("Gagal Approve");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Approve");
     var reload = function () {
      window.location.href = url.base_url(Alat.module()) + "alat_baru";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Approve");
    }
   }
  });
 },

 uploadImage: function (elm, e) {
  var uploader = $('<input type="file" accept="image/*" />');
  uploader.click();
  var images = $(elm).closest('div.images');

  Alat.cloneAddFoto(elm);
  $(elm).closest('div').remove();
  uploader.on('change', function () {
   var reader = new FileReader();
   reader.onload = function (event) {
    images.html('<div class="img" onclick="Alat.removeGambar(this)" style="background-image: url(\'' + event.target.result + '\');margin-top:16px;height:200px;" rel="' + event.target.result + '"><span class="label label-danger hover">remove</span></div>');
    images.prepend('<input type="hidden" class="text_image" value = "' + event.target.result + '"/>');
    images.prepend('<input type="text" placeholder="Keterangan Foto" id="keterangan_foto" class="form-control" value = ""/>');
   };
   reader.readAsDataURL(uploader[0].files[0]);
  });
 },

 cloneAddFoto: function (elm) {
  var html = '<hr/><br/><div class="row">';
  html += "<div class='col-md-12 text-center'>";
  html += "<div class='images'>";
  html += "<div class='pic' onclick='Alat.upload(this, event)'>";
  html += 'ADD';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  var newTr = $(elm).closest('div.row').clone();
  console.log(newTr);
  $(elm).closest('div.row').after(newTr);
 },

 removeGambar: function (elm) {
  $(elm).closest('div.row').remove();
 },

 removeFoto: function (elm) {
  $(elm).closest('div.data_image').addClass('remove').addClass('display-none');
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  Alat.checkFile(elm);
 },

 getGardu: function (elm) {
  var upt = $(elm).val();
//  console.log('upt', upt);
  var list_gardu = $('select#gardu_induk').find('option');
  $.each(list_gardu, function () {
   $(this).removeClass('display-none');
   $(this).removeAttr('selected');
  });


  if (upt != '1') {
   var i = 0;
   $.each(list_gardu, function () {
    var upt_gardu = $(this).attr('upt');
//    console.log('upt_gardu', upt_gardu);
    if (upt != upt_gardu) {
     $(this).addClass('display-none');
    } else {
     if (i == 0) {
      console.log($(this));
      $(this).attr("selected", "");
      i += 1;
     }
    }
   });
  }
 },

 showManualBook: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    manual_book: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Alat.module()) + "showManualBook",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'pdf') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('input#filename').val($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Pdf');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("*<label class='badge badge-success label-change'>Manual Book Diubah</label>&nbsp<label class='badge badge-danger hover label-change' onclick='Alat.cancelChangeManual(this)'>Batalkan</label>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('label.label-change').remove();

  var inputFile = '<div class="input-group col-xs-12">';
  inputFile += '<input id="filename" class="form-control file-upload-info" disabled="" placeholder="Upload File Pdf" type="text">';
  inputFile += '<span class="input-group-append">';
  inputFile += '<button class="file-upload-browse btn btn-warning" type="button" onclick="Alat.upload(this)">Import</button>';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="Alat.getFilename(this)"/>';
  inputFile += '</span>';
  inputFile += '</div>';
  $('div.manual_upload').html(inputFile);
 },

 getNamaAlatList: function (elm) {
  var kategori = $(elm).val();
//  console.log('upt', upt);
  var list_nama = $('select#alat').find('option');
  $.each(list_nama, function () {
   $(this).removeClass('display-none');
   $(this).removeAttr('selected');
  });

//  console.log('kategori', kategori);
  if (kategori != '') {
   var i = 0;
   $.each(list_nama, function () {
    var kategori_alat = $(this).attr('kategori');
//    console.log('upt_gardu', upt_gardu);
    if (kategori != kategori_alat) {
     if ($(this).val() != 'lain') {
      $(this).addClass('display-none');
     }
    } else {
     if (i == 0) {
      console.log($(this));
      $(this).attr("selected", "");
      Alat.getAlatNama(this);
      i += 1;
     }
    }
   });
  }
 },

 getAlatNama: function (elm) {
  var nama = $(elm).val();
  if (nama == 'lain') {
   $('div.nama_alat').removeClass('display-none');
   $('input#nama_alat').addClass('required');
  } else {
   if (!$('div.nama_alat').hasClass('display-none')) {
    $('div.nama_alat').addClass('display-none');
    $('input#nama_alat').removeClass('required');
    $('input#nama_alat').val('');
   }
  }
 },

 setDate: function () {
  $('input#tgl_kalibrasi').pickadate({
   format: 'yyyy-mm-dd',
   formatSubmit: 'yyyy-mm-dd',
  });
 },

 printBarcode: function (elm, jenis) {
  var barcode = $('input#barcode').val();
  window.location.href = url.base_url(Alat.module()) + "printBarcode/" + barcode + "/" + jenis;
 }
};

$(function () {
 Alat.setDate();
 $('select#alat').select2();
 $('select#gardu_induk').select2();
 $('select#tahun').select2();
 $('select#tahun_produksi').select2();
});