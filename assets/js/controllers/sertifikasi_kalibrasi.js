var SertifikasiKalibrasi = {
 module: function () {
  return 'sertifikasi_kalibrasi';
 },

 add: function () {
  window.location.href = url.base_url(SertifikasiKalibrasi.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(SertifikasiKalibrasi.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(SertifikasiKalibrasi.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(SertifikasiKalibrasi.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'no_sertifikat': $('#no_sertifikat').val(),
   'kode_alat': $('#kode_alat').val(),
   'tanggal_kalibrasi': $('#tanggal_kalibrasi').val(),
   'nilai_ketidak_pastian': $('#nilai_ketidak_pastian').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = SertifikasiKalibrasi.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(SertifikasiKalibrasi.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(SertifikasiKalibrasi.module()) + "detail" + '/' + resp.upt;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(SertifikasiKalibrasi.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(SertifikasiKalibrasi.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(SertifikasiKalibrasi.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(SertifikasiKalibrasi.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal_kalibrasi').pickadate({
   format: 'yyyy-mm-dd',
   formatSubmit: 'yyyy-mm-dd',
  });
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  SertifikasiKalibrasi.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'pdf') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('input#filename').val($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Pdf');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showSertifikat: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(SertifikasiKalibrasi.module()) + "showSertifikat",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<label class='badge badge-success label-change'>Manual Book Diubah</label>&nbsp<label class='badge badge-danger hover label-change' onclick='SertifikasiKalibrasi.cancelChangeManual(this)'>Batalkan</label>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('label.label-change').remove();

  var inputFile = '<div class="input-group col-xs-12">';
  inputFile += '<input id="filename" class="form-control file-upload-info" disabled="" placeholder="Upload File Pdf" type="text">';
  inputFile += '<span class="input-group-append">';
  inputFile += '<button class="file-upload-browse btn btn-warning" type="button" onclick="SertifikasiKalibrasi.upload(this)">Upload</button>';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="SertifikasiKalibrasi.getFilename(this)"/>';
  inputFile += '</span>';
  inputFile += '</div>';
  $('div.manual_upload').html(inputFile);
 }
};

$(function () {
 SertifikasiKalibrasi.setDate();
 $('select#kode_alat').select2();
});