var AnomaliAlat = {
 module: function () {
  return 'anomali_alat';
 },

 add: function () {
  window.location.href = url.base_url(AnomaliAlat.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(AnomaliAlat.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(AnomaliAlat.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(AnomaliAlat.module()) + "index";
   }
  }
 },

 getDetailImagPost: function () {
  var images = $('div.images');
  var data = [];
  $.each(images, function () {
   var text_image = $.trim($(this).find('input.text_image').val());
   var keterangan = $.trim($(this).find('input#keterangan_foto').val());
   if (text_image != '') {
    data.push({
     'keterangan_foto': keterangan,
     'img': text_image
    });
   }
  });

  return data;
 },

 getDetailFotoRemove: function () {
  var image = $('div.data_image');
  var data = [];
  $.each(image, function () {
   if ($(this).hasClass('remove')) {
    data.push({
     'id': $(this).attr('id')
    });
   }
  });

  return data;
 },

 getDetailFotoUpdate: function () {
  var image = $('div.data_image');
  var data = [];
  $.each(image, function () {
   if (!$(this).hasClass('remove')) {
    data.push({
     'id': $(this).attr('id'),
     'title': $.trim($(this).find('input#title').val())
    });
   }
  });

  return data;
 },

 getDetailPostRegu: function () {
  var data = [];
  var table = $('table#tb_regu').find('tbody').find('tr');
//  console.log(table);
  $.each(table, function () {
   if ($(this).hasClass('baru')) {
    if (!$(this).hasClass('display-none')) {
     data.push({
      'regu_pemeliharaan': $(this).find('select#regu_pemeliharaan').val()
     });
    }
   }
  });

  return data;
 },

 getDetailPostReguEdit: function () {
  var data = [];
  var table = $('table#tb_regu').find('tbody').find('tr');
  $.each(table, function () {
   if (!$(this).hasClass('baru')) {
    var is_edit = $(this).hasClass('display-none') ? 1 : 0;
    data.push({
     'id': $(this).attr('id'),
     'is_edit': is_edit,
     'regu_pemeliharaan': $(this).find('select#regu_pemeliharaan').val()
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'kategori_alat': $('#kategori_alat').val(),
   'tipe_alat': $('#tipe_alat').val(),
   'alat': $('select#alat').val(),
   'nama_alat': $('input#nama_alat').val(),
   'merk': $('input#merk').val(),
   'tahun': $('#tahun').val(),
   'nomer_seri': $('input#nomer_seri').val(),
   'gardu_induk': $('select#gardu_induk').val(),
   'keterangan': $('textarea#keterangan').val(),
   'kelengkapan': $('input#kelengkapan').val(),
   'tonase': $('input#tonase').val(),
   'detail_image': AnomaliAlat.getDetailImagPost(),
   'foto_remove': AnomaliAlat.getDetailFotoRemove(),
   'foto_update': AnomaliAlat.getDetailFotoUpdate(),
   'regu': AnomaliAlat.getDetailPostRegu(),
   'regu_edit': AnomaliAlat.getDetailPostReguEdit()
  };

  return data;
 },

 simpan: function (id) {
  var data = AnomaliAlat.getPostData();
//  console.log(data);
//  return;
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(AnomaliAlat.module()) + "simpan",
    error: function () {
     toastr.error("Gagal Simpan");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(AnomaliAlat.module()) + "detail" + '/' + resp.alat;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(AnomaliAlat.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(AnomaliAlat.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(AnomaliAlat.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(AnomaliAlat.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 uploadImage: function (elm, e) {
  var uploader = $('<input type="file" accept="image/*" />');
  uploader.click();
  var images = $(elm).closest('div.images');

  AnomaliAlat.cloneAddFoto(elm);
  $(elm).closest('div').remove();
  uploader.on('change', function () {
   var reader = new FileReader();
   reader.onload = function (event) {
    images.html('<div class="img" onclick="AnomaliAlat.removeGambar(this)" style="background-image: url(\'' + event.target.result + '\');margin-top:16px;" rel="' + event.target.result + '"><span>remove</span></div>');
    images.prepend('<input type="hidden" class="text_image" value = "' + event.target.result + '"/>');
    images.prepend('<input type="text" placeholder="Keterangan Foto" id="keterangan_foto" class="form-control" value = ""/>');
   };
   reader.readAsDataURL(uploader[0].files[0]);
  });
 },

 cloneAddFoto: function (elm) {
  var html = '<hr/><br/><div class="row">';
  html += "<div class='col-md-12 text-center'>";
  html += "<div class='images'>";
  html += "<div class='pic' onclick='AnomaliAlat.upload(this, event)'>";
  html += 'ADD';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  html += '</div>';
  var newTr = $(elm).closest('div.row').clone();
  console.log(newTr);
  $(elm).closest('div.row').after(newTr);
 },

 removeGambar: function (elm) {
  $(elm).closest('div.row').remove();
 },

 removeFoto: function (elm) {
  $(elm).closest('div.data_image').addClass('remove').addClass('display-none');
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  AnomaliAlat.checkFile(elm);
 },

 getGardu: function (elm) {
  var upt = $(elm).val();
//  console.log('upt', upt);
  var list_gardu = $('select#gardu_induk').find('option');
  $.each(list_gardu, function () {
   $(this).removeClass('display-none');
   $(this).removeAttr('selected');
  });


  if (upt != '1') {
   var i = 0;
   $.each(list_gardu, function () {
    var upt_gardu = $(this).attr('upt');
//    console.log('upt_gardu', upt_gardu);
    if (upt != upt_gardu) {
     $(this).addClass('display-none');
    } else {
     if (i == 0) {
      console.log($(this));
      $(this).attr("selected", "");
      i += 1;
     }
    }
   });
  }
 },

 moduleAlat: function () {
  return "alat";
 },

 showManualBook: function (elm, e) {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    manual_book: $.trim($(elm).text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(AnomaliAlat.moduleAlat()) + "showManualBook",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'pdf') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('input#filename').val($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Pdf');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("*<label class='badge badge-success label-change'>Manual Book Diubah</label>&nbsp<label class='badge badge-danger hover label-change' onclick='AnomaliAlat.cancelChangeManual(this)'>Batalkan</label>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('label.label-change').remove();

  var inputFile = '<div class="input-group col-xs-12">';
  inputFile += '<input id="filename" class="form-control file-upload-info" disabled="" placeholder="Upload File Pdf" type="text">';
  inputFile += '<span class="input-group-append">';
  inputFile += '<button class="file-upload-browse btn btn-warning" type="button" onclick="AnomaliAlat.upload(this)">Import</button>';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="AnomaliAlat.getFilename(this)"/>';
  inputFile += '</span>';
  inputFile += '</div>';
  $('div.manual_upload').html(inputFile);
 },

 getNamaAlatList: function (elm) {
  var kategori = $(elm).val();
//  console.log('upt', upt);
  var list_nama = $('select#alat').find('option');
  $.each(list_nama, function () {
   $(this).removeClass('display-none');
   $(this).removeAttr('selected');
  });

//  console.log('kategori', kategori);
  if (kategori != '') {
   var i = 0;
   $.each(list_nama, function () {
    var kategori_alat = $(this).attr('kategori');
//    console.log('upt_gardu', upt_gardu);
    if (kategori != kategori_alat) {
     if ($(this).val() != 'lain') {
      $(this).addClass('display-none');
     }
    } else {
     if (i == 0) {
      console.log($(this));
      $(this).attr("selected", "");
      AnomaliAlat.getAlatNama(this);
      i += 1;
     }
    }
   });
  }
 },

 getAlatNama: function (elm) {
  var nama = $(elm).val();
  if (nama == 'lain') {
   $('div.nama_alat').removeClass('display-none');
   $('input#nama_alat').addClass('required');
  } else {
   if (!$('div.nama_alat').hasClass('display-none')) {
    $('div.nama_alat').addClass('display-none');
    $('input#nama_alat').removeClass('required');
    $('input#nama_alat').val('');
   }
  }
 },

 addRegu: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(1)').html('<i class="mdi mdi-minus-circle mdi-24px hover" onclick="AnomaliAlat.removeRegu(this)"></i>');
  tr.after(newTr);
 },

 removeReguData: function (elm) {
  $(elm).closest('tr').addClass('display-none');
 },

 removeRegu: function (elm) {
  $(elm).closest('tr').remove();
 },

 tambahRegu: function (elm) {
  if ($(elm).text() == 'Tambah Regu') {
   $(elm).text('Batal');
   $('tr.baru_edit').removeClass('display-none');
  } else {
   $(elm).text('Tambah Regu');
   $('tr.baru_edit').addClass('display-none');
  }
 }
};

$(function () {
});