var Usulan = {
 module: function () {
  return 'usulan';
 },

 add: function () {
  window.location.href = url.base_url(Usulan.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Usulan.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Usulan.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Usulan.module()) + "index";
   }
  }
 },

 getPostDetailItem: function () {
  var data = [];
  var tr = $('tbody.table_usulan').find('tr');
  $.each(tr, function () {
   var id_alat = $(this).find('td:eq(0)').attr('id_alat');
   data.push({
    'id_alat': id_alat,
    'id': $(this).attr('id'),
    'kategori': $(this).find('select#kategori').val(),
    'nama_alat': $(this).find('input#nama_alat').val(),
   });
  });

  return data;
 },

 getPostDetailItemUbah: function () {
  var data = [];
  var tr = $('tbody.table_usulan').find('tr');
  $.each(tr, function () {
   if ($(this).hasClass('ubah')) {
    var id_alat = $(this).find('td:eq(0)').attr('id_alat');
    data.push({
     'id': $(this).attr('id'),
     'id_alat': id_alat,
     'kategori': $(this).find('select#kategori').val(),
     'nama_alat': $(this).find('input#nama_alat').val(),
    });
   }
  });

  return data;
 },

 getPostDetailItemHapus: function () {
  var data = [];
  var tr = $('tbody.table_usulan').find('tr');
  $.each(tr, function () {
   if ($(this).hasClass('hapus')) {
    data.push({
     'id': $(this).attr('id'),
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'tanggal': $('#tanggal').val(),
   'detail_item': Usulan.getPostDetailItem(),
   'detail_item_ubah': Usulan.getPostDetailItemUbah(),
   'detail_item_hapus': Usulan.getPostDetailItemHapus()
  };

  return data;
 },

 simpan: function (id) {
  var data = Usulan.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Usulan.module()) + "simpan",
    error: function () {
     toastr.error("Gagal Konek Ke Server");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Usulan.module()) + "detail" + '/' + resp.usulan;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Usulan.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Usulan.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Usulan.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Usulan.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal').pickadate({
   format: 'yyyy-mm-dd',
   formatSubmit: 'yyyy-mm-dd',
  });
 },

 showDataAlat: function (elm) {
  var index_tr = $(elm).closest('tr').index();
  $.ajax({
   type: 'POST',
   data: {
    index_tr: index_tr
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Usulan.module()) + "showDataAlat",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },
 pilihAlat: function (elm, idAlat) {
  var index_tr = $('input#index_tr').val();
  var kode_alat = $(elm).closest('tr').attr('kode_alat');
  var nama_alat = $.trim($(elm).closest('tr').find('td:eq(4)').text());
  var table_usulan = $('tbody.table_usulan').find('tr:eq(' + index_tr + ')');
  var no_seri = $.trim($(elm).closest('tr').find('td:eq(3)').text());
  var data = "<b>" + nama_alat + " <label class='text-success'>(" + no_seri + ")</label></b>";
  table_usulan.find('td:eq(0)').html(data);
  table_usulan.find('td:eq(0)').attr('id_alat', idAlat);
  table_usulan.find('td:eq(1)').html('<b>' + kode_alat + '</b>');
  message.closeDialog();
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(2)').html('<i class="mdi mdi-minus mdi-24px hover" onclick="Usulan.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 hapusUsulan: function (elm) {
  $(elm).closest('tr').addClass('hapus');
  $(elm).closest('tr').addClass('display-none');
 },

 ubahUsulan: function (elm) {
  var tr = $(elm).closest('tr');
  var id = tr.attr('id');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Usulan.module()) + "ubahUsulan/" + id,
   success: function (resp) {
    tr.html(resp);
    if (!tr.hasClass('ubah')) {
     tr.addClass('ubah');
    }
   }
  });
 },

 cariAlat: function (elm, e) {
  var value = $(elm).val();
  Usulan.searchInTable(value, "#tb_alat_detail");
 },

 searchInTable: function (value, elm) {
  $("" + elm + " tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },

 reject: function () {
  var html = "<div class='row'>";
  html += "<div class='col-md-12'>";
  html += "<h4>Isi Keterangan Ditolak</h4>";
  html += "<textarea class='form-control required' id='ket_reject'></textarea>";
  html += "<br/>";
  html += "<div class='text-right'>";
  html += "<button class='btn btn-primary' onclick='Usulan.execApprove(0)'>Proses</button>";
  html += "</div>";
  html += "<div>";
  html += "</div>";

  bootbox.dialog({
   message: html
  });
 },

 approve: function () {
  Usulan.execApprove(1);
 },
 
 execApprove: function (status = '1') {
  var id = $('input#id').val();
  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: {
     status: status,
     id: id,
     ket: $('#ket_reject').val()
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Usulan.module()) + "execApprove",
    error: function () {
     toastr.error("Gagal Koneksi Ke Server");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Approve...");
    },

    success: function (resp) {
     message.closeLoading();
     if (resp.is_valid) {
      toastr.success("Berhasil Approve");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
    }
   });
 }
 }
};

$(function () {
 Usulan.setDate();
});