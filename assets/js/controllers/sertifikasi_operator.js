var SertifikasiOperator = {
 module: function () {
  return 'sertifikasi_operator';
 },

 add: function () {
  window.location.href = url.base_url(SertifikasiOperator.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(SertifikasiOperator.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(SertifikasiOperator.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(SertifikasiOperator.module()) + "index";
   }
  }
 },

 getPostFileData: function () {
  var data = [];
  var tr_table = $('table#data_file').find('tbody').find('tr');
  $.each(tr_table, function () {
   var file_value = $(this).find('span.fileinput-filename').text();
   data.push({
    'file': file_value,
    'id': $(this).attr('data_id'),
    'deleted': $(this).hasClass('deleted') ? 1 : 0
   });
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'kode_alat': $('#kode_alat').val(),
   'tanggal': $('#tanggal').val(),
   'keterangan': $('textarea#keterangan').val(),
   'file_data': SertifikasiOperator.getPostFileData()
  };

  return data;
 },

 simpan: function (id) {
  var data = SertifikasiOperator.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  var file = $('input.file');
  var no = 1;
  $.each(file, function () {
   no = $(this).closest('tr').index();
   formData.append('file' + no, $(this).prop('files')[0]);
  });

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(SertifikasiOperator.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(SertifikasiOperator.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(SertifikasiOperator.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(SertifikasiOperator.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(SertifikasiOperator.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(SertifikasiOperator.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal').pickadate({
   format: 'yyyy-mm-dd',
   formatSubmit: 'yyyy-mm-dd',
  });
 },

 upload: function (elm) {
  $(elm).closest('tr').find('input#file').click();
 },

 getFilename: function (elm) {
  SertifikasiOperator.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'pdf'
           || type_file == 'png'
           || type_file == 'jpeg'
           || type_file == 'jpg') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('input#filename').val($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat ' + type_file);
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showSertifikat: function (elm) {
  $.ajax({
   type: 'POST',
   data: {
    file: $.trim($(elm).closest('tr').find('td:eq(0)').text())
   },
   dataType: 'html',
   async: false,
   url: url.base_url(SertifikasiOperator.module()) + "showSertifikat",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<label class='badge badge-success label-change'>Manual Book Diubah</label>&nbsp<label class='badge badge-danger hover label-change' onclick='SertifikasiOperator.cancelChangeManual(this)'>Batalkan</label>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('label.label-change').remove();

  var inputFile = '<div class="input-group col-xs-12">';
  inputFile += '<input id="filename" class="form-control file-upload-info" disabled="" placeholder="Upload File Pdf" type="text">';
  inputFile += '<span class="input-group-append">';
  inputFile += '<button class="file-upload-browse btn btn-warning" type="button" onclick="SertifikasiOperator.upload(this)">Upload</button>';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="SertifikasiOperator.getFilename(this)"/>';
  inputFile += '</span>';
  inputFile += '</div>';
  $('div.manual_upload').html(inputFile);
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(1)').html('<i class="mdi mdi-minus-circle mdi-24px hover" onclick="SertifikasiOperator.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 removeDetailData: function (elm) {
  $(elm).closest('tr').addClass('display-none');
  $(elm).closest('tr').addClass('deleted');
 }
};

$(function () {
 SertifikasiOperator.setDate();
 $('select#kode_alat').select2();
});