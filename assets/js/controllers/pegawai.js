var Pegawai = {
 module: function () {
  return 'pegawai';
 },

 add: function () {
  window.location.href = url.base_url(Pegawai.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Pegawai.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Pegawai.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Pegawai.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'upt': $('#upt').val(),
   'nip': $('#nip').val(),
   'nama': $('#nama').val(),
   'email': $('#email').val(),
   'username': $('#username').val(),
   'password': $('#password').val(),
   'akses': $('#akses').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = Pegawai.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Pegawai.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Pegawai.module()) + "detail" + '/' + resp.pegawai;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Pegawai.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Pegawai.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Pegawai.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Pegawai.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 }
};