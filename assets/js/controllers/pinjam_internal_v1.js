var PinjamInternal = {
 module: function () {
  return 'pinjam_internal';
 },

 make: function () {
  window.location.href = url.base_url(PinjamInternal.module()) + "make";
 },

 back: function () {
  window.location.href = url.base_url(PinjamInternal.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(PinjamInternal.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(PinjamInternal.module()) + "index";
   }
  }
 },

 getDataPostAlatPinjam: function () {
  var data_alat = $('table#data_alat').find('tbody').find('tr');
  var data = [];
  $.each(data_alat, function () {
   if ($(this).hasClass('baru')) {
    data.push({
     'alat': $(this).attr('id'),
     'qty': $.trim($(this).find('td:eq(3)').text()),
     'upt': $(this).attr('upt')
    });
   }
  });
  return data;
 },

 getDataPostAlatPinjamHapus: function () {
  var data_alat = $('table#data_alat').find('tbody').find('tr');
  var data = [];
  $.each(data_alat, function () {
   if ($(this).hasClass('display-none')) {
    data.push({
     'id': $(this).attr('id'),
    });
   }
  });
  return data;
 },

 getDataPostSuratPinjam: function () {
  var data_surat = $('table#data_surat').find('tbody').find('tr');
  var data = [];
  $.each(data_surat, function () {
   data.push({
    'baru': $(this).hasClass('baru') ? 1 : 0,
    'id': $(this).attr('id'),
    'edit': $(this).hasClass('display-none') ? 0 : 1,
    'upt': $(this).find('select#upt_surat').val(),
    'status_pinjaman': $(this).find('select#status_pinjaman').val(),
   });
  });
  return data;
 },

 getDataPostSuratPinjamEdit: function () {
  var data_surat = $('table#data_surat').find('tbody').find('tr');
  var data = [];
  $.each(data_surat, function () {
   if (!$(this).hasClass('baru')) {
    data.push({
     'id': $(this).attr('id'),
     'edit': $(this).hasClass('display-none') ? 0 : 1,
     'upt': $(this).find('select#upt_surat').val(),
     'status_pinjaman': $(this).find('select#status_pinjaman').val(),
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'tanggal_pinjam': $('#tanggal_pinjam').val(),
   'keterangan': $('#keterangan').val(),
   'alat': PinjamInternal.getDataPostAlatPinjam(),
   'alat_hapus': PinjamInternal.getDataPostAlatPinjamHapus(),
   'surat': PinjamInternal.getDataPostSuratPinjam(),
//   'surat_edit': PinjamInternal.getDataPostSuratPinjamEdit(),
  };

  return data;
 },

 isValidSimpan: function () {
  var table_alat = $('table#data_alat').find('tbody').find('tr');

  var is_valid = true;
  if (table_alat.length == 0) {
   is_valid = false;
   toastr.error("Tidak ada Alat yang Terpilih")
  }

  return is_valid;
 },

 simpan: function (id) {
  var data = PinjamInternal.getPostData();

//  console.log(data);
//  return;
  var is_valid = PinjamInternal.isValidSimpan();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);
  var file = $('input.file');
  var no = 1;
  $.each(file, function () {
   no = $(this).closest('tr').index();
   formData.append('file' + no, $(this).prop('files')[0]);
  });

  if (validation.run() && is_valid) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(PinjamInternal.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(PinjamInternal.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(PinjamInternal.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(PinjamInternal.module()) + "detail/" + id;
 },

 setDate: function () {
  $('input#tanggal_pinjam').pickadate({
   format: 'yyyy-mm-dd',
   formatSubmit: 'yyyy-mm-dd',
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(PinjamInternal.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(PinjamInternal.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 upload: function (elm) {
  $(elm).closest('tr').find('input#file').click();
 },

 getFilename: function (elm) {
  PinjamInternal.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'pdf' || type_file == 'jpg' || type_file == 'png') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('input#filename').val($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Pdf, Png, Jpg');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 getStatus: function (elm) {
  var status = $(elm).val();

  if (status == 2) {
   $('div#surat_peminjaman').addClass('display-none');
   $('input#filename').val('');
   $('input#file').val('');
  } else {
   $('div#surat_peminjaman').removeClass('display-none');
  }
 },

 getDataAlat: function (elm) {
  var upt = $(elm).val();

  if (upt != '') {
   $.ajax({
    type: 'POST',
    data: {
     upt: upt
    },
    dataType: 'html',
    async: false,
    url: url.base_url(PinjamInternal.module()) + "getDataAlat",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Retriving Data Alat...");
    },

    success: function (resp) {
     message.closeLoading();

     bootbox.dialog({
      message: resp,
      size: 'large'
     });
    }
   });
  }
 },

 cariAlat: function (elm, e) {
  var kodealat = $(elm).val();

  helper.searchInTable(kodealat, 'table')
 },

 isValidDataAlat: function () {
  var total_check = $('input.check_input').length;

  var total_uncheck = 0;
  var is_valid = true;
  $.each($('input.check_input'), function () {
   if (!$(this).is(':checked')) {
    total_uncheck += 1;
   }
  });

  if (total_uncheck == total_check) {
   is_valid = false;
   toastr.error("Tidak ada alat yang terpilih")
  }

  return is_valid;
 },

 getNamaUpt: function () {
  var upt = "";
  var option = $('select#upt').find('option');
  $.each($('select#upt').find('option'), function () {
   if ($(this).is(':selected')) {
    upt = $(this).text();
   }
  });
  return upt;
 },

 getDataAlatFromList: function () {
  var check_list = $('input.check_input');
  var data = [];
  $.each(check_list, function () {
   data.push({
    'alat': $(this).closest('tr').attr('id'),
    'kode_alat': $.trim($(this).closest('tr').find('td:eq(1)').text()),
    'upt': $.trim(PinjamInternal.getNamaUpt()),
    'upt_id': $.trim($(this).closest('tr').attr('upt')),
    'jumlah': $.trim($(this).closest('tr').find('input#qty').val()),
   });
  });

  return data;
 },

 cancelAlat: function (elm) {
  $(elm).closest('tr').remove();
 },

 prosesAmbilAlat: function () {
  var is_valid = PinjamInternal.isValidDataAlat();
  if (is_valid) {
   message.closeDialog();
   var data = PinjamInternal.getDataAlatFromList();


   var alat_table = $('table#data_alat').find('tbody');
   var tr_html = "";
   for (var i = 0; i < data.length; i++) {
    var alat = data[i].alat;
    var kode_alat = data[i].kode_alat;
    var upt = data[i].upt;
    var jumlah = data[i].jumlah;
    var upt_id = data[i].upt_id;

    var no = i + 1;
    tr_html += "<tr class='baru' id= '" + alat + "' upt='" + upt_id + "'>";
    tr_html += "<td class='font-12'>" + no + "</td>";
    tr_html += "<td class='font-12'>" + kode_alat + "</td>";
    tr_html += "<td class='font-12'>" + upt + "</td>";
    tr_html += "<td><label class='label label-success font-12'>" + jumlah + "</label></td>";
    tr_html += "<td>";
    tr_html += "<i class='mdi mdi-delete mdi-18px hover' onclick='PinjamInternal.cancelAlat(this)'></i>";
    tr_html += "</td>";
    tr_html += "</tr>";
   }
   alat_table.append(tr_html);
  }
 },

 checkValidInput: function (elm, e) {
  var stok = $(elm).closest('tr').find('td:eq(2)').text();
  stok = parseInt(stok);
  if (isNaN(parseInt($(elm).val()))) {
   if ($(elm).val().length > 1) {
    $(elm).val(0);
   }
  } else {
   var qty = $(elm).val();
   if (qty > stok) {
    toastr.error("Melebihi Stok");
   } else {
    $(elm).val(qty);
   }
  }
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(3)').html('<i class="mdi mdi-minus-circle mdi-24px hover" onclick="PinjamInternal.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 approveAsman: function (status) {
  var id = $('input#id').val();
  var status = status;
  $.ajax({
   type: 'POST',
   data: {
    id: id,
    status: status
   },
   dataType: 'json',
   async: false,
   url: url.base_url(PinjamInternal.module()) + "checkHaveTtd",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    if (resp.has_ttd) {
     PinjamInternal.execApproveAsman(id, status);
    } else {
     bootbox.dialog({
      message: resp.view
     });
    }
   }
  });
 },

 execApproveAsman: function (id, status) {
  $.ajax({
   type: 'POST',
   data: {
    id: id,
    status: status,
   },
   dataType: 'json',
   async: false,
   url: url.base_url(PinjamInternal.module()) + "approveAsman",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Approve Asman...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diapprove");
     var reload = function () {
      window.location.href = url.base_url(PinjamInternal.module()) + "detail" + '/' + id;
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Diapprove");
    }
    message.closeLoading();
   }
  });
 },

 execrejectAsman: function () {
  var id = $('input#id').val();
  var status = "REJECT";
  var keterangan = $('textarea#keterangan_reject').val();
  $.ajax({
   type: 'POST',
   data: {
    id: id,
    status: status,
    keterangan: keterangan
   },
   dataType: 'json',
   async: false,
   url: url.base_url(PinjamInternal.module()) + "approveAsman",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Approve Asman...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diapprove");
     var reload = function () {
      window.location.href = url.base_url(PinjamInternal.module()) + "detail" + '/' + id;
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Diapprove");
    }
    message.closeLoading();
   }
  });
 },

 rejectAsman: function () {
  var html = "<div class='row'>";
  html += "<div class='col-md-12'>";
  html += "<h4>Alasan di reject</h4>";
  html += "<div class='text-right'>";
  html += '<textarea id="keterangan_reject" class="form-control"></textarea>';
  html += "<br/>";
  html += "<button class='btn btn-success font-10' onclick='PinjamInternal.execrejectAsman()'>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 showDokumen: function (elm, e) {
  e.preventDefault();
  var file = $(elm).text();
  $.ajax({
   type: 'POST',
   data: {
    file: file
   },
   dataType: 'html',
   async: false,
   url: url.base_url(PinjamInternal.module()) + "showDokumen",
   error: function () {
    toastr.error("Gagal");
   },

   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 showScanQr: function (elm) {
  var id = $(elm).closest('tr').attr('id');
  var alat = $(elm).closest('tr').attr('alat');
  var qty = $(elm).closest('tr').attr('qty');
  var status = $.trim($(elm).closest('tr').find('td:eq(4)').text());
//  window.location.href = url.base_url(PinjamInternal.module()) + "showScanQr/" + id;
//  return;
  $.ajax({
   type: 'POST',
   data: {
    id: id,
    alat: alat,
    qty: qty,
    status: status
   },
   dataType: 'html',
   async: false,
   url: url.base_url(PinjamInternal.module()) + "showScanQr",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Scanner...");
   },

   success: function (resp) {
    message.closeLoading();

    bootbox.dialog({
     message: resp,
    });
   }
  });
 },

 prosesTakeAlat: function (elm) {
  var tr = $(elm).closest('tr');
  var qrcode = $.trim(tr.attr('barcode'));
  var id_pinjam = tr.attr('id');
  var alat_pinjam = tr.attr('alat');
  var qty_pinjam = tr.attr('qty');
  var status_pinjam = $.trim(tr.find('td:eq(4)').find('label').text());

  $.ajax({
   type: 'POST',
   data: {
    id: id_pinjam,
    alat: alat_pinjam,
    qrcode: qrcode,
    qty: qty_pinjam,
    status: status_pinjam,
   },
   dataType: 'json',
   async: false,
   url: url.base_url(PinjamInternal.module()) + "generateBarcode",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Verifikasi...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diproses");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error(resp.message);
    }
    message.closeLoading();
   }
  });
 },

 generateBarcode: function () {
  var qrcode = $.trim($('p#scanned-QR').text());
  var id_pinjam = $('input#id_pinjam').val();
  var alat_pinjam = $('input#alat_pinjam').val();
  var qty_pinjam = $('input#qty_pinjam').val();
  var status_pinjam = $('input#status_pinjam').val();
  $.ajax({
   type: 'POST',
   data: {
    id: id_pinjam,
    alat: alat_pinjam,
    qrcode: qrcode,
    qty: qty_pinjam,
    status: status_pinjam,
   },
   dataType: 'json',
   async: false,
   url: url.base_url(PinjamInternal.module()) + "generateBarcode",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Verifikasi...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diproses");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error(resp.message);
    }
    message.closeLoading();
   }
  });
 },

 removeData: function (elm) {
  $(elm).closest('tr').addClass('display-none');
 },

 cetak: function (id) {
  window.open(url.base_url(PinjamInternal.module()) + "cetak/" + id)
 }
};

$(function () {
 PinjamInternal.setDate();
});