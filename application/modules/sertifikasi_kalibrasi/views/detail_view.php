<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Alat
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nama_alat . ' - ' . $kategori . ' - ' . $kode_alat ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       No Sertifikat
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $no_sertifikat ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tanggal Kalibrasi
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $tanggal_kalibrasi ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Nilai Ketidak Pastian
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nilai_ketidak_pastian ?>       
      </div>     
     </div>
     <br/>

     <div class='row manual_detail <?php echo!isset($file) ? 'display-none' : '' ?>'>
      <div class='col-md-3'>
       Sertifikat Kalibrasi (<b>.Pdf</b>)
      </div>
      <div class='col-md-4'>
       <a href="#" onclick="SertifikasiKalibrasi.showSertifikat(this, event)" class="label label-danger"><?php echo $file ?></a>        
      </div>
     </div>
     <hr/>    
     <br/>

     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn" onclick="SertifikasiKalibrasi.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
