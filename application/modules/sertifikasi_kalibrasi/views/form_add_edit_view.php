<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Kode Alat
      </div>
      <div class='col-md-4'>
       <select id="kode_alat" error="Kode Alat" class="form-control required">
        <?php if (!empty($list_alat)) { ?>
         <?php foreach ($list_alat as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($alat)) { ?>
           <?php $selected = $value['id'] == $alat ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_alat']. ' - '. $value['kategori'].' - '.$value['kode_alat'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       No Sertifikat
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='no_sertifikat' class='form-control required' 
              value='<?php echo isset($no_sertifikat) ? $no_sertifikat : '' ?>' error="No. Sertifikat"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tanggal Kalibrasi
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='tanggal_kalibrasi' class='form-control required' 
              value='<?php echo isset($tanggal_kalibrasi) ? $tanggal_kalibrasi : '' ?>' error="Tanggal Kalibrasi"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Nilai Ketidak Pastian
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='nilai_ketidak_pastian' class='form-control required text-right' 
              value='<?php echo isset($nilai_ketidak_pastian) ? $nilai_ketidak_pastian : '0' ?>' error="Nilai Ketidak Pasitian"/>
      </div>     
     </div>
     <br/>

<!--      <div class='row manual_add <?php echo!isset($file) ? '' : 'display-none' ?>'>
      <div class='col-md-3'>
       Sertifikat Kalibrasi (<b>.Pdf</b>)
      </div>
      <div class='col-md-4'>
       <div class="form-group manual_upload">
        <div class="input-group col-xs-12">
         <input id="filename" class="form-control file-upload-info" disabled="" placeholder="Upload Sertifikat Pdf" type="text">
         <span class="input-group-append">
          <button class="file-upload-browse btn btn-warning" type="button" onclick="SertifikasiKalibrasi.upload(this)">Upload</button>
          <input type="file" style="display: none;" id="file" onchange="SertifikasiKalibrasi.getFilename(this)"/>
         </span>
        </div>
       </div>
      </div>
     </div>
     <div class='row manual_detail <?php echo!isset($file) ? 'display-none' : '' ?>'>
      <div class='col-md-3'>
       Sertifikat Kalibrasi (<b>.Pdf</b>)
      </div>
      <div class='col-md-6'>
       <a href="#" onclick="SertifikasiKalibrasi.showSertifikat(this, event)" class="badge badge-danger"><?php echo $file ?></a>        
      </div>
      <div class='col-md-3 text-right'>
       <i class="mdi mdi-close mdi-24px hover" onclick="SertifikasiKalibrasi.changeManual(this)"></i>
      </div>
     </div>-->

     <div class="row">
      <div class="col-md-3">
       Sertifikat Kalibrasi (<b>.Pdf</b>)
      </div>
      <div class="col-md-4">
       <div class="form-group">
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
         <div class="form-control" data-trigger="fileinput"> 
          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
          <span class="fileinput-filename"><?php echo isset($file) ? $file : '' ?></span>
         </div> 
         <span class="input-group-addon btn btn-default btn-file"> 
          <span class="fileinput-new">Select file</span> 
          <span class="fileinput-exists">Change</span>
          <input id='file' type="file" name="..." onchange="SertifikasiKalibrasi.getFilename(this, 'xls')"> 
         </span> 
         <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
        </div>
       </div>
      </div>
     </div>
     <hr/>    
     <br/>

     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="SertifikasiKalibrasi.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="SertifikasiKalibrasi.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
