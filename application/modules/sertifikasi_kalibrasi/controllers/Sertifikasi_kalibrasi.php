<?php

class Sertifikasi_kalibrasi extends MX_Controller {

    public $segment;
    public $limit;
    public $page;
    public $last_no;

    public function __construct() {
        parent::__construct();
        $this->limit = 10;
    }

    public function getModuleName() {
        return 'sertifikasi_kalibrasi';
    }

    public function getHeaderJSandCSS() {
        $data = array(
            '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
            '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
            '<script src="' . base_url() . 'assets/js/controllers/sertifikasi_kalibrasi.js"></script>'
        );

        return $data;
    }

    public function getTableName() {
        return 'sertifikasi_kalibrasi_alat';
    }

    public function index() {
        $this->segment = 3;
        $this->page = $this->uri->segment($this->segment) ?
                $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;

        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data Sertifikasi Kalibrasi";
        $data['title_content'] = 'Data Sertifikasi Kalibrasi';
        $content = $this->getDataSertifikasi();
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function getTotalDataSertifikasi($keyword = '') {
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('a.alat', $keyword),
                array('ska.no_sertifikat', $keyword),
                array('a.kode_alat', $keyword),
                array('na.nama_alat', $keyword),
            );
        }

        $upt_id = $this->session->userdata('upt_id');
        $where = "ska.deleted = 0";
        if ($this->session->userdata('hak_akses') != "Superadmin") {
            $where = "ska.deleted = 0 and a.upt = '" . $upt_id . "'";
        }
        $total = Modules::run('database/count_all', array(
                    'table' => $this->getTableName() . ' ska',
                    'field' => array('ska.*', 'a.alat as nama_alat', 
                        'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'ska.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'like' => $like,
                    'is_or_like' => true,
                    'where' => $where
        ));

        return $total;
    }

    public function getDataSertifikasi($keyword = '') {
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('a.alat', $keyword),
                array('a.kode_alat', $keyword),
                array('ska.no_sertifikat', $keyword),
                array('na.nama_alat', $keyword),
            );
        }

        $upt_id = $this->session->userdata('upt_id');
        $where = "ska.deleted = 0";
        if ($this->session->userdata('hak_akses') != "Superadmin") {
            $where = "ska.deleted = 0 and a.upt = '" . $upt_id . "'";
        }
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' ska',
                    'field' => array('ska.*', 'a.alat as nama_alat', 
                        'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'ska.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'like' => $like,
                    'is_or_like' => true,
                    'limit' => $this->limit,
                    'offset' => $this->last_no,
                    'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }

        return array(
            'data' => $result,
            'total_rows' => $this->getTotalDataSertifikasi($keyword)
        );
    }

    public function getDetailDataSertifikasi($id) {
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' ska',
                    'field' => array('ska.*', 'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'ska.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'where' => "ska.id = '" . $id . "'"
        ));

        return $data->row_array();
    }

    public function getDataAlat() {
        $upt_id = $this->session->userdata('upt_id');
        $where = "a.deleted = 0";

        if ($this->session->userdata('hak_akses') != "Superadmin") {
//   $where = "deleted = 0 and upt = '" . $upt_id . "'";
            $where = "(a.alat_baru = 0 and a.approver_alat_baru = 0 and a.upt = '" . $upt_id . "' and a.deleted = 0) "
                    . "or (a.alat_baru = 1 and a.approver_alat_baru = 1 and a.upt = '" . $upt_id . "' and a.deleted = 0)";
        }
        $data = Modules::run('database/get', array(
                    'table' => 'alat a',
                    'field' => array('a.*', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }


        return $result;
    }

    public function add() {
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Tambah Sertifikasi Kalibrasi";
        $data['title_content'] = 'Tambah Sertifikasi Kalibrasi';
        $data['list_alat'] = $this->getDataAlat();
        echo Modules::run('template', $data);
    }

    public function ubah($id) {
        $data = $this->getDetailDataSertifikasi($id);
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Ubah Sertifikasi Kalibrasi";
        $data['title_content'] = 'Ubah Sertifikasi Kalibrasi';
        $data['list_alat'] = $this->getDataAlat();
        echo Modules::run('template', $data);
    }

    public function detail($id) {
        $data = $this->getDetailDataSertifikasi($id);
        $data['view_file'] = 'detail_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Detail Sertifikasi Kalibrasi";
        $data['title_content'] = 'Detail Sertifikasi Kalibrasi';
        echo Modules::run('template', $data);
    }

    public function getPostDataHeader($value) {
        $data['no_sertifikat'] = $value->no_sertifikat;
        $data['alat'] = $value->kode_alat;
        $data['tanggal_kalibrasi'] = $value->tanggal_kalibrasi;
        $data['nilai_ketidak_pastian'] = $value->nilai_ketidak_pastian;
        return $data;
    }

    public function simpan() {
        $data = json_decode($this->input->post('data'));

        $id = $this->input->post('id');
        $is_valid = false;
        $upt = $id;
        $this->db->trans_begin();
        try {
            $post_upt = $this->getPostDataHeader($data);
            if ($id == '') {
                if (!empty($_FILES)) {
                    $this->uploadData("file");
                    $post_upt['file'] = $_FILES['file']['name'];
                }
                $upt = Modules::run('database/_insert', $this->getTableName(), $post_upt);
            } else {
                //update
                if (!empty($_FILES)) {
                    $this->uploadData("file");
                    $post_upt['file'] = $_FILES['file']['name'];
                }
                Modules::run('database/_update', $this->getTableName(), $post_upt, array('id' => $id));
            }
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid, 'upt' => $upt));
    }

    public function search($keyword) {
        $this->segment = 4;
        $this->page = $this->uri->segment($this->segment) ?
                $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;
        $keyword = urldecode($keyword);

        $data['keyword'] = $keyword;
        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data Sertifikasi Kalibrasi";
        $data['title_content'] = 'Data Sertifikasi Kalibrasi';
        $content = $this->getDataSertifikasi($keyword);
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function delete($id) {
        $is_valid = false;
        $this->db->trans_begin();
        try {
            Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid));
    }

    public function uploadData($name_of_field) {
        $config['upload_path'] = 'files/berkas/sertifikasi_kalibrasi/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '1000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';

        $this->load->library('upload', $config);
        $this->upload->do_upload($name_of_field);
    }

    public function showSertifikat() {
        $file = str_replace(' ', '_', $this->input->post('file'));
        $data['file'] = $file;
        echo $this->load->view('file_sertifikat', $data, true);
    }

}
