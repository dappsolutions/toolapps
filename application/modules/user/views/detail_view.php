<div class="container-fluid">
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Hak Akses
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $hak_akses ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Pegawai
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nama_pegawai ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Username
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $username ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Password
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $password ?>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="User.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
