<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid">
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Hak Akses
      </div>
      <div class='col-md-4'>
       <select id="akses" error="Hak Akses" class="form-control required">       
        <?php if (!empty($list_akses)) { ?>
         <?php foreach ($list_akses as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($priveledge)) { ?>
           <?php $selected = $value['id'] == $priveledge ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['hak_akses'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Pegawai
      </div>
      <div class='col-md-4'>
       <select id="pegawai" error="Pegawai" class="form-control required">       
        <?php if (!empty($list_pegawai)) { ?>
         <?php foreach ($list_pegawai as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($pegawai)) { ?>
           <?php $selected = $value['id'] == $pegawai ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Username
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='username' error="Username" class='form-control required' value='<?php echo isset($username) ? $username : '' ?>'/>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Password
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='password' error="Password" class='form-control required' value='<?php echo isset($password) ? $password : '' ?>'/>
      </div>
     </div>
     <br/>
     
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="User.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="User.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
