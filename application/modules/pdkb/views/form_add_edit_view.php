<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid">
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       UPT
      </div>
      <div class='col-md-4'>
       <select id="upt" error="UPT" class="form-control required">       
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($upt)) { ?>
           <?php $selected = $value['id'] == $upt ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['upt'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       PDKB
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='gardu_induk' class='form-control required' 
              value='<?php echo isset($gardu) ? $gardu : '' ?>' error="PDKB"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Alamat
      </div>
      <div class='col-md-4'>
       <textarea id="alamat" class="form-control required" error="Alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Pdkb.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="Pdkb.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
