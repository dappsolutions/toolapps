<div class="container-fluid">
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">   
     <label><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       UPT
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nama_upt ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       PDKB
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $gardu ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Alamat
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $alamat ?>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="Pdkb.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
