<?php

class Stockin extends MX_Controller {

    public $segment;
    public $limit;
    public $page;
    public $last_no;

    public function __construct() {
        parent::__construct();
        $this->limit = 10;
    }

    public function getModuleName() {
        return 'stockin';
    }

    public function getHeaderJSandCSS() {
        $data = array(
            '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
            '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
            '<script src="' . base_url() . 'assets/js/controllers/stockin.js"></script>'
        );

        return $data;
    }

    public function getTableName() {
        return 'stock_masuk';
    }

    public function index() {
        $this->segment = 3;
        $this->page = $this->uri->segment($this->segment) ?
                $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;

        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data Stok Masuk";
        $data['title_content'] = 'Data Stok Masuk';
        $content = $this->getDataStock();
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function getTotalDataStock($keyword = '') {
        $upt_id = $this->session->userdata('upt_id');
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('a.kode_alat', $keyword),
                array('na.nama_alat', $keyword),
            );
        }

        $where = "sm.deleted = 0";
        if ($this->session->userdata('hak_akses') != "Superadmin") {
            $where = "sm.deleted = 0 and a.upt = '" . $upt_id . "'";
        }
        $total = Modules::run('database/count_all', array(
                    'table' => $this->getTableName() . ' sm',
                    'field' => "sm.*
                     , sb.jumlah as stok_kembali
                     , sk.jumlah as stok_dipinjam
                     , skk.jumlah  as stok_diservis
                     , a.kode_alat
                     , na.nama_alat
                     , ka.kategori",
                    'join' => array(
                        array('alat a', 'sm.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                        array('(select sum(alat) as jumlah, alat from stock_back GROUP by alat) sb', 'sm.alat = sb.alat', 'left'),
                        array("(select sum(alat) as jumlah, alat from stock_keluar where status = 'BORROW' GROUP by alat) sk", "sm.alat = sk.alat", 'left'),
                        array("(select sum(alat) as jumlah, alat from stock_keluar where status = 'SERVIS' GROUP by alat) skk", "sm.alat = skk.alat", 'left'),
                    ),
                    'like' => $like,
                    'is_or_like' => true,
                    'where' => $where
        ));

        return $total;
    }

    public function getDataStock($keyword = '') {
        $upt_id = $this->session->userdata('upt_id');
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('a.kode_alat', $keyword),
                array('na.nama_alat', $keyword),
            );
        }

        $where = "sm.deleted = 0";
        if ($this->session->userdata('hak_akses') != "Superadmin") {
            $where = "sm.deleted = 0 and a.upt = '" . $upt_id . "'";
        }
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' sm',
                    'field' => "sm.*
                     , sb.jumlah as stok_kembali
                     , sk.jumlah as stok_dipinjam
                     , skk.jumlah  as stok_diservis
                     , a.kode_alat
                     , na.nama_alat
                     , ka.kategori",
                    'join' => array(
                        array('alat a', 'sm.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                        array('(select sum(alat) as jumlah, alat from stock_back GROUP by alat) sb', 'sm.alat = sb.alat', 'left'),
                        array("(select sum(alat) as jumlah, alat from stock_keluar where status = 'BORROW' GROUP by alat) sk", "sm.alat = sk.alat", 'left'),
                        array("(select sum(alat) as jumlah, alat from stock_keluar where status = 'SERVIS' GROUP by alat) skk", "sm.alat = skk.alat", 'left'),
                    ),
                    'like' => $like,
                    'is_or_like' => true,
                    'limit' => $this->limit,
                    'offset' => $this->last_no,
                    'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }

        return array(
            'data' => $result,
            'total_rows' => $this->getTotalDataStock($keyword)
        );
    }

    public function getDetailDataStock($id) {
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' kr',
                    'field' => array('kr.*', 'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'kr.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'where' => "kr.id = '" . $id . "'"
        ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
        return $data->row_array();
    }

    public function getListalat() {
        $upt_id = $this->session->userdata('upt_id');
        $where = "(a.alat_baru = 0 and a.approver_alat_baru = 0 and a.deleted = 0) "
                . "or (a.alat_baru = 1 and a.approver_alat_baru = 1 and a.deleted = 0)";
        if ($this->session->userdata('hak_akses') != 'Superadmin') {
            $where = "(a.alat_baru = 0 and a.approver_alat_baru = 0 and a.upt = '" . $upt_id . "' and a.deleted = 0) "
                    . "or (a.alat_baru = 1 and a.approver_alat_baru = 1 and a.upt = '" . $upt_id . "' and a.deleted = 0)";
        }
        $data = $data = Modules::run('database/get', array(
                    'table' => 'alat a',
                    'field' => array('a.*', 'sa.status', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('(SELECT aad.alat, aad.status_alat, aa.validation 
      FROM anomali_alat_detail aad 
      JOIN anomali_alat aa ON aad.anomali_alat = aa.id
      WHERE aa.validation != 0
      GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'a.id = alat_anomali.alat and a.deleted = 0', 'left'),
                        array('status_alat sa', 'alat_anomali.status_alat = sa.id', 'left'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }


//  echo '<pre>';
//  print_r($result);die;
        return $result;
    }

    public function add() {
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Tambah Stok Masuk";
        $data['title_content'] = 'Tambah Stok Masuk';
        $data['list_alat'] = $this->getListalat();
        echo Modules::run('template', $data);
    }

    public function ubah($id) {
        $data = $this->getDetailDataStock($id);
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Ubah Stok Masuk";
        $data['title_content'] = 'Ubah Stok Masuk';
        $data['list_alat'] = $this->getListalat();
        echo Modules::run('template', $data);
    }

    public function detail($id) {
        $data = $this->getDetailDataStock($id);
        $data['view_file'] = 'detail_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Detail Stok Masuk";
        $data['title_content'] = 'Detail Stok Masuk';
        echo Modules::run('template', $data);
    }

    public function getPostDataHeader($value) {
        $data['alat'] = $value->alat;
        $data['stock'] = $value->stock;
        return $data;
    }

    public function checkIsExistData($alat) {
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' s',
                    'where' => "s.deleted = 0 and s.alat = '" . $alat . "'"
        ));

        $is_exist = false;
        if (!empty($data)) {
            $is_exist = true;
        }

        return $is_exist;
    }

    public function simpan() {
        $data = json_decode($this->input->post('data'));
        $id = $this->input->post('id');
        $is_valid = false;
//  echo '<pre>';
//  print_r($data);die;

        $this->db->trans_begin();
        $message = '';
        try {
            $post = $this->getPostDataHeader($data);
            if ($id == '') {
                $is_exist = $this->checkIsExistData($data->alat);
                if (!$is_exist) {
                    $id = Modules::run('database/_insert', $this->getTableName(), $post);
                } else {
                    $message = 'Data sudah pernah diinput';
                }
            } else {
                //update
                Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
            }
            $this->db->trans_commit();
            if ($message == '') {
                $is_valid = true;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
    }

    public function search($keyword) {
        $this->segment = 4;
        $this->page = $this->uri->segment($this->segment) ?
                $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;
        $keyword = urldecode($keyword);

        $data['keyword'] = $keyword;
        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data Stok Masuk";
        $data['title_content'] = 'Data Stok Masuk';
        $content = $this->getDataStock($keyword);
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function delete($id) {
        $is_valid = false;
        $this->db->trans_begin();
        try {
            Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid));
    }

}
