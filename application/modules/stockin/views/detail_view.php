<div class="container-fluid">
 <div class="row">
  <div class='col-12'>

   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Alat
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nama_alat . ' - ' . $kategori . ' - ' . $kode_alat ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Stok
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $stock ?>
      </div>     
     </div>
     <br/>

     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="Stockin.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
