<?php

class Alat extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'alat';
  }
		
		public function getTableName()
  {
    return 'alat';
  }

  public function index()
  {
    echo 'Alat';
  }


  public function getDetailAlatBarcode(){
    $barcode = $this->input->post('barcode');
    // $barcode = "QRALAT20JAN001";
				$data = Modules::run('database/get', array(
					'table' => $this->getTableName() . ' a',
					'field' => array('a.*', 'ka.kategori', 'u.upt as nama_upt',
									't.tahun as tahun_perolehan',
									'tp.tahun as tahun_produksi_alat', 'gi.gardu as nama_gardu',
									'na.nama_alat', 'na.kategori_alat', 'sa.status', 'gi.context'),
					'join' => array(
									array('nama_alat na', 'a.alat = na.id'),
									array('kategori_alat ka', 'na.kategori_alat = ka.id'),
									array('upt u', 'a.upt = u.id'),
									array('tahun t', 'a.tahun = t.id', 'left'),
									array('tahun tp', 'a.tahun_produksi = tp.id', 'left'),
									array('gardu_induk gi', 'a.gardu_induk = gi.id', 'left'),
									array('status_alat sa', 'sa.id = a.status_alat', 'left'),
					),
					'where' => "a.barcode = '" . $barcode . "'"
		));

    $result = array();
    if(!empty($data)){
      foreach ($data->result_array() as $value) {
				$value['tanggal_kalibrasi'] = $this->getDataTanggalLastKalibrasi($value['id']);
				$value['pemilik_alat'] = $value['context'].' - '.$value['nama_gardu'];
        array_push($result, $value);
      }
    }

    echo json_encode($result);
	}
	
	public function getDataTanggalLastKalibrasi($alat){
		$data = Modules::run('database/get', array(
			'table' => 'sertifikasi_kalibrasi_alat ska',
			'field' => array('ska.*'),
			'where' => "ska.deleted = 0 and ska.alat = '".$alat."'",
			'orderby'=> 'ska.tanggal_kalibrasi desc'
		));

		$date = "";
		if (!empty($data)) {
			$data = $data->row_array();
			$date = $data['tanggal_kalibrasi'];
		}

		return $date;
	}

	public function getListFotoAlat() {
	$barcode = $this->input->post('barcode');
	// $barcode = "QRALAT20JAN5360";
  $data = Modules::run('database/get', array(
							'table' => 'alat_has_foto ahf',
							'field' => array('ahf.*'),
							'join' => array(
								array('alat a', 'a.id = ahf.alat')
							),
              'where' => array('a.barcode' => $barcode)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  echo json_encode($result);
 }
}
