<?php

class Histori extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'histori';
  }

  public function index()
  {
    echo 'histori';
  }

  function getTableName()
  {
    return "pinjaman";
  }

  public function getDataPeminjaman()
  {
    $upt = $this->input->post("upt");
    // $upt = '2';
    $status_dokumen = $this->input->post("status_dokumen");
    // $status_dokumen = '1';
    $hak_akses = $this->input->post("hak_akses");
    // $hak_akses = 'ASMAN';
    $field = array(
      'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
      'phs.status'
    );

    $where = "p.deleted = 0 and p.status_dokumen = " . $status_dokumen . " and phs.status != 'DRAFT'";
    $join = array(
      array('user u', 'p.user = u.id'),
      array('pegawai pg', 'u.pegawai = pg.id'),
      array('upt ut', 'pg.upt = ut.id'),
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id')
    );
    if ($hak_akses == 'ASMAN') {
      $field = array(
        'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
        'asp.status'
      );
      $where = "ut.id = '" . $upt . "' and asp.id is not null and p.status_dokumen = " . $status_dokumen . " ";
      $join = array(
        array('user u', 'p.user = u.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
        array('upt ut', 'pg.upt = ut.id'),
        array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
        array('pinjaman_has_status phs', 'sd.id = phs.id'),
        array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
        array('pinjaman_has_alat pha', 'phh.id = pha.id'),
        array('alat a', 'pha.alat = a.id'),
        array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
        array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
      );
    }

    if ($hak_akses == 'TOOLSMAN') {
      $field = array(
        'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
        'phs.status'
      );
      $where = "ut.id = '" . $upt . "' and phs.status = 'BACK' and p.status_dokumen = " . $status_dokumen . " ";
      $join = array(
        array('user u', 'p.user = u.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
        array('upt ut', 'pg.upt = ut.id'),
        array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
        array('pinjaman_has_status phs', 'sd.id = phs.id'),
        array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
        array('pinjaman_has_alat pha', 'phh.id = pha.id'),
        array('alat a', 'pha.alat = a.id'),
        array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
        array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
      );
    }


    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' p',
      'field' => $field,
      'join' => $join,
      'where' => $where,
      'orderby'=> 'p.id desc'
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        if ($value['status'] == '') {
          $value['status'] = 'DRAFT';
        }
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data'=> $result
    ));
  }
	
	public function getDataSearchPeminjaman()
  {
		$keyword = $this->input->post('keyword');
    $upt = $this->input->post("upt");
    // $upt = '2';
    $status_dokumen = $this->input->post("status_dokumen");
    // $status_dokumen = '1';
    $hak_akses = $this->input->post("hak_akses");
    // $hak_akses = 'ASMAN';
    $field = array(
      'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
      'phs.status'
    );

		$like = array(
			array('p.no_peminjaman', $keyword),
			array('p.tanggal_pinjam', $keyword),
		);

    $where = "(p.deleted = 0 and p.status_dokumen = " . $status_dokumen . " and phs.status != 'DRAFT')";
    $join = array(
      array('user u', 'p.user = u.id'),
      array('pegawai pg', 'u.pegawai = pg.id'),
      array('upt ut', 'pg.upt = ut.id'),
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id')
    );
    if ($hak_akses == 'ASMAN') {
      $field = array(
        'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
        'asp.status'
      );
      $where = "(ut.id = '" . $upt . "' and asp.id is not null and p.status_dokumen = " . $status_dokumen . ") ";
      $join = array(
        array('user u', 'p.user = u.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
        array('upt ut', 'pg.upt = ut.id'),
        array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
        array('pinjaman_has_status phs', 'sd.id = phs.id'),
        array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
        array('pinjaman_has_alat pha', 'phh.id = pha.id'),
        array('alat a', 'pha.alat = a.id'),
        array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
        array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
      );
    }

    if ($hak_akses == 'TOOLSMAN') {
      $field = array(
        'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
        'phs.status'
      );
      $where = "(ut.id = '" . $upt . "' and phs.status = 'BACK' and p.status_dokumen = " . $status_dokumen . ") ";
      $join = array(
        array('user u', 'p.user = u.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
        array('upt ut', 'pg.upt = ut.id'),
        array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
        array('pinjaman_has_status phs', 'sd.id = phs.id'),
        array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
        array('pinjaman_has_alat pha', 'phh.id = pha.id'),
        array('alat a', 'pha.alat = a.id'),
        array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
        array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
      );
    }


    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' p',
      'field' => $field,
      'join' => $join,
			'where' => $where,
			'like'=> $like,
			'is_or_like'=> true,
      'orderby'=> 'p.id desc'
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        if ($value['status'] == '') {
          $value['status'] = 'DRAFT';
        }
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data'=> $result
    ));
  }
}
