<?php

class Surat_pinjam extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'surat_pinjam';
  }

  public function index()
  {
    echo 'surat_pinjam';
  }

  public function getStatusId($status){
    $data = Modules::run('database/get', array(
      'table' => 'status_pinjaman',
      'where'=> array('status'=> $status)
    ));

    return $data->row_array()['id'];
  }

  public function getUptId($upt){
    $data = Modules::run('database/get', array(
      'table' => 'upt',
      'where'=> array('upt'=> $upt)
    ));

    return $data->row_array()['id'];
  }

  public function simpan()
  {
    $file_name = $this->input->post('file_name');
    $upt = $this->input->post('upt');    
    $id_pinjaman = $this->input->post('id_pinjaman');
    $status = $this->input->post('status');
    $base64_file = $this->input->post('base64_file');
    $is_upload = $this->uploadFile($file_name, $base64_file);
        
    $post_surat['upt'] = $this->getUptId($upt);
    $post_surat['pinjaman'] = $id_pinjaman;
    if($is_upload){
      $post_surat['file'] = $file_name;
    }    
    $post_surat['status_pinjaman'] = $this->getStatusId($status);
    Modules::run('database/_insert', 'surat_pinjaman', $post_surat);
    
    echo json_encode(array(
      'is_valid'=> true
    ));
  }


  public function getDataSuratPinjaman(){
    $id_pinjaman = $this->input->post('id_pinjaman');
    // $id_pinjaman = 7;
    $data = Modules::run('database/get', array(
      'table'=> 'surat_pinjaman sp',
      'field' => array('sp.*', 'spp.status', 'ut.upt as nama_upt'),
      'join' => array(
        array('status_pinjaman spp', 'spp.id = sp.status_pinjaman'),
        array('upt ut', 'ut.id = sp.upt')
      ),
      'where' => array('sp.pinjaman'=> $id_pinjaman, 'sp.deleted'=> false)
    ));

    $result = array();
    if(!empty($data)){
      foreach ($data->result_array() as $value) {
				$value['file'] = str_replace(" ", "_", $value['file']);
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data'=> $result
    ));
  }

  public function uploadFile($file_name, $base64_file)
  {
    $is_valid = false;
    try {
      $imageData = $base64_file;
      $base64 = $imageData;
      if (trim($base64) != "") {
        $filename = $file_name;
        $extension = "pdf";

        // $filename = 'files/berkas/surat_pinjaman/' . $filename . "." . $extension; // path and name of image, change as per your need          
        $filename = 'files/berkas/surat_pinjaman/' . $filename; // path and name of image, change as per your need          
        //
        $base64 = str_replace(" ", "+", $base64);
        if ($base64 == '') {
          $is_valid = false;
          return $is_valid;
        }
        //
        $binarydata = base64_decode($base64);
        //
        //     echo $filename.'<br/>';
        $result = file_put_contents($filename, $binarydata);
        if ($result) {
          @chmod($filename, 0777);
          $is_valid = true;
        }
      }
    } catch (Exception $e) {

      $is_valid = false;
    }

    return $is_valid;
  }

  public function getListUpt()
  {
    $data = Modules::run('database/get', array(
      'table' => 'upt',
      'where' => "deleted = 0"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }

  public function getListStatusPeminjaman() {
    $data = Modules::run('database/get', array(
                'table' => 'status_pinjaman',
    ));
  
    $result = array();
    if (!empty($data)) {
     foreach ($data->result_array() as $value) {
      array_push($result, $value);
     }
    }
  
  
    echo json_encode(array(
      'data' => $result
    ));
   }

   public function execDelete(){
    $id = $this->input->post('id');
    Modules::run("database/_update", "surat_pinjaman", array('deleted'=> true), array('id'=> $id));
  }
}
