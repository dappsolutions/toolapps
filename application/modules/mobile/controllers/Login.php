<?php

class Login extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'login';
  }

  public function index()
  {
    echo 'login';
  }

  public function getDataUserDb($username, $password)
  {
    $data = Modules::run('database/get', array(
      'table' => 'user u',
      'field' => array('u.*', 'p.hak_akses', 'ut.id as upt_id'),
      'join' => array(
        array('priveledge p', 'u.priveledge = p.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
        array('upt ut', 'pg.upt = ut.id'),
      ),
      'where' => array(
        'u.username' => $username,
        'u.password' => $password,
        'u.deleted' => 0
      )
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
    }

    return $result;
  }

  public function getDataUser($username, $password)
  {
    $data = $this->getDataUserDb($username, $password);
    $result = array();
    if (!empty($data)) {
      $result = $data;
    }

    return $result;
  }

  public function sign_in()
  {
    $username = $this->input->post('username');
    $password = $this->input->post('password');

    $is_valid = false;
    try {
      $data = $this->getDataUser($username, $password);

      if (!empty($data)) {
        $is_valid = true;
      }
    } catch (Exception $exc) {
      $is_valid = false;
    }

    if ($is_valid) {
      $this->setSessionData($data);
    } else {
      echo json_encode(array(
        'is_valid' => $is_valid
      ));
    }
  }

  public function setSessionData($data)
  {
    $session['is_valid'] = true;
    $session['user_id'] = $data['id'];
    $session['username'] = $data['username'];
    $session['hak_akses'] = $data['hak_akses'];
    $session['upt_id'] = $data['upt_id'];
    echo json_encode($session);
  }
}
