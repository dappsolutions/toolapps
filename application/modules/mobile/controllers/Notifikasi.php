<?php

class Notifikasi extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'notifikasi';
  }

  public function index()
  {
    echo 'notifikasi';
  }

  function getTableName()
  {
    return "notifikasi";
  }

  public function jumlahNotif()
  {
    $user = $this->input->post("user");
    // $user = 3;
    $total = Modules::run('database/count_all', array(
      'table' => $this->getTableName() . ' n',
      'where' => "n.deleted = 0 and n.user = '" . $user . "' and n.read_status = 0"
    ));

    $total = $total == '0' ? '' : $total;
    echo $total;
  }

  public function getDataNotifikasi()
  {    
    $user = $this->input->post("user");
    // $user = 3;

    Modules::run("database/_update", $this->getTableName(), array('read_status'=> true), array('user'=> $user, "deleted"=> "0"));
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' n',
      'field' => array("n.*"),
      'where' => "n.deleted = 0 and n.user = '" . $user . "'"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }
}
