<?php

class Pinjam extends MX_Controller
{

    public function getDataUpt($upt)
    {
        $data = Modules::run('database/get', array(
            'table' => 'upt',
            'where' => array('upt' => $upt)
        ));

        $result = array();
        if (!empty($data)) {
            $result = $data->row_array();
        }

        return $result;
    }

    public function getListalat()
    {
        // $upt = $this->getDataUpt();
        $upt = $this->input->post('upt');
        $data_upt = $this->getDataUpt($upt);

        $data = $data = Modules::run('database/get', array(
            'table' => 'alat a',
            'field' => array('a.*', 'sa.status', 'sm.stock', 'a.upt'),
            'join' => array(
                array('(SELECT aad.alat, aad.status_alat, aa.validation 
        FROM anomali_alat_detail aad 
        JOIN anomali_alat aa ON aad.anomali_alat = aa.id
        WHERE aa.validation != 0
        GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'a.id = alat_anomali.alat and a.deleted = 0', 'left'),
                array('status_alat sa', 'alat_anomali.status_alat = sa.id', 'left'),
                array('stock_masuk sm', 'a.id = sm.alat', 'left'),
            ),
            'where' => "(sa.status is null or (sa.status != 'Rusak')) and a.upt = '" . $data_upt['id'] . "' and a.deleted = 0"
            // 'where' => "(sa.status is null or (sa.status != 'Rusak')) and a.deleted = 0"
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                $value['stock'] = $value['stock'] == '' ? 0 : $value['stock'];
                $value['stok'] = $value['stock'];
                array_push($result, $value);
            }
        }


        echo json_encode(array(
            'data' => $result
        ));
    }

    public function getDataUptIdData($upt)
    {
        $upt_id = 0;
        $data = Modules::run('database/get', array(
            'table' => 'upt u',
            'where' => array('u.upt' => $upt)
        ));

        if (!empty($data)) {
            $data = $data->row_array();
            $upt_id = $data['id'];
        }


        return $upt_id;
    }

    public function simpan()
    {
        $user = $this->input->post('user');
        $tanggal = $this->input->post('tanggal');
        $keterangan = $this->input->post('keterangan');
        $status_dokumen = $this->input->post('status_dokumen');
        $data_alat = json_decode($this->input->post('pinjam_alat'));


        $is_valid = false;

        $upt = array();
        $upt_id_data = array();
        $pinjaman_id = "";
        $this->db->trans_begin();
        try {

            //pinjaman
            if ($status_dokumen == 1) {
                $pinjaman['no_peminjaman'] = Modules::run('no_generator/generateNoPeminjaman', 'internal');
            } else {
                $pinjaman['no_peminjaman'] = Modules::run('no_generator/generateNoPeminjaman', 'eksternal');
            }
            $tanggal = date('Y-m-d', strtotime($tanggal));
            $pinjaman['tanggal_pinjam'] = $tanggal . ' ' . date('H:i:s');
            $pinjaman['user'] = $user;
            $pinjaman['status_dokumen'] = $status_dokumen;
            $pinjaman['keterangan'] = $keterangan;
            $id = Modules::run('database/_insert', "pinjaman", $pinjaman);

            //pinjaman_has_alat
            foreach ($data_alat as $value) {
                $pha['pinjaman'] = $id;
                $pha['alat'] = $value->alat;
                $pha['qty'] = $value->qty;

                //untuk notif ke masing2 upt
                $upt_id = $this->getDataUptIdData($value->upt);
                array_push($upt, $value->upt);
                array_push($upt_id_data, $upt_id);

                $pha_id = Modules::run('database/_insert', 'pinjaman_has_alat', $pha);

                //pinjaman has alat status
                $psa['pinjaman_has_alat'] = $pha_id;
                $psa['status'] = 'NORMAL';
                $psa['status_alat'] = 'NOT TAKEN';
                Modules::run('database/_insert', 'status_pinjam', $psa);
            }

            $ps['pinjaman'] = $id;
            if ($status_dokumen == '1') {
                $ps['status'] = 'COMPLETE';
            } else {
                $ps['status'] = 'DRAFT';
            }
            Modules::run('database/_insert', 'pinjaman_has_status', $ps);

            $pinjaman_id = $id;
            //notifikasi
            $status_pinjam = $status_dokumen == "1" ? "Internal" : 'Eksternal';

            $this->sendNotifikasi(
                $id,
                "pinjaman",
                "Pengajuan Peminjaman " . $status_pinjam . " Alat dengan Nomer Peminjaman " . $pinjaman['no_peminjaman'] . "",
                "Pinjaman " . $status_pinjam,
                "DRAFT",
                strtolower($status_pinjam),
                $upt
            );

            //send email
            $this->sendEmailToReceiver($upt_id_data, $pinjaman, $status_pinjam);
            $this->db->trans_commit();
            $is_valid = true;
        } catch (\Throwable $th) {
            $this->db->trans_rollback();
        }

        echo json_encode(array(
            'is_valid' => $is_valid,
            'pinjaman' => $pinjaman_id
        ));
    }

    public function sendEmailToReceiver($upt = array(), $data_pinjaman, $status_pinjam = "")
    {
        $upt = array_unique($upt);
        $upt_id = implode(',', $upt);
        $no_peminjaman = $data_pinjaman['no_peminjaman'];
        $data = Modules::run('database/get', array(
            'table' => 'user u',
            'field' => array('u.*', 'p.email'),
            'join' => array(
                array('pegawai p', 'u.pegawai = p.id'),
                array('upt ut', 'p.upt = ut.id'),
            ),
            'where' => "ut.id in (" . $upt_id . ") and u.priveledge = 3"
        ));

        if (!empty($data)) {
            $pesan = "Pengajuan " . $status_pinjam . " dengan No Peminjaman : " . $data_pinjaman['no_peminjaman'];
            foreach ($data->result_array() as $value) {
                $send = Modules::run("email/send_email", "Pengajuan " . $status_pinjam . "", $pesan, $value['email']);
                Modules::run("database/_insert", "status_email", array(
                    'no_peminjaman' => $no_peminjaman,
                    "status" => $send
                ));
            }
        }
    }

    public function getDataUptId($upt)
    {
        $upt_id = array();
        foreach ($upt as $value) {
            $data = Modules::run('database/get', array(
                'table' => 'upt u',
                'where' => "u.deleted = 0 and u.upt = '" . $value . "'"
            ));

            $upt_id_str = "";
            if (!empty($data)) {
                $data = $data->row_array();
                $upt_id_str = $data['id'];
            }
            array_push($upt_id, $upt_id_str);
        }

        return implode(",", $upt_id);
    }

    public function getDataToken($upt = array(), $document, $id_destination, $pesan, $status, $jenis)
    {

        $upt_id = $this->getDataUptId($upt);
        $data = Modules::run('database/get', array(
            'table' => 'user u',
            'field' => array("u.*"),
            'join' => array(
                array('pegawai pg', "u.pegawai = pg.id"),
                array('upt ut', "pg.upt = ut.id"),
            ),
            'where' => "u.deleted = 0 and u.priveledge = '3' and u.token is not null and ut.id in (" . $upt_id . ")"
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value['token']);
                Modules::run("database/_insert", "notifikasi", array(
                    'user' => $value['id'],
                    'document' => $document,
                    'id_destination' => $id_destination,
                    'pesan' => $pesan,
                    'status' => $status,
                    'jenis' => $jenis
                ));
            }
        }

        // echo '<pre>';
        // print_r($result);die;
        return $result;
    }

    public function sendNotifikasi($id_destination, $document, $pesan, $title, $status = "", $jenis = "", $data_upt = array())
    {
        $token = array();
        if (!empty($data_upt)) {
            $data_upt = array_unique($data_upt);
            $token = $this->getDataToken($data_upt, $document, $id_destination, $pesan, $status, $jenis);
        }
        // $id_pengajuan = 8;

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            'registration_ids' => $token,
            'notification' => array(
                'title' => $title,
                'body' => $pesan,
            ),
            'data' => array(
                'id' => $id_destination,
                'document' => $document,
                "status" => $status,
                "jenis" => $jenis
            )
        );

        // echo '<pre>';
        // print_r($fields);die;
        $headers = array(
            'Authorization:key=AAAAF4rFPvU:APA91bFi6do7PwxsrfnQ9A563fRH2cQlxpOs_e6rKe-cTF6hnU6LMro-flW9V_Ibqg7z8ECxwz6fRN82ozDlgSDCMKH59Vrgc190HofidzbkKe6_rsotT1QaETQ1rgwoitv_6BE0Hhel',
            'Content-Type:application/json'
        );



        // echo json_encode($fields);die;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);


        // echo 'asdad';die;
        // echo '<pre>';
        // print_r($result);die;

        if ($result === false) {

            die('Curl failed: ' . curl_error($ch));
        }

        curl_close($ch);
    }

    public function getListUpt()
    {
        $data = Modules::run('database/get', array(
            'table' => 'upt',
            'where' => "deleted = 0"
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result
        ));
    }

    public function getDataPeminjaman()
    {
        $hak_akses = $this->input->post('hak_akses');
        $status_dokumen = $this->input->post('status_dokumen');
        $upt = $this->input->post('upt');
        // $status_dokumen = 1;
        $field = array(
            'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
            'phs.status'
        );
        $where = "p.deleted = 0 and p.status_dokumen = " . $status_dokumen . " and phs.status = 'DRAFT'";
        $join = array(
            array('user u', 'p.user = u.id'),
            array('pegawai pg', 'u.pegawai = pg.id'),
            array('upt ut', 'pg.upt = ut.id'),
            array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
            array('pinjaman_has_status phs', 'sd.id = phs.id')
        );
        if ($hak_akses == 'ASMAN') {
            $field = array(
                'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
                'asp.status'
            );
            $where = "ut.id = '" . $upt . "' and asp.id is null and p.status_dokumen = '" . $status_dokumen . "'";
            $join = array(
                array('user u', 'p.user = u.id'),
                array('pegawai pg', 'u.pegawai = pg.id'),
                array('upt ut', 'pg.upt = ut.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
                array('pinjaman_has_status phs', 'sd.id = phs.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
                array('pinjaman_has_alat pha', 'phh.id = pha.id'),
                array('alat a', 'pha.alat = a.id'),
                array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
                array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
            );
        }

        if ($hak_akses == 'TOOLSMAN') {
            $field = array(
                'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
                'phs.status'
            );
            $where = "ut.id = '" . $upt . "' and phs.status = 'COMPLETE' and p.status_dokumen = '" . $status_dokumen . "'";
            $join = array(
                array('user u', 'p.user = u.id'),
                array('pegawai pg', 'u.pegawai = pg.id'),
                array('upt ut', 'pg.upt = ut.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
                array('pinjaman_has_status phs', 'sd.id = phs.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
                array('pinjaman_has_alat pha', 'phh.id = pha.id'),
                array('alat a', 'pha.alat = a.id'),
                array('approver_status_pinjam asp', 'p.id = asp.pinjaman'),
                array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
            );
        }


        $data = Modules::run('database/get', array(
            'table' => 'pinjaman p',
            'field' => $field,
            'join' => $join,
            'where' => $where,
            'orderby' => 'p.id desc'
        ));

        // echo '<pre>';
        // echo $this->db->last_query();
        // die;
        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                if ($value['status'] == '') {
                    $value['status'] = 'DRAFT';
                }
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result
        ));
    }
    
	public function getDataPeminjamanInternal()
    {
        $hak_akses = $this->input->post('hak_akses');
        $status_dokumen = $this->input->post('status_dokumen');
        $upt = $this->input->post('upt');
		// $status_dokumen = 1;
		
		// echo '<pre>';
		// print_r($_POST);die;
        $field = array(
            'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
            'phs.status'
        );
        $where = "p.deleted = 0 and p.status_dokumen = " . $status_dokumen . " and (phs.status = 'DRAFT' or phs.status = 'COMPLETE')";
        $join = array(
            array('user u', 'p.user = u.id'),
            array('pegawai pg', 'u.pegawai = pg.id'),
            array('upt ut', 'pg.upt = ut.id'),
            array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
            array('pinjaman_has_status phs', 'sd.id = phs.id')
        );
        if ($hak_akses == 'ASMAN') {
            $field = array(
                'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
                'asp.status'
            );
            $where = "ut.id = '" . $upt . "' and asp.id is null and p.status_dokumen = '" . $status_dokumen . "'";
            $join = array(
                array('user u', 'p.user = u.id'),
                array('pegawai pg', 'u.pegawai = pg.id'),
                array('upt ut', 'pg.upt = ut.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
                array('pinjaman_has_status phs', 'sd.id = phs.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
                array('pinjaman_has_alat pha', 'phh.id = pha.id'),
                array('alat a', 'pha.alat = a.id'),
                array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
                array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
            );
        }

        if ($hak_akses == 'TOOLSMAN') {
            $field = array(
                'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
                'phs.status'
            );
            $where = "ut.id = '" . $upt . "' and phs.status = 'COMPLETE' and p.status_dokumen = '" . $status_dokumen . "'";
            $join = array(
                array('user u', 'p.user = u.id'),
                array('pegawai pg', 'u.pegawai = pg.id'),
                array('upt ut', 'pg.upt = ut.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
                array('pinjaman_has_status phs', 'sd.id = phs.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
                array('pinjaman_has_alat pha', 'phh.id = pha.id'),
                array('alat a', 'pha.alat = a.id'),
                array('approver_status_pinjam asp', 'p.id = asp.pinjaman'),
                array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
            );
        }


        $data = Modules::run('database/get', array(
            'table' => 'pinjaman p',
            'field' => $field,
            'join' => $join,
            'where' => $where,
            'orderby' => 'p.id desc'
        ));

        // echo '<pre>';
        // echo $this->db->last_query();
        // die;
        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                if ($value['status'] == '') {
                    $value['status'] = 'DRAFT';
                }
                array_push($result, $value);
            }
        }

        echo json_encode($result);
    }
		
	public function getSearchDataPeminjaman()
    {
        $hak_akses = $this->input->post('hak_akses');
        $keyword = $this->input->post('keyword');
        $status_dokumen = $this->input->post('status_dokumen');
        $upt = $this->input->post('upt');
		// $status_dokumen = 1;
		
		$like = array(
			array('p.no_peminjaman', $keyword),
			array('p.tanggal_pinjam', $keyword),
		);

        $field = array(
            'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
            'phs.status'
        );
        $where = "(p.deleted = 0 and p.status_dokumen = " . $status_dokumen . " and phs.status = 'DRAFT')";
        $join = array(
            array('user u', 'p.user = u.id'),
            array('pegawai pg', 'u.pegawai = pg.id'),
            array('upt ut', 'pg.upt = ut.id'),
            array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
            array('pinjaman_has_status phs', 'sd.id = phs.id')
        );
        if ($hak_akses == 'ASMAN') {
            $field = array(
                'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
                'asp.status'
            );
            $where = "(ut.id = '" . $upt . "' and asp.id is null and p.status_dokumen = '" . $status_dokumen . "')";
            $join = array(
                array('user u', 'p.user = u.id'),
                array('pegawai pg', 'u.pegawai = pg.id'),
                array('upt ut', 'pg.upt = ut.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
                array('pinjaman_has_status phs', 'sd.id = phs.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
                array('pinjaman_has_alat pha', 'phh.id = pha.id'),
                array('alat a', 'pha.alat = a.id'),
                array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
                array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
            );
        }

        if ($hak_akses == 'TOOLSMAN') {
            $field = array(
                'p.*', 'pg.nama as nama_pegawai', 'ut.upt',
                'phs.status'
            );
            $where = "(ut.id = '" . $upt . "' and phs.status = 'COMPLETE' and p.status_dokumen = '" . $status_dokumen . "')";
            $join = array(
                array('user u', 'p.user = u.id'),
                array('pegawai pg', 'u.pegawai = pg.id'),
                array('upt ut', 'pg.upt = ut.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
                array('pinjaman_has_status phs', 'sd.id = phs.id'),
                array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
                array('pinjaman_has_alat pha', 'phh.id = pha.id'),
                array('alat a', 'pha.alat = a.id'),
                array('approver_status_pinjam asp', 'p.id = asp.pinjaman'),
                array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $upt . "' "),
            );
        }


        $data = Modules::run('database/get', array(
            'table' => 'pinjaman p',
            'field' => $field,
            'join' => $join,
			'where' => $where,
			'like'=> $like,
			'is_or_like'=> true,
            'orderby' => 'p.id desc'
        ));

        // echo '<pre>';
        // echo $this->db->last_query();
        // die;
        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                if ($value['status'] == '') {
                    $value['status'] = 'DRAFT';
                }
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result
        ));
    }

    public function getDetailPeminjaman()
    {
        $id_peminjaman = $this->input->post('id_pengajuan');

        $data = Modules::run('database/get', array(
            'table' => 'pinjaman p',
            'where' => array('p.id' => $id_peminjaman)
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result
        ));
    }

    public function getListAlatDipinjam()
    {
        $id_peminjaman = $this->input->post('id_pengajuan');
        // $id_peminjaman = 7;

        $data = Modules::run('database/get', array(
            'table' => 'pinjaman_has_alat pha',
            'field' => array('pha.*', 'a.kode_alat', 'u.upt as nama_upt', 'sp.status_alat'),
            'join' => array(
                array('alat a', 'pha.alat = a.id'),
                array('upt u', 'a.upt = u.id'),
                array('status_pinjam sp', 'pha.id = sp.pinjaman_has_alat'),
            ),
            'where' => array('pha.pinjaman' => $id_peminjaman, 'pha.deleted' => 0)
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }

        echo json_encode(array(
            'data' => $result
        ));
    }

    public function getDataAlatValid($qrcode)
    {
        $data = Modules::run('database/get', array(
            'table' => 'alat',
            'where' => "deleted = 0 and barcode = '" . $qrcode . "'"
        ));

        // echo '<pre>';
        // echo $this->db->last_query();die;
        $is_valid = false;
        if (!empty($data)) {
            $is_valid = true;
        }

        return $is_valid;
    }

    public function getDataPinjaman($id)
    {
        $data = Modules::run('database/get', array(
            'table' => 'pinjaman p',
            'field' => array('p.*'),
            'join' => array(
                array('pinjaman_has_alat pha', 'pha.pinjaman = p.id')
            ),
            'where' => array('pha.id' => $id)
        ));


        $result = array();
        if (!empty($data)) {
            $result = $data->row_array();
        }


        return $result;
    }

    public function getDataQtyStockMasuk($alat)
    {
        $data = Modules::run('database/get', array(
            'table' => 'stock_masuk',
            'where' => "deleted = 0 and alat = '" . $alat . "'"
        ));

        $result = array();
        if (!empty($data)) {
            $result = $data->row_array();
        }


        return $result;
    }

    public function updateStatusDokumenPengajuanPinjaman($pinjaman)
    {
        $data = Modules::run('database/get', array(
            'table' => 'status_pinjam sp',
            'field' => array('sp.*'),
            'join' => array(
                array('pinjaman_has_alat pha', 'sp.pinjaman_has_alat = pha.id'),
            ),
            'where' => "pha.deleted = 0 and pha.pinjaman = '" . $pinjaman . "'"
        ));


        $total = !empty($data) ? count($data->result_array()) : 0;

        $total_back = 0;
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                if ($value['status_alat'] == 'BACK') {
                    $total_back += 1;
                }
            }
        }

        if ($total_back != 0) {
            if ($total_back == $total) {
                $post['pinjaman'] = $pinjaman;
                $post['status'] = 'BACK';
                Modules::run('database/_insert', 'pinjaman_has_status', $post);
            }
        }
    }

    public function doScanAlat()
    {
        $id = $this->input->post('id_pinjaman');
        $alat = $this->input->post('alat');
        $qty = $this->input->post('qty');
        $status = $this->input->post('status');
        $qrcode = str_replace('Code128', '', $this->input->post('qrcode'));
        $qrcode = str_replace(':', '', $qrcode);
        $qrcode = trim($qrcode);


        $alat_valid = $this->getDataAlatValid($qrcode);


        // echo $alat_valid;die;
        $is_valid = false;
        $message = "Tidak Valid";

        $data_pinjam = $this->getDataPinjaman($id);

        //  $alat_valid = true;
        if ($alat_valid) {
            $is_valid = false;
            $this->db->trans_begin();
            try {
                //insert stock keluar
                if ($status == "NOT TAKEN") {
                    $post_keluar['no'] = $data_pinjam['no_peminjaman'];
                    $post_keluar['alat'] = $alat;
                    $post_keluar['status'] = 'BORROW';
                    $post_keluar['qty'] = $qty;
                    Modules::run('database/_insert', 'stock_keluar', $post_keluar);
                }

                if ($status == "TAKEN") {
                    $post_back['no'] = $data_pinjam['no_peminjaman'];
                    $post_back['alat'] = $alat;
                    $post_back['status'] = 'NORMAL';
                    $post_back['qty'] = $qty;
                    Modules::run('database/_insert', 'stock_back', $post_back);
                }

                //minus stock masuk
                $stock_masuk = $this->getDataQtyStockMasuk($alat);
                $total_stock = $stock_masuk['stock'];

                if ($status == "NOT TAKEN") {
                    $qty_stock = $total_stock - $qty;
                    $out_of_stock = $qty_stock < 0 ? true : false;
                } else {
                    $out_of_stock = false;
                    $qty_stock = $total_stock + $qty;
                }

                $is_save = true;
                if ($out_of_stock) {
                    $message = "Stok Alat Tidak Cukup, Kuantitas pinjam melebihi stock";
                    $is_valid = false;
                    $is_save = false;
                } else {
                    $is_valid = true;


                    Modules::run('database/_update', 'stock_masuk', array('stock' => $qty_stock), array('id' => $stock_masuk['id']));

                    switch ($status) {
                        case 'NOT TAKEN':
                            $status_alat = "TAKEN";
                            break;
                        case 'TAKEN':
                            $status_alat = "BACK";
                            break;
                    }
                    Modules::run('database/_update', 'status_pinjam', array('status_alat' => $status_alat), array('pinjaman_has_alat' => $id));

                    $this->updateStatusDokumenPengajuanPinjaman($data_pinjam['id']);
                }

                if ($is_save) {
                    $this->db->trans_commit();
                } else {
                    $this->db->trans_rollback();
                }
            } catch (Exception $ex) {
                $this->db->trans_rollback();
            }
        }

        echo json_encode(array('is_valid' => $is_valid, 'message' => $message));
    }

    public function execDelete()
    {
        $id = $this->input->post('id_pinjaman');
        Modules::run("database/_update", "pinjaman", array('deleted' => true), array('id' => $id));
    }

    public function approveAsman()
    {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $upt = $this->input->post('upt');
        $user = $this->input->post('user');

        $is_valid = false;
        $this->db->trans_begin();
        try {
            $post_data['pinjaman'] = $id;
            $post_data['upt'] = $upt;
            $post_data['user'] = $user;
            $post_data['status'] = $status;
            if (isset($_POST['keterangan'])) {
                $post_data['keterangan'] = $this->input->post('keterangan');
            }
            Modules::run('database/_insert', "approver_status_pinjam", $post_data);


            $this->updateStatusDokumenPengajuan($id);
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid));
    }

    public function updateStatusDokumenPengajuan($pinjaman)
    {
        $data = Modules::run('database/get', array(
            'table' => 'pinjaman_has_alat pha',
            'field' => array('distinct(ut.id) as upt', 'asp.id as asp_id'),
            'join' => array(
                array('alat a', 'pha.alat = a.id'),
                array('upt ut', 'a.upt = ut.id'),
                array('approver_status_pinjam asp', 'pha.pinjaman = asp.pinjaman', 'left'),
            ),
            'where' => "pha.deleted = 0 and pha.pinjaman = '" . $pinjaman . "'"
        ));

        $total = !empty($data) ? count($data->result_array()) : 0;

        $total_approve = 0;
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                if ($value['asp_id'] != '') {
                    $total_approve += 1;
                }
            }
        }

        if ($total_approve != 0) {
            if ($total_approve == $total) {
                $post['pinjaman'] = $pinjaman;
                $post['status'] = 'COMPLETE';
                Modules::run('database/_insert', 'pinjaman_has_status', $post);
            }
        }
    }

    public function checkStatusPeminjaman($id)
    {
        $data = Modules::run('database/get', array(
            'table' => 'approver_status_pinjam',
            'where' => "pinjaman = '" . $id . "'"
        ));

        $status = "DRAFT";
        if (!empty($data)) {
            $status = $data->row_array()['status'];
        }
        echo $status;
    }

    public function getListStatusPengajuan()
    {
        $pinjaman = $this->input->post('id');
        $hak_akses = $this->input->post('hak_akses');
        $upt = $this->input->post('upt');
        $where = array('pinjaman' => $pinjaman);
        if ($hak_akses == 'ASMAN') {
            $where = array('asp.pinjaman' => $pinjaman, 'asp.upt' => $upt);
        }

        $data = Modules::run('database/get', array(
            'table' => 'approver_status_pinjam asp',
            'field' => array('asp.*', 'ut.upt', 'p.nama as pegawai', 'asp.createddate as tgl_approve'),
            'join' => array(
                array('user u', 'asp.user = u.id'),
                array('pegawai p', 'u.pegawai = p.id'),
                array('upt ut', 'p.upt = ut.id'),
            ),
            'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                $value['tgl_approve'] = Modules::run('helper/getIndoDate', $value['tgl_approve']);
                array_push($result, $value);
            }
        }


        echo json_encode(array(
            'data' => $result
        ));
    }
}
