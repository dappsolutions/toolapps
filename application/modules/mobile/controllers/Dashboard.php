<?php

class Dashboard extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'dashboard';
  }

  public function index()
  {
    echo 'dashboard';
  }

  function getTableName()
  {
    return "pinjaman";
  }

  public function getDataAlatKeluar()
  {
    $data = Modules::run('database/get', array(
      'table' => 'pinjaman_has_alat pha',
      'field' => array(
        'pha.*', 'ut.upt as nama_upt',
        'a.kode_alat', 'pg.nama as nama_pegawai',
        'sp.status_alat', 'sd.status',
        'p.no_peminjaman', 'p.tanggal_pinjam'
      ),
      'join' => array(
        array('pinjaman p', 'p.id = pha.pinjaman and p.deleted = 0'),
        array('alat a', 'a.id = pha.alat'),
        array('upt ut', 'a.upt = ut.id'),
        array('user u', 'u.id = pha.createdby'),
        array('pegawai pg', 'pg.id = u.pegawai'),
        array('status_dokumen sd', 'sd.id = p.status_dokumen'),
        array('status_pinjam sp', "sp.pinjaman_has_alat = pha.id and sp.status_alat != 'BACK'"),
      ),
      'orderby' => "pha.id desc"
    ));

    //  echo '<pre>';
    //  print_r($data->result_array());die;
    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }

  public function getDataAlatKembali()
  {
    $data = Modules::run('database/get', array(
      'table' => 'pinjaman_has_alat pha',
      'field' => array(
        'pha.*', 'ut.upt as nama_upt',
        'a.kode_alat', 'pg.nama as nama_pegawai',
        'sp.status_alat', 'sd.status',
        'p.no_peminjaman', 'p.tanggal_pinjam'
      ),
      'join' => array(
        array('pinjaman p', 'p.id = pha.pinjaman and p.deleted = 0'),
        array('alat a', 'a.id = pha.alat'),
        array('upt ut', 'a.upt = ut.id'),
        array('user u', 'u.id = pha.createdby'),
        array('pegawai pg', 'pg.id = u.pegawai'),
        array('status_dokumen sd', 'sd.id = p.status_dokumen'),
        array('status_pinjam sp', "sp.pinjaman_has_alat = pha.id and sp.status_alat = 'BACK'"),
      ),
      'orderby' => "pha.id desc"
    ));

    //  echo '<pre>';
    //  print_r($data->result_array());die;
    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }

    echo json_encode(array(
      'data' => $result
    ));
  }
}
