<?php

class Main extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function getModuleName()
  {
    return 'main';
  }

  public function index()
  {
    echo 'main';
  }

  function getTableName()
  {
    return "user";
  }

  public function setTokenUser()
  {
    $user = $this->input->post('user');
    $token = $this->input->post('token');
    Modules::run("database/_update", $this->getTableName(), array('token' => $token), array('id' => $user));
    echo 'update token';
  }

  public function getDataToken()
  {

    $data = Modules::run('database/get', array(

      'table' => 'user u',
      'where' => "u.deleted = 0 and u.priveledge = '3' and u.token is not null"

    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value['token']);
      }
    }

    // echo '<pre>';
    // print_r($result);die;
    return $result;
  }

  public function sendNotifikasi()
  {
    $token = $this->getDataToken();    
    $id_pengajuan = $this->input->post("id_pengajuan");
    $id_pengajuan = 8;

    $url = 'https://fcm.googleapis.com/fcm/send';

    $fields = array(
      'registration_ids' => $token,
      'notification' => array(
        'title' => "Emergency",
        'body' => "Pengajuan Peminjaman Alat dengan Nomor Pengajuan " . $id_pengajuan,
      ),
      'data' => array(
        'id_pengajuan' => $id_pengajuan,
      )

    );

    // echo '<pre>';
    // print_r($fields);die;
    $headers = array(
      'Authorization:key=AAAAF4rFPvU:APA91bFi6do7PwxsrfnQ9A563fRH2cQlxpOs_e6rKe-cTF6hnU6LMro-flW9V_Ibqg7z8ECxwz6fRN82ozDlgSDCMKH59Vrgc190HofidzbkKe6_rsotT1QaETQ1rgwoitv_6BE0Hhel',
      'Content-Type:application/json'
    );



    // echo json_encode($fields);die;

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    $result = curl_exec($ch);


    // echo 'asdad';die;
    // echo '<pre>';
    // print_r($result);die;

    if ($result === false) {

      die('Curl failed: ' . curl_error($ch));
    }

    // Modules::run("database/_insert", "tombol_action", array(
    //   'user' => $user,
    //   'tombol' => $taruna . " telah menekan tombol emergency segera hubungi no telp. " . $no_telp,
    // ));
    curl_close($ch);
  }
}
