<div class='container-fluid'>
 <div class="row">
  <!-- column -->
  <div class="col-12">
   <div class="white-box">
    <div class="card-body">
     <div class='row'>
      <div class='col-md-2'>
       <button id="" class="btn btn-block btn-warning" onclick="Ttd.add()">Tambah</button>
      </div>
      <div class='col-md-10'>
       <input type='text' name='' id='' onkeyup="Ttd.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
      </div>     
     </div>        
     <br/>
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div>
     <br/>
     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr class="bg-success text-white">
           <th class="font-12">No</th>
           <th class="font-12">NIP</th>
           <th class="font-12">Nama</th>
           <th class="font-12">File</th>
           <th class="font-12">Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr>
             <td class="font-12"><?php echo $no++ ?></td>
             <td class="font-12"><?php echo $value['nip'] ?></td>
             <td class="font-12"><?php echo $value['nama_pegawai'] ?></td>
             <td class="font-12">
              <label class="label label-danger font-10 hover" onclick="Ttd.showFile(this, event)"><?php echo $value['file'] ?></label>
             </td>
             <td class="text-center">
              <label id="" class="label label-warning font-10" hover 
                      onclick="Ttd.ubah('<?php echo $value['id'] ?>')">Ubah</label>
              &nbsp;
              <label id="" class="label label-success font-10 hover" 
                      onclick="Ttd.detail('<?php echo $value['id'] ?>')">Detail</label>
              &nbsp;
              <label id="" class="label label-danger font-10 hover" 
                      onclick="Ttd.delete('<?php echo $value['id'] ?>')">Hapus</label>
              &nbsp;
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td class="text-center font-12" colspan="5">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>
     </div>    
     <br/>
     <div class="row">
      <div class="col-md-12">
       <div class="pagination">
        <?php echo $pagination['links'] ?>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div> 
</div>