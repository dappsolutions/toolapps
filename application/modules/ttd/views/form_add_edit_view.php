<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid">
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       UPT
      </div>
      <div class='col-md-4'>
       <select id="pegawai" error="Pegawai" class="form-control required">       
        <?php if (!empty($list_pegawai)) { ?>
         <?php foreach ($list_pegawai as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($pegawai)) { ?>
           <?php $selected = $value['id'] == $pegawai ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama'].' ('.$value['hak_akses'].')' ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class="col-md-3">
       File Ttd (<b>.Png, Jpg, Jpeg</b>)
      </div>
      <div class="col-md-4">
       <div class="form-group">
         <div class="fileinput fileinput-new input-group" data-provides="fileinput">
          <div class="form-control" data-trigger="fileinput"> 
           <i class="glyphicon glyphicon-file fileinput-exists"></i> 
           <span class="fileinput-filename"><?php echo isset($file) ? $file : '' ?></span>
          </div> 
          <span class="input-group-addon btn btn-default btn-file"> 
           <span class="fileinput-new">Select file</span> 
           <span class="fileinput-exists">Change</span>
           <input id='file' type="file" name="..." onchange="Alat.getFilename(this, 'xls')"> 
          </span> 
          <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
         </div>
       </div>
      </div>
     </div>
     <br/>
     <hr/>
     
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Ttd.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="Ttd.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
