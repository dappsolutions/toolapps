<div class="container-fluid">
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Pegawai
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nip.' - '.$nama_pegawai ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       File (Png, Jpg, Jpeg)
      </div>
      <div class='col-md-4 text-danger'>
       <label class="label label-danger hover" onclick="Ttd.showFile(this, event)"><?php echo $file ?></label>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="Ttd.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
