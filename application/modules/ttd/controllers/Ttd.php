<?php

class Ttd extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'ttd';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/ttd.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'ttd_pegawai';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Ttd";
  $data['title_content'] = 'Data Ttd';
  $content = $this->getDataTtd();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataTtd($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'pg.nama as nama_pegawai', 'pg.nip'),
              'join' => array(
                  array('pegawai pg', 't.pegawai = pg.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "t.deleted = 0"
  ));

  return $total;
 }

 public function getDataTtd($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'pg.nama as nama_pegawai', 'pg.nip'),
              'join' => array(
                  array('pegawai pg', 't.pegawai = pg.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "t.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataTtd($keyword)
  );
 }

 public function getDetailDataTtd($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*', 'p.nama as nama_pegawai', 'p.nip'),
              'join' => array(
                  array('pegawai p', 'kr.pegawai = p.id')
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*', 'pp.hak_akses'),
              'join' => array(
                  array('user u', 'p.id = u.pegawai', 'left'),
                  array('priveledge pp', 'pp.id = u.priveledge', 'left'),
              ),
              'where' => 'p.deleted = 0 and p.id > 1',
              'orderby' => "p.nip"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Ttd";
  $data['title_content'] = 'Tambah Ttd';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataTtd($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Ttd";
  $data['title_content'] = 'Ubah Ttd';
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataTtd($id);
  $data['file'] = str_replace(' ', '_', $data['file']);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Ttd";
  $data['title_content'] = 'Detail Ttd';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['pegawai'] = $value->pegawai;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $message = "";
  $this->db->trans_begin();
  try {
   $post_ttd = $this->getPostDataHeader($data);
   $is_uploaded = true;
   if ($id == '') {
    if (isset($_FILES['file'])) {
     $data_file = $this->uploadData("file");
     $is_uploaded = $data_file['is_valid'];
     if ($is_uploaded) {
      $post_ttd['file'] = $_FILES['file']['name'];
     } else {
      $is_valid = false;
      $message = $data_file['response'];
     }
    }

    if ($is_uploaded) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post_ttd);
    }
   } else {
    //update
    if (isset($_FILES['file'])) {
     $data_file = $this->uploadData("file");
     $is_uploaded = $data_file['is_valid'];
     if ($is_uploaded) {
      $post_ttd['file'] = $_FILES['file']['name'];
     } else {
      $is_valid = false;
      $message = $data_file['response'];
     }
    }
    if ($is_uploaded) {
     Modules::run('database/_update', $this->getTableName(), $post_ttd, array('id' => $id));
    }
   }
   if ($is_uploaded) {
    $this->db->trans_commit();
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Ttd";
  $data['title_content'] = 'Data Ttd';
  $content = $this->getDataTtd($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/ttd_pegawai/';
  $config['allowed_types'] = 'png|jpg|jpeg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showFile() {
  $file = str_replace(' ', '_', $this->input->post('file'));
  $data['file'] = str_replace(' ', '_', $file);
  echo $this->load->view('file', $data, true);
 }

}
