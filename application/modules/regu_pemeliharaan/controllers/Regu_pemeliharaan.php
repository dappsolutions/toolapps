<?php

class Regu_pemeliharaan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'regu_pemeliharaan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/regu_pemeliharaan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'regu_pemeliharaan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Regu Pemeliharaan";
  $data['title_content'] = 'Regu Pemeliharaan';
  $content = $this->getDataRegu();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataRegu($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('u.regu', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' u',
  'field' => array('u.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "u.deleted = 0"
  ));

  return $total;
 }

 public function getDataRegu($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('u.regu', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' u',
  'field' => array('u.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "u.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataRegu($keyword)
  );
 }

 public function getDetailDataRegu($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Regu Pemeliharaan";
  $data['title_content'] = 'Tambah Regu Pemeliharaan';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataRegu($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Regu Pemeliharaan";
  $data['title_content'] = 'Ubah Regu Pemeliharaan';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataRegu($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Regu Pemeliharaan";
  $data['title_content'] = 'Detail Regu Pemeliharaan';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['regu'] = $value->regu;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $upt = $id;
  $this->db->trans_begin();
  try {
   $post_upt = $this->getPostDataHeader($data);
   if ($id == '') {
    $upt = Modules::run('database/_insert', $this->getTableName(), $post_upt);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_upt, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'upt' => $upt));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Regu Pemeliharaan";
  $data['title_content'] = 'Regu Pemeliharaan';
  $content = $this->getDataRegu($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
