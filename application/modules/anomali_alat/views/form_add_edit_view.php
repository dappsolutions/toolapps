<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-8 grid-margin stretch-card'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>         

     <div class="row">
      <div class='col-md-3'>
       Gardu Induk / ULTG
      </div>
      <div class='col-md-9'>
       <select id="gardu_induk" error="Gardu Induk / ULTG" class="form-control required">       
        <?php if (!empty($list_gardu)) { ?>
         <?php foreach ($list_gardu as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($gardu_induk)) { ?>
           <?php $selected = $value['id'] == $gardu_induk ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['gardu'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kategori Alat
      </div>
      <div class='col-md-9'>
       <select id="kategori_alat" error="Kategori Alat" class="form-control required"
               onchange="AnomaliAlat.getNamaAlatList(this)">
        <option value="">Pilih Kategori</option>
        <?php if (!empty($list_kategori)) { ?>
         <?php foreach ($list_kategori as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($kategori_alat)) { ?>
           <?php $selected = $value['id'] == $kategori_alat ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       List Alat
      </div>
      <div class='col-md-9'>
       <select id="alat" error="List Alat" class="form-control required" onchange="AnomaliAlat.getAlatNama(this)">
        <option value="lain">Lain - lain</option>
        <?php if (!empty($list_nama_alat)) { ?>
         <?php foreach ($list_nama_alat as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($alat)) { ?>
           <?php $selected = $value['id'] == $alat ? 'selected' : '' ?>
          <?php } ?>
          <option kategori="<?php echo $value['kategori_alat'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_alat'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <hr/>

     <?php $hide_alat = ""; ?>
     <?php $required_alat = "required"; ?>
     <?php if (isset($alat)) { ?>
      <?php $hide_alat = 'display-none'; ?>
      <?php $required_alat = "" ?>
     <?php } ?>

     <div class="row nama_alat <?php echo $hide_alat ?>">
      <div class='col-md-3'>
       Nama Alat
      </div>
      <div class='col-md-9'>
       <input type='text' name='' id='nama_alat' error="Nama Alat" class='form-control <?php echo $required_alat ?>' value=''/>
       <p class="label label-success" style="margin-top: 8px">Nama Alat Baru</p>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Merk Alat
      </div>
      <div class='col-md-9'>
       <input type='text' name='' id='merk' error="Merk AnomaliAlat" class='form-control required' value='<?php echo isset($merk) ? $merk : '' ?>'/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tipe Alat
      </div>
      <div class='col-md-9'>
       <input type='text' name='' id='tipe_alat' error="Tipe AnomaliAlat" class='form-control required' value='<?php echo isset($tipe) ? $tipe : '' ?>'/>
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       Nomer Seri
      </div>
      <div class='col-md-9'>
       <input type='text' name='' id='nomer_seri' error="Nomer Seri" class='form-control required' value='<?php echo isset($nomer_seri) ? $nomer_seri : '' ?>'/>
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       Tahun Perolehan
      </div>
      <div class='col-md-9'>
       <select id="tahun" error="Tahun Perolehan" class="form-control required">       
        <?php if (!empty($list_tahun)) { ?>
         <?php foreach ($list_tahun as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($tahun)) { ?>
           <?php $selected = $value['id'] == $tahun ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tahun'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kelengkapan
      </div>
      <div class='col-md-9'>
       <input type='text' name='' id='kelengkapan' error="Kelengkapan Alat" class='form-control required' value='<?php echo isset($kelengkapan_alat) ? $kelengkapan_alat : '' ?>'/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tonase
      </div>
      <div class='col-md-9'>
       <input type='text' name='' id='tonase' error="Tonase Alat" class='form-control required text-right' value='<?php echo isset($tonase) ? $tonase : '' ?>'/>
      </div>     
     </div>
     <br/>   

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-9'>
       <textarea id="keterangan" class="form-control required" error="Keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>     
     </div>
     <br/>

     <div class='row manual_add <?php echo!isset($manual_book) ? '' : 'display-none' ?>'>
      <div class='col-md-3'>
       Manual Book (<b>.Pdf</b>)
      </div>
      <div class='col-md-9'>
       <div class="form-group manual_upload">
        <div class="input-group col-xs-12">
         <input id="filename" class="form-control file-upload-info" disabled="" placeholder="Upload File Pdf" type="text">
         <span class="input-group-append">
          <button class="file-upload-browse btn btn-warning" type="button" onclick="AnomaliAlat.upload(this)">Import</button>
          <input type="file" style="display: none;" id="file" onchange="AnomaliAlat.getFilename(this)"/>
         </span>
        </div>
       </div>
      </div>
     </div>
     <div class='row manual_detail <?php echo!isset($manual_book) ? 'display-none' : '' ?>'>
      <div class='col-md-3'>
       Manual Book (<b>.Pdf</b>)
      </div>
      <div class='col-md-6'>
       <a href="#" onclick="AnomaliAlat.showManualBook(this, event)" class="label label-danger"><?php echo $manual_book ?></a>        
      </div>
      <div class='col-md-3 text-right'>
       <i class="mdi mdi-close mdi-24px hover" onclick="AnomaliAlat.changeManual(this)"></i>
      </div>
     </div>
     <hr/>    
     <br/>

     <div class="row">
      <div class='col-md-12'>
       <h4>Regu Pemeliharaan Alat</h4>
       <?php if (isset($list_regu_alat)) { ?>     
       <button style="margin-bottom: 8px;" class="btn btn-warning" onclick="AnomaliAlat.tambahRegu(this)">Tambah Regu</button>
       <?php } ?>
       <table class="table table-bordered table-striped" id="tb_regu">
        <thead class="bg-success text-white">
         <tr>
          <th>Regu</th>
          <th>Action</th>
         </tr>
        </thead>
        <tbody>
         <?php if (isset($list_regu_alat)) { ?>
          <?php if (!empty($list_regu_alat)) { ?>
           <?php foreach ($list_regu_alat as $value) { ?>
            <tr class="" id="<?php echo $value['id'] ?>">
             <td>
              <select id="regu_pemeliharaan" error="Regu Pemeliharaan" class="form-control">
               <?php if (!empty($list_regu)) { ?>
                <?php foreach ($list_regu as $value) { ?>
                 <?php $selected = "" ?>
                 <?php if (isset($regu_pemeliharaan)) { ?>
                  <?php $selected = $value['id'] == $regu_pemeliharaan ? 'selected' : '' ?>
                 <?php } ?>
                 <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['regu'] ?></option>
                <?php } ?>
               <?php } ?>
              </select>
             </td>
             <td class="text-center">
              <i class="mdi mdi-minus-circle mdi-24px hover" onclick="AnomaliAlat.removeReguData(this)"></i>
             </td>
            </tr>
           <?php } ?>
          <?php } ?>
          <tr class="baru display-none baru_edit">
           <td>
            <select id="regu_pemeliharaan" error="Regu Pemeliharaan" class="form-control">
             <?php if (!empty($list_regu)) { ?>
              <?php foreach ($list_regu as $value) { ?>
               <?php $selected = "" ?>
               <?php if (isset($regu_pemeliharaan)) { ?>
                <?php $selected = $value['id'] == $regu_pemeliharaan ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['regu'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </td>
           <td class="text-center">
            <i class="mdi mdi-plus-circle mdi-24px hover" onclick="AnomaliAlat.addRegu(this)"></i>
           </td>
          </tr>
         <?php } else { ?>
          <tr class="baru">
           <td>
            <select id="regu_pemeliharaan" error="Regu Pemeliharaan" class="form-control">
             <?php if (!empty($list_regu)) { ?>
              <?php foreach ($list_regu as $value) { ?>
               <?php $selected = "" ?>
               <?php if (isset($regu_pemeliharaan)) { ?>
                <?php $selected = $value['id'] == $regu_pemeliharaan ? 'selected' : '' ?>
               <?php } ?>
               <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['regu'] ?></option>
              <?php } ?>
             <?php } ?>
            </select>
           </td>
           <td class="text-center">
            <i class="mdi mdi-plus-circle mdi-24px hover" onclick="AnomaliAlat.addRegu(this)"></i>
           </td>
          </tr>
         <?php } ?>
        </tbody>
       </table>       
      </div>     
     </div>
     <br/>
     <hr/>

     <?php if (isset($list_foto)) { ?>
      <div class="row">
       <div class='col-md-3'>
        <u>Detail Foto</u>
       </div>   
      </div>
      <br/>     

      <div class="row">
       <?php if (!empty($list_foto)) { ?>
        <?php foreach ($list_foto as $value) { ?>
         <div class='col-md-4 data_image' id="<?php echo $value['id'] ?>">
          <div class='col-12 text-right'>
           <i class="mdi mdi-close mdi-24px hover" onclick="AnomaliAlat.removeFoto(this)"></i>
          </div>
          <div class='col-12 text-center'>           
           <img width="200" height="150" src="<?php echo $value['foto'] ?>"/>
          </div>         
          <br/>
          <div class='col-12 text-center'>
           <input style="margin-left: 16px;" type='text' name='' placeholder="Keterangan Foto" id='title' class='form-control' value='<?php echo $value['title'] ?>'/>
          </div>         
         </div>
        <?php } ?>
       <?php } ?>            
      </div>
      <br/>
      <hr/>
     <?php } ?>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="AnomaliAlat.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="AnomaliAlat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div> 

  <div class='col-md-4 grid-margin'>
   <div class='card'>
    <div class='card-body'>
     <div class='row'>
      <div class='col-md-12'>
       <u>Foto Alat</u>
      </div>
     </div>
     <hr/>
     <div class="row">
      <div class='col-md-12 text-center'>
       <div class='images'>
        <div class='pic' onclick="AnomaliAlat.upload(this, event)">
         <i class="mdi mdi-plus mdi-24px hover"></i>
        </div>
       </div>
      </div>
     </div>
    </div>
    <br/>
   </div>
  </div>
 </div>
</div>
