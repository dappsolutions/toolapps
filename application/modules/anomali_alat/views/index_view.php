<div class='container-fluid'>

 <div class='row'>
  <div class='col-md-12'>
   <div class="white-box">  
    <div class="card-body">
     <h4 class="card-title"><u><?php echo $title ?></u></h4>

     <div class='row'>    
      <div class='col-md-4'>
       <button id="" class="btn btn-warning" onclick="AnomaliAlat.add()">Tambah</button>
      </div>
      <div class='col-md-8'>
       <input type='text' name='' id='' onkeyup="AnomaliAlat.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
      </div>     
     </div>        
     <br/>
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div> 
    </div>
   </div>
  </div>
 </div>

 <br/>
 <div class='row'>
  <div class='col-md-12'>
   <div class='white-box'>
    <div class='card-body'>
     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr class="bg-success text-white">
           <th class="font-12">No</th>
           <th class="font-12">UPT</th>
           <th class="font-12">Gardu Induk / ULTG</th>
           <th class="font-12">Kode Alat</th>
           <th class="font-12">Nomer Seri</th>
           <th class="font-12">Nama Alat</th>
           <th class="font-12">Merk Alat</th>
           <th class="font-12">Kategori Alat</th>
           <th class="font-12">Tipe Alat</th>
           <th class="font-12">Tahun Perolehan</th>
           <th class="font-12">Keterangan</th>
           <th class="font-12">Status Alat</th>
           <th class="font-12">Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr>
             <td class="font-12"><?php echo $no++ ?></td>
             <td class="font-12"><?php echo $value['nama_upt'] ?></td>
             <td class="font-12"><?php echo $value['gardu'] == '' ? '-' : $value['gardu'] ?></td>
             <td class="font-12"><?php echo '<b>' . $value['kode_alat'] . '</b>' ?></td>
             <td class="font-12"><?php echo $value['nomer_seri'] ?></td>
             <td class="font-12"><?php echo $value['nama_alat'] ?></td>
             <td class="font-12"><?php echo $value['merk'] ?></td>
             <td class="font-12"><?php echo $value['kategori'] ?></td>
             <td class="font-12"><?php echo $value['tipe'] ?></td>
             <td class="font-12"><?php echo $value['tahun_perolehan'] ?></td>
             <td class="font-12"><?php echo $value['keterangan'] ?></td>
             <td class="font-12">
              <?php if ($value['status_alat'] != '') { ?>
               <?php echo $value['status_alat'] == '2' ? '<label class="label label-danger font-12 ">Rusak</label>' : '<label class="label label-success font-12">Normal<label>' ?>
               &nbsp;
              <?php } ?>
             </td>
             <td class="text-center">
              <label id="" class="label label-warning font-10" 
                     onclick="AnomaliAlat.ubah('<?php echo $value['id'] ?>')">Ubah</label>
              &nbsp;
              <label id="" class="label label-success font-10" 
                     onclick="AnomaliAlat.detail('<?php echo $value['id'] ?>')">Detail</label>
              &nbsp;              
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td class="text-center" colspan="13">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>
     </div>    
     <br/>
     <div class="row">
      <div class="col-md-12">
       <div class="pagination">
        <?php echo $pagination['links'] ?>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>