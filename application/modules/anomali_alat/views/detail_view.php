<div class="container-fluid">
 <div class="row">
  <div class='col-3'>
   <div class='white-box'>
    <div class='card-body'>
     <div class='row'>
      <div class='col-md-12 text-center'>
       <img width="150" height="150" src="<?php echo base_url() . 'files/qr_code/alat/' . $barcode . '.png' ?>"/>
      </div>
     </div>
    </div>
   </div>
  </div>
  <div class='col-9'>
   <div class="white-box">
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       UPT
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $nama_upt ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Gardu Induk / ULTG  
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $gardu == '' ? 'Belum Di Atur' : $gardu ?>
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       Kategori Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $kategori ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $nama_alat ?>
      </div>     
     </div>
     <br/>              

     <div class="row">
      <div class='col-md-3'>
       Merk Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $merk ?>
      </div>     
     </div>
     <br/>    

     <div class="row">
      <div class='col-md-3'>
       Tipe Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $tipe ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Nomer Seri
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $nomer_seri ?>
      </div>     
     </div>
     <br/>                        

     <div class="row">
      <div class='col-md-3'>
       Kelengkapan
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $kelengkapan_alat ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tonase
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $tonase ?>
      </div>     
     </div>
     <br/> 

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $keterangan ?>
      </div>     
     </div>
     <br/>

     <div class='row'>
      <div class='col-md-3'>
       Manual Book (<b>.Pdf</b>)
      </div>
      <div class='col-md-9 text-danger'>
       <a href="#" onclick="AnomaliAlat.showManualBook(this, event)" class="label label-danger"><?php echo $manual_book ?></a>
      </div>
     </div>
     <br/>
     <hr/>

     <div class="row">
      <div class='col-md-12'>
       <h4>Regu Pemeliharaan Alat</h4>
       <table class="table color-bordered-table success-bordered-table" id="tb_regu">
        <thead class="bg-success text-white">
         <tr>
          <th>Regu</th>
         </tr>
        </thead>
        <tbody>
         <?php if (isset($list_regu_alat)) { ?>
          <?php if (!empty($list_regu_alat)) { ?>
           <?php foreach ($list_regu_alat as $value) { ?>
            <tr class="" id="<?php echo $value['id'] ?>">
             <td>
              <?php echo $value['regu']; ?>
             </td>
            </tr>
           <?php } ?>
          <?php } ?>
         <?php } ?>
        </tbody>
       </table>       
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       <u>Detail Foto</u>
      </div>   
     </div>
     <br/>
     <hr/>

     <div class="row">
      <?php if (!empty($list_foto)) { ?>
       <?php foreach ($list_foto as $value) { ?>
        <div class='col-md-4'>       
         <div class='col-12 text-center'>
          <img width="200" height="150" src="<?php echo $value['foto'] ?>"/>
         </div>         
         <br/>
         <div class='col-12 text-center'>
          <label class="text-danger"><?php echo $value['title'] == '' ? 'Tidak Ada Keterangan' : $value['title'] ?></label>
         </div>         
        </div>
       <?php } ?>
      <?php } ?>            
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="AnomaliAlat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
