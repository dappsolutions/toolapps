<?php

class Anomali_alat extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt_id = array();
  $hak_akses = $this->session->userdata('hak_akses');

  $upt = $this->session->userdata('upt_id');

  if ($hak_akses != "Superadmin") {
   $this->upt_id = array('u.upt' => $upt);
  }
 }

 public function getModuleName() {
  return 'anomali_alat';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/anomali_alat.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'alat';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Alat";
  $data['title_content'] = 'Data Alat';
  $content = $this->getDataAlat();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataAlat($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('na.nama_alat', $keyword),
       array('u.barcode', $keyword),
       array('ka.kategori', $keyword),
       array('u.tipe', $keyword),
       array('ut.upt', $keyword),
       array('t.tahun', $keyword),
       array('u.nomer_seri', $keyword),
       array('gi.gardu', $keyword),
   );
  }

  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                  'alat_anomali.status_alat AS status_alat', 'gi.gardu'),
              'join' => array(
                  array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                  array('tahun t', 'u.tahun = t.id and u.deleted = 0'),
                  array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                  array('gardu_induk gi', 'u.gardu_induk = gi.id', 'left'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'u.id = alat_anomali.alat and u.deleted = 0', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $this->upt_id
  ));

  return $total;
 }

 public function getDataAlat($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('na.nama_alat', $keyword),
       array('u.barcode', $keyword),
       array('ka.kategori', $keyword),
       array('u.tipe', $keyword),
       array('ut.upt', $keyword),
       array('t.tahun', $keyword),
       array('u.nomer_seri', $keyword),
       array('gi.gardu', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                  'alat_anomali.status_alat AS status_alat', 'gi.gardu', 'na.nama_alat'),
              'join' => array(
                  array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                  array('tahun t', 'u.tahun = t.id and u.deleted = 0'),
                  array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                  array('gardu_induk gi', 'u.gardu_induk = gi.id', 'left'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'u.id = alat_anomali.alat and u.deleted = 0', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $this->upt_id
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAlat($keyword)
  );
 }

 public function getDetailDataAlat($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' a',
              'field' => array('a.*', 'ka.kategori', 'u.upt as nama_upt',
                  't.tahun as tahun_perolehan', 'gi.gardu',
                  'na.nama_alat', 'na.kategori_alat'),
              'join' => array(
                  array('nama_alat na', 'a.alat = na.id'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id'),
                  array('upt u', 'a.upt = u.id'),
                  array('tahun t', 'a.tahun = t.id'),
                  array('gardu_induk gi', 'a.gardu_induk = gi.id', 'left'),
              ),
              'where' => "a.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListKategoriAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_alat',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTipeAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'tipe_alat',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTahun() {
  $data = Modules::run('database/get', array(
              'table' => 'tahun',
              'where' => array('deleted' => 0),
              'orderby' => "tahun asc"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListRegu() {
  $data = Modules::run('database/get', array(
              'table' => 'regu_pemeliharaan',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListGardu() {
  $upt = $this->session->userdata('upt_id');

  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk',
              'where' => array('deleted' => 0, "upt" => $upt)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListNamaAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'nama_alat',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Alat";
  $data['title_content'] = 'Tambah Alat';
  $data['list_kategori'] = $this->getListKategoriAlat();
  $data['list_upt'] = $this->getListUpt();
  $data['list_tahun'] = $this->getListTahun();
  $data['list_regu'] = $this->getListRegu();
  $data['list_gardu'] = $this->getListGardu();
  $data['list_nama_alat'] = $this->getListNamaAlat();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataAlat($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Alat";
  $data['title_content'] = 'Ubah Alat';
  $data['list_foto'] = $this->getListFotoAlat($id);
  $data['list_kategori'] = $this->getListKategoriAlat();
  $data['list_upt'] = $this->getListUpt();
  $data['list_tahun'] = $this->getListTahun();
  $data['list_regu'] = $this->getListRegu();
  $data['list_gardu'] = $this->getListGardu();
  $data['list_nama_alat'] = $this->getListNamaAlat();
  $data['list_regu_alat'] = $this->getListReguAlat($id);
//  echo '<pre>';
//  print_r($data['list_regu_alat']);die;
  echo Modules::run('template', $data);
 }

 public function getListFotoAlat($alat) {
  $data = Modules::run('database/get', array(
              'table' => 'alat_has_foto',
              'where' => array('alat' => $alat)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListReguAlat($id) {
  $data = Modules::run('database/get', array(
              'table' => 'alat_has_regu_pemeliharaan ap',
              'field' => array('ap.*', 'r.regu'),
              'join' => array(
                  array('regu_pemeliharaan r', 'ap.regu_pemeliharaan = r.id')
              ),
              'where' => "ap.alat = '" . $id . "' and ap.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataAlat($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Alat";
  $data['title_content'] = 'Detail Alat';
  $data['list_foto'] = $this->getListFotoAlat($id);
  $data['list_regu_alat'] = $this->getListReguAlat($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $alat = $value->alat;
  if ($value->alat == 'lain') {
   $post_nama['nama_alat'] = $value->nama_alat;
   $post_nama['kategori_alat'] = $value->kategori_alat;
   $alat = Modules::run('database/_insert', 'nama_alat', $post_nama);
  }

  $data['barcode'] = Modules::run('no_generator/generateQrCode');
  $data['kode_alat'] = Modules::run('no_generator/generateKodeAlat', $value);
  $data['alat'] = $alat;
  $data['tipe'] = $value->tipe_alat;
  $data['upt'] = $this->session->userdata('upt_id');
  $data['keterangan'] = $value->keterangan;
  $data['status_alat'] = 0;
  $data['nomer_seri'] = $value->nomer_seri;
  $data['merk'] = $value->merk;
  $data['tahun'] = $value->tahun;
  $data['kelengkapan_alat'] = $value->kelengkapan;
  $data['tonase'] = $value->tonase;
  $data['gardu_induk'] = $value->gardu_induk;
  return $data;
 }

 public function getPostAlatFoto($alat, $value) {
  $data['alat'] = $alat;
  $data['title'] = $value->keterangan_foto;
  $data['foto'] = $value->img;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $alat = $id;
  $this->db->trans_begin();
  try {
   $post_alat = $this->getPostDataHeader($data);
   if ($id == '') {
    if (!empty($_FILES)) {
     $this->uploadData("file");
     $post_alat['manual_book'] = $_FILES['file']['name'];
    }
    $alat = Modules::run('database/_insert', $this->getTableName(), $post_alat);
    if (!empty($data->detail_image)) {
     foreach ($data->detail_image as $value) {
      $post_alat_foto = $this->getPostAlatFoto($alat, $value);
      Modules::run('database/_insert', 'alat_has_foto', $post_alat_foto);
     }
    }

    if (!empty($data->regu)) {
     foreach ($data->regu as $value) {
      $post_regu['alat'] = $alat;
      $post_regu['regu_pemeliharaan'] = $value->regu_pemeliharaan;
      Modules::run('database/_insert', 'alat_has_regu_pemeliharaan', $post_regu);
     }
    }
   } else {
    //update
    unset($post_alat['barcode']);
    if (!empty($_FILES)) {
     $this->uploadData("file");
     $post_alat['manual_book'] = $_FILES['file']['name'];
    }
    Modules::run('database/_update', $this->getTableName(), $post_alat, array('id' => $id));
    if (!empty($data->detail_image)) {
     foreach ($data->detail_image as $value) {
      $post_alat_foto = $this->getPostAlatFoto($alat, $value);
      Modules::run('database/_insert', 'alat_has_foto', $post_alat_foto);
     }
    }

    //update foto
    if (!empty($data->foto_update)) {
     foreach ($data->foto_update as $value) {
      Modules::run('database/_update', 'alat_has_foto', array('title' => $value->title), array('id' => $value->id));
     }
    }

    //hapus foto
    if (!empty($data->foto_remove)) {
     foreach ($data->foto_remove as $value) {
      Modules::run('database/_delete', 'alat_has_foto', array('id' => $value->id));
     }
    }

    //regu baru
    if (!empty($data->regu)) {
     foreach ($data->regu as $value) {
      $post_regu['alat'] = $alat;
      $post_regu['regu_pemeliharaan'] = $value->regu_pemeliharaan;
      Modules::run('database/_insert', 'alat_has_regu_pemeliharaan', $post_regu);
     }
    }

    //regu edit
    if (!empty($data->regu_edit)) {
     foreach ($data->regu_edit as $value) {
      if ($value->is_edit) {
       //update
       $post_regu['alat'] = $alat;
       $post_regu['regu_pemeliharaan'] = $value->regu_pemeliharaan;
       Modules::run('database/_update', 'alat_has_regu_pemeliharaan', $post_regu, array('id' => $value->id));
      } else {
       //delete
       Modules::run('database/_update', 'alat_has_regu_pemeliharaan', array('deleted' => 0), array('id' => $value->id));
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
   if ($is_valid) {
    if ($id == '') {
     $this->make_barcode($post_alat['barcode']);
    }
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'alat' => $alat));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Alat";
  $data['title_content'] = 'Data Alat';
  $content = $this->getDataAlat($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function make_barcode($barcode) {
  try {
   $this->load->library('ciqrcode');
   $qr_code = $this->ciqrcode;
   $param['data'] = $barcode;
   $param['level'] = 'H';
   $param['size'] = 20;
   $param['savename'] = 'files/qr_code/alat/' . $barcode . '.png';
   $qr_code->generate($param);
  } catch (Exception $ex) {
   
  }
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/manual_book/';
  $config['allowed_types'] = 'pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

}
