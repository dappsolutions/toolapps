<?php

class Usulan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt_id;
 public $hak_akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 40;
  $this->hak_akses = $this->session->userdata('hak_akses');

  $this->upt_id = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'usulan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/usulan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'usulan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Usulan";
  $data['title_content'] = 'Data Usulan';
  $content = $this->getDataUsulan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataUsulan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.no_usulan', $keyword),
   );
  }

  if ($this->hak_akses == 'Superadmin') {
   $where = array('u.deleted = 0');
  } else {
   $where = "u.deleted = 0 and u.upt = '" . $this->upt_id . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*', 'ut.upt as nama_upt', 'us.status'),
              'join' => array(
                  array('upt ut', 'u.upt = ut.id'),
                  array('(select max(id) id, usulan from usulan_status group by usulan) uss', 'uss.usulan = u.id'),
                  array('usulan_status us', 'us.id = uss.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  return $total;
 }

 public function getDataUsulan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.no_usulan', $keyword),
   );
  }

  if ($this->hak_akses == 'Superadmin') {
   $where = array('u.deleted = 0');
  } else {
   $where = "u.deleted = 0 and u.upt = '" . $this->upt_id . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*', 'ut.upt as nama_upt', 'us.status'),
              'join' => array(
                  array('upt ut', 'u.upt = ut.id'),
                  array('(select max(id) id, usulan from usulan_status group by usulan) uss', 'uss.usulan = u.id'),
                  array('usulan_status us', 'us.id = uss.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataUsulan($keyword)
  );
 }

 public function getDetailDataUsulan($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_alat',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Usulan";
  $data['title_content'] = 'Tambah Usulan';
  $data['list_kategori'] = $this->getListKategori();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataUsulan($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Usulan";
  $data['title_content'] = 'Ubah Usulan';
  $data['detail_alat'] = $this->getDataDetailAlat($id);
  $data['list_kategori'] = $this->getListKategori();
  echo Modules::run('template', $data);
 }

 public function getDataDetailAlat($anomali) {
  $data = Modules::run('database/get', array(
              'table' => 'usulan_alat us',
              'field' => array('us.*', 'ka.kategori'),
              'join' => array(
                  array('kategori_alat ka', 'ka.id = us.kategori_alat'),
              ),
              'where' => array('us.usulan' => $anomali)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataApproval($anomali) {
  $data = Modules::run('database/get', array(
              'table' => 'usulan_status us',
              'field' => array('us.*', 'pg.nama as nama_pegawai', 'pg.nip', 'pv.hak_akses'),
              'join' => array(
                  array('user u', 'us.createdby = u.id'),
                  array('priveledge pv', 'u.priveledge = pv.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => array('us.usulan' => $anomali)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataUsulan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Usulan";
  $data['title_content'] = 'Detail Usulan';
  $data['detail_alat'] = $this->getDataDetailAlat($id);
  $data['approval'] = $this->getDataApproval($id);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function showDataAlat() {
  $index_tr = $this->input->post('index_tr');
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $content = $this->getDataAlat(3000, 0);
  $data['detail_content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/seekPagination/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['index_tr'] = $index_tr;
  echo $this->load->view('detail_alat', $data, true);
 }

 public function seekPagination($offset) {
  $this->segment = 3;
  $offset = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $offset * $this->limit;

  $content = $this->getDataAlat($this->limit, $this->last_no);
  $data['detail_content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/seekPagination/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo $this->load->view('table_alat', $data, true);
 }

 public function getTotalDataAlat() {
  $upt = $this->session->userdata('upt_id');
  $like = array();
  $total = Modules::run('database/count_all', array(
              'table' => 'alat aa',
              'field' => array('aa.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt'),
              'join' => array(
                  array('nama_alat na', 'aa.alat = na.id and aa.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id and aa.deleted = 0'),
                  array('tahun t', 'aa.tahun = t.id and aa.deleted = 0'),
                  array('upt ut', 'aa.upt = ut.id and aa.deleted = 0'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'aa.id = alat_anomali.alat and aa.deleted = 0', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "aa.upt = '" . $upt . "' and (alat_anomali.status_alat = 1 or alat_anomali.status_alat is null)"
  ));

  return $total;
 }

 public function getDataAlat($limit = 1, $offset = 0) {
  $upt = $this->session->userdata('upt_id');

  $like = array();
  $data = Modules::run('database/get', array(
              'table' => 'alat aa',
              'field' => array('aa.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                  'alat_anomali.status_alat AS status_alat', 'na.nama_alat'),
              'join' => array(
                  array('nama_alat na', 'aa.alat = na.id and aa.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id and aa.deleted = 0'),
                  array('tahun t', 'aa.tahun = t.id and aa.deleted = 0', 'left'),
                  array('upt ut', 'aa.upt = ut.id and aa.deleted = 0'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'aa.id = alat_anomali.alat and aa.deleted = 0', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $limit,
              'offset' => $offset,
              'where' => "aa.upt = '" . $upt . "' and (alat_anomali.status_alat = 1 or alat_anomali.status_alat is null)"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAlat()
  );
 }

 public function getPostDataHeader($value) {
  $data['no_usulan'] = Modules::run('no_generator/generateNomerUsulan');
  $data['tanggal'] = $value->tanggal;
  $data['upt'] = $this->upt_id;
  return $data;
 }

 public function getPostUsulanAlatDetail($usulan, $value) {
  $data['usulan'] = $usulan;
  $data['kategori_alat'] = $value->kategori;
  $data['nama_alat'] = $value->nama_alat;
  ;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $usulan = $id;
  $this->db->trans_begin();
  try {
   $post_usulan = $this->getPostDataHeader($data);
   if ($id == '') {
    $usulan = Modules::run('database/_insert', $this->getTableName(), $post_usulan);

    if (!empty($data->detail_item)) {
     foreach ($data->detail_item as $value) {
      $post_usulan_detail = $this->getPostUsulanAlatDetail($usulan, $value);
      Modules::run('database/_insert', 'usulan_alat', $post_usulan_detail);
     }
    }

    $post_status = array();
    $post_status['status'] = 'DRAFT';
    $post_status['usulan'] = $usulan;
    Modules::run('database/_insert', 'usulan_status', $post_status);
   } else {
    //update
    unset($post_usulan['no_usulan']);
    Modules::run('database/_update', $this->getTableName(), $post_usulan, array('id' => $id));

    if (!empty($data->detail_item_ubah)) {
     foreach ($data->detail_item_ubah as $value) {
      if (isset($value->id_alat)) {
       $post_usulan_detail = $this->getPostUsulanAlatDetail($id, $value);
       Modules::run('database/_update', 'usulan_alat', $post_usulan_detail, array('id' => $value->id));
      }
     }
    }

    if (!empty($data->detail_item_hapus)) {
     foreach ($data->detail_item_hapus as $value) {
      Modules::run('database/_delete', "usulan_alat", array('id' => $value->id));
     }
    }


    if (!empty($data->detail_item)) {
     foreach ($data->detail_item as $value) {
      if (!isset($value->id) || $value->id == '') {
       if (isset($value->id_alat)) {
        $post_usulan_detail = $this->getPostUsulanAlatDetail($usulan, $value);
        Modules::run('database/_insert', 'usulan_alat', $post_usulan_detail);
       }
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'usulan' => $usulan));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Usulan";
  $data['title_content'] = 'Data Usulan';
  $content = $this->getDataUsulan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function ubahUsulan($id) {
  $data['list_status'] = $this->getListStatus();
//  $data['id'] = $id;
  echo $this->load->view('tr_detail_alat', $data, true);
 }

 public function execApprove() {
  $status = $_POST['status'];
  $id = $_POST['id'];
  $ket = $_POST['ket'];

  $post_status['usulan'] = $id;
  if ($status == '1') {
   $post_status['status'] = 'APPROVE';
  } else {
   $post_status['status'] = 'REJECT';
   if ($ket != '') {
    $post_status['keterangan'] = $ket;
   }
  }

  $status = Modules::run('database/_insert', 'usulan_status', $post_status);

  echo json_encode(array('is_valid' => true, 'id' => $status));
 }

}
