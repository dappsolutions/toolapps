<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <div class="row">
      <div class='col-md-3'>
       No. Usulan
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $no_usulan ?>
      </div>     
     </div>
     <br/>
     <div class="row">
      <div class='col-md-3'>
       Tanggal Usulan
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo date('d F Y', strtotime($tanggal)) ?>
      </div>     
     </div>
     <br/>     
    </div>
   </div>
  </div>
 </div>

 <br/>
 <div class='row'>
  <div class='col-md-12'>
   <div class='white-box'>
    <div class='card-body'>
     <div class="row">
      <div class='col-md-12'>
       <div class='table-responsive'>
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr>
           <th>Nama Alat</th>
           <th>Kategori Alat</th>
          </tr>
         </thead>
         <tbody class="table_anomali">
          <?php if (!empty($detail_alat)) { ?>
           <?php foreach ($detail_alat as $value) { ?>
            <tr>
             <td class='font-12'><?php echo $value['nama_alat'] ?></td>
             <td class='font-12'><b><?php echo $value['kategori'] ?></b></td>
            </tr>
           <?php } ?>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>
     </div>
     <hr/>

     <?php if (!empty($approval)) { ?>
      <div class="row">       
       <div class="col-md-5">
        <h4>Alasan Ditolak</h4>
        <textarea class="form-control" readonly=""><?php echo $approval[count($approval) - 1]['keterangan']; ?></textarea>
       </div>
      </div>
     <br/>
     <br/>
     <?php } ?>
     <?php if (!empty($approval)) { ?>
      <div class='row'>
       <?php foreach ($approval as $value) { ?>
        <div class='col-md-3 text-center ' style="font-weight: bold;">
         <p><?php echo $value['status'] == 'DRAFT' ? 'Dibuat' : 'Divalidasi' ?> Oleh, <?php echo $value['hak_akses'] ?></p>
         <br/>
         <br/>
         <br/>
         <p><?php echo $value['nama_pegawai'] ?></p>
         <p><?php echo date('d F Y', strtotime($value['createddate'])) ?></p>
        </div>
       <?php } ?>      
      </div>
     <?php } ?>

     <div class='row'>
      <div class='col-md-12 text-right'>
       <?php if (!empty($approval)) { ?>
        <?php $status = $approval[count($approval) - 1]['status']; ?>
        <?php if ($status == 'DRAFT') { ?>
         <button id="" class="btn btn-success" onclick="Usulan.approve()">Approve</button>
         &nbsp;
         <button id="" class="btn btn-danger" onclick="Usulan.reject()">Reject</button>
        <?php } ?>
       <?php } ?>
       <button id="" class="btn" onclick="Usulan.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
