<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <div class="row">
      <div class='col-md-3'>
       Tanggal Usulan
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='tanggal' class='form-control required' 
              value='<?php echo isset($tanggal) ? $tanggal : '' ?>' error="Tanggal Usulan"/>
      </div>     
     </div>
     <br/>     
    </div>
   </div>
  </div>
 </div>

 <div class='row'>
  <div class='col-md-12'>
   <div class='white-box'>
    <div class='card-body'>
     <div class="row">
      <div class='col-md-3'>
       <u>Alat yang diusulkan</u>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-12'>
       <div class='table-responsive'>
        <?php if (!isset($detail_alat)) { ?>
         <table class="table color-bordered-table primary-bordered-table">
          <thead>
           <tr class="bg-success text-white">
            <th>Nama Alat</th>
            <th>Kategori</th>
            <th>&nbsp;</th>
           </tr>
          </thead>
          <tbody class="table_usulan">
           <tr id="">
            <td class='font-12'>
             <input type="text" value="" id="nama_alat" class="form-control required"/>
            </td>
            <td class='font-12'>
             <select id="kategori" error="Status" class="form-control required">
              <?php if (!empty($list_kategori)) { ?>
               <?php foreach ($list_kategori as $value) { ?>
                <option value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </td>            
            <td class="text-center">
             <i class="mdi mdi-plus mdi-24px hover" onclick="Usulan.addDetail(this)"></i>
            </td>
           </tr>
          </tbody>
         </table>
        <?php } else { ?>
         <table class="table color-bordered-table primary-bordered-table">
          <thead>
           <tr class="bg-success text-white">
            <th>Nama Alat</th>
            <th>Kategori</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody class="table_usulan">
           <?php if (!empty($detail_alat)) { ?>
            <?php foreach ($detail_alat as $value) { ?>
             <tr id="<?php echo $value['id'] ?>">
              <td class='font-12'><?php echo $value['nama_alat'] ?></td>
              <td class='font-12'><?php echo $value['kategori'] ?></td>
              <td class='font-12'>
               <label class="label label-danger hover" onclick="Usulan.hapusUsulan(this)">Hapus</label>
               &nbsp;
               <label class="label label-success hover" onclick="Usulan.ubahUsulan(this)">Ubah</label>
              </td>
             </tr>
            <?php } ?>
            <tr id="">
             <td>
              <input type="text" value="" id="nama_alat" class="form-control required"/>
             </td>
             <td>
              <select id="kategori" error="Status" class="form-control required">
              <?php if (!empty($list_kategori)) { ?>
               <?php foreach ($list_kategori as $value) { ?>
                <option value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
             </td>
             <td class="text-center">
              <i class="mdi mdi-plus mdi-24px hover" onclick="Usulan.addDetail(this)"></i>
             </td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        <?php } ?>
       </div>
      </div>
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Usulan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="Usulan.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
