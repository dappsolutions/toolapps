<div class='container-fluid'>

 <div class='row'>
  <div class='col-12'>
   <div class="white-box">  
    <div class="card-body">
     <h4 class="card-title"><u><?php echo $title ?></u></h4>

     <div class='row'>
      <div class='col-md-2'>
       <button id="" class="btn btn-block btn-warning" onclick="Usulan.add()">Tambah</button>
      </div>
      <div class='col-md-10'>
       <input type='text' name='' id='' onkeyup="Usulan.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
      </div>     
     </div>        
     <br/>
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div>
    </div>
   </div> 
  </div>
 </div>

 <div class='row'>
  <div class='col-12'>
   <div class='white-box'>
    <div class='card-body'>
     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr class="">
           <th class="font-12">No</th>
           <th class="font-12">No Usulan</th>
           <th class="font-12">Tanggal Usulan</th>
           <th class="font-12">Status</th>
           <th class="font-12">Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr>
             <td class='font-12'><?php echo $no++ ?></td>
             <td class='font-12'><?php echo '<b>' . $value['no_usulan'] . '</b>' ?></td>
             <td class='font-12'><?php echo date('d F Y', strtotime($value['tanggal'])) ?></td>
             <td class='font-12'>
              <?php if ($value['status'] == 'APPROVE') { ?>
               <label id="" class="label label-success font-10"><?php echo $value['status'] ?></label>
              <?php } ?>
              <?php if ($value['status'] == 'DRAFT') { ?>
               <label id="" class="label label-warning font-10"><?php echo $value['status'] ?></label>
              <?php } ?>
              <?php if ($value['status'] == 'REJECT') { ?>
               <label id="" class="label label-danger font-10"><?php echo $value['status'] ?></label>
              <?php } ?>
             </td>
             <td class="text-center">
              <?php if ($value['status'] == 'DRAFT') { ?>
               <label id="" class="label label-warning font-10" 
                      onclick="Usulan.ubah('<?php echo $value['id'] ?>')">Ubah</label>
                     <?php } ?>
              &nbsp;
              <label id="" class="label label-success font-10" 
                     onclick="Usulan.detail('<?php echo $value['id'] ?>')">Detail</label>
              &nbsp;
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td class="text-center" colspan="5">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>
     </div>    
     <br/>
     <div class="row">
      <div class="col-md-12">
       <div class="pagination">
        <?php echo $pagination['links'] ?>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>