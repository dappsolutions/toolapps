<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class="col-md-12 text-right">
       <?php
       if ($status_dokumen == 'DRAFT') {
        $status_color = 'btn-warning';
       }
       if ($status_dokumen == 'REJECT') {
        $status_color = 'btn-danger';
       }

       if ($status_dokumen == 'APPROVE' || $status_dokumen == 'COMPLETE' || $status_dokumen == 'BACK') {
        $status_color = 'btn-success';
       }
       ?>
       <button class="btn <?php echo $status_color ?>"><?php echo $status_dokumen ?></button>
      </div>
     </div>
     <div class="row">
      <div class='col-md-3'>
       No Pinjaman
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $no_peminjaman ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tanggal Peminjaman
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $tanggal_pinjam ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $keterangan ?>
      </div>     
     </div>
     <br/>
     <br/>


     <u>Data Alat</u>
     <hr/>

     <div class='row'>
      <div class='col-md-3'>
       <label class="label label-success">Alat Dipinjam</label>
      </div>      
     </div>
     <br/>

     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table" id='data_alat'>
         <thead>
          <tr class="bg-primary text-white">
           <th class="font-12">No</th>
           <th class="font-12">Kode Alat</th>
           <th class="font-12">UPT Pemilik Alat</th>
           <th class="font-12">Jumlah</th>
           <th class="font-12">Status Alat</th>
          </tr>
         </thead>
         <tbody>
          <?php if (isset($list_alat)) { ?>
           <?php if (!empty($list_alat)) { ?>
            <?php $no = 1; ?>
            <?php foreach ($list_alat as $value) { ?>
             <tr id='<?php echo $value['id'] ?>'>
              <td class="font-12"><?php echo $no++ ?></td>
              <td class="font-12"><b><?php echo $value['kode_alat'] ?></b></td>
              <td class="font-12"><?php echo $value['upt'] ?></td>
              <td class="font-12"><?php echo $value['qty'] ?></td>
              <td>
               <label class="label label-success font-10"><?php echo $value['status_alat'] ?></label>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td class="text-center font-12" colspan="6">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>         
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>      
     </div>
     <br/>
     <br/>

     <div class='row'>
      <div class='col-md-3'>
       <label class="label label-success">Surat Peminjaman (pdf, png, jpg) </label>
      </div>      
     </div>
     <br/>

     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table" id='data_surat'>
         <thead>
          <tr class="bg-primary text-white">
           <th class="font-12">UPT Pemilik Alat</th>
           <th class="font-12">Surat Pinjaman</th>
           <th class="font-12">Status Pinjaman</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($list_surat)) { ?>
           <?php foreach ($list_surat as $value) { ?>
            <tr>
             <td class="font-12">
              <?php echo $value['upt'] ?>
             </td>
             <td class="font-12">
              <a class="label label-danger" href="" onclick="HistoriInternal.showDokumen(this, event)"><?php echo $value['file'] ?></a>
             </td>
             <td class="font-12">
              <label class="label label-success"><?php echo $value['status'] ?></label>
             </td>
            </tr>
           <?php } ?>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>      
     </div>
     <br/>
     <hr/>

     <div class="row">
      <?php if (!empty($status_approve)) { ?>
       <?php foreach ($status_approve as $value) { ?>
        <div class="col-md-3">        
         <div class="row">
          <div class="col-md-12 text-center">
           <?php echo 'Di' . strtolower($value['status']) . ' oleh, ' ?>           
          </div>          
          <div class="col-md-12 text-center" style="margin-top: 100px;">
           <?php echo $value['pegawai'] ?>
          </div>
          <div class="col-md-12 text-center">
           ASMAN <?php echo $value['upt'] ?>
          </div>
          <div class="col-md-12 text-center">
           <?php echo $value['tgl_approve'] ?>
          </div>
          <?php if ($value['keterangan'] != '') { ?>
           <div class="col-md-12 text-center text-danger">
            <?php echo 'Alasan (' . $value['keterangan'] . ')' ?>
           </div>
          <?php } ?>
         </div>
        </div>
       <?php } ?>
      <?php } ?>
     </div>
     <hr/>          
     <div class='row'>
      <div class='col-md-12 text-right'>
       <?php if (empty($status_approve)) { ?>
        <?php if ($this->session->userdata('hak_akses') == 'ASMAN') { ?>
         <button id="" class="btn btn-success" onclick="HistoriInternal.approveAsman('APPROVE')">Approve</button>
         <button id="" class="btn btn-danger" onclick="HistoriInternal.rejectAsman()">Reject</button>
        <?php } ?>        
       <?php } ?>       
       <button id="" class="btn btn-default" onclick="HistoriInternal.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
