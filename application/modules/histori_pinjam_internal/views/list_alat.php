<div class="row">
 <div class="col-md-3">
  Pencarian
 </div>
 <div class="col-md-9">
  <input type="text" value="" placeholder="Kode Alat" id="" class="form-control" onkeyup="PinjamInternal.cariAlat(this, event)"/>
 </div>
</div>
<br/>
<div class='row'>
 <div class='col-md-12'>
  <div class="table-responsive">
   <table class="table table-bordered table-striped" id='table_alat'>
    <thead>
     <tr class="bg-primary text-white">
      <th class="">No</th>
      <th class="">Kode Alat</th>
      <th class="">Stok</th>
      <th class="">Jumlah</th>
      <th class="">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_alat)) { ?>
      <?php if (!empty($list_alat)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($list_alat as $value) { ?>
        <tr id='<?php echo $value['id'] ?>' upt='<?php echo $value['upt'] ?>'>
         <td><?php echo $no++ ?></td>
         <td><?php echo $value['kode_alat'] ?></td>         
         <td>
          <?php if ($value['stock'] > 0) { ?>
           <label class="badge badge-success"><?php echo $value['stock'] ?></label>
          <?php } else { ?> 
           <label class="badge badge-danger"><?php echo $value['stock'] ?></label>
          <?php } ?>
         </td>
         <td>
          <?php if ($value['stock'] > 0) { ?>
           <input type="text" value="0" id="qty" class="form-control text-right" onkeyup="PinjamInternal.checkValidInput(this, event)"/>
          <?php } ?>
         </td>
         <td>
          <?php if ($value['stock'] > 0) { ?>
           <input type="checkbox" value="" id="check" class="check_input" />
          <?php } ?>
         </td>
        </tr>
       <?php } ?>
      <?php } else { ?>
       <tr>
        <td class="text-center" colspan="6">Tidak Ada Data Ditemukan</td>
       </tr>
      <?php } ?>         
     <?php } ?>         
    </tbody>
   </table>
  </div>
 </div>      
</div>
<hr/>

<div class="row">
 <div class="col-md-12 text-right">
  <button class="btn btn-success" onclick="PinjamInternal.prosesAmbilAlat()">Proses</button>
 </div>
</div>