<?php

class Histori_pinjam_internal extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;
 public $hak_akses;
 public $user;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt = $this->session->userdata('upt_id');
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->user = $this->session->userdata('user_id');
 }

 public function getModuleName() {
  return 'histori_pinjam_internal';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<link rel="stylesheet" href="' . base_url() . 'assets/css/stickyTable.min.css">',
      '<script src="' . base_url() . 'assets/js/stickyTable.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/helper.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/histori_pinjam_internal.js"></script>',
  );

  return $data;
 }

 public function getTableName() {
  return 'pinjaman';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Histori Peminjaman Internal";
  $data['title_content'] = 'Data Histori Peminjaman Internal';
  $content = $this->getDataPeminjaman();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPeminjaman($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_peminjaman', $keyword),
       array('p.tanggal_pinjam', $keyword),
   );
  }

  $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
      'phs.status', 'asp.status as status_approve');
  $where = "p.deleted = 0 and p.status_dokumen = 1 and phs.status != 'DRAFT'";
  $join = array(
      array('user u', 'p.user = u.id'),
      array('pegawai pg', 'u.pegawai = pg.id'),
      array('upt ut', 'pg.upt = ut.id'),
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id'),
      array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
  );
  $join = array(
      array('user u', 'p.user = u.id'),
      array('pegawai pg', 'u.pegawai = pg.id'),
      array('upt ut', 'pg.upt = ut.id'),
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id')
  );
  if ($this->hak_akses == 'ASMAN') {
   $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
       'asp.status');
   $where = "(ut.id = '" . $this->upt . "' and asp.id is not null and p.status_dokumen = 1 and p.deleted = 0) or "
           . "(ut.id = '" . $this->upt . "' and phs.status = 'COMPLETE' and p.status_dokumen = 1 and p.deleted = 0)";
   $join = array(
       array('user u', 'p.user = u.id'),
       array('pegawai pg', 'u.pegawai = pg.id'),
       array('upt ut', 'pg.upt = ut.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
       array('pinjaman_has_status phs', 'sd.id = phs.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
       array('pinjaman_has_alat pha', 'phh.id = pha.id'),
       array('alat a', 'pha.alat = a.id'),
       array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
       array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $this->upt . "' "),
   );
  }

  if ($this->hak_akses == 'TOOLSMAN') {
   $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
       'phs.status');
   $where = "ut.id = '" . $this->upt . "' and phs.status = 'BACK' and p.status_dokumen = 1";
   $join = array(
       array('user u', 'p.user = u.id'),
       array('pegawai pg', 'u.pegawai = pg.id'),
       array('upt ut', 'pg.upt = ut.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
       array('pinjaman_has_status phs', 'sd.id = phs.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
       array('pinjaman_has_alat pha', 'phh.id = pha.id'),
       array('alat a', 'pha.alat = a.id'),
       array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
       array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $this->upt . "' "),
   );
  }

  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'pg.nama as nama_pegawai', 'ut.upt', 'phs.status'),
              'join' => $join,
              'like' => $like,
              'is_or_like' => true,
              'where' => $where
  ));

  return $total;
 }

 public function getDataPeminjaman($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_peminjaman', $keyword),
       array('p.tanggal_pinjam', $keyword),
   );
  }

  $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
      'phs.status', 'asp.status as status_approve');
  $where = "p.deleted = 0 and p.status_dokumen = 1 and phs.status != 'DRAFT'";
  $join = array(
      array('user u', 'p.user = u.id'),
      array('pegawai pg', 'u.pegawai = pg.id'),
      array('upt ut', 'pg.upt = ut.id'),
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id'),
      array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
  );
  if ($this->hak_akses == 'ASMAN') {
   $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
       'asp.status', 'phs.status as status_doc', 'asp.status as status_approve');
   $where = "(ut.id = '" . $this->upt . "' and asp.id is not null and p.status_dokumen = 1 and p.deleted = 0) or "
           . "(ut.id = '" . $this->upt . "' and phs.status = 'COMPLETE' and p.status_dokumen = 1 and p.deleted = 0)";
   $join = array(
       array('user u', 'p.user = u.id'),
       array('pegawai pg', 'u.pegawai = pg.id'),
       array('upt ut', 'pg.upt = ut.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
       array('pinjaman_has_status phs', 'sd.id = phs.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
       array('pinjaman_has_alat pha', 'phh.id = pha.id'),
       array('alat a', 'pha.alat = a.id'),
       array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
       array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $this->upt . "' "),
   );
  }

  if ($this->hak_akses == 'TOOLSMAN') {
   $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
       'phs.status', 'asp.status as status_approve');
   $where = "ut_alat.id = '" . $this->upt . "' and phs.status = 'BACK' and p.status_dokumen = 1";
   $join = array(
       array('user u', 'p.user = u.id'),
       array('pegawai pg', 'u.pegawai = pg.id'),
       array('upt ut', 'pg.upt = ut.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
       array('pinjaman_has_status phs', 'sd.id = phs.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
       array('pinjaman_has_alat pha', 'phh.id = pha.id'),
       array('alat a', 'pha.alat = a.id'),
       array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
       array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $this->upt . "' "),
   );
  }


  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => $field,
              'join' => $join,
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where,
              'orderby'=> 'p.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['status'] == '') {
     $value['status'] = $value['status_doc'];
    }
    
    if($value['status_approve'] == 'REJECT'){
     $value['status'] = $value['status_approve'];
    }
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPeminjaman($keyword)
  );
 }

 public function getDetailDataPeminjaman($id) {
  $field = array('kr.*', 'phs.status as status_dokumen', 'ut.upt as upt_peminjam');
  $join = array(
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = kr.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id'),
      array('user us', 'us.id = kr.user'),
      array('pegawai pg', 'pg.id = us.pegawai'),
      array('upt ut', 'ut.id = pg.upt'),
  );

  if ($this->hak_akses == 'ASMAN') {
   $field = array('kr.*', 'asp.status as status_dokumen', 'ut.upt as upt_peminjam');
   $join = array(
       array('approver_status_pinjam asp', 'asp.pinjaman = kr.id', 'left'),
       array('user us', 'us.id = kr.user', 'left'),
       array('pegawai pg', 'pg.id = us.pegawai', 'left'),
       array('upt ut', 'ut.id = pg.upt', 'left'),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => $field,
              'join' => $join,
              'where' => "kr.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   if ($result['status_dokumen'] == '') {
    $result['status_dokumen'] = 'DRAFT';
   }
  }


  return $result;
 }

 public function getListStatusPeminjaman() {
  $data = Modules::run('database/get', array(
              'table' => 'status_pinjaman',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function make() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Buat Histori Peminjaman Internal";
  $data['title_content'] = 'Buat Histori Peminjaman Internal';
  $data['list_status'] = $this->getListStatusPeminjaman();
  $data['list_upt'] = $this->getListUpt();

  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPeminjaman($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Histori Peminjaman Internal";
  $data['title_content'] = 'Ubah Histori Peminjaman Internal';
  $data['list_status'] = $this->getListStatusPeminjaman();
  echo Modules::run('template', $data);
 }

 public function getListAlatDipinjam($pinjaman) {
  $join = array(
      array('alat a', 'pha.alat = a.id'),
      array('upt ut', 'a.upt = ut.id'),
      array('status_pinjam sp', 'pha.id = sp.pinjaman_has_alat'),
  );

  if ($this->hak_akses == 'ASMAN') {
   $join = array(
       array('alat a', 'pha.alat = a.id'),
       array('upt ut', "a.upt = ut.id and ut.id = '" . $this->upt . "'"),
       array('status_pinjam sp', 'pha.id = sp.pinjaman_has_alat'),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'pinjaman_has_alat pha',
              'field' => array('pha.*', 'a.kode_alat', 'ut.upt',
                  'sp.status_alat', 'a.merk', 'a.tipe', 'a.nomer_seri'),
              'join' => $join,
              'where' => "pha.deleted = 0 and pha.pinjaman = '" . $pinjaman . "'"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSuratPinjaman($pinjaman) {
  $join = array(
      array('upt ut', 'sp.upt = ut.id'),
      array('status_pinjaman spm', 'sp.status_pinjaman = spm.id'),
  );

  if ($this->hak_akses == 'ASMAN') {
   $join = array(
       array('upt ut', "sp.upt = ut.id and ut.id = '" . $this->upt . "'"),
       array('status_pinjaman spm', 'sp.status_pinjaman = spm.id'),
   );
  }

  $data = Modules::run('database/get', array(
              'table' => 'surat_pinjaman sp',
              'field' => array('sp.*', 'ut.upt', 'spm.status'),
              'join' => $join,
              'where' => "sp.deleted = 0 and sp.pinjaman = '" . $pinjaman . "'"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataApproveAsman($pinjaman) {
  $where = array('pinjaman' => $pinjaman);
  if ($this->hak_akses == 'ASMAN') {
   $where = array('asp.pinjaman' => $pinjaman, 'asp.upt' => $this->upt, 'tp.deleted' => 0);
  }

  $data = Modules::run('database/get', array(
              'table' => 'approver_status_pinjam asp',
              'field' => array('asp.*', 'ut.upt', 'p.nama as pegawai',
                  'asp.createddate as tgl_approve', 'tp.file', 'tp.signature'),
              'join' => array(
                  array('user u', 'asp.user = u.id'),
                  array('pegawai p', 'u.pegawai = p.id'),
                  array('upt ut', 'p.upt = ut.id'),
                  array('ttd_pegawai tp', 'tp.pegawai = p.id and tp.deleted = 0', 'left'),
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tgl_approve'] = Modules::run('helper/getIndoDate', $value['tgl_approve']);
    if ($value['file'] != "") {
     $value['file'] = str_replace(' ', '_', $value['file']);
     $value['file'] = base_url() . 'files/berkas/ttd_pegawai/' . $value['file'];
    }
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataPeminjaman($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Histori Peminjaman Internal";
  $data['title_content'] = 'Detail Histori Peminjaman Internal';
  $data['list_alat'] = $this->getListAlatDipinjam($id);
  $data['list_surat'] = $this->getListSuratPinjaman($id);
  $data['status_approve'] = $this->getDataApproveAsman($id);

  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['no_peminjaman'] = Modules::run('no_generator/generateNoPeminjaman', 'internal');
  $data['tanggal_pinjam'] = $value->tanggal_pinjam . ' ' . date('H:i:s');
  $data['user'] = $this->session->userdata('user_id');
  $data['status_dokumen'] = 1;
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function getPostDataAlat($value, $pinjaman) {
  $data['pinjaman'] = $pinjaman;
  $data['alat'] = $value->alat;
  $data['qty'] = $value->qty;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;

  $message = "";
  $this->db->trans_begin();
  try {
   $post_pinjaman = $this->getPostDataHeader($data);
   $is_uploaded = true;
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_pinjaman);

    //pinjaman has alat
    foreach ($data->alat as $value) {
     $post_alat = $this->getPostDataAlat($value, $id);
     $pinjaman_has_alat = Modules::run('database/_insert', 'pinjaman_has_alat', $post_alat);

     //pinjaman has alat status
     $post_status_alat['pinjaman_has_alat'] = $pinjaman_has_alat;
     $post_status_alat['status'] = 'NORMAL';
     $post_status_alat['status_alat'] = 'NOT TAKEN';
     Modules::run('database/_insert', 'status_pinjam', $post_status_alat);
    }

    //surat pinjaman
    $index = 0;
    foreach ($data->surat as $value) {
     if (isset($_FILES['file' . $index])) {
      $data_file = $this->uploadData("file" . $index);
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post_surat['file'] = $_FILES['file' . $index]['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }
     $post_surat['upt'] = $value->upt;
     $post_surat['pinjaman'] = $id;
     Modules::run('database/_insert', 'surat_pinjaman', $post_surat);
     $index += 1;
    }

    //pinjaman has status
    $post_status['pinjaman'] = $id;
    $post_status['status'] = 'DRAFT';
    Modules::run('database/_insert', 'pinjaman_has_status', $post_status);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Histori Peminjaman Internal";
  $data['title_content'] = 'Data Histori Peminjaman Internal';
  $content = $this->getDataPeminjaman($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getListalat($upt) {
  $data = $data = Modules::run('database/get', array(
              'table' => 'alat a',
              'field' => array('a.*', 'sa.status', 'sm.stock', 'a.upt'),
              'join' => array(
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
      FROM anomali_alat_detail aad 
      JOIN anomali_alat aa ON aad.anomali_alat = aa.id
      WHERE aa.validation != 0
      GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'a.id = alat_anomali.alat and a.deleted = 0', 'left'),
                  array('status_alat sa', 'alat_anomali.status_alat = sa.id', 'left'),
                  array('stock_masuk sm', 'a.id = sm.alat', 'left'),
              ),
              'where' => "(sa.status is null or (sa.status != 'Rusak')) and a.upt = '" . $upt . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['stock'] = $value['stock'] == '' ? 0 : $value['stock'];
    array_push($result, $value);
   }
  }



  return $result;
 }

 public function getDataAlat() {
  $upt = $this->input->post('upt');
  $data['list_alat'] = $this->getListalat($upt);
  echo $this->load->view('list_alat', $data, true);
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/surat_pinjaman/';
  $config['allowed_types'] = 'png|jpg|pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function updateStatusDokumenPengajuan($pinjaman) {
  $data = Modules::run('database/get', array(
              'table' => 'pinjaman_has_alat pha',
              'field' => array('distinct(ut.id) as upt', 'asp.id as asp_id'),
              'join' => array(
                  array('alat a', 'pha.alat = a.id'),
                  array('upt ut', 'a.upt = ut.id'),
                  array('approver_status_pinjam asp', 'pha.pinjaman = asp.pinjaman', 'left'),
              ),
              'where' => "pha.deleted = 0 and pha.pinjaman = '" . $pinjaman . "'"
  ));

  $total = !empty($data) ? count($data->result_array()) : 0;

  $total_approve = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['asp_id'] != '') {
     $total_approve += 1;
    }
   }
  }

  if ($total_approve != 0) {
   if ($total_approve == $total) {
    $post['pinjaman'] = $pinjaman;
    $post['status'] = 'COMPLETE';
    Modules::run('database/_insert', 'pinjaman_has_status', $post);
   }
  }
 }

 public function approveAsman() {
  $id = $this->input->post('id');
  $status = $this->input->post('status');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_data['pinjaman'] = $id;
   $post_data['upt'] = $this->upt;
   $post_data['user'] = $this->user;
   $post_data['status'] = $status;
   if (isset($_POST['keterangan'])) {
    $post_data['keterangan'] = $this->input->post('keterangan');
   }
   Modules::run('database/_insert', "approver_status_pinjam", $post_data);

   $this->updateStatusDokumenPengajuan($id);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function showDokumen() {
  $file = $this->input->post('file');
  $file = str_replace(' ', '_', $file);

  $data['file'] = $file;
  echo $this->load->view('detail_file', $data, true);
 }

 public function cetak($id = '') {
  $mpdf = Modules::run('mpdf/getInitPdf');
  $data = $this->getDetailDataPeminjaman($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Histori Peminjaman Internal";
  $data['title_content'] = 'Detail Histori Peminjaman Internal';
  $data['list_alat'] = $this->getListAlatDipinjam($id);
  $data['list_surat'] = $this->getListSuratPinjaman($id);
  $data['status_approve'] = $this->getDataApproveAsman($id);

  //  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output($data['no_peminjaman'] . '.pdf', 'I');
 }

}
