<?php

class Dashboard extends MX_Controller {

 public $akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");

  $this->akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/validasi_anomali.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'new_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['list_upt'] = $this->getListUpt();
  $data['total_alat_keluar'] = $this->getTotalDataAlatKeluar();
  $data['total_alat_kembali'] = $this->getTotalDataAlatKembali();
  $data['total_alat_rusak'] = $this->getTotalAlatRusak();
  $data['total_alat_normal'] = $this->getTotalAlatNormal();
//  echo '<pre>';
//  print_r($data);die;
  $json = $this->getDataJsonTotalGrafik($data);
  $data['data_json'] = $json['json'];
  $data['max_json'] = $json['max'];

  $data['total_alat_dikalibrasi'] = $this->getTotalAlatDikalibrasi();
  $data['total_alat_diservis'] = $this->getTotalAlatDiservis();
//  echo '<pre>';
//  print_r($data);
//  die;
  $data['total_alat'] = $this->getTotalAlat();
  $data['total_anomali'] = $this->getTotalAnomali();
  $data['total_alat_kategori'] = $this->getTotalAlatKategori();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getTotalAlatKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_alat ka',
              'field' => array('ka.*'),
              'where' => "ka.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $kategori_id = $value['id'];
    $total = $this->getCountAlatKategori($kategori_id);
    $value['total'] = $total;
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getCountAlatKategori($kategori_id) {
  $where = "na.deleted = 0 and na.kategori_alat = '" . $kategori_id . "'";
  if ($this->akses != 'Superadmin') {
   $where = "na.deleted = 0 and na.kategori_alat = '" . $kategori_id . "' and a.upt = '".$this->upt."'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'nama_alat na',
              'field' => array('na.*'),
              'join' => array(
                  array('alat a', 'na.id = a.alat')
              ),
              'where' => $where
  ));

  return $total;
 }

 public function getTotalDataGrafikAlat() {
  $upt = $this->input->post('upt');

  $data['total_alat_keluar'] = $this->getTotalDataAlatKeluar($upt);
  $data['total_alat_kembali'] = $this->getTotalDataAlatKembali($upt);
  $data['total_alat_rusak'] = $this->getTotalAlatRusak($upt);
  $data['total_alat_normal'] = $this->getTotalAlatNormal($upt);

  $json = $this->getDataJsonTotalGrafik($data);
  $data['data_json'] = $json['json'];
  $data['max_json'] = $json['max'];

  $data['total_alat_dikalibrasi'] = $this->getTotalAlatDikalibrasi($upt);
  $data['total_alat_diservis'] = $this->getTotalAlatDiservis($upt);

  echo json_encode($data);
 }

 public function getTotalAlatDikalibrasi($upt = "") {
  $where = array('ska.deleted' => 0);
  if ($this->akses == 'Superadmin') {
   if ($upt != '') {
    $where = "ut.id = '" . $upt . "' and ska.deleted = 0";
   }
  } else {
   if ($upt != '') {
    $where = "ut.id = '" . $upt . "' and ska.deleted = 0";
   } else {
    $where = "ut.id = '" . $this->upt . "' and ska.deleted = 0";
   }
  }
  $total = Modules::run("database/count_all", array(
              'table' => "sertifikasi_kalibrasi_alat ska",
              'join' => array(
                  array('alat a', 'ska.alat = a.id and a.deleted = 0'),
                  array('upt ut', 'a.upt = ut.id and ut.deleted = 0'),
              ),
              'where' => $where
  ));

  return $total;
 }

 public function getTotalAlatDiservis($upt = '') {
  $where = array('sp.deleted' => 0);
  if ($this->akses == 'Superadmin') {
   if ($upt != "") {
    $where = "sp.deleted = 0 and ut.id = '" . $upt . "'";
   }
  } else {
   if ($upt != "") {
    $where = "sp.deleted = 0 and ut.id = '" . $upt . "'";
   } else {
    $where = "sp.deleted = 0 and ut.id = '" . $this->upt . "'";
   }
  }
  $total = Modules::run("database/count_all", array(
              'table' => "servis_peralatan sp",
              'join' => array(
                  array('alat a', 'sp.alat = a.id and a.deleted = 0'),
                  array('upt ut', 'a.upt = ut.id and ut.deleted = 0'),
              ),
              'where' => $where,
  ));

  return $total;
 }

 public function getDataJsonTotalGrafik($data) {
  $result = array();

  $total = array();
  $json_rusak['jumlah'] = $data['total_alat_rusak'];
  array_push($total, $data['total_alat_rusak']);
  $json_rusak['alat'] = 'Alat Rusak';
  array_push($result, $json_rusak);

  $json_normal['jumlah_normal'] = $data['total_alat_normal'];
  array_push($total, $data['total_alat_normal']);
  $json_normal['alat'] = 'Alat Normal';
  array_push($result, $json_normal);

  $json_keluar['jumlah_pengajuan'] = $data['total_alat_keluar'];
  array_push($total, $data['total_alat_keluar']);
  $json_keluar['alat'] = 'Alat Keluar';
  array_push($result, $json_keluar);

  $json_kembali['jumlah_kembali'] = $data['total_alat_kembali'];
  array_push($total, $data['total_alat_kembali']);
  $json_kembali['alat'] = 'Alat Kembali';
  array_push($result, $json_kembali);

  sort($total);

  return array(
      'json' => json_encode($result),
      'max' => $total[3]
  );
 }

 public function getListUpt() {
  $data = Modules::run("database/get", array(
              'table' => "upt",
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDataAnomali() {
  $data = Modules::run('database/get', array(
              'table' => 'anomali_alat aa',
              'field' => array('aa.*', 'u.username as validator', 'ut.upt as nama_upt'),
              'join' => array(
                  array('user u', 'aa.validation = u.id', 'left'),
                  array('upt ut', 'aa.upt = ut.id'),
              ),
              'is_or_like' => true,
              'limit' => 5,
              'where' => "aa.validation = 0",
              'orderby' => "aa.id desc"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDataAlatKeluar($upt = "") {
  $where = array();
  if ($upt != "") {
   $where = "ut.id = '" . $upt . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'pinjaman_has_alat pha',
              'field' => array('pha.*', 'ut.upt as nama_upt',
                  'a.kode_alat', 'pg.nama as nama_pegawai',
                  'sp.status_alat', 'sd.status',
                  'p.no_peminjaman', 'p.tanggal_pinjam'),
              'join' => array(
                  array('pinjaman p', 'p.id = pha.pinjaman and p.deleted = 0'),
                  array('alat a', 'a.id = pha.alat'),
                  array('upt ut', 'a.upt = ut.id'),
                  array('user u', 'u.id = pha.createdby'),
                  array('pegawai pg', 'pg.id = u.pegawai'),
                  array('status_dokumen sd', 'sd.id = p.status_dokumen'),
                  array('status_pinjam sp', "sp.pinjaman_has_alat = pha.id and sp.status_alat != 'BACK'"),
              ),
              'where' => $where,
              'orderby' => "pha.id desc"
  ));

//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getTotalDataAlatKeluar($upt = "") {
  $where = array();
  if ($upt != "") {
   $where = "ut.id = '" . $upt . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'pinjaman_has_alat pha',
              'field' => array('pha.*', 'ut.upt as nama_upt',
                  'a.kode_alat', 'pg.nama as nama_pegawai',
                  'sp.status_alat', 'sd.status',
                  'p.no_peminjaman', 'p.tanggal_pinjam'),
              'join' => array(
                  array('pinjaman p', 'p.id = pha.pinjaman and p.deleted = 0'),
                  array('alat a', 'a.id = pha.alat'),
                  array('upt ut', 'a.upt = ut.id'),
                  array('user u', 'u.id = pha.createdby'),
                  array('pegawai pg', 'pg.id = u.pegawai'),
                  array('status_dokumen sd', 'sd.id = p.status_dokumen'),
                  array('status_pinjam sp', "sp.pinjaman_has_alat = pha.id and sp.status_alat != 'BACK'"),
              ),
              'where' => $where,
              'orderby' => "pha.id desc"
  ));


  return $total;
 }

 public function getDataAlatKembali($upt = "") {
  $where = array();
  if ($upt != "") {
   $where = "ut.id = '" . $upt . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'pinjaman_has_alat pha',
              'field' => array('pha.*', 'ut.upt as nama_upt',
                  'a.kode_alat', 'pg.nama as nama_pegawai',
                  'sp.status_alat', 'sd.status',
                  'p.no_peminjaman', 'p.tanggal_pinjam', 'sp.createddate as tanggal_kembali'),
              'join' => array(
                  array('pinjaman p', 'p.id = pha.pinjaman and p.deleted = 0'),
                  array('alat a', 'a.id = pha.alat'),
                  array('upt ut', 'a.upt = ut.id'),
                  array('user u', 'u.id = pha.createdby'),
                  array('pegawai pg', 'pg.id = u.pegawai'),
                  array('status_dokumen sd', 'sd.id = p.status_dokumen'),
                  array('status_pinjam sp', "sp.pinjaman_has_alat = pha.id and sp.status_alat = 'BACK'"),
              ),
              'where' => $where,
              'orderby' => "pha.id desc"
  ));

//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getTotalDataAlatKembali($upt = '') {
  $where = array();
  if ($upt != "") {
   $where = "ut.id = '" . $upt . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'pinjaman_has_alat pha',
              'field' => array('pha.*', 'ut.upt as nama_upt',
                  'a.kode_alat', 'pg.nama as nama_pegawai',
                  'sp.status_alat', 'sd.status',
                  'p.no_peminjaman', 'p.tanggal_pinjam'),
              'join' => array(
                  array('pinjaman p', 'p.id = pha.pinjaman and p.deleted = 0'),
                  array('alat a', 'a.id = pha.alat'),
                  array('upt ut', 'a.upt = ut.id'),
                  array('user u', 'u.id = pha.createdby'),
                  array('pegawai pg', 'pg.id = u.pegawai'),
                  array('status_dokumen sd', 'sd.id = p.status_dokumen'),
                  array('status_pinjam sp', "sp.pinjaman_has_alat = pha.id and sp.status_alat = 'BACK'"),
              ),
              'where' => $where,
              'orderby' => "pha.id desc"
  ));

  return $total;
 }

 public function getTotalAlat() {
  $where = "u.deleted = 0";
  if ($this->akses != 'Superadmin') {
   $where = "u.deleted = 0 and ut.id = '" . $this->upt . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'alat u',
              'join' => array(
                  array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = na.id and u.deleted = 0', 'left'),
                  array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                  array('upt ut', 'ut.id = u.upt and u.deleted = 0', 'left'),
              ),
              'where' => $where
  ));
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  return $total;
 }

 public function getTotalAnomali() {
  $where = "al.deleted = 0";
  if ($this->akses != 'Superadmin') {
   $where = "al.deleted = 0 and ut.id = '" . $this->upt . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => 'anomali_alat al',
              'join' => array(
                  array('upt ut', 'ut.id = al.upt', 'left'),
              ),
              'where' => $where
  ));

  return $total;
 }

 public function getTotalAlatRusak($upt = "") {
  $where = "alat_anomali.status_alat = 2 or u.status_alat = 2";
  if ($this->akses == 'Superadmin') {
   if ($upt != "") {
    $where = "alat_anomali.status_alat = 2 and ut.id = '" . $upt . "' or u.status_alat = 2";
   }
  } else {
   if ($upt != "") {
    $where = "alat_anomali.status_alat = 2 and ut.id = '" . $upt . "' or u.status_alat = 2";
   } else {
    $where = "alat_anomali.status_alat = 2 and ut.id = '" . $this->upt . "' or u.status_alat = 2";
   }
  }

  $data = Modules::run('database/get', array(
              'table' => 'alat u',
              'field' => array('count(*) jumlah'),
              'join' => array(
                  array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                  array('tahun t', 'u.tahun = t.id and u.deleted = 0'),
                  array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                  array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'u.id = alat_anomali.alat and u.deleted = 0', 'left'),
              ),
              'where' => $where
  ));

//   echo '<pre>';
//   print_r($this->db->last_query());die;
  $total = 0;
  if (!empty($data)) {
   $total = $data->row_array()['jumlah'];
  }
  return $total;
 }

 public function getTotalAlatNormal($upt = "") {
  $where = "(alat_anomali.status_alat = 1 or alat_anomali.status_alat is null) and u.deleted = 0";
  if ($this->akses == 'Superadmin') {
   if ($upt != "") {
    $where = "(alat_anomali.status_alat = 1 or alat_anomali.status_alat is null) and ut.id = '" . $upt . "' and u.deleted = 0";
   }
  } else {
   if ($upt != "") {
    $where = "(alat_anomali.status_alat = 1 or alat_anomali.status_alat is null) and ut.id = '" . $upt . "' and u.deleted = 0";
   } else {
    $where = "(alat_anomali.status_alat = 1 or alat_anomali.status_alat is null) and ut.id = '" . $this->upt . "' and u.deleted = 0";
   }
  }

//  echo $where;die;
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $data = Modules::run('database/get', array(
              'table' => 'alat u',
              'field' => array('count(*) as jumlah'),
              'join' => array(
                  array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = na.id and u.deleted = 0', 'left'),
                  array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                  array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                  array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0 and aa.deleted = 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'u.id = alat_anomali.alat and u.deleted = 0', 'left'),
              ),
              'where' => $where,
  ));
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $total = 0;
  if (!empty($data)) {
   $total = $data->row_array()['jumlah'];
  }
  return $total;
 }

 public function getTotalPinjaman() {
  $total = Modules::run('database/count_all', array(
              'table' => 'pinjaman p',
              'where' => "p.deleted = 0"
  ));

  return $total;
 }

 public function displayDataAlat() {
  $upt = $this->input->post('upt');
  $alat = $this->input->post('alat');

  $upt = $upt == 'all' ? '' : $upt;
  switch ($alat) {
   case 'Alat Rusak':
    $data['data_alat'] = $this->getDataAlatRusak($upt);
    $view = $this->load->view('daftar_alat_rusak', $data, true);
    break;
   case 'Alat Normal':
    $data['data_alat'] = $this->getDataAlatNormal($upt);
    $view = $this->load->view('daftar_alat_normal', $data, true);
    break;
   case 'Alat Keluar':
    $data['data_alat'] = $this->getDataAlatKeluar($upt);
    $view = $this->load->view('daftar_alat_keluar', $data, true);
    break;
   case 'Alat Kembali':
    $data['data_alat'] = $this->getDataAlatKembali($upt);
    $view = $this->load->view('daftar_alat_kembali', $data, true);
    break;
   case 'Alat Dikalibrasi':
    $data['data_alat'] = $this->getDataAlatDikalibrasi($upt);
    $view = $this->load->view('daftar_alat_kalibrasi', $data, true);
    break;
   case 'Alat Diservis':
    $data['data_alat'] = $this->getDataAlatDiservis($upt);
    $view = $this->load->view('daftar_alat_servis', $data, true);
    break;

   default:
    break;
  }

  echo json_encode(array(
      'view' => $view
  ));
 }

 public function getDataAlatRusak($upt = "") {
  $where = "alat_anomali.status_alat = 2";
  if ($upt != "") {
   $where = "alat_anomali.status_alat = 2 and ut.id = '" . $upt . "'";
  }

  $data = Modules::run('database/get', array(
              'table' => 'alat u',
              'field' => array('u.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                  'gi.gardu', 'alat_anomali.status_alat AS status_alat'),
              'join' => array(
                  array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = na.id and u.deleted = 0'),
                  array('tahun t', 'u.tahun = t.id and u.deleted = 0'),
                  array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                  array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'u.id = alat_anomali.alat and u.deleted = 0', 'left'),
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDataAlatNormal($upt = "") {
  $where = "alat_anomali.status_alat = 1";
  if ($upt != "") {
   $where = "alat_anomali.status_alat = 1 and ut.id = '" . $upt . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'alat u',
              'field' => array('u.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                  'gi.gardu', 'alat_anomali.status_alat AS status_alat'),
              'join' => array(
                  array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = na.id and u.deleted = 0'),
                  array('tahun t', 'u.tahun = t.id and u.deleted = 0'),
                  array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                  array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'u.id = alat_anomali.alat and u.deleted = 0', 'left'),
              ),
              'where' => $where,
              'groupby' => 'u.id'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDataAlatDikalibrasi($upt = "") {
  $where = array('ska.deleted' => 0);
  if ($upt != '') {
   $where = "ut.id = '" . $upt . "' and ska.deleted = 0";
  }
  $data = Modules::run("database/get", array(
              'table' => "sertifikasi_kalibrasi_alat ska",
              'field' => array('ska.*', 'a.kode_alat', 'ut.upt as nama_upt'),
              'join' => array(
                  array('alat a', 'ska.alat = a.id and a.deleted = 0'),
                  array('upt ut', 'a.upt = ut.id and ut.deleted = 0'),
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getDataAlatDiservis($upt = '') {
  $where = array('sp.deleted' => 0);
  if ($upt != "") {
   $where = "sp.deleted = 0 and ut.id = '" . $upt . "'";
  }
  $data = Modules::run("database/get", array(
              'table' => "servis_peralatan sp",
              'field' => array('sp.*', 'a.kode_alat', 'ut.upt as nama_upt'),
              'join' => array(
                  array('alat a', 'sp.alat = a.id and a.deleted = 0'),
                  array('upt ut', 'a.upt = ut.id and ut.deleted = 0'),
              ),
              'where' => $where,
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

}
