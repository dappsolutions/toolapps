<div class="white-box">
 <div class="row">
  <div class="col-md-12">
   <h4> Daftar Alat Keluar</h4>
  </div>
 </div>
 <div class="row">
  <div class="col-md-12">
   <div class="table-responsive">
    <table class="table color-bordered-table primary-bordered-table">
     <thead>
      <tr>
       <th class="font-12">
        No
       </th>
       <th class="font-12">
        No. Peminjaman
       </th>
       <th class="font-12">
        UPT Pemilik Alat
       </th>
       <th class="font-12">
        Kode Alat
       </th>
       <th class="font-12">
        Tanggal Peminjaman
       </th>
       <th class="font-12">
        Peminjam
       </th>
       <th class="font-12">
        Status Dokumen
       </th>
       <th class="font-12">
        Status Alat
       </th>
      </tr>
     </thead>
     <tbody>
      <?php if (!empty($data_keluar)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($data_keluar as $value) { ?>
        <tr>
         <td class="font-12"><?php echo $no++ ?></td>
         <td class="font-12"><b><?php echo $value['no_peminjaman'] ?></b></td>
         <td class="font-12"><?php echo $value['nama_upt'] ?></td>
         <td class="font-12"><?php echo $value['kode_alat'] ?></td>
         <td class="font-12"><?php echo date('d F Y H:i:s', strtotime($value['tanggal_pinjam'])) ?></td>
         <td class="font-12"><?php echo $value['nama_pegawai'] ?></td>
         <td class="font-12">
          <?php if ($value['status'] == 'Internal') { ?>
           <label class="label label-success font-10"><?php echo $value['status'] ?></label>
          <?php } else { ?>
           <label class="label label-primary font-10"><?php echo $value['status'] ?></label>
          <?php } ?>
         </td>
         <td class="font-12">
          <label class="label label-warning font-10"><?php echo $value['status_alat'] ?></label>
         </td>         
        </tr>
       <?php } ?>
      <?php } else { ?>
       <tr>
        <td class="text-center font-12" colspan="8"><b>Tidak Ada Data Ditemukan</b></td>
       </tr>
      <?php } ?>
     </tbody>
    </table>
   </div>
  </div>
 </div>
</div>