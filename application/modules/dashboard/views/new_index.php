
<input type="hidden" class="form-control" id="upt" value="all"/>
<div class="container-fluid">
 <?php echo $this->load->view('top_dashboard') ?>

 <div class="white-box">
  <div class="row">
   <div class="col-md-8">   
    <div class='white-box'>             
     <h4 id='title_dashboard'>Grafik Alat Seluruh UPT</h4>
     <br/>  
     <div id="canvas_alat"></div>
     <label class='display-none' id="json_alat"><?php echo $data_json ?></label>
     <label class='display-none' id="max_alat"><?php echo $max_json ?></label>
    </div>

    <div class="white-box display-none">
     <div class="row">
      <div class='col-md-12 text-center' id="content_data_alat">

      </div>
     </div>
    </div>
   </div>  

   <div class="col-md-4">
    <div class="white-box">
     <label class="label label-primary font-12">Daftar UPT</label><br/><hr/>
     <?php if (!empty($list_upt)) { ?>
      <?php foreach ($list_upt as $v_upt) { ?>
       <div class="row">
        <div class="col-md-6" id='title_upt' data_id='<?php echo $v_upt['id'] ?>'>
         <?php echo $v_upt['upt'] ?> 
        </div>
        <div class="col-md-6 text-right">
         <label class="label label-success font-10 hover" onclick="Dashboard.reDrawGrafikAlat(this)">Detail</label>
        </div>
       </div>
       <br/>
      <?php } ?>
     <?php } ?>
    </div>

    <div class="white-box">
     <label class="label label-primary font-12">Keterangan Alat</label><br/><hr/>
     <div class="row">
      <div class="col-md-6">
       <label id="jenis_alat">Alat Dikalibrasi</label>
      </div>
      <div class="col-md-6 text-right">
       <label class="label label-warning font-10 hover" id='total_kalibrasi' onclick="Dashboard.getDisplayAlat(this)"><?php echo $total_alat_dikalibrasi ?></label>
      </div>
     </div>

     <br/>
     <div class="row">
      <div class="col-md-6">
       <label id="jenis_alat">Alat Diservis</label>
      </div>
      <div class="col-md-6 text-right">
       <label class="label label-warning font-10 hover" id='total_servis' onclick="Dashboard.getDisplayAlat(this)"><?php echo $total_alat_diservis ?></label>
      </div>
     </div>

     <br/>
     <div class="row">
      <div class="col-md-6">
       <label id="jenis_alat">Alat Rusak</label>
      </div>
      <div class="col-md-6 text-right">
       <label class="label label-warning font-10 hover" id='total_rusak' onclick="Dashboard.getDisplayAlat(this)"><?php echo $total_alat_rusak ?></label>
      </div>
     </div>

     <br/>
     <div class="row">
      <div class="col-md-6">
       <label id="jenis_alat">Alat Normal</label>
      </div>
      <div class="col-md-6 text-right">
       <label class="label label-warning font-10 hover" id='total_normal' onclick="Dashboard.getDisplayAlat(this)"><?php echo $total_alat_normal ?></label>
      </div>
     </div>

     <br/>
     <div class="row">
      <div class="col-md-6">
       <label id="jenis_alat">Alat Keluar</label>
      </div>
      <div class="col-md-6 text-right">
       <label class="label label-warning font-10 hover" id='total_keluar' onclick="Dashboard.getDisplayAlat(this)"><?php echo $total_alat_keluar ?></label>
      </div>
     </div>

     <br/>
     <div class="row">
      <div class="col-md-6">
       <label id="jenis_alat">Alat Kembali</label>
      </div>
      <div class="col-md-6 text-right">
       <label class="label label-warning font-10 hover" id='total_kembali' onclick="Dashboard.getDisplayAlat(this)"><?php echo $total_alat_kembali ?></label>
      </div>
     </div>

     <?php if (!empty($total_alat_kategori)) { ?>
      <?php foreach ($total_alat_kategori as $value) { ?>
       <br/>
       <div class="row">
        <div class="col-md-6">
         <label id="jenis_alat"><?php echo ucfirst(strtolower($value['kategori'])) ?></label>
        </div>
        <div class="col-md-6 text-right">
         <label class="label label-warning font-10 hover" id='total_kembali' onclick=""><?php echo $value['total'] ?></label>
        </div>
       </div>     
      <?php } ?>
     <?php } ?>
    </div>
   </div>
  </div>  
  <br/>
 </div>
</div>