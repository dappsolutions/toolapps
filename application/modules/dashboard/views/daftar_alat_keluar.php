<label class="label label-primary font-10">Daftar Alat Keluar</label>
<hr/>
<div class="table-responsive">
    <table class="table color-bordered-table primary-bordered-table">
        <thead>
            <tr>
                <th class="font-12">
                    No
                </th>
                <th class="font-12">
                    UPT Pemilik Alat
                </th>
                <th class="font-12">
                    Kode Alat
                </th>
                <th class="font-12">
                    Tanggal Peminjaman
                </th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($data_alat)) { ?>
             <?php $no = 1; ?>
             <?php $temp_nama = "" ?>
             <?php foreach ($data_alat as $value) { ?>
              <tr>
                  <td class="font-12"><?php echo $no++ ?></td>
                  <td class="font-12"><?php echo $value['nama_upt'] ?></td>
                  <td class="font-12"><?php echo $value['kode_alat'] ?></td>
                  <td class="font-12"><?php echo date('d F Y H:i:s', strtotime($value['tanggal_pinjam'])) ?></td>     
              </tr>
             <?php } ?>
            <?php } else { ?>
             <tr>
                 <td class="text-center font-12" colspan="8"><b>Tidak Ada Data Ditemukan</b></td>
             </tr>
            <?php } ?>
        </tbody>
    </table>
</div>