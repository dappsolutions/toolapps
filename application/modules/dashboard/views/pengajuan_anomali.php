<div class="white-box">
 <div class="row">
  <div class="col-md-12">
   <h4>Top <label class="label label-danger font-12">5</label> Pengajuan Anomali Peralatan</h4>
  </div>
 </div>
 <div class="row">
  <div class="col-md-12">
   <div class="table-responsive">
    <table class="table color-bordered-table primary-bordered-table">
     <thead>
      <tr>
       <th class="font-12">
        No
       </th>
       <th class="font-12">
        Nomer Anomali
       </th>
       <th class="font-12">
        UPT
       </th>
       <th class="font-12">
        Tanggal Anomali
       </th>
       <th class="font-12">
        &nbsp;
       </th>
      </tr>
     </thead>
     <tbody>
      <?php if (!empty($data_anomali)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($data_anomali as $value) { ?>
        <tr>
         <td class="font-12"><?php echo $no++ ?></td>
         <td class="font-12"><b><?php echo $value['no_anomali'] ?></b></td>
         <td class="font-12"><?php echo $value['nama_upt'] ?></td>
         <td class="font-12"><?php echo date('d F Y', strtotime($value['tgl_anomali'])) ?></td>
         <td class="text-center">
          <label id="" class="label label-warning font-10 hover" 
                 onclick="ValidasiAnomali.detail('<?php echo $value['id'] ?>')">Detail</label>
         </td>
        </tr>
       <?php } ?>
      <?php } else { ?>
       <tr>
        <td class="text-center" colspan="5"><b>Tidak Ada Anomali</b></td>
       </tr>
      <?php } ?>
     </tbody>
    </table>
   </div>
  </div>
 </div>
</div>