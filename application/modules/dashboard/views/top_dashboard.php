<div class="row colorbox-group-widget">
 <div class="col-md-3 col-sm-6 info-color-box">
  <div class="white-box">
   <div class="media bg-primary">
    <div class="media-body hover">
     <h3 class="info-count font-15"><a href="<?php echo base_url().'alat' ?>" style="color:#fff;">Total Alat</a><span class="pull-right"><i class="mdi mdi-settings mdi-18px"></i></span></h3>
     <p class="info-text font-12"></p>
     <p class="info-ot font-15"> <span class="label label-rounded"><?php echo $total_alat ?></span></p>
    </div>
    <br/>       
   </div>
  </div>
 </div>

 <?php if ($this->session->userdata('hak_akses') == 'Superadmin' || $this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
  <div class="col-md-3 col-sm-6 info-color-box">
   <div class="white-box">
    <div class="media bg-success">
     <div class="media-body hover">
      <h3 class="info-count font-15">Total Anomali<span class="pull-right"><i class="mdi mdi-comment-text-outline mdi-18px"></i></span></h3>
      <p class="info-text font-12"></p>
      <p class="info-ot font-15"><span class="label label-rounded"><?php echo $total_anomali ?></span></p>
     </div>
     <br/>
    </div>
   </div>
  </div>
 <?php } ?>

 <div class="col-md-3 col-sm-6 info-color-box">
  <div class="white-box">
   <div class="media bg-warning">
    <div class="media-body hover">
     <h3 class="info-count font-15">Total Alat Rusak<span class="pull-right"><i class="mdi mdi-file-import"></i></span></h3>
     <p class="info-text font-12"></p>
     <p class="info-ot font-15"><span class="label label-rounded"><?php echo $total_alat_rusak ?></span></p>
    </div>
    <br/>
   </div>
  </div>
 </div>

 <div class="col-md-3 col-sm-6 info-color-box">
  <div class="white-box">
   <div class="media bg-info">
    <div class="media-body hover">
     <h3 class="info-count font-15">Total Alat Normal<span class="pull-right"><i class="mdi mdi-file-import"></i></span></h3>
     <p class="info-text font-12"></p>
     <p class="info-ot font-15"><span class="label label-rounded"><?php echo $total_alat_normal ?></span></p>
    </div>
    <br/>
   </div>
  </div>
 </div>
</div>