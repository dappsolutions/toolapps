<?php

class Pinjam_internal extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;
 public $hak_akses;
 public $user;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt = $this->session->userdata('upt_id');
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->user = $this->session->userdata('user_id');
//  echo '<pre>';
//  print_r($_SESSION);die;
 }

 public function getModuleName() {
  return 'pinjam_internal';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<link rel="stylesheet" href="' . base_url() . 'assets/css/stickyTable.min.css">',
      '<script src="' . base_url() . 'assets/js/stickyTable.min.js"></script>',
      '<link rel="stylesheet" href="' . base_url() . 'assets/css/signature-pad.css">',
      '<script src="' . base_url() . 'assets/js/signature_pad.umd.js"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/helper.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pinjam_internal_v1.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pinjaman';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Peminjaman Internal";
  $data['title_content'] = 'Data Peminjaman Internal';
  $content = $this->getDataPeminjaman();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPeminjaman($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_peminjaman', $keyword),
       array('p.tanggal_pinjam', $keyword),
   );
  }

  $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
      'phs.status');
  $where = "p.deleted = 0 and p.status_dokumen = 1 and phs.status = 'DRAFT'";
  $join = array(
      array('user u', 'p.user = u.id'),
      array('pegawai pg', 'u.pegawai = pg.id'),
      array('upt ut', 'pg.upt = ut.id'),
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id')
  );
  if ($this->hak_akses == 'ASMAN') {
   $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
       'phs.status');
   $where = "ut.id = '" . $this->upt . "' and asp.id is null and p.status_dokumen = 1 and phs.status = 'DRAFT'";
   $join = array(
       array('user u', 'p.user = u.id'),
       array('pegawai pg', 'u.pegawai = pg.id'),
       array('upt ut', 'pg.upt = ut.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
       array('pinjaman_has_status phs', 'sd.id = phs.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
       array('pinjaman_has_alat pha', 'phh.id = pha.id'),
       array('alat a', 'pha.alat = a.id'),
       array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
       array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $this->upt . "' "),
   );
  }

  if ($this->hak_akses == 'TOOLSMAN') {
   $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
       'asp.status');
   $where = "ut.id = '" . $this->upt . "' and phs.status = 'COMPLETE' and p.status_dokumen = 1";
   $join = array(
       array('user u', 'p.user = u.id'),
       array('pegawai pg', 'u.pegawai = pg.id'),
       array('upt ut', 'pg.upt = ut.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
       array('pinjaman_has_status phs', 'sd.id = phs.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
       array('pinjaman_has_alat pha', 'phh.id = pha.id'),
       array('alat a', 'pha.alat = a.id'),
       array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
       array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $this->upt . "' "),
   );
  }

  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'pg.nama as nama_pegawai', 'ut.upt', 'phs.status'),
              'join' => $join,
              'like' => $like,
              'is_or_like' => true,
              'where' => $where,
              'orderby' => 'p.id desc'
  ));

  return $total;
 }

 public function getDataPeminjaman($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_peminjaman', $keyword),
       array('p.tanggal_pinjam', $keyword),
   );
  }

  $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
      'phs.status');
  $where = "p.deleted = 0 and p.status_dokumen = 1 and phs.status = 'DRAFT'";
  $join = array(
      array('user u', 'p.user = u.id'),
      array('pegawai pg', 'u.pegawai = pg.id'),
      array('upt ut', 'pg.upt = ut.id'),
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id')
  );
  if ($this->hak_akses == 'ASMAN') {
   $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
       'asp.status');
   $where = "ut_alat.id = '" . $this->upt . "' and asp.id is null and p.status_dokumen = 1 and phs.status = 'DRAFT'";
   $join = array(
       array('user u', 'p.user = u.id'),
       array('pegawai pg', 'u.pegawai = pg.id'),
       array('upt ut', 'pg.upt = ut.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
       array('pinjaman_has_status phs', 'sd.id = phs.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
       array('pinjaman_has_alat pha', 'phh.id = pha.id'),
       array('alat a', 'pha.alat = a.id'),
       array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
       array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $this->upt . "' "),
   );
  }

  if ($this->hak_akses == 'TOOLSMAN') {
   $field = array('p.*', 'pg.nama as nama_pegawai', 'ut.upt',
       'phs.status');
   $where = "ut_alat.id = '" . $this->upt . "' and phs.status = 'COMPLETE' and p.status_dokumen = 1";
   $join = array(
       array('user u', 'p.user = u.id'),
       array('pegawai pg', 'u.pegawai = pg.id'),
       array('upt ut', 'pg.upt = ut.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd', 'sd.pinjaman = p.id'),
       array('pinjaman_has_status phs', 'sd.id = phs.id'),
       array('(select max(id) as id, pinjaman from pinjaman_has_alat group by pinjaman) phh', 'p.id = phh.pinjaman'),
       array('pinjaman_has_alat pha', 'phh.id = pha.id'),
       array('alat a', 'pha.alat = a.id'),
       array('approver_status_pinjam asp', 'p.id = asp.pinjaman', 'left'),
       array('upt ut_alat', "a.upt = ut_alat.id and ut_alat.id = '" . $this->upt . "' "),
   );
  }


  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => $field,
              'join' => $join,
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where,
              'orderby' => 'p.id desc'
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
//   for ($i = 0; $i < 10; $i++) {
   foreach ($data->result_array() as $value) {
    if ($value['status'] == '') {
     $value['status'] = 'DRAFT';
    }
    array_push($result, $value);
//   }
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPeminjaman($keyword)
  );
 }

 public function getDetailDataPeminjaman($id) {
  $field = array('kr.*', 'phs.status as status_dokumen', 'ut.upt as upt_peminjam');
  $join = array(
      array('(select max(id) as id, pinjaman from pinjaman_has_status group by pinjaman) sd',
          'sd.pinjaman = kr.id'),
      array('pinjaman_has_status phs', 'sd.id = phs.id'),
      array('user us', 'us.id = kr.user'),
      array('pegawai pg', 'pg.id = us.pegawai'),
      array('upt ut', 'ut.id = pg.upt'),
  );

  if ($this->hak_akses == 'ASMAN') {
   $field = array('kr.*', 'asp.status as status_dokumen', 'ut.upt as upt_peminjam');
   $join = array(
       array('approver_status_pinjam asp', 'asp.pinjaman = kr.id', 'left'),
       array('user us', 'us.id = kr.user', 'left'),
       array('pegawai pg', 'pg.id = us.pegawai', 'left'),
       array('upt ut', 'ut.id = pg.upt', 'left'),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => $field,
              'join' => $join,
              'where' => "kr.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   if ($result['status_dokumen'] == '') {
    $result['status_dokumen'] = 'DRAFT';
   }
  }


  return $result;
 }

 public function getListStatusPeminjaman() {
  $data = Modules::run('database/get', array(
              'table' => 'status_pinjaman',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function make() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Buat Peminjaman Internal";
  $data['title_content'] = 'Buat Peminjaman Internal';
  $data['list_status'] = $this->getListStatusPeminjaman();
  $data['list_upt'] = $this->getListUpt();

  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPeminjaman($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Peminjaman Internal";
  $data['title_content'] = 'Ubah Peminjaman Internal';
  $data['list_status'] = $this->getListStatusPeminjaman();
  $data['list_alat'] = $this->getListAlatDipinjam($id);

  $data['list_surat'] = $this->getListSuratPinjaman($id);
//  echo '<pre>';
//  print_r($data['list_surat']);die;
  $data['list_upt'] = $this->getListUpt();
  echo Modules::run('template', $data);
 }

 public function getListAlatDipinjam($pinjaman) {
  $join = array(
      array('alat a', 'pha.alat = a.id'),
      array('nama_alat na', 'na.id = a.alat'),
      array('kategori_alat ka', 'ka.id = na.kategori_alat'),
      array('upt ut', 'a.upt = ut.id'),
      array('status_pinjam sp', 'pha.id = sp.pinjaman_has_alat'),
  );

  if ($this->hak_akses == 'ASMAN') {
   $join = array(
       array('alat a', 'pha.alat = a.id'),
       array('nama_alat na', 'na.id = a.alat'),
       array('kategori_alat ka', 'ka.id = na.kategori_alat'),
       array('upt ut', "a.upt = ut.id and ut.id = '" . $this->upt . "'"),
       array('status_pinjam sp', 'pha.id = sp.pinjaman_has_alat'),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => 'pinjaman_has_alat pha',
              'field' => array('pha.*', 'a.kode_alat', 'ut.upt',
                  'sp.status_alat', 'a.merk', 'a.tipe',
                  'a.nomer_seri', 'na.nama_alat', 'ka.kategori', 'a.barcode'),
              'join' => $join,
              'where' => "pha.deleted = 0 and pha.pinjaman = '" . $pinjaman . "'"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSuratPinjaman($pinjaman) {
  $join = array(
      array('upt ut', 'sp.upt = ut.id'),
      array('status_pinjaman spm', 'sp.status_pinjaman = spm.id'),
  );

  if ($this->hak_akses == 'ASMAN') {
   $join = array(
       array('upt ut', "sp.upt = ut.id and ut.id = '" . $this->upt . "'"),
       array('status_pinjaman spm', 'sp.status_pinjaman = spm.id'),
   );
  }

  $data = Modules::run('database/get', array(
              'table' => 'surat_pinjaman sp',
              'field' => array('sp.*', 'ut.upt', 'spm.status', 'ut.id as upt_id'),
              'join' => $join,
              'where' => "sp.deleted = 0 and sp.pinjaman = '" . $pinjaman . "'"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataApproveAsman($pinjaman) {
  $where = array('asp.pinjaman' => $pinjaman);
  if ($this->hak_akses == 'ASMAN') {
   $where = array('asp.pinjaman' => $pinjaman, 'asp.upt' => $this->upt, 'tp.deleted' => 0);
  }

  $data = Modules::run('database/get', array(
              'table' => 'approver_status_pinjam asp',
              'field' => array('asp.*', 'ut.upt', 'p.nama as pegawai',
                  'asp.createddate as tgl_approve', 'tp.file'),
              'join' => array(
                  array('user u', 'asp.user = u.id'),
                  array('pegawai p', 'u.pegawai = p.id'),
                  array('upt ut', 'p.upt = ut.id'),
                  array('ttd_pegawai tp', 'tp.pegawai = p.id', 'left'),
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tgl_approve'] = Modules::run('helper/getIndoDate', $value['tgl_approve']);
    if ($value['file'] != "") {
     $value['file'] = str_replace(' ', '_', $value['file']);
     $value['file'] = base_url() . 'files/berkas/ttd_pegawai/' . $value['file'];
    }
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataPeminjaman($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Peminjaman Internal";
  $data['title_content'] = 'Detail Peminjaman Internal';
  $data['list_alat'] = $this->getListAlatDipinjam($id);
  $data['list_surat'] = $this->getListSuratPinjaman($id);
  $data['status_approve'] = $this->getDataApproveAsman($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['no_peminjaman'] = Modules::run('no_generator/generateNoPeminjaman', 'internal');
  $data['tanggal_pinjam'] = $value->tanggal_pinjam . ' ' . date('H:i:s');
  $data['user'] = $this->session->userdata('user_id');
  $data['status_dokumen'] = 1;
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function getPostDataAlat($value, $pinjaman) {
  $data['pinjaman'] = $pinjaman;
  $data['alat'] = $value->alat;
  $data['qty'] = $value->qty;
  return $data;
 }

 public function sendEmailToReceiver($upt = array(), $data_pinjaman, $id_destination) {
  $upt_id = implode(',', $upt);
  $no_peminjaman = $data_pinjaman['no_peminjaman'];
  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'field' => array('u.*', 'p.email'),
              'join' => array(
                  array('pegawai p', 'u.pegawai = p.id'),
                  array('upt ut', 'p.upt = ut.id'),
              ),
              'where' => "ut.id in (" . $upt_id . ") and u.priveledge = 3"
  ));

  if (!empty($data)) {
   $pesan = "Pengajuan Internal dengan No Peminjaman : " . $data_pinjaman['no_peminjaman'];
   $token = array();
   foreach ($data->result_array() as $value) {
    $send = Modules::run("email/send_email", "Pengajuan Internal", $pesan, $value['email']);
    Modules::run("database/_insert", "status_email", array(
        'no_peminjaman' => $no_peminjaman,
        "status" => $send
    ));

    //notfikasi mobile
    Modules::run("database/_insert", "notifikasi", array(
        'user' => $value['id'],
        'document' => "pinjaman",
        'id_destination' => $id_destination,
        'pesan' => $pesan,
        'status' => "COMPLETE",
        'jenis' => "internal"
    ));

    array_push($token, $value['token']);
   }

   //notif to mobile firebase
   $this->sendNotifikasi($token, $data_pinjaman, $id_destination);
  }
 }

 public function sendNotifikasi($token = array(), $data_pinjaman = array(), $id_destination = "") {
  // $id_pengajuan = 8;
  $no_peminjaman = $data_pinjaman['no_peminjaman'];
  $title = "Pinjaman Internal";
  $pesan = "Pengajuan Internal dengan No Peminjaman : " . $no_peminjaman;

  $status = "COMPLETE";
  $jenis = "internal";

  $url = 'https://fcm.googleapis.com/fcm/send';
  $fields = array(
      'registration_ids' => $token,
      'notification' => array(
          'title' => $title,
          'body' => $pesan,
      ),
      'data' => array(
          'id' => $id_destination,
          'document' => "pinjaman",
          "status" => $status,
          "jenis" => $jenis
      )
  );
  // echo '<pre>';
  // print_r($fields);die;
  $headers = array(
      'Authorization:key=AAAAF4rFPvU:APA91bFi6do7PwxsrfnQ9A563fRH2cQlxpOs_e6rKe-cTF6hnU6LMro-flW9V_Ibqg7z8ECxwz6fRN82ozDlgSDCMKH59Vrgc190HofidzbkKe6_rsotT1QaETQ1rgwoitv_6BE0Hhel',
      'Content-Type:application/json'
  );

  // echo json_encode($fields);die;
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
  $result = curl_exec($ch);

  if ($result === false) {
   //gagal notifikasi
   // return "gagal";
  }
  curl_close($ch);
  // return "success";
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;

  $message = "";
  $this->db->trans_begin();
  try {
   $post_pinjaman = $this->getPostDataHeader($data);
   $is_uploaded = true;
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_pinjaman);

    //pinjaman has alat
    foreach ($data->alat as $value) {
     $post_alat = $this->getPostDataAlat($value, $id);
     $pinjaman_has_alat = Modules::run('database/_insert', 'pinjaman_has_alat', $post_alat);

     //pinjaman has alat status
     $post_status_alat['pinjaman_has_alat'] = $pinjaman_has_alat;
     $post_status_alat['status'] = 'NORMAL';
     $post_status_alat['status_alat'] = 'NOT TAKEN';
     Modules::run('database/_insert', 'status_pinjam', $post_status_alat);
    }

    //surat pinjaman
    $index = 0;
    $upt = array();
    foreach ($data->surat as $value) {
     if (isset($_FILES['file' . $index])) {
      $data_file = $this->uploadData("file" . $index);
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post_surat['file'] = $_FILES['file' . $index]['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     $post_surat['upt'] = $value->upt;
     $post_surat['pinjaman'] = $id;
     $post_surat['status_pinjaman'] = $value->status_pinjaman;
     Modules::run('database/_insert', 'surat_pinjaman', $post_surat);

     array_push($upt, $value->upt);
     $index += 1;
    }

    $upt = array_unique($upt);
    //pinjaman has status
    $post_status['pinjaman'] = $id;
    $post_status['status'] = 'COMPLETE';
    Modules::run('database/_insert', 'pinjaman_has_status', $post_status);

    $this->sendEmailToReceiver($upt, $post_pinjaman, $id);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_pinjaman, array('id' => $id));

    if (!empty($data->alat)) {
     //pinjaman has alat
     foreach ($data->alat as $value) {
      $post_alat = $this->getPostDataAlat($value, $id);
      $pinjaman_has_alat = Modules::run('database/_insert', 'pinjaman_has_alat', $post_alat);

      //pinjaman has alat status
      $post_status_alat['pinjaman_has_alat'] = $pinjaman_has_alat;
      $post_status_alat['status'] = 'NORMAL';
      $post_status_alat['status_alat'] = 'NOT TAKEN';
      Modules::run('database/_insert', 'status_pinjam', $post_status_alat);
     }
    }

    if (!empty($data->alat_hapus)) {
     foreach ($data->alat_hapus as $value) {
      Modules::run('database/_update', 'pinjaman_has_alat', array('deleted' => 1), array('id' => $value->id));
      Modules::run('database/_update', 'status_pinjam', array('deleted' => 1), array('pinjaman_has_alat' => $value->id));
     }
    }
    //
    //surat pinjaman
    $index = 0;
    foreach ($data->surat as $value) {
     if (isset($_FILES['file' . $index])) {
      $data_file = $this->uploadData("file" . $index);
      $is_uploaded = $data_file['is_valid'];
      if ($is_uploaded) {
       $post_surat['file'] = $_FILES['file' . $index]['name'];
      } else {
       $is_valid = false;
       $message = $data_file['response'];
      }
     }

     $post_surat['upt'] = $value->upt;
     $post_surat['pinjaman'] = $id;
     $post_surat['status_pinjaman'] = $value->status_pinjaman;
     if ($value->baru == 1) {
      Modules::run('database/_insert', 'surat_pinjaman', $post_surat);
     } else {
      if ($value->is_edit == 0) {
       $post_surat['deleted'] = 1;
      }
      Modules::run('database/_update', 'surat_pinjaman', $post_surat, array('id' => $value->id));
     }
     $index += 1;
    }
   }
   $this->db->trans_commit();
   if ($is_uploaded) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Peminjaman Internal";
  $data['title_content'] = 'Data Peminjaman Internal';
  $content = $this->getDataPeminjaman($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getListalat($upt) {
  $data = $data = Modules::run('database/get', array(
              'table' => 'alat a',
              'field' => array('a.*', 'sa.status', 'sm.stock', 'a.upt', 'na.nama_alat', 'ka.kategori'),
              'join' => array(
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
      FROM anomali_alat_detail aad 
      JOIN anomali_alat aa ON aad.anomali_alat = aa.id
      WHERE aa.validation != 0
      GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'a.id = alat_anomali.alat and a.deleted = 0', 'left'),
                  array('status_alat sa', 'alat_anomali.status_alat = sa.id', 'left'),
                  array('nama_alat na', 'na.id = a.alat'),
                  array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                  array('stock_masuk sm', 'a.id = sm.alat', 'left'),
              ),
              'where' => "(sa.status is null or (sa.status != 'Rusak')) and a.upt = '" . $upt . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['stock'] = $value['stock'] == '' ? 0 : $value['stock'];
    array_push($result, $value);
   }
  }



  return $result;
 }

 public function getDataAlat() {
  $upt = $this->input->post('upt');
  $data['list_alat'] = $this->getListalat($upt);
  echo $this->load->view('list_alat', $data, true);
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/surat_pinjaman/';
  $config['allowed_types'] = 'png|jpg|pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function updateStatusDokumenPengajuan($pinjaman) {
  $data = Modules::run('database/get', array(
              'table' => 'pinjaman_has_alat pha',
              'field' => array('distinct(ut.id) as upt', 'asp.id as asp_id'),
              'join' => array(
                  array('alat a', 'pha.alat = a.id'),
                  array('upt ut', 'a.upt = ut.id'),
                  array('approver_status_pinjam asp', 'pha.pinjaman = asp.pinjaman', 'left'),
              ),
              'where' => "pha.deleted = 0 and pha.pinjaman = '" . $pinjaman . "'"
  ));

  $total = !empty($data) ? count($data->result_array()) : 0;

  $total_approve = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['asp_id'] != '') {
     $total_approve += 1;
    }
   }
  }

  if ($total_approve != 0) {
   if ($total_approve == $total) {
    $post['pinjaman'] = $pinjaman;
    $post['status'] = 'COMPLETE';
    Modules::run('database/_insert', 'pinjaman_has_status', $post);
   }
  }
 }

 public function updateStatusDokumenPengajuanPinjaman($pinjaman) {
  $data = Modules::run('database/get', array(
              'table' => 'status_pinjam sp',
              'field' => array('sp.*'),
              'join' => array(
                  array('pinjaman_has_alat pha', 'sp.pinjaman_has_alat = pha.id'),
              ),
              'where' => "pha.deleted = 0 and pha.pinjaman = '" . $pinjaman . "'"
  ));


  $total = !empty($data) ? count($data->result_array()) : 0;

  $total_back = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['status_alat'] == 'BACK') {
     $total_back += 1;
    }
   }
  }

  if ($total_back != 0) {
   if ($total_back == $total) {
    $post['pinjaman'] = $pinjaman;
    $post['status'] = 'BACK';
    Modules::run('database/_insert', 'pinjaman_has_status', $post);
   }
  }
 }

 public function approveAsman() {
  $id = $this->input->post('id');
  $status = $this->input->post('status');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_data['pinjaman'] = $id;
   $post_data['upt'] = $this->upt;
   $post_data['user'] = $this->user;
   $post_data['status'] = $status;
   if (isset($_POST['keterangan'])) {
    $post_data['keterangan'] = $this->input->post('keterangan');
   }
   Modules::run('database/_insert', "approver_status_pinjam", $post_data);

   $this->updateStatusDokumenPengajuan($id);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function showDokumen() {
  $file = $this->input->post('file');
  $file = str_replace(' ', '_', $file);

  $data['file'] = $file;
  echo $this->load->view('detail_file', $data, true);
 }

 public function showScanQr($id = '') {
  $id = $this->input->post('id');
  $alat = $this->input->post('alat');
  $qty = $this->input->post('qty');
  $status = $this->input->post('status');
  $data['id'] = $id;
  $data['alat'] = $alat;
  $data['qty'] = $qty;
  $data['status'] = $status;
  echo $this->load->view('scanner_qrcode', $data, true);
 }

 public function getDataAlatValid($qrcode) {
  $data = Modules::run('database/get', array(
              'table' => 'alat',
              'where' => "deleted = 0 and barcode = '" . $qrcode . "'"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;

  $is_valid = false;
  if (!empty($data)) {
   $is_valid = true;
  }

  return $is_valid;
 }

 public function getDataPinjaman($id) {
  $data = Modules::run('database/get', array(
              'table' => 'pinjaman p',
              'field' => array('p.*'),
              'join' => array(
                  array('pinjaman_has_alat pha', 'pha.pinjaman = p.id')
              ),
              'where' => array('pha.id' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getDataQtyStockMasuk($alat) {
  $data = Modules::run('database/get', array(
              'table' => 'stock_masuk',
              'where' => "deleted = 0 and alat = '" . $alat . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function generateBarcode() {
  $id = $this->input->post('id');
  $alat = $this->input->post('alat');
  $qty = $this->input->post('qty');
  $status = $this->input->post('status');
  $qrcode = str_replace('Code128', '', $this->input->post('qrcode'));
  $qrcode = str_replace(':', '', $qrcode);
  $qrcode = str_replace('QR Code', '', $qrcode);
  $qrcode = trim($qrcode);

//  echo '<pre>';
//  print_r($_POST);die;
  $alat_valid = $this->getDataAlatValid($qrcode);
//  echo '<pre>';
//  print_r($alat_valid);die;

  $is_valid = false;
  $message = "Tidak Valid";

  $data_pinjam = $this->getDataPinjaman($id);
//  echo '<pre>';
//  print_r($data_pinjam);die;
//  $alat_valid = true;
  if ($alat_valid) {
   $is_valid = false;
   $this->db->trans_begin();
   try {
    //insert stock keluar
    if ($status == "NOT TAKEN") {
     $post_keluar['no'] = $data_pinjam['no_peminjaman'];
     $post_keluar['alat'] = $alat;
     $post_keluar['status'] = 'BORROW';
     $post_keluar['qty'] = $qty;
     Modules::run('database/_insert', 'stock_keluar', $post_keluar);
    }

    if ($status == "TAKEN") {
     $post_back['no'] = $data_pinjam['no_peminjaman'];
     $post_back['alat'] = $alat;
     $post_back['status'] = 'NORMAL';
     $post_back['qty'] = $qty;
     Modules::run('database/_insert', 'stock_back', $post_back);
    }

    //minus stock masuk
    $stock_masuk = $this->getDataQtyStockMasuk($alat);
    $total_stock = $stock_masuk['stock'];

//    echo $total_stock;die;
    if ($status == "NOT TAKEN") {
     $qty_stock = $total_stock - $qty;
//     echo $qty_stock;die;
     $out_of_stock = $qty_stock < 0 ? true : false;
    } else {
     $out_of_stock = false;
     $qty_stock = $total_stock + $qty;
    }

    $is_save = true;
    if ($out_of_stock) {
     $message = "Stok Alat Tidak Cukup, Kuantitas pinjam melebihi stock";
     $is_valid = false;
     $is_save = false;
    } else {
     $is_valid = true;


     Modules::run('database/_update', 'stock_masuk', array('stock' => $qty_stock), array('id' => $stock_masuk['id']));

//     echo $status;die;
     switch ($status) {
      case 'NOT TAKEN':
       $status_alat = "TAKEN";
       break;
      case 'TAKEN':
       $status_alat = "BACK";
       break;
     }
//     echo $status_alat;die;
     Modules::run('database/_update', 'status_pinjam', array('status_alat' => $status_alat), array('pinjaman_has_alat' => $id));

//     echo "<pre>";
//     echo $this->db->last_query();
//     die;
     $this->updateStatusDokumenPengajuanPinjaman($data_pinjam['id']);
    }

    if ($is_save) {
     $this->db->trans_commit();
    } else {
     $this->db->trans_rollback();
    }
   } catch (Exception $ex) {
    $this->db->trans_rollback();
   }
  }

  echo json_encode(array('is_valid' => $is_valid, 'message' => $message));
 }

 public function cetak($id = '') {
  $mpdf = Modules::run('mpdf/getInitPdf');
  $data = $this->getDetailDataPeminjaman($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Peminjaman Internal";
  $data['title_content'] = 'Detail Peminjaman Internal';
  $data['list_alat'] = $this->getListAlatDipinjam($id);
  $data['list_surat'] = $this->getListSuratPinjaman($id);
  $data['status_approve'] = $this->getDataApproveAsman($id);


  //  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output($data['no_peminjaman'] . '.pdf', 'I');
 }

 public function checkTtd($pegawai) {
  $data = Modules::run('database/get', array(
              'table' => 'ttd_pegawai',
              'where' => "deleted = 0 and pegawai = '" . $pegawai . "'"
  ));

  $has = 0;
  if (!empty($data)) {
   $has = 1;
  }


  return $has;
 }

 public function checkHaveTtd() {
  $user_id = $this->session->userdata('user_id');
  $data['id'] = $this->input->post('id');
  $data['status'] = $this->input->post('status');
  $data['user'] = $user_id;
  $data_pegawai = Modules::run('database/get', array(
              'table' => 'user',
              'where' => "deleted = 0 and id = '" . $user_id . "'"
  ));

  $data_pegawai = $data_pegawai->row_array();

  $is_has_ttd = $this->checkTtd($data_pegawai['pegawai']);
  $data['pegawai'] = $data_pegawai['pegawai'];
  $view = $this->load->view('signature_content', $data, true);
  echo json_encode(array(
      'has_ttd' => $is_has_ttd,
      'view' => $view
  ));
 }

 public function simpanTtd() {
  $signature = $this->input->post('signature');
  $pegawai = $this->input->post('pegawai');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['signature'] = $signature;
   $post['pegawai'] = $pegawai;
   Modules::run('database/_insert', 'ttd_pegawai', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
