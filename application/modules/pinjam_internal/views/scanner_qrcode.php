<!DOCTYPE html>
<link href="<?php echo base_url() ?>assets/css/style_qr.css" rel="stylesheet">

<input type="hidden" value="<?php echo $id ?>" id="id_pinjam" class="form-control" />
<input type="hidden" value="<?php echo $alat ?>" id="alat_pinjam" class="form-control" />
<input type="hidden" value="<?php echo $qty ?>" id="qty_pinjam" class="form-control" />
<input type="hidden" value="<?php echo $status ?>" id="status_pinjam" class="form-control" />
<div class="row">  
 <div class="col-md-8">
  <h4>Scan QrCode</h4>  
 </div>
</div>  
<br/>

<div class="row">
 <div class="col-md-12 text-right">
  Driver Camera : <select class="form-control" id="camera-select"></select>        
 </div> 
</div>
<br/>
<div class="row">
<div class="col-md-10">  
  <input id="image-url" type="text" class="form-control" placeholder="Image url">    
 </div>
 <div class="col-md-2">
  <button title="Decode Image" class="btn btn-default btn-sm" id="decode-img" type="button" data-toggle="tooltip"><span class="mdi mdi-auto-upload"></span></button>
 </div>
</div>
<hr/>

<div class="row">
 <div class="col-md-12 text-center">  
  <button title="Image shoot" class="btn btn-info btn-sm disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="mdi mdi-image mdi-18px"></span></button>
  <button title="Play" class="btn btn-success btn-sm" id="play" type="button" data-toggle="tooltip"><span class="mdi mdi-play mdi-18px"></span></button>
  <button title="Pause" class="btn btn-warning btn-sm" id="pause" type="button" data-toggle="tooltip"><span class="mdi mdi-pause mdi-18px"></span></button>
  <button title="Stop streams" class="btn btn-danger btn-sm" id="stop" type="button" data-toggle="tooltip"><span class="mdi mdi-stop mdi-18px"></span></button>
 </div>
</div>
<br/>

<div class="row">
 <div class="col-md-12 text-center">
  <div class="well" style="position: relative;display: inline-block;">
   <canvas width="320" height="240" id="webcodecam-canvas"></canvas>
   <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
   <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
   <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
   <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
  </div>
 </div> 
</div>
<br/>

<div class="row">
 <div class="col-md-12 text-center">
  <div class="thumbnail" id="result">
   <div class="well" style="overflow: hidden;">
    <img width="320" height="240" id="scanned-img" src="">
   </div>
   <div class="caption" style="margin-top: 16px;">
    <h3>Scanned result</h3>
    <p id="scanned-QR"></p>
    <button class="btn btn-success" onclick="PinjamInternal.generateBarcode()">Proses</button>
   </div>
  </div>
 </div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/barcode/filereader.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/barcode/qrcodelib.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/barcode/webcodecamjs.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/barcode/main.js"></script>
