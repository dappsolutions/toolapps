<?php

class Import_alat extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'import_alat';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/import_alat.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'alat';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Import Alat";
  $data['title_content'] = 'Import Alat';
  $data['list_kategori'] = $this->getListKategoriAlat();
  echo Modules::run('template', $data);
 }

 public function getListKategoriAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_alat ka',
              'where' => "ka.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getGarduInduk($gardu, $upt, $context) {
  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk',
							'where' => array('gardu' => $gardu, 'deleted' => 0, 
							'upt' => $upt, 'context'=> $context)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'gardu_induk', array('gardu' => $gardu, 'upt' => $upt, 'context'=> $context));
  }

  return $id;
 }
 
 public function getGarduULTG($gardu, $upt) {
  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk',
							'where' => array('gardu' => $gardu, 'deleted' => 0, 
							'upt' => $upt,)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'gardu_induk', array('gardu' => $gardu, 'upt' => $upt, 'context'=> 'ULTG'));
  }

  return $id;
 }
 
 public function getGarduId($gardu, $ultg, $upt) {
  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk',
							'where' => array('gardu' => $gardu, 
							'deleted' => 0, 
							'parent' => $ultg,)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'gardu_induk', array('gardu' => $gardu, 'upt' => $upt, 'parent'=> $ultg,'context'=> 'GI'));
  }

  return $id;
 }

 public function getKategoriAlat($kategori) {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_alat',
              'where' => array('kategori' => $kategori, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'kategori_alat', array('kategori' => $kategori));
  }

  return $id;
 }

 public function getDataAlat($nama_alat, $kategori_alat) {
  $data = Modules::run('database/get', array(
              'table' => 'nama_alat',
              'where' => array('nama_alat' => $nama_alat, 'deleted' => 0, 'kategori_alat' => $kategori_alat)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'nama_alat', array('nama_alat' => $nama_alat,
               'kategori_alat' => $kategori_alat));
  }

  return $id;
 }

 public function getUpt($upt) {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => array('upt' => $upt, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'upt', array('upt' => $upt));
  }

  return $id;
 }

 public function getTahun($tahun) {
  $data = Modules::run('database/get', array(
              'table' => 'tahun',
              'where' => array('tahun' => $tahun, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'tahun', array('tahun' => $tahun));
  }

  return $id;
 }

 public function getStatusAlat($status) {
  $data = Modules::run('database/get', array(
              'table' => 'status_alat',
              'where' => array('status' => $status, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'status_alat', array('status' => $status));
  }

  return $id;
 }

 public function getRegu($regu) {
  $data = Modules::run('database/get', array(
              'table' => 'regu_pemeliharaan',
              'where' => array('regu' => $regu, 'deleted' => 0)
  ));

  $id = 0;
  if (!empty($data)) {
   $id = $data->row_array()['id'];
  } else {
   $id = Modules::run('database/_insert', 'regu_pemeliharaan', array('regu' => $regu));
  }

  return $id;
 }

 public function checkExistDataAlat($data_alat) {
  $sql = "select * from alat where alat = " . $data_alat['alat'] . " "
          . "and upt = " . $data_alat['upt'] . " "
          . "and nomer_seri = '" . $data_alat['nomer_seri'] . "'";

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }
 
 public function checkExistDataAlatByKategori($data_alat) {
  $sql = "select * from alat where alat = " . $data_alat['alat'] . " "
          . "and upt = " . $data_alat['upt'] . " "
          . "and kode_alat = '" . $data_alat['kode_alat'] . "'";

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function import() {
  $data = json_decode($this->input->post('data'));
  $kategori_alat = $_POST['kategori'];
//  echo '<pre>';
//  print_r($data);die;
  $is_valid = false;
  $result = array();
  foreach ($data as $value) {
//    $data_alat = explode(',', $value[0]);
//    if (is_numeric(trim($data_alat[0]))) {
//    echo '<pre>';
//    print_r($data_alat);
//    die;


    $is_valid_insert = false;
    $this->db->trans_begin();
    try {
     $post_alat = array();
     $upt = $this->getUpt(trim($value[1]));
     $post_alat['barcode'] = Modules::run('no_generator/generateQrCode');
		 $post_alat['nomer_seri'] = trim($value[4]);
		 $context = "OTHER";
		 
		 $data_pemilik = explode(' ', trim($value[2]));
		 switch (trim($data_pemilik[0])) {
			 case 'PDKB':
				$context = "PDKB";
				 break;
			 case 'ULTG':
				$context = "ULTG";
				 break;
			 
			 default:
			 $context = strtoupper(trim($data_pemilik[0]));
				 break;
		 }

		//  $context = "PDKB";
		// if(trim($value[3]) == trim($value[2])){
 			$post_alat['gardu_induk'] = $this->getGarduInduk(trim($value[2]), $upt, $context);
		// }else{
		// 	$ultg = $this->getGarduULTG(trim($value[2]), $upt);    
		// 	$post_alat['gardu_induk'] = $this->getGarduId(trim($value[3]), $ultg, $upt);
		// }		 
     $post_alat['upt'] = $upt;
     $kategori_alat = $this->getKategoriAlat(trim($value[7]));     
     $post_alat['alat'] = $this->getDataAlat(trim($value[5]), $kategori_alat);
     $post_alat['tipe'] = $value[8];
//     $post_alat['keterangan'] = $data_alat[7];
     if (trim($value[10]) != '') {
      $post_alat['tahun'] = $this->getTahun(trim($value[10]));
		 }
		 $post_alat['status_alat'] = 1;
    //  if (trim($value[10]) != '') {
    //   $post_alat['status_alat'] = $this->getStatusAlat(ucfirst(trim($value[10])));
    //  }
     $post_alat['merk'] = $value[6];
    //  $regu_id = $this->getRegu(trim($value[3]));
     $post_alat['kode_alat'] = Modules::run('no_generator/generateKodeAlatImpor', $data);
    //  $post_alat['kode_alat'] = '--'.trim($value[3]);

     $post_alat['keterangan'] = trim($value[11]);
//     echo '<pre>';
//     print_r($post_alat);die;  
    //  $data_peralatan = $this->checkExistDataAlat($post_alat);


			
			// Modules::run('database/_update', 'alat', $post_alat, 
			// array('kode_alat'=> $post_alat['kode_alat'], 'alat'=> $post_alat['alat']));
			// echo '<pre>';
			// echo $this->db->last_query();die;
    //  if (empty($data_peralatan)) {
      $alat_id = Modules::run('database/_insert', 'alat', $post_alat);
    //  } else {
    //   $alat_id = $data_peralatan['id'];

    //   if (isset($post_alat['status_alat']) && isset($post_alat['keterangan'])) {
    //    Modules::run('database/_update', 'alat',
    //            array('status_alat' => $post_alat['status_alat'],
    //                'keterangan' => $post_alat['keterangan']),
    //            array('id' => $alat_id));
    //   }
    //  }

    //  $this->insertReguAlat($alat_id, $regu_id);
     $this->db->trans_commit();
     $is_valid_insert = true;
     if ($is_valid_insert) {
      $post_alat['upt'] = $value[1];
      $post_alat['gardu_induk'] = $value[2];
      $post_alat['nama_alat'] = trim($value[3]);
      array_push($result, $post_alat);
     }
    } catch (Exception $ex) {
     $this->db->trans_rollback();
    }
//    }
  }

  $content['list_upload'] = $result;
  echo $this->load->view('tabel_alat', $content, true);
 }

 public function insertReguAlat($alat, $regu) {
  $data = Modules::run('database/get', array(
              'table' => 'alat_has_regu_pemeliharaan',
              'where' => "deleted = 0 and alat = '" . $alat . "' and regu_pemeliharaan = '" . $regu . "'"
  ));

  if (empty($data)) {
   $post_insert['alat'] = $alat;
   $post_insert['regu_pemeliharaan'] = $regu;
   Modules::run('database/_insert', 'alat_has_regu_pemeliharaan', $post_insert);
  }
 }

 public function importOld() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $result = array();
  foreach ($data as $value) {
   $data_alat = explode(',', $value[0]);
   if (is_numeric(trim($data_alat[0]))) {
//    echo '<pre>';
//    print_r($data_alat);
//    die;
    $is_valid_insert = false;
    $this->db->trans_begin();
    try {
     $upt = $this->getUpt(trim($data_alat[4]));
     $post_alat['barcode'] = Modules::run('no_generator/generateQrCode');
     $post_alat['nomer_seri'] = trim($data_alat[1]);
     $post_alat['gardu_induk'] = $this->getGarduInduk(trim($data_alat[2]), $upt);
     $kategori_alat = $this->getKategoriAlat(trim($data_alat[3]));
     $post_alat['upt'] = $upt;
     $post_alat['alat'] = $this->getDataAlat(trim($data_alat[5]), $kategori_alat);
     $post_alat['tipe'] = $data_alat[6];
     $post_alat['keterangan'] = $data_alat[7];
     $post_alat['tahun'] = $this->getTahun(trim($data_alat[8]));
     $post_alat['merk'] = $data_alat[9];
     $post_alat['regu_pemeliharaan'] = $this->getRegu(trim($data_alat[10]));
     $post_alat['kode_alat'] = Modules::run('no_generator/generateKodeAlatImpor', $data);

     Modules::run('database/_insert', 'alat', $post_alat);
     $this->db->trans_commit();
     $is_valid_insert = true;
     if ($is_valid_insert) {
      $post_alat['upt'] = $data_alat[4];
      $post_alat['gardu_induk'] = $data_alat[2];
      $post_alat['nama_alat'] = trim($data_alat[5]);
      array_push($result, $post_alat);
     }
    } catch (Exception $ex) {
     $this->db->trans_rollback();
    }
   }
  }

  $content['list_upload'] = $result;
  echo $this->load->view('tabel_alat', $content, true);
 }

 public function download() {
  $this->load->helper("download");
  $data = file_get_contents('files/template_import/template_import_alat.csv');
  $name = 'template_import_alat.csv';
  force_download($name, $data);
 }

}
