<div class='container-fluid'>

 <div class="white-box">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class="col-md-3">
     <div class="form-group">
      <select class="form-control required" id="kategori" error="Kategori">
       <?php if (!empty($list_kategori)) { ?>
        <?php foreach ($list_kategori as $value) { ?>
         <?php $selected = '' ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>
    </div>
    <div class='col-md-6'>
     <div class="form-group">
      <div class="fileinput fileinput-new input-group" data-provides="fileinput">
       <div class="form-control" data-trigger="fileinput"> 
        <i class="glyphicon glyphicon-file fileinput-exists"></i> 
        <span class="fileinput-filename"></span>
       </div> 
       <span class="input-group-addon btn btn-default btn-file"> 
        <span class="fileinput-new" onclick="ImportAlat.upload(this)">Select file</span> 
        <span class="fileinput-exists">Change</span>
        <input id='file' type="file" name="..." onchange="ImportAlat.getUploadedData(this, 'xls')">         
       </span>        
       <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
      </div>
     </div>
    </div>
    <div class='col-md-3 text-right'>
     <a href="<?php echo base_url() . $module . '/download' ?>" id="" class="btn btn-warning">Download Template</a>
    </div>
   </div>
   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive" id="table_alat">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr class="bg-success text-white">
         <th class="font-12">No</th>
         <th class="font-12">Alat</th>
         <th class="font-12">UPT</th>
         <th class="font-12">Gardu Induk / ULTG</th>
         <th class="font-12">Status</th>
        </tr>
       </thead>
       <tbody>
        <tr>
         <td class="text-center font-12" colspan="5">Tidak Ada Data yang Diimport</td>
        </tr>
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
  </div>
 </div>
</div>