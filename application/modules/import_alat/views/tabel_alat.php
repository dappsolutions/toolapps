<table class="table color-bordered-table primary-bordered-table">
 <thead>
  <tr class="bg-success text-white">
   <th class="font-12">No</th>
   <th class="font-12">Alat</th>
   <th class="font-12">UPT</th>
   <th class="font-12">Gardu Induk / ULTG</th>
   <th class="font-12">Status</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($list_upload)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($list_upload as $value) { ?>
    <tr>
     <td class="font-12"><?php echo $no++ ?></td>
     <td class="font-12"><?php echo $value['nama_alat'] ?></td>
     <td class="font-12"><?php echo $value['upt'] ?></td>
     <td class="font-12"><?php echo $value['gardu_induk'] ?></td>
     <td class="text-center">
      <label id="" class="badge badge-success font-12">Sukses</label>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td class="text-center font-12" colspan="4">Tidak Ada Data yang Diimport</td>
   </tr>
  <?php } ?>         
 </tbody>
</table>