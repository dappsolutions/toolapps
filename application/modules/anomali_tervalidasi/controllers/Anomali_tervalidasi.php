<?php

class Anomali_tervalidasi extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'anomali_tervalidasi';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/anomali_tervalidasi.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'anomali_alat';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Anomali Tervalidasi";
  $data['title_content'] = 'Data Anomali Tervalidasi';
  $content = $this->getDataAnomali();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataAnomali($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('aa.no_anomali', $keyword),
       array('ut.upt', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' aa',
              'field' => array('aa.*', 'u.username as validator',
                  'uv.date_validation', 'ut.upt as nama_upt'),
              'join' => array(
                  array('user u', 'aa.validation = u.id'),
                  array('user_validation uv', 'aa.id = uv.anomali_alat'),
                  array('upt ut', 'aa.upt = ut.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => 'aa.validation != 0'
  ));

  return $total;
 }

 public function getDataAnomali($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('aa.no_anomali', $keyword),
       array('ut.upt', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' aa',
              'field' => array('aa.*', 'u.username as validator',
                  'uv.date_validation', 'ut.upt as nama_upt'),
              'join' => array(
                  array('user u', 'aa.validation = u.id'),
                  array('user_validation uv', 'aa.id = uv.anomali_alat'),
                  array('upt ut', 'aa.upt = ut.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => 'aa.validation != 0'
  ));
//  echo '<pre>';
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
//  echo '<pre>';
//  print_r($result);
//  die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAnomali($keyword)
  );
 }

 public function getDetailDataAnomali($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListStatus() {
  $data = Modules::run('database/get', array(
              'table' => 'status_alat',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Anomali";
  $data['title_content'] = 'Tambah Anomali';
  $data['list_status'] = $this->getListStatus();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataAnomali($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Anomali";
  $data['title_content'] = 'Ubah Anomali';
  echo Modules::run('template', $data);
 }

 public function getDataDetailAlat($anomali) {
  $data = Modules::run('database/get', array(
              'table' => 'anomali_alat_detail aad',
              'field' => array('aad.*', 'na.nama_alat',
                  'a.nomer_seri', 'sa.status', 'gi.gardu',
                  'ka.kategori', 'a.tipe', 'ut.upt as nama_upt', 't.tahun'),
              'join' => array(
                  array('alat a', 'aad.alat = a.id'),
                  array('nama_alat na', 'a.alat = na.id'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id'),
                  array('upt ut', 'a.upt = ut.id'),
                  array('gardu_induk gi', 'a.gardu_induk = gi.id', 'left'),
                  array('status_alat sa', 'aad.status_alat = sa.id'),
                  array('tahun t', 'a.tahun = t.id', 'left'),
              ),
              'where' => array('aad.anomali_alat' => $anomali)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['regu'] = $this->getDataReguPemeliharaan($value['alat']);
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataReguPemeliharaan($id) {
  $data = Modules::run('database/get', array(
              'table' => 'alat_has_regu_pemeliharaan ap',
              'field' => array('ap.*', 'r.regu'),
              'join' => array(
                  array('regu_pemeliharaan r', 'r.id = ap.regu_pemeliharaan')
              ),
              'where' => "ap.deleted = 0 and ap.alat = '" . $id . "'"
  ));

  $regu = "";
  if (!empty($data)) {
   $regu_data = array();
   foreach ($data->result_array() as $value) {
    $regu_data[] = $value['regu'];
   }

   $regu = implode(',', $regu_data);
  }
  return $regu;
 }

 public function getDataApproval($anomali) {
  $data = Modules::run('database/get', array(
              'table' => 'user_validation uv',
              'field' => array('uv.*', 'pg.nama as nama_pegawai', 'pg.nip', 'pv.hak_akses'),
              'join' => array(
                  array('user u', 'uv.user = u.id'),
                  array('priveledge pv', 'u.priveledge = pv.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => array('uv.anomali_alat' => $anomali)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataAnomali($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Anomali";
  $data['title_content'] = 'Detail Anomali';
  $data['detail_alat'] = $this->getDataDetailAlat($id);
  $data['approval'] = $this->getDataApproval($id);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function detailExportExcel($id) {
  $data = $this->getDetailDataAnomali($id);
  $data['title'] = "Detail Anomali";
  $data['title_content'] = 'Detail Anomali';
  $data['detail_alat'] = $this->getDataDetailAlat($id);

  echo $this->load->view('detail_export_excel', $data, true);
 }

 public function showDataAlat() {
  $index_tr = $this->input->post('index_tr');
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $content = $this->getDataAlat($this->limit, $this->last_no);
  $data['detail_content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/seekPagination/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['index_tr'] = $index_tr;
  echo $this->load->view('detail_alat', $data, true);
 }

 public function seekPagination($offset) {
  $this->segment = 3;
  $offset = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $offset * $this->limit;

  $content = $this->getDataAlat($this->limit, $this->last_no);
  $data['detail_content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/seekPagination/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo $this->load->view('table_alat', $data, true);
 }

 public function getTotalDataAlat() {
  $like = array();
  $total = Modules::run('database/count_all', array(
              'table' => 'alat aa',
              'field' => array('aa.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt'),
              'join' => array(
                  array('kategori_alat ka', 'aa.kategori_alat = ka.id and aa.deleted = 0'),
                  array('tahun t', 'aa.tahun = t.id and aa.deleted = 0'),
                  array('upt ut', 'aa.upt = ut.id and aa.deleted = 0'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $this->upt_id
  ));

  return $total;
 }

 public function getDataAlat($limit = 1, $offset = 0) {
  $like = array();
  $data = Modules::run('database/get', array(
              'table' => 'alat aa',
              'field' => array('aa.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt'),
              'join' => array(
                  array('kategori_alat ka', 'aa.kategori_alat = ka.id and aa.deleted = 0'),
                  array('tahun t', 'aa.tahun = t.id and aa.deleted = 0'),
                  array('upt ut', 'aa.upt = ut.id and aa.deleted = 0'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $limit,
              'offset' => $offset,
              'where' => $this->upt_id
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAlat()
  );
 }

 public function getPostDataHeader($value) {
  $data['no_anomali'] = Modules::run('no_generator/generateNomerAnomali');
  $data['tgl_anomali'] = $value->tanggal_anomali;
  $data['upt'] = $this->session->userdata('upt_id');
  return $data;
 }

 public function getPostAnomaliAlatDetail($anomali, $value) {
  $data['anomali_alat'] = $anomali;
  $data['user'] = $this->session->userdata('user_id');
  $data['alat'] = $value->id_alat;
  $data['status_alat'] = $value->status;
  $data['keterangan_anomali'] = $value->keterangan_anomali;
  ;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $anomali = $id;
  $this->db->trans_begin();
  try {
   $post_anomali = $this->getPostDataHeader($data);
   if ($id == '') {
    $anomali = Modules::run('database/_insert', $this->getTableName(), $post_anomali);

    if (!empty($data->detail_item)) {
     foreach ($data->detail_item as $value) {
      $post_anomali_detail = $this->getPostAnomaliAlatDetail($anomali, $value);
      Modules::run('database/_insert', 'anomali_alat_detail', $post_anomali_detail);
     }
    }
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_anomali, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'anomali' => $anomali));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Anomali";
  $data['title_content'] = 'Data Anomali';
  $content = $this->getDataAnomali($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

}
