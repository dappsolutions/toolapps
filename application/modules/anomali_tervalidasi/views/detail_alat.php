<script src="<?php echo base_url() ?>assets/js/url.js"></script>
<script src="<?php echo base_url() ?>assets/js/controllers/pagination.js"></script>
<input type='hidden' name='' id='index_tr' class='form-control' value='<?php echo $index_tr ?>'/>
<div class='row' id="data_alat">
 <div class='col-md-12'>
  <div class='card'>
   <div class='card-body'>
    <h4><u>Pilih Alat</u></h4>
    <div class="table-responsive">
     <table class="table table-bordered table-striped">
      <thead>
       <tr>
        <th class="">No</th>
        <th class="">UPT</th>
        <th class="">Kode Alat</th>
        <th class="">Nomer Seri</th>
        <th class="">Nama Alat</th>
        <th class="">Merk Alat</th>
        <th class="">Kategori Alat</th>
        <th class="">Tipe Alat</th>
        <th class="">Tahun Perolehan</th>
        <th class="">Keterangan</th>
        <th class="">Action</th>
       </tr>
      </thead>
      <tbody>
       <?php if (!empty($detail_content)) { ?>
        <?php $no = 1; ?>
        <?php foreach ($detail_content as $value) { ?>
         <tr>
          <td><?php echo $no++ ?></td>
          <td><?php echo $value['nama_upt'] ?></td>
          <td><?php echo '<b>' . $value['barcode'] . '</b>' ?></td>
          <td><?php echo $value['nomer_seri'] ?></td>
          <td><?php echo $value['alat'] ?></td>
          <td><?php echo $value['merk'] ?></td>
          <td><?php echo $value['kategori'] ?></td>
          <td><?php echo $value['tipe'] ?></td>
          <td><?php echo $value['keterangan'] ?></td>
          <td><?php echo $value['tahun_perolehan'] ?></td>
          <td class="text-center">
           <label id="" class="badge badge-warning font-14 hover" 
                  onclick="Anomali.pilihAlat(this, '<?php echo $value['id'] ?>')">Pilih</label>
          </td>
         </tr>
        <?php } ?>
       <?php } else { ?>
        <tr>
         <td class="text-center" colspan="12">Tidak Ada Data Ditemukan</td>
        </tr>
       <?php } ?>         
      </tbody>
     </table>
    </div>
   </div>
  </div>
 </div>
 <br/>
 <br/>
 <div class="col-md-12">
  <div class="pagination">
   <?php echo $pagination['links'] ?>
  </div>
 </div>
</div>
