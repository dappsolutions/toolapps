<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <div class="row">
      <div class='col-md-3'>
       No. Anomali
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $no_anomali ?>
      </div>     
     </div>
     <br/>
     <div class="row">
      <div class='col-md-3'>
       Tanggal Anomali
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo date('d F Y', strtotime($tgl_anomali)) ?>
      </div>     
     </div>
     <br/>     
    </div>
   </div>
  </div>
 </div>

 <br/>
 <div class='row'>
  <div class='col-12'>
   <div class='card'>
    <div class='card-body'>
     <div class="row">
      <div class='col-md-3'>
       <u>Anomali Alat</u>
      </div>
      <div class='col-md-9 text-right'>
       <a class="btn btn-success" download="<?php echo $no_anomali ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tabel_anomali', 'Anomali Alat');">Download</a>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-12'>
       <div class='table-responsive'>
        <table class="table color-bordered-table primary-bordered-table" id="tabel_anomali">
         <thead class="thead-default">
          <tr class="bg-success text-white">
           <th class="font-12">No</th>
           <th class="font-12">Nama Alat</th>
           <th class="font-12">UPT</th>
           <th class="font-12">Gardu Induk / ULTG</th>
           <th class="font-12">Regu Pemeliharaan</th>
           <th class="font-12">Nomer Seri</th>
           <th class="font-12">Tipe</th>
           <th class="font-12">Kategori</th>
           <th class="font-12">Tahun Perolehan</th>
           <th class="font-12">Status</th>
           <th class="font-12">Keterangan</th>
          </tr>
         </thead>
         <tbody class="table_anomali">
          <?php if (!empty($detail_alat)) { ?>
           <?php $no = 1 ?>
           <?php foreach ($detail_alat as $value) { ?>
            <tr>
             <td class="font-12"><?php echo $no++ ?></td>
             <td class="font-12"><?php echo $value['nama_alat'] ?></td>
             <td class="font-12"><?php echo $value['nama_upt'] ?></td>
             <td class="font-12"><?php echo $value['gardu'] ?></td>
             <td class="font-12"><?php echo $value['regu'] ?></td>
             <td class="font-12"><?php echo $value['nomer_seri'] ?></td>
             <td class="font-12"><?php echo $value['tipe'] ?></td>
             <td class="font-12"><?php echo $value['kategori'] ?></td>
             <td class="font-12"><?php echo $value['tahun'] ?></td>
             <td class="font-12"><?php echo $value['status'] == 'Rusak' ? '<label class="text-danger">Rusak</label>' : '<label class="text-success">Normal</label>' ?></td>
             <td class="font-12"><?php echo $value['keterangan_anomali'] ?></td>
            </tr>
           <?php } ?>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>
     </div>
     <br/>
     <hr/>
    </div>
   </div>
  </div>
 </div>
</div>
