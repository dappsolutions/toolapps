<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Kode Alat
      </div>
      <div class='col-md-4'>
       <select id="kode_alat" error="Kode Alat" class="form-control required">
        <?php if (!empty($list_alat)) { ?>
         <?php foreach ($list_alat as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($alat)) { ?>
           <?php $selected = $value['id'] == $alat ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_alat']. ' - '. $value['kategori'].' - '.$value['kode_alat'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tanggal
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='tanggal' class='form-control required' 
              value='<?php echo isset($tgl_sertifikasi) ? $tgl_sertifikasi : '' ?>' error="Tanggal"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4'>
       <textarea class="form-control" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>     
     </div>
     <hr/>    
     <br/>

     <div class="row">
      <div class="col-md-7">
       <table class="table color-bordered-table primary-bordered-table" id='data_file'>
        <thead>
         <tr class="bg-primary text-white">
          <th class="font-12">File</th>
          <th class="font-12">Action</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($list_file)) { ?>
          <?php foreach ($list_file as $value) { ?>
           <tr data_id="<?php echo $value['id'] ?>">
            <td>
             <div class="fileinput fileinput-new input-group" data-provides="fileinput">
              <div class="form-control" data-trigger="fileinput"> 
               <i class="glyphicon glyphicon-file fileinput-exists"></i> 
               <span class="fileinput-filename"><?php echo isset($value['file']) ? $value['file'] : '' ?></span>
              </div> 
              <span class="input-group-addon btn btn-default btn-file"> 
               <span class="fileinput-new" onclick="SertifikasiOperator.upload(this)">Select file</span> 
               <span class="fileinput-exists">Change</span>
               <input class="file" id='file' type="file" name="..." onchange="SertifikasiOperator.getFilename(this, 'xls')"> 
              </span> 
              <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
             </div>
            </td>
            <td>
             <i class="mdi mdi-minus-circle mdi-24px hover text-danger" onclick="SertifikasiOperator.removeDetailData(this)"></i>
            </td>
           </tr>
          <?php } ?>
         <?php } ?>
         <tr data_id="">
          <td>
           <div class="fileinput fileinput-new input-group" data-provides="fileinput">
            <div class="form-control" data-trigger="fileinput"> 
             <i class="glyphicon glyphicon-file fileinput-exists"></i> 
             <span class="fileinput-filename"></span>
            </div> 
            <span class="input-group-addon btn btn-default btn-file"> 
             <span class="fileinput-new" onclick="SertifikasiOperator.upload(this)">Select file</span> 
             <span class="fileinput-exists">Change</span>
             <input class="file" id='file' type="file" name="..." onchange="SertifikasiOperator.getFilename(this, 'xls')"> 
            </span> 
            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
           </div>
          </td>
          <td>
           <i class="mdi mdi-plus-circle mdi-24px hover text-warning" onclick="SertifikasiOperator.addDetail(this)"></i>
          </td>
         </tr>
        </tbody>
       </table>
      </div>
     </div>

     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="SertifikasiOperator.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="SertifikasiOperator.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
