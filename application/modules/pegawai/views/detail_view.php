<div class="container-fluid">
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       UPT
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nama_upt ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       NIP
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nip ?>
      </div>     
     </div>
     <br/>
     <div class="row">
      <div class='col-md-3'>
       Nama
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nama ?>
      </div>     
     </div>
     <br/>
     <div class="row">
      <div class='col-md-3'>
       Email
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $email ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Username
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $username ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Password
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $password ?>
      </div>     
     </div>
     <br/>
     <div class="row">
      <div class='col-md-3'>
       Hak Akses
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $hak_akses ?>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="Pegawai.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
