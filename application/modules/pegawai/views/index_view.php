<div class='container-fluid'>
 <div class="row">
  <!-- column -->
  <div class="col-12">
   <div class="white-box">
    <div class="card-body">
     <div class='row'>
      <div class='col-md-2'>
       <button id="" class="btn btn-block btn-warning" onclick="Pegawai.add()">Tambah</button>
      </div>
      <div class='col-md-10'>
       <input type='text' name='' id='' onkeyup="Pegawai.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
      </div>     
     </div>        
     <br/>
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div>
     <br/>
     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr class="bg-success text-white">
           <th class="font-12">No</th>
           <th class="font-12">NIP</th>
           <th class="font-12">Nama</th>
           <th class="font-12">Email</th>
           <th class="font-12">Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr>
             <td class="font-12"><?php echo $no++ ?></td>
             <td class="font-12"><?php echo $value['nip'] ?></td>
             <td class="font-12"><?php echo $value['nama'] ?></td>
             <td class="font-12"><?php echo $value['email'] ?></td>
             <td class="text-center">
              <label id="" class="label label-warning font-10" 
                      onclick="Pegawai.ubah('<?php echo $value['id'] ?>')">Ubah</label>
              &nbsp;
              <label id="" class="label label-success font-10" 
                      onclick="Pegawai.detail('<?php echo $value['id'] ?>')">Detail</label>
              &nbsp;
              <label id="" class="label label-danger font-10" 
                      onclick="Pegawai.delete('<?php echo $value['id'] ?>')">Hapus</label>
              &nbsp;
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td class="text-center" colspan="4">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>
     </div>    
     <br/>
     <div class="row">
      <div class="col-md-12">
       <div class="pagination">
        <?php echo $pagination['links'] ?>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div> 
</div>