<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid">
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       UPT
      </div>
      <div class='col-md-4'>
       <select id="upt" error="UPT" class="form-control required">       
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($upt)) { ?>
           <?php $selected = $value['id'] == $upt ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['upt'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       NIP
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='nip' class='form-control required' 
              value='<?php echo isset($nip) ? $nip : '' ?>' error="NIP"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Nama
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='nama' class='form-control required' 
              value='<?php echo isset($nama) ? $nama : '' ?>' error="Nama"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Email
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='email' class='form-control required' 
              value='<?php echo isset($email) ? $email : '' ?>' error="Email"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Username
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='username' class='form-control required' 
              value='<?php echo isset($username) ? $username : '' ?>' error="Username"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Password
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='password' class='form-control required' 
              value='<?php echo isset($password) ? $password : '' ?>' error="Password"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Hak Akses
      </div>
      <div class='col-md-4'>
       <select id="akses" error="Hak Akses" class="form-control required">       
        <?php if (!empty($list_akses)) { ?>
         <?php foreach ($list_akses as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($priveledge)) { ?>
           <?php $selected = $value['id'] == $priveledge ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['hak_akses'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Pegawai.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="Pegawai.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
