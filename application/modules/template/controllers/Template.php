<?php

class Template extends MX_Controller {

 public function index($data) {
  $hak_akses = $this->session->userdata('hak_akses');
  $data['jumlah_anomali'] = '';
   $data['jumlah_pengajuan_alat'] = '';
  if ($hak_akses == 'Superadmin') {
   $data['jumlah_anomali'] = Modules::run('validasi_anomali/getDataAnomali')['total_rows'];
   $data['jumlah_pengajuan_alat'] = Modules::run("alat/getTotalPengajuanDataAlatBaru");
   $data['jumlah_usulan'] = $this->getJumlahUsulan();
//   echo '<pre>';
//   print_r($data);die;
  }
  echo $this->load->view('template_view', $data, true);
 }

 public function getJumlahUsulan() {
  $total = Modules::run('database/count_all', array(
              'table' => 'usulan u',
              'field' => array('u.*', 'ut.upt as nama_upt', 'us.status'),
              'join' => array(
                  array('upt ut', 'u.upt = ut.id'),
                  array('(select max(id) id, usulan from usulan_status group by usulan) uss', 'uss.usulan = u.id'),
                  array('usulan_status us', 'us.id = uss.id'),
              ),
              'where' => "u.deleted = 0 and us.status = 'DRAFT'"
  ));
  
  return $total;
 }

}
