<?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
 <li class="active">
  <a class="waves-effect" href="<?php echo base_url() . 'dashboard' ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Dashboard </span></a>
 </li>
 <!-- <li>
   <a class="waves-effect" href="<?php echo base_url() . 'grafik' ?>" aria-expanded="false"><i class="icon-pie-chart fa-fw"></i> <span class="hide-menu"> Grafik </span></a>
  </li>-->
<?php } ?>   

<?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-doc fa-fw"></i> <span class="hide-menu"> Master </span></a>
  <ul aria-expanded="false" class="collapse">     
   <li> <a href="<?php echo base_url() . 'upt' ?>"><?php echo 'UPT' ?></a> </li>
   <li> <a href="<?php echo base_url() . 'gardu_induk' ?>"><?php echo 'Gardu Induk' ?></a> </li>
   <li> <a href="<?php echo base_url() . 'ultg' ?>"><?php echo 'ULTG' ?></a> </li>
   <li> <a href="<?php echo base_url() . 'pdkb' ?>"><?php echo 'PDKB' ?></a> </li>
   <li> <a href="<?php echo base_url() . 'pegawai' ?>"><?php echo 'Pegawai' ?></a> </li>
   <!--<li> <a href="<?php echo base_url() . 'user' ?>"><?php echo 'User' ?></a> </li>-->
   <li> <a href="<?php echo base_url() . 'ttd' ?>"><?php echo 'Tanda Tangan Pegawai' ?></a> </li>
  </ul>
 </li> 
<?php } ?>

<?php if ($this->session->userdata('hak_akses') == 'Superadmin' || $this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-settings fa-fw"></i> <span class="hide-menu"> Peralatan </span></a>
  <ul aria-expanded="false" class="collapse">     
   <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
    <li> <a href="<?php echo base_url() . 'alat' ?>"><?php echo 'Alat' ?></a> </li>
    <li> <a href="<?php echo base_url() . 'status_alat' ?>"><?php echo 'Status Alat' ?></a> </li>
    <li> <a href="<?php echo base_url() . 'usulan' ?>"><?php echo 'Usulan Alat' ?> &nbsp; <label class="label label-warning font-10"><?php echo isset($jumlah_usulan) ? $jumlah_usulan : '' ?><label></a> </li>           
   <?php } ?>
   <li> <a href="<?php echo base_url() . 'alat/alat_baru' ?>"><?php echo 'Pengajuan Alat Baru' ?> &nbsp;
     <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
      <label class="label label-warning font-10"><?php echo isset($jumlah_pengajuan_alat) ? $jumlah_pengajuan_alat : '' ?><label>
       <?php } ?>
       </a> 
       </li>
       <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
        <li> <a href="<?php echo base_url() . 'nama_alat' ?>"><?php echo 'Nama Alat' ?></a> </li>
       <?php } ?>       
       <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
        <li> <a href="<?php echo base_url() . 'alat' ?>"><?php echo 'Alat' ?></a> </li>           
        <li> <a href="<?php echo base_url() . 'usulan' ?>"><?php echo 'Usulan Alat' ?></a> </li>           
       <?php } ?>   
       <li> <a href="<?php echo base_url() . 'pemakaian_alat' ?>"><?php echo 'Pemakaian Alat' ?></a> </li>

       <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
        <li> <a href="<?php echo base_url() . 'regualat' ?>"><?php echo 'Regu Pemeliharaan Alat' ?></a> </li>
        <li> <a href="<?php echo base_url() . 'stockin' ?>"><?php echo 'Stok Masuk Alat' ?></a> </li>
        <li> <a href="<?php echo base_url() . 'import_alat' ?>"><?php echo 'Import Alat' ?></a> </li>
        <li> <a href="<?php echo base_url() . 'kategori_alat' ?>"><?php echo 'Kategori Alat' ?></a> </li>
       <?php } ?>       
       <li> <a href="<?php echo base_url() . 'sertifikasi_kalibrasi' ?>"><?php echo 'Sertifikasi Kalibrasi' ?></a> </li>
       <li> <a href="<?php echo base_url() . 'sertifikasi_angkat' ?>"><?php echo 'Sertifikasi Angkat Angkut' ?></a> </li>
       <li> <a href="<?php echo base_url() . 'sertifikasi_operator' ?>"><?php echo 'Sertifikasi Operator' ?></a> </li>
       <li> <a href="<?php echo base_url() . 'servis' ?>"><?php echo 'Servis Peralatan' ?></a> </li>
       <?php
       if ($this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI' ||
               $this->session->userdata('hak_akses') == 'Superadmin') {
        ?>        
        <li> <a href="<?php echo base_url() . 'regu_pemeliharaan' ?>"><?php echo 'Regu Pemeliharaan' ?></a> </li>   
       <?php } ?>
       </ul>
       </li> 
      <?php } ?>


      <?php if ($this->session->userdata('hak_akses') == 'Superadmin' || $this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
       <li>
        <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-book-open fa-fw"></i> <span class="hide-menu"> Anomali Peralatan </span></a>
        <ul aria-expanded="false" class="collapse">     
         <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
          <li> <a href="<?php echo base_url() . 'anomali' ?>"><?php echo 'Anomali' ?></a> </li>
          <li> <a href="<?php echo base_url() . 'anomali/add' ?>"><?php echo 'Tambah Anomali' ?></a> </li>
         <?php } ?>   

         <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
          <li> <a href="<?php echo base_url() . 'validasi_anomali' ?>"><?php echo 'Validasi Anomali' ?> &nbsp;<label class="label label-warning font-10"><?php echo isset($jumlah_anomali) ? $jumlah_anomali : '' ?></label></a>  </li>
          <li> <a href="<?php echo base_url() . 'anomali_tervalidasi' ?>"><?php echo 'Anomali Tervalidasi' ?></a> </li>
         <?php } ?>   
        </ul>
       </li> 
      <?php } ?>

      <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
       <li>
        <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-settings fa-fw"></i> <span class="hide-menu"> Peminjaman Alat </span></a>
        <ul aria-expanded="false" class="collapse">     
         <li> <a href="<?php echo base_url() . 'histori_pinjam_internal' ?>"><?php echo 'Histori Pengajuan Internal' ?></a> </li>
         <li> <a href="<?php echo base_url() . 'histori_pinjam_eksternal' ?>"><?php echo 'Histori Pengajuan Eksternal' ?></a> </li>
        </ul>
       </li> 
      <?php } ?>


