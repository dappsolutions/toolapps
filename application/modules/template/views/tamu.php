<?php if ($this->session->userdata('hak_akses') == 'TAMU') { ?>
 <li class="active">
  <a class="waves-effect" href="<?php echo base_url() . 'dashboard' ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Dashboard </span></a>
 </li>

 <li>
  <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-settings fa-fw"></i> <span class="hide-menu"> Peralatan </span></a>
  <ul aria-expanded="false" class="collapse">     
		<li> <a href="<?php echo base_url() . 'alat' ?>"><?php echo 'Alat' ?></a> </li>
  </ul>
 </li> 
<?php } ?>   
