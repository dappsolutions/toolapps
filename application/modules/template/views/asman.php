<?php if ($this->session->userdata('hak_akses') == 'ASMAN') { ?>
<li class="active">
    <a class="waves-effect" href="<?php echo base_url() . 'dashboard' ?>" aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i> <span class="hide-menu"> Dashboard </span></a>
</li>

<li>
    <a class="waves-effect" href="javascript:void(0);" aria-expanded="false"><i class="icon-settings fa-fw"></i> <span class="hide-menu"> Peminjaman Alat </span></a>
    <ul aria-expanded="false" class="collapse">     
        <li> <a href="<?php echo base_url() . 'pinjam_internal' ?>"><?php echo 'Pengajuan Internal' ?></a> </li>
        <li> <a href="<?php echo base_url() . 'pinjam_eksternal' ?>"><?php echo 'Pengajuan Eksternal' ?></a> </li>
        <li> <a href="<?php echo base_url() . 'histori_pinjam_internal' ?>"><?php echo 'Histori Pengajuan Internal' ?></a> </li>
        <li> <a href="<?php echo base_url() . 'histori_pinjam_eksternal' ?>"><?php echo 'Histori Pengajuan Eksternal' ?></a> </li>
    </ul>
</li> 
<?php } ?>   