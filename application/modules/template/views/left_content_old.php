<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
 <ul class="nav">
  <li class="nav-item nav-profile">
   <div class="nav-link">
    <div class="user-wrapper">
     <hr/>
     <div class="profile-image">
      <img src="<?php echo base_url() ?>assets/images/faces-clipart/pic-4.png" alt="profile image">
     </div>
     <div class="text-wrapper">
      <p class="profile-name"><?php echo strtoupper($this->session->userdata('username')); ?></p>
      <div>
       <small class="designation text-muted"><?php echo $this->session->userdata('hak_akses'); ?></small>
       <span class="status-indicator online"></span>
      </div>
     </div>
    </div>
    <button class="btn btn-success btn-block" onclick="Template.homeApps(this)">ToolAPPS
     <!--<i class="mdi mdi-plus"></i>-->
    </button>
   </div>
  </li>
  <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
   <li class="nav-item">
    <a class="nav-link" href="<?php echo base_url() . 'dashboard' ?>">
     <i class="menu-icon mdi mdi-television"></i>
     <span class="menu-title">Dashboard</span>
    </a>
   </li>
   <li class="nav-item">
    <a class="nav-link" href="<?php echo base_url() . 'grafik' ?>">
     <i class="menu-icon mdi mdi-chart-line"></i>
     <span class="menu-title">Grafik</span>
    </a>
   </li>
  <?php } ?>
  <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
   <li class="nav-item">   
    <a class="nav-link" data-toggle="collapse" href="#master" aria-expanded="false" aria-controls="master">
     <i class="menu-icon mdi mdi-content-copy"></i>
     <span class="menu-title">Master</span>
     <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="master">
     <ul class="nav flex-column sub-menu">
      <li class="nav-item">
       <a class="nav-link" href="<?php echo base_url() . 'upt' ?>">UPT</a>
      </li>
      <li class="nav-item">
       <a class="nav-link" href="<?php echo base_url() . 'gardu_induk' ?>">Gardu Induk / ULTG</a>
      </li>
      <li class="nav-item">
       <a class="nav-link" href="<?php echo base_url() . 'pegawai' ?>">Pegawai</a>
      </li>
      <li class="nav-item">
       <a class="nav-link" href="<?php echo base_url() . 'user' ?>">User</a>
      </li>
     </ul>
    </div>
   </li>
  <?php } ?>

  <?php
  if ($this->session->userdata('hak_akses') == 'Superadmin' ||
          $this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') {
   ?>
   <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#alat" aria-expanded="false" aria-controls="alat">
     <i class="menu-icon mdi mdi-settings"></i>
     <span class="menu-title">Peralatan</span>
     <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="alat">
     <ul class="nav flex-column sub-menu">
      <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'alat' ?>">Alat</a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'nama_alat' ?>">Nama Alat</a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'stockin' ?>">Stok Masuk Alat</a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'import_alat' ?>">Import Alat</a>
       </li>      
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'kategori_alat' ?>">Kategori Alat</a>
       </li>      
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'sertifikasi_kalibrasi' ?>">Sertifikasi Kalibrasi</a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'servis' ?>">Servis Peralatan</a>
       </li>
      <?php } ?>         
      <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'anomali_alat' ?>">Alat</a>
       </li>     
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'nama_alat' ?>">Nama Alat</a>
       </li>
      <?php } ?>
      <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'regu_pemeliharaan' ?>">Regu Pemeliharaan</a>
       </li>
      <?php } ?>     
     </ul>
    </div>
   </li>
  <?php } ?>


  <?php
  if ($this->session->userdata('hak_akses') == 'Superadmin' ||
          $this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') {
   ?>
   <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#anomali" aria-expanded="false" aria-controls="ui-basic">
     <i class="menu-icon mdi mdi-cube-outline"></i>
     <span class="menu-title">Anomali Peralatan</span>
     <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="anomali">
     <ul class="nav flex-column sub-menu">     
      <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'anomali' ?>">Anomali</a>
       </li>
      <?php } ?>
      <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'anomali/add' ?>">Tambah Anomali</a>
       </li>
      <?php } ?>
      <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'validasi_anomali' ?>">Validasi Anomali <label class="badge badge-warning"><?php echo isset($jumlah_anomali) ? $jumlah_anomali : '' ?></label></a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'anomali_tervalidasi' ?>">Anomali Tervalidasi</a>
       </li>
      <?php } ?>
      <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
       <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() . 'berita_acara' ?>" onclick="Template.showUpdateSystem(this, event)">Berita Acara <label class="badge badge-warning">Upgrade</label></a>
       </li>
      <?php } ?>
     </ul>
    </div>
   </li>
  <?php } ?>

  <?php echo $this->load->view('requestor_peminjaman'); ?>
  <?php echo $this->load->view('asman'); ?>
  <?php echo $this->load->view('toolsman'); ?>
 </ul>
</nav>
<!-- partial -->