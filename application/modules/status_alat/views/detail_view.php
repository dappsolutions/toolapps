<div class="container-fluid">
 <div class="row">
  <div class='col-12'>

   <div class="white-box">
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Status Alat
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $status ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $keterangan ?>
      </div>     
     </div>
     <br/>

     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="StatusAlat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
