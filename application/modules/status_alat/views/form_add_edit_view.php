<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <u><?php echo $title ?></u>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Status Alat
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='status' class='form-control required' 
              value='<?php echo isset($status) ? $status : '' ?>' error="Status Alat"/>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4'>
       <textarea id="keterangan" class="form-control required" error="Keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="StatusAlat.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="StatusAlat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
