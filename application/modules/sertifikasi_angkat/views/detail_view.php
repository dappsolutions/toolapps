<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Alat
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $nama_alat . ' - ' . $kategori . ' - ' . $kode_alat ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tanggal
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $tgl_sertifikasi ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $keterangan ?>       
      </div>     
     </div>
     <br/>

<!--     <div class='row manual_detail <?php echo!isset($file) ? 'display-none' : '' ?>'>
      <div class='col-md-3'>
       Sertifikat Kalibrasi (<b>.Pdf</b>)
      </div>
      <div class='col-md-4'>
       <a href="#" onclick="SertifikasiAngkat.showSertifikat(this, event)" class="label label-danger"><?php echo $file ?></a>        
      </div>
     </div>
     <hr/>    
     <br/>-->

     <div class="row">
      <div class="col-md-7">
       <table class="table color-bordered-table primary-bordered-table" id='data_file'>
        <thead>
         <tr class="bg-primary text-white">
          <th class="font-12">File</th>
          <th class="font-12">Action</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($list_file)) { ?>
          <?php foreach ($list_file as $value) { ?>
           <tr>
            <td class="font-12">
               <?php echo $value['file'] ?>
            </td>
            <td class="font-12">
             <i class="mdi mdi-image mdi-24px hover text-warning" onclick="SertifikasiAngkat.showSertifikat(this, event)"></i>
            </td>
           </tr>
          <?php } ?>
         <?php } ?>        
        </tbody>
       </table>
      </div>
     </div>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn" onclick="SertifikasiAngkat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
