<?php

class Sertifikasi_angkat extends MX_Controller {

    public $segment;
    public $limit;
    public $page;
    public $last_no;

    public function __construct() {
        parent::__construct();
        $this->limit = 10;
    }

    public function getModuleName() {
        return 'sertifikasi_angkat';
    }

    public function getHeaderJSandCSS() {
        $data = array(
            '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
            '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
            '<script src="' . base_url() . 'assets/js/controllers/sertifikasi_angkat.js"></script>'
        );

        return $data;
    }

    public function getTableName() {
        return 'sertifikasi_angkat';
    }

    public function index() {
        $this->segment = 3;
        $this->page = $this->uri->segment($this->segment) ?
                $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;

        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data Sertifikasi Angkat";
        $data['title_content'] = 'Data Sertifikasi Angkat';
        $content = $this->getDataSertifikasi();
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function getTotalDataSertifikasi($keyword = '') {
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('a.alat', $keyword),
                array('a.kode_alat', $keyword),
                array('na.nama_alat', $keyword),
            );
        }

        $upt_id = $this->session->userdata('upt_id');
        $where = "ska.deleted = 0";
        if ($this->session->userdata('hak_akses') != "Superadmin") {
            $where = "ska.deleted = 0 and a.upt = '" . $upt_id . "'";
        }
        $total = Modules::run('database/count_all', array(
                    'table' => $this->getTableName() . ' ska',
                    'field' => array('ska.*', 'a.alat as nama_alat', 
                        'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'ska.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'like' => $like,
                    'is_or_like' => true,
                    'where' => $where
        ));

        return $total;
    }

    public function getDataSertifikasi($keyword = '') {
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('a.alat', $keyword),
                array('a.kode_alat', $keyword),
                array('na.nama_alat', $keyword),
            );
        }

        $upt_id = $this->session->userdata('upt_id');
        $where = "ska.deleted = 0";
        if ($this->session->userdata('hak_akses') != "Superadmin") {
            $where = "ska.deleted = 0 and a.upt = '" . $upt_id . "'";
        }
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' ska',
                    'field' => array('ska.*', 'a.alat as nama_alat', 
                        'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'ska.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'like' => $like,
                    'is_or_like' => true,
                    'limit' => $this->limit,
                    'offset' => $this->last_no,
                    'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }

        return array(
            'data' => $result,
            'total_rows' => $this->getTotalDataSertifikasi($keyword)
        );
    }

    public function getDetailDataSertifikasi($id) {
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' ska',
                    'field' => array('ska.*', 'a.kode_alat', 'na.nama_alat', 
                        'ka.kategori'),
                    'join' => array(
                        array('alat a', 'ska.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'where' => "ska.id = '" . $id . "'"
        ));

        return $data->row_array();
    }

    public function getDataAlat() {
         $upt_id = $this->session->userdata('upt_id');
        $where = "a.deleted = 0";

        if ($this->session->userdata('hak_akses') != "Superadmin") {
//   $where = "deleted = 0 and upt = '" . $upt_id . "'";
            $where = "(a.alat_baru = 0 and a.approver_alat_baru = 0 and a.upt = '" . $upt_id . "' and a.deleted = 0) "
                    . "or (a.alat_baru = 1 and a.approver_alat_baru = 1 and a.upt = '" . $upt_id . "' and a.deleted = 0)";
        }
        $data = Modules::run('database/get', array(
                    'table' => 'alat a',
                    'field' => array('a.*', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }


        return $result;
    }

    public function add() {
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Tambah Sertifikasi Angkat";
        $data['title_content'] = 'Tambah Sertifikasi Angkat';
        $data['list_alat'] = $this->getDataAlat();
        echo Modules::run('template', $data);
    }

    public function ubah($id) {
        $data = $this->getDetailDataSertifikasi($id);
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Ubah Sertifikasi Angkat";
        $data['title_content'] = 'Ubah Sertifikasi Angkat';
        $data['list_alat'] = $this->getDataAlat();
        $data['list_file'] = $this->getListDataFile($id);
        echo Modules::run('template', $data);
    }

    public function getListDataFile($id) {
        $data = Modules::run('database/get', array(
                    'table' => 'file_sertifikasi_angkat fa',
                    'field' => array('fa.*'),
                    'where' => "fa.deleted = 0 and fa.sertifikasi_angkat = '" . $id . "'"
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }


        return $result;
    }

    public function detail($id) {
        $data = $this->getDetailDataSertifikasi($id);
//  echo '<pre>';
//  print_r($data);die;
        $data['view_file'] = 'detail_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Detail Sertifikasi Angkat";
        $data['title_content'] = 'Detail Sertifikasi Angkat';
        $data['list_file'] = $this->getListDataFile($id);
        echo Modules::run('template', $data);
    }

    public function getPostDataHeader($value) {
        $data['alat'] = $value->kode_alat;
        $data['tgl_sertifikasi'] = $value->tanggal;
        $data['keterangan'] = $value->keterangan;
        return $data;
    }

    public function simpan() {
        $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
        $id = $this->input->post('id');
        $is_valid = false;
        $is_save = true;

        $message = "";
        $this->db->trans_begin();
        try {
            $post_head = $this->getPostDataHeader($data);
            if ($id == '') {
                if (!empty($_FILES)) {
                    $total_upload = 0;
                    foreach ($_FILES as $key => $v_file) {
                        $file_upload = $this->uploadData($key);
                        if ($file_upload['is_valid']) {
                            $total_upload += 1;
                        } else {
                            $message = $file_upload['response'];
                        }
                    }
                }

                if ($total_upload == count($_FILES)) {
                    $is_save = true;
                } else {
                    $is_save = false;
                }

                if ($is_save) {
                    $id = Modules::run('database/_insert', $this->getTableName(), $post_head);

                    if (!empty($_FILES)) {
                        foreach ($_FILES as $v_file) {
                            $filename = $v_file['name'];
                            $post_child['sertifikasi_angkat'] = $id;
                            $post_child['file'] = $filename;
                            Modules::run('database/_insert', 'file_sertifikasi_angkat', $post_child);
                        }
                    }
                }
            } else {
                //update
                if (!empty($_FILES)) {
                    $total_upload = 0;
                    foreach ($_FILES as $key => $v_file) {
                        $file_upload = $this->uploadData($key);
                        if ($file_upload['is_valid']) {
                            $total_upload += 1;
                        } else {
                            $message = $file_upload['response'];
                        }
                    }
                }

                if ($total_upload == count($_FILES)) {
                    $is_save = true;
                } else {
                    $is_save = false;
                }


                if ($is_save) {
                    Modules::run('database/_update', $this->getTableName(), $post_head, array('id' => $id));

                    if (!empty($data->file_data)) {
                        foreach ($data->file_data as $v_d) {
                            $file_id = $v_d->id;
                            $filename = $v_d->file;
                            $post_child['sertifikasi_angkat'] = $id;
                            $post_child['file'] = $filename;

                            if ($file_id == "") {
                                Modules::run('database/_insert', 'file_sertifikasi_angkat', $post_child);
                            } else {
                                if ($v_d->deleted == 1) {
                                    $post_child['deleted'] = 1;
                                }

                                Modules::run('database/_update', 'file_sertifikasi_angkat', $post_child, array('id' => $v_d->id));
                            }
                        }
                    }
                }
            }
            $this->db->trans_commit();
            if ($is_save) {
                $is_valid = true;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
    }

    public function search($keyword) {
        $this->segment = 4;
        $this->page = $this->uri->segment($this->segment) ?
                $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;
        $keyword = urldecode($keyword);

        $data['keyword'] = $keyword;
        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data Sertifikasi Angkat";
        $data['title_content'] = 'Data Sertifikasi Angkat';
        $content = $this->getDataSertifikasi($keyword);
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function delete($id) {
        $is_valid = false;
        $this->db->trans_begin();
        try {
            Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid));
    }

    public function uploadData($name_of_field) {
        $config['upload_path'] = 'files/berkas/sertifikasi_angkat/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '1000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';

        $this->load->library('upload', $config);
//  $this->upload->do_upload($name_of_field);

        $is_valid = false;
        if (!$this->upload->do_upload($name_of_field)) {
            $response = $this->upload->display_errors();
        } else {
            $response = $this->upload->data();
            $is_valid = true;
        }

        return array(
            'is_valid' => $is_valid,
            'response' => $response
        );
    }

    public function showSertifikat() {
        $file = str_replace(' ', '_', $this->input->post('file'));
        $data['file'] = $file;
        echo $this->load->view('file_sertifikat', $data, true);
    }

}
