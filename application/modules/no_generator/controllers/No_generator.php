<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class No_generator extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateQrCode() {
  $qr_code = 'QRALAT' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
              'table' => 'alat',
              'like' => array(
                  array('barcode', $qr_code)
              ),
              'orderby' => 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($qr_code, '', $data['barcode']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(3, $seq);
  $qr_code .= $seq;
  return $qr_code;
 }

 public function generateNomerAnomali() {
  $no_anomali = 'ANOMAL' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
              'table' => 'anomali_alat',
              'like' => array(
                  array('no_anomali', $no_anomali)
              ),
              'orderby' => 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_anomali, '', $data['no_anomali']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(3, $seq);
  $no_anomali .= $seq;
  return $no_anomali;
 }
 
 public function generateNomerUsulan() {
  $no_anomali = 'USULAN' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
              'table' => 'usulan',
              'like' => array(
                  array('no_usulan', $no_anomali)
              ),
              'orderby' => 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no_anomali, '', $data['no_usulan']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(3, $seq);
  $no_anomali .= $seq;
  return $no_anomali;
 }

 public function getDataTahun($id) {
  $data = Modules::run('database/get', array(
              'table' => 'tahun',
              'where' => array('id' => $id)
  ));

  return $data->row_array()['tahun'];
 }

 public function getDataKategoriAlat($id) {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_alat',
              'where' => array('id' => $id)
  ));

  return $data->row_array()['kategori'];
 }

 public function getNamaAlat($id) {
  $data = Modules::run('database/get', array(
              'table' => 'nama_alat',
              'field' => array('nama_alat'),
              'where' => array('id' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function generateKodeAlat($value) {
  $tahun = $this->getDataTahun($value->tahun);
  $kategori = $this->getDataKategoriAlat($value->kategori_alat);
  if ($value->alat == 'lain') {
   $alat = $value->nama_alat;
  } else {
   $alat = $this->getNamaAlat($value->alat)['nama_alat'];
  }
  $kode_alat = $kategori . '-' . $alat . '-' . $tahun . '-';
  $data = Modules::run('database/get', array(
              'table' => 'alat',
              'like' => array(
                  array('kode_alat', $kode_alat)
              ),
              'orderby' => 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($kode_alat, '', $data['kode_alat']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(3, $seq);
  $kode_alat .= $seq;
  return $kode_alat;
 }

 public function generateKodeAlatImpor($value) {
  $kode_alat = trim($value[3]) . '-' . trim($value[5]) . '-' . trim($value[8]) . '-';
  $data = Modules::run('database/get', array(
              'table' => 'alat',
              'like' => array(
                  array('kode_alat', $kode_alat)
              ),
              'orderby' => 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($kode_alat, '', $data['kode_alat']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(3, $seq);
  $kode_alat .= $seq;
  return $kode_alat;
 }

 public function generateNoPeminjaman($internal = 'internal') {
  $kode_pinjam = 'PINT' . date('Y') . date('m');
  if ($internal == 'eksternal') {
   $kode_pinjam = 'PEKS' . date('Y') . date('m');
  }
  $data = Modules::run('database/get', array(
              'table' => 'pinjaman',
              'like' => array(
                  array('no_peminjaman', $kode_pinjam)
              ),
              'orderby' => 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($kode_pinjam, '', $data['no_peminjaman']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(3, $seq);
  $kode_pinjam .= $seq;
  return $kode_pinjam;
 }

}
