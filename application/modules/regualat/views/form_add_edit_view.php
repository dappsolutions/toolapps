<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Alat
      </div>
      <div class='col-md-4'>
       <select class="form-control required" id="alat" error="Alat">
        <option value="">Pilih Kode Alat</option>
        <?php if (!empty($list_alat)) { ?>
         <?php foreach ($list_alat as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($alat)) { ?>
           <?php $selected = $alat == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_alat'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Regu Pemeliharaan
      </div>
      <div class='col-md-4'>
       <select class="form-control required" id="regu" error="Alat">
        <option value="">Pilih Regu Pemeliharaan</option>
        <?php if (!empty($list_regu)) { ?>
         <?php foreach ($list_regu as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($regu_pemeliharaan)) { ?>
           <?php $selected = $regu_pemeliharaan == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['regu'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Regualat.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="Regualat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
