<?php

class Regualat extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'regualat';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/regualat.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'alat_has_regu_pemeliharaan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Regu Peralatan";
  $data['title_content'] = 'Data Regu Peralatan';
  $content = $this->getDataRegualat();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataRegualat($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('a.kode_alat', $keyword),
   );
  }

  $upt_id = $this->session->userdata('upt_id');
  $where = "u.deleted = 0 and a.approver_alat_baru != '-1'";
  if ($this->session->userdata('hak_akses') != "Superadmin") {
   $where = "u.deleted = 0 and a.approver_alat_baru != '-1' and a.upt = '" . $upt_id . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*', 'a.kode_alat', 'rp.regu'),
              'join' => array(
                  array('alat a', 'u.alat = a.id and a.deleted = 0'),
                  array('regu_pemeliharaan rp', 'u.regu_pemeliharaan = rp.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $where
  ));

  return $total;
 }

 public function getDataRegualat($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('a.kode_alat', $keyword),
   );
  }

  $upt_id = $this->session->userdata('upt_id');
  $where = "u.deleted = 0 and a.approver_alat_baru != '-1'";
  if ($this->session->userdata('hak_akses') != "Superadmin") {
   $where = "u.deleted = 0 and a.approver_alat_baru != '-1' and a.upt = '" . $upt_id . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*', 'a.kode_alat', 'rp.regu'),
              'join' => array(
                  array('alat a', 'u.alat = a.id and a.deleted = 0'),
                  array('regu_pemeliharaan rp', 'u.regu_pemeliharaan = rp.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataRegualat($keyword)
  );
 }

 public function getDetailDataRegualat($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*', 'a.kode_alat', 'rp.regu'),
              'join' => array(
                  array('alat a', 'kr.alat = a.id'),
                  array('regu_pemeliharaan rp', 'kr.regu_pemeliharaan = rp.id'),
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  return $data->row_array();
 }

 public function getListalat() {
  $upt_id = $this->session->userdata('upt_id');
  $where = "a.deleted = 0";

  if ($this->session->userdata('hak_akses') != "Superadmin") {
//   $where = "deleted = 0 and upt = '" . $upt_id . "'";
   $where = "(a.alat_baru = 0 and a.approver_alat_baru = 0 and a.upt = '" . $upt_id . "' and a.deleted = 0) "
           . "or (a.alat_baru = 1 and a.approver_alat_baru = 1 and a.upt = '" . $upt_id . "' and a.deleted = 0)";
  }
  $data = $data = Modules::run('database/get', array(
              'table' => 'alat a',
              'field' => array('a.*', 'sa.status'),
              'join' => array(
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
      FROM anomali_alat_detail aad 
      JOIN anomali_alat aa ON aad.anomali_alat = aa.id
      WHERE aa.validation != 0
      GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'a.id = alat_anomali.alat and a.deleted = 0', 'left'),
                  array('status_alat sa', 'alat_anomali.status_alat = sa.id', 'left')
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }

 public function getListReguPemeliharaan() {
  $data = Modules::run('database/get', array(
              'table' => 'regu_pemeliharaan  rp',
              'where' => "rp.deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Regu Alat";
  $data['title_content'] = 'Tambah Regu Alat';
  $data['list_alat'] = $this->getListalat();
  $data['list_regu'] = $this->getListReguPemeliharaan();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataRegualat($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Regu Alat";
  $data['title_content'] = 'Ubah Regu Alat';
  $data['list_alat'] = $this->getListalat();
  $data['list_regu'] = $this->getListReguPemeliharaan();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataRegualat($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Regualat";
  $data['title_content'] = 'Detail Regualat';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['alat'] = $value->alat;
  $data['regu_pemeliharaan'] = $value->regu;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
//  echo '<pre>';
//  print_r($data);die;

  $this->db->trans_begin();
  $message = '';
  try {
   $post = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   if ($message == '') {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Regualat";
  $data['title_content'] = 'Data Regualat';
  $content = $this->getDataRegualat($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
