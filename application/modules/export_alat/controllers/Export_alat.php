<?php

class Export_alat extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'export_alat';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<link rel="stylesheet" href="' . base_url() . 'assets/css/stickyTable.min.css">',
      '<script src="' . base_url() . 'assets/js/stickyTable.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/export_alat.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'alat';
 }

 public function index($keyword = "") {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Export Alat";
  $data['title_content'] = 'Data Export Alat';
  $content = $this->getDataExport_alat($keyword);
//  echo '<pre>';
//  print_r($content);die;
  $data['content'] = $content['data'];
  $data['list_kategori'] = $this->getListKategoriAlat();
  echo Modules::run('template', $data);
 }

 public function getListKategoriAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_alat ka',
              'where' => "ka.deleted = 0"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getDataExport_alat($keyword = '') {
  $like = array();
  
  $where = "u.deleted = 0";
  if($this->akses != 'Superadmin'){
   $where = "ut.id = '".$this->upt."' and u.deleted = 0";
  }
  
  if($keyword != ''){
   $where .= " and ka.id = '".$keyword."'";
//   echo $where;die;
  }
   
  $data = Modules::run('database/get', array(
	'table' => $this->getTableName() . ' u',
	'field' => array('u.*', 'ka.kategori',
		't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
		'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat', 'gi.context'),
	'join' => array(
		array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
		array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
		array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
		array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
		array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
		array('status_alat sa', 'sa.id = u.status_alat', 'left')
	),
	'like' => $like,
	'is_or_like' => true,
	'limit' => 10000,
	'where' => $where,
	'orderby' => 'u.id desc'
  ));


//   echo '<pre>';
//   print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
  );
 }
}
