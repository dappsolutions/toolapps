<div class='container-fluid'>

 <div class="white-box">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <a class="btn btn-success" download="<?php echo 'Peraltan' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_alat', 'Peralatan');">Export</a>
    </div>
    <div class='col-md-10'>
     <select class="form-control required" id="kategori" 
             error="Kategori" onchange="ExportAlat.getByKategori(this)">
      <option value="">Pilih Kategori</option>
      <?php if (!empty($list_kategori)) { ?>
       <?php foreach ($list_kategori as $value) { ?>
        <?php $selected = '' ?>
        <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
       <?php } ?>
      <?php } ?>
     </select>
    </div>     
   </div>        
   <br/>
   <!--   <div class='row'>
       <div class='col-md-12'>-->
   <?php if (isset($keyword)) { ?>
    <?php if ($keyword != '') { ?>
              <!--Cari Data : "<b><?php echo $keyword; ?></b>"-->
    <?php } ?>
   <?php } ?>
   <!--    </div>
      </div>-->
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <div class="sticky-table sticky-headers sticky-ltr-cells">
       <table class="table color-bordered-table primary-bordered-table" id="tb_alat">
        <thead>
         <tr class="sticky-row">
          <th class="font-12 bg-primary">No</th>
          <th class="font-12 bg-primary">UPT</th>
          <th class="font-12 bg-primary">Pemilik Alat</th>
          <th class="font-12 bg-primary">Kode Alat</th>
          <th class="font-12 bg-primary">Nomer Seri</th>
          <th class="font-12 bg-primary">Nama Alat</th>
          <th class="font-12 bg-primary">Merk Alat</th>
          <th class="font-12 bg-primary">Kategori Alat</th>
          <th class="font-12 bg-primary">Tipe Alat</th>
          <th class="font-12 bg-primary">Status Alat</th>
          <th class="font-12 bg-primary">Tahun Perolehan</th>
          <th class="font-12 bg-primary">Keterangan</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($content)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($content as $value) { ?>
           <tr>
            <td class="font-12"><?php echo $no++ ?></td>
            <td class="font-12"><?php echo $value['nama_upt'] ?></td>
            <td class="font-12"><?php echo $value['context'].' - '.$value['gardu'] ?></td>
            <td class="font-12"><?php echo '<b>' . $value['kode_alat'] . '</b>' ?></td>
            <td class="font-12"><?php echo $value['nomer_seri'] ?></td>
            <td class="font-12"><?php echo $value['nama_alat'] ?></td>
            <td class="font-12"><?php echo $value['merk'] ?></td>
            <td class="font-12"><?php echo $value['kategori'] ?></td>
            <td class="font-12"><?php echo $value['tipe'] ?></td>
            <td class="font-12">
			<?php if ($value['status_alat'] != '') { ?>
              <?php echo $value['status_alat'] != 'Normal' ? '<label class="label label-danger font-12">' . $value['status_alat'] . '</label>' : '<label class="label label-success font-12">' . $value['status_alat'] . '<label>' ?>
              &nbsp;
             <?php } else { ?>
              <label class="label label-success font-12">Normal<label>
               <?php } ?>
            </td>
            <td class="font-12"><?php echo $value['tahun_perolehan'] ?></td>
            <td class="font-12"><?php echo $value['keterangan'] ?></td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td class="text-center" colspan="13">Tidak Ada Data Ditemukan</td>
          </tr>
         <?php } ?>         
        </tbody>
       </table>
      </div>      
     </div>
    </div>
   </div>    
   <br/>
  </div>
 </div>
</div>

