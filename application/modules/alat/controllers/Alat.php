<?php

class Alat extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt;
 public $hak_akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
  $this->upt = $this->session->userdata('upt_id');
  $this->hak_akses = $this->session->userdata('hak_akses');

//   echo '<pre>';
//   print_r($_SESSION);die;
 }

 public function getModuleName() {
  return 'alat';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<link rel="stylesheet" href="' . base_url() . 'assets/css/stickyTable.min.css">',
      '<script src="' . base_url() . 'assets/js/stickyTable.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/alat.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'alat';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Alat";
  $data['title_content'] = 'Data Alat';
  $content = $this->getDataAlat();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataAlat($keyword = '') {  
  $like = array();
  if ($keyword != '') {
   $keyword = urlencode($keyword);
   
   $like = array(
       array('na.nama_alat', $keyword),
       array('u.barcode', $keyword),
       array('ka.kategori', $keyword),
       array('u.tipe', $keyword),
       array('ut.upt', $keyword),
       array('t.tahun', $keyword),
       array('u.nomer_seri', $keyword),
       array('gi.gardu', $keyword),
   );
  }

  $where = "((u.alat_baru = 0 and u.approver_alat_baru = 0) or (u.alat_baru = 1 and u.approver_alat_baru = 1))";
  if ($this->hak_akses != 'Superadmin') {
   $where = "((u.alat_baru = 0 and u.approver_alat_baru = 0 and ut.id = '" . $this->upt . "') or (u.alat_baru = 1 and u.approver_alat_baru = 1 and ut.id = '" . $this->upt . "'))";
  }

  if ($keyword == '') {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' u',
               'field' => array('u.*', 'ka.kategori',
                   't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                   'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat'),
               'join' => array(
                   array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                   array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                   array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                   array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                   array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                   array('status_alat sa', 'sa.id = u.status_alat', 'left')
               ),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where,
   ));
  } else {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' u',
               'field' => array('u.*', 'ka.kategori',
                   't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                   'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat'),
               'join' => array(
                   array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                   array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                   array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                   array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                   array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                   array('status_alat sa', 'sa.id = u.status_alat', 'left')
               ),
               'like' => $like,
               'is_or_like' => true,
               'inside_bracktes' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where,
   ));
  }

  return $total;
 }

 public function getTotalDataAlatBaru($keyword = '') {  
  $like = array();
  if ($keyword != '') {
   $keyword = urldecode($keyword);
   $like = array(
       array('na.nama_alat', $keyword),
       array('u.barcode', $keyword),
       array('ka.kategori', $keyword),
       array('u.tipe', $keyword),
       array('ut.upt', $keyword),
       array('t.tahun', $keyword),
       array('u.nomer_seri', $keyword),
       array('gi.gardu', $keyword),
   );
  }

  $where = "((u.alat_baru = 1 and u.approver_alat_baru = 0 and ut.id = '" . $this->upt . "') or (u.alat_baru = 1 and u.approver_alat_baru = 1 and ut.id = '" . $this->upt . "'))";
  if ($this->session->userdata('hak_akses') == "Superadmin") {
   $where = "((u.alat_baru = 1 and u.approver_alat_baru = 0))";
  }

  if ($keyword == '') {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' u',
               'field' => array('u.*', 'ka.kategori',
                   't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                   'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat'),
               'join' => array(
                   array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                   array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                   array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                   array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                   array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                   array('status_alat sa', 'sa.id = u.status_alat', 'left')
               ),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where,
               'orderby' => 'u.id desc'
   ));
  } else {
   $total = Modules::run('database/count_all', array(
               'table' => $this->getTableName() . ' u',
               'field' => array('u.*', 'ka.kategori',
                   't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                   'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat'),
               'join' => array(
                   array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                   array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                   array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                   array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                   array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                   array('status_alat sa', 'sa.id = u.status_alat', 'left')
               ),
               'like' => $like,
               'is_or_like' => true,
               'inside_brackets' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where,
               'orderby' => 'u.id desc'
   ));
  }

  return $total;
 }

 public function getTotalPengajuanDataAlatBaru($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $keyword = urldecode($keyword);
   echo $keyword;die;
   $like = array(
       array('na.nama_alat', $keyword),
       array('u.barcode', $keyword),
       array('ka.kategori', $keyword),
       array('u.tipe', $keyword),
       array('ut.upt', $keyword),
       array('t.tahun', $keyword),
       array('u.nomer_seri', $keyword),
       array('gi.gardu', $keyword),
       array('sa.status', $keyword),
   );
  }

  $where = "(u.alat_baru = 1 and u.approver_alat_baru = 0)";
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' u',
              'field' => array('u.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                  'gi.gardu', 'alat_anomali.status_alat AS status_alat', 'na.nama_alat'),
              'join' => array(
                  array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                  array('tahun t', 'u.tahun = t.id and u.deleted = 0'),
                  array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                  array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'u.id = alat_anomali.alat and u.deleted = 0', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $where,
  ));

  return $total;
 }

 public function getDataAlat($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('na.nama_alat', $keyword),
       array('u.barcode', $keyword),
       array('ka.kategori', $keyword),
       array('u.tipe', $keyword),
       array('ut.upt', $keyword),
       array('t.tahun', $keyword),
       array('u.nomer_seri', $keyword),
       array('gi.gardu', $keyword),
       array('sa.status', $keyword),
   );
  }
  $where = "((u.alat_baru = 0 and u.approver_alat_baru = 0) or (u.alat_baru = 1 and u.approver_alat_baru = 1))";

  if ($this->hak_akses != 'Superadmin') {
   $where = "((u.alat_baru = 0 and u.approver_alat_baru = 0 and ut.id = '" . $this->upt . "') or (u.alat_baru = 1 and u.approver_alat_baru = 1 and ut.id = '" . $this->upt . "'))";
  }


  if ($keyword == '') {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' u',
               'field' => array('u.*', 'ka.kategori',
                   't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                   'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat', 'gi.context'),
               'join' => array(
                   array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                   array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                   array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                   array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                   array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                   array('status_alat sa', 'sa.id = u.status_alat', 'left')
               ),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where,
               'orderby' => 'u.id desc'
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' u',
               'field' => array('u.*', 'ka.kategori',
                   't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                   'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat', 'gi.context'),
               'join' => array(
                   array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                   array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                   array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                   array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                   array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                   array('status_alat sa', 'sa.id = u.status_alat', 'left')
               ),
               'like' => $like,
               'is_or_like' => true,
               'inside_bracktes' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where,
               'orderby' => 'u.id desc'
   ));
  }


//
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAlat($keyword)
  );
 }

 public function getDataAlatBaru($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('na.nama_alat', $keyword),
       array('u.barcode', $keyword),
       array('ka.kategori', $keyword),
       array('u.tipe', $keyword),
       array('ut.upt', $keyword),
       array('t.tahun', $keyword),
       array('u.nomer_seri', $keyword),
       array('gi.gardu', $keyword),
   );
  }

  $where = "((u.alat_baru = 1 and u.approver_alat_baru = 0 and ut.id = '" . $this->upt . "') or (u.alat_baru = 1 and u.approver_alat_baru = 1 and ut.id = '" . $this->upt . "'))";
  if ($this->session->userdata('hak_akses') == "Superadmin") {
   $where = "((u.alat_baru = 1 and u.approver_alat_baru = 0))";
  }

  if ($keyword == '') {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' u',
               'field' => array('u.*', 'ka.kategori',
                   't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                   'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat'),
               'join' => array(
                   array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                   array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                   array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                   array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                   array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                   array('status_alat sa', 'sa.id = u.status_alat', 'left')
               ),
               'like' => $like,
               'is_or_like' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where,
               'orderby' => 'u.id desc'
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' u',
               'field' => array('u.*', 'ka.kategori',
                   't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
                   'gi.gardu', 'sa.status AS status_alat', 'na.nama_alat'),
               'join' => array(
                   array('nama_alat na', 'u.alat = na.id and u.deleted = 0'),
                   array('kategori_alat ka', 'na.kategori_alat = ka.id and u.deleted = 0'),
                   array('tahun t', 'u.tahun = t.id and u.deleted = 0', 'left'),
                   array('upt ut', 'u.upt = ut.id and u.deleted = 0'),
                   array('gardu_induk gi', 'u.gardu_induk = gi.id and u.deleted = 0', 'left'),
                   array('status_alat sa', 'sa.id = u.status_alat', 'left')
               ),
               'like' => $like,
               'is_or_like' => true,
               'inside_brackets' => true,
               'limit' => $this->limit,
               'offset' => $this->last_no,
               'where' => $where,
               'orderby' => 'u.id desc'
   ));
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAlatBaru($keyword)
  );
 }

 public function getDetailDataAlat($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' a',
              'field' => array('a.*', 'ka.kategori', 'u.upt as nama_upt',
                  't.tahun as tahun_perolehan',
                  'tp.tahun as tahun_produksi_alat', 'gi.gardu as nama_gardu',
                  'na.nama_alat', 'na.kategori_alat', 'sa.status', 'gi.context'),
              'join' => array(
                  array('nama_alat na', 'a.alat = na.id'),
                  array('kategori_alat ka', 'na.kategori_alat = ka.id'),
                  array('upt u', 'a.upt = u.id'),
                  array('tahun t', 'a.tahun = t.id', 'left'),
                  array('tahun tp', 'a.tahun_produksi = tp.id', 'left'),
                  array('gardu_induk gi', 'a.gardu_induk = gi.id', 'left'),
                  array('status_alat sa', 'sa.id = a.status_alat', 'left'),
              ),
              'where' => "a.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListKategoriAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_alat',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTipeAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'tipe_alat',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTahun() {
  $data = Modules::run('database/get', array(
              'table' => 'tahun',
              'where' => array('deleted' => 0),
              'orderby' => 'tahun asc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListGardu() {
  $where = "gi.deleted = 0";
  if ($this->hak_akses != 'Superadmin') {
   $where = "gi.deleted = 0 and gi.upt = '" . $this->upt . "'";
  }
  $data = Modules::run('database/get', array(
			  'table' => 'gardu_induk gi',
			  'field' => array('gi.*', 'ut.upt as nama_upt'),
			  'join' => array(
				  array('upt ut', 'ut.id = gi.upt')
			  ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListNamaAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'nama_alat',
              'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListStatusAlat() {
  $data = Modules::run('database/get', array(
              'table' => 'status_alat sa',
              'field' => array('sa.*'),
              'where' => "sa.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add($baru = "") {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Alat";
  $data['title_content'] = 'Tambah Alat';
  $data['list_kategori'] = $this->getListKategoriAlat();
  $data['list_upt'] = $this->getListUpt();
  $data['list_tahun'] = $this->getListTahun();
  $data['list_gardu'] = $this->getListGardu();
  $data['list_nama_alat'] = $this->getListNamaAlat();
  $data['list_status'] = $this->getListStatusAlat();

  $data['baru'] = $baru;
  $data['upt'] = $this->upt;
  $data['hak_akses'] = $this->hak_akses;
  echo Modules::run('template', $data);
 }

 public function ubah($id, $baru = "") {
  $data = $this->getDetailDataAlat($id);
  $data['tanggal_kalibrasi'] = $this->getLastDateKalibrasi($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Alat";
  $data['title_content'] = 'Ubah Alat';
  $data['list_foto'] = $this->getListFotoAlat($id);
  $data['list_kategori'] = $this->getListKategoriAlat();
  $data['list_upt'] = $this->getListUpt();
  $data['list_tahun'] = $this->getListTahun();
  $data['list_gardu'] = $this->getListGardu();
  $data['list_nama_alat'] = $this->getListNamaAlat();
  $data['list_status'] = $this->getListStatusAlat();
  $data['baru'] = $baru;
//  if ($this->hak_akses != 'Superadmin') {
//   $data['upt'] = $this->upt;
//  }
  $data['hak_akses'] = $this->hak_akses;
  echo Modules::run('template', $data);
 }

 public function getListFotoAlat($alat) {
  $data = Modules::run('database/get', array(
              'table' => 'alat_has_foto',
              'where' => array('alat' => $alat)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getLastDateKalibrasi($alat) {
  $data = Modules::run('database/get', array(
              'table' => 'sertifikasi_kalibrasi_alat ska',
              'field' => array('ska.*'),
              'where' => "ska.deleted = 0 and ska.alat = '".$alat."'",
              'orderby'=> 'ska.tanggal_kalibrasi desc'
  ));
  
  $date = "";
  if (!empty($data)) {
   $data = $data->row_array();
   $date = $data['tanggal_kalibrasi'];
  }


  return $date;
 }
 
 
 public function detail($id) {
  $data = $this->getDetailDataAlat($id);
  $data['tanggal_kalibrasi'] = $this->getLastDateKalibrasi($id);
//  echo '<pre>';
//  print_r($data);die;
  $this->make_barcode($data['barcode']);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Alat";
  $data['title_content'] = 'Detail Alat';  
  $data['list_foto'] = $this->getListFotoAlat($id);
  $data['barcode_bar'] = $this->getBarcode128($data['barcode']);
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {

  $alat = $value->alat;
  $data['status_alat'] = $value->status_alat;
  if ($value->alat == 'lain') {
   $post_nama['nama_alat'] = $value->nama_alat;
   $post_nama['kategori_alat'] = $value->kategori_alat;
   $alat = Modules::run('database/_insert', 'nama_alat', $post_nama);
  }

  $data['barcode'] = Modules::run('no_generator/generateQrCode');
  $data['kode_alat'] = Modules::run('no_generator/generateKodeAlat', $value);
  $data['alat'] = $alat;
  $data['tipe'] = $value->tipe_alat;
  $data['upt'] = $value->upt;
  ;
  $data['tanggal_kalibrasi'] = $value->tgl_kalibrasi;
  $data['keterangan'] = $value->keterangan;
  $data['nomer_seri'] = $value->nomer_seri;
  $data['merk'] = $value->merk;
  $data['tahun'] = $value->tahun;
  $data['kelengkapan_alat'] = $value->kelengkapan;
  $data['tonase'] = $value->tonase;
  $data['nilai_perolehan'] = $value->nilai_perolehan;
  $data['tahun_produksi'] = $value->tahun_produksi;
  if ($value->gardu_induk != '') {
   $data['gardu_induk'] = $value->gardu_induk;
  }

  if ($this->session->userdata('hak_akses') != 'Superadmin') {
   $data['alat_baru'] = 1;
  }
  return $data;
 }

 public function getPostAlatFoto($alat, $value) {
  $data['alat'] = $alat;
  $data['title'] = $value->keterangan_foto;
  $data['foto'] = $value->img;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;
  $alat = $id;
  $this->db->trans_begin();
  try {
   $post_alat = $this->getPostDataHeader($data);
//   echo '<pre>';
//   print_r($post_alat);die;
   if ($id == '') {
    if (!empty($_FILES)) {
     $this->uploadData("file");
     $post_alat['manual_book'] = $_FILES['file']['name'];
    }
    $alat = Modules::run('database/_insert', $this->getTableName(), $post_alat);
    if (!empty($data->detail_image)) {
     foreach ($data->detail_image as $value) {
      $post_alat_foto = $this->getPostAlatFoto($alat, $value);
      Modules::run('database/_insert', 'alat_has_foto', $post_alat_foto);
     }
    }
   } else {
    //update
    unset($post_alat['barcode']);

    if (!empty($_FILES)) {
     $this->uploadData("file");
     $post_alat['manual_book'] = $_FILES['file']['name'];
    }
    Modules::run('database/_update', $this->getTableName(), $post_alat, array('id' => $id));
    if (!empty($data->detail_image)) {
     foreach ($data->detail_image as $value) {
      $post_alat_foto = $this->getPostAlatFoto($alat, $value);
      Modules::run('database/_insert', 'alat_has_foto', $post_alat_foto);
     }
    }

    //update foto
    if (!empty($data->foto_update)) {
     foreach ($data->foto_update as $value) {
      Modules::run('database/_update', 'alat_has_foto', array('title' => $value->title), array('id' => $value->id));
     }
    }

    //hapus foto
    if (!empty($data->foto_remove)) {
     foreach ($data->foto_remove as $value) {
      Modules::run('database/_delete', 'alat_has_foto', array('id' => $value->id));
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
   if ($is_valid) {
    if ($id == '') {
     $this->make_barcode($post_alat['barcode']);
    }
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'alat' => $alat));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
//  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);
//  echo $keyword;die;

  $keyword = str_replace('persen', '%', $keyword);
//  echo $keyword;die;
  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Alat";
  $data['title_content'] = 'Data Alat';
  $content = $this->getDataAlat($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function approveAlatBaru($id) {
  $status = $this->input->post('status');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post['approver_alat_baru'] = $this->session->userdata('user_id');
   if ($status == "REJECT") {
    $post['approver_alat_baru'] = -1;
   }
   Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));

   $post_insert['alat'] = $id;
   $post_insert['status'] = $status;
   Modules::run('database/_insert', 'status_alat_baru', $post_insert);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function make_barcode($barcode) {
  try {
   $this->load->library('ciqrcode');
   $qr_code = $this->ciqrcode;
   $param['data'] = $barcode;
   $param['level'] = 'H';
   $param['size'] = 20;
   $param['savename'] = 'files/qr_code/alat/' . $barcode . '.png';
   $qr_code->generate($param);
  } catch (Exception $ex) {
   
  }
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/manual_book/';
  $config['allowed_types'] = 'pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showManualBook() {
  $manual_book = str_replace(' ', '_', $this->input->post('manual_book'));
  $data['manual_book'] = $manual_book;
  echo $this->load->view('manual_book', $data, true);
 }

 public function alat_baru() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'daftar_alat_baru';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Alat Baru";
  $data['title_content'] = 'Data Alat Baru';
  $content = $this->getDataAlatBaru();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getBarcode128($barcode) {
  require_once APPPATH . '/third_party/barcode/BarcodeGenerator.php'; //php 7
  require_once APPPATH . '/third_party/barcode/BarcodeGeneratorPNG.php'; //php 7
  $generatorPng = new Picqer\Barcode\BarcodeGeneratorPNG();

  $generated = $generatorPng->getBarcode($barcode, $generatorPng::TYPE_CODE_128);
  $barcode = 'data:image/png;base64,' . base64_encode($generated);
  return $barcode;
 }

 public function printBarcode($barcode, $jenis) {
  $mpdf = Modules::run('mpdf/getInitPdf');
  $data['jenis'] = $jenis;
  $data['barcode'] = $barcode;
  $data['barcode_bar'] = $this->getBarcode128($barcode);
  $view = $this->load->view('cetak_barcode', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output($data['barcode'] . '.pdf', 'I');
 }

}
