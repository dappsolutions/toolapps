<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-md-8 grid-margin stretch-card'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>     

     <div class="row">
      <div class='col-md-3'>
       UPT
      </div>
      <div class='col-md-6'>
       <?php $disabled = $hak_akses == 'Superadmin' ? '' : 'disabled' ?>
       <select <?php echo $disabled ?> id="upt" error="UPT" class="form-control required" onchange="Alat.getGardu(this)">
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($upt)) { ?>
           <?php $selected = $value['id'] == $upt ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['upt'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>


     <div class="row">
      <div class='col-md-3'>
       Pemilik Alat
      </div>
      <div class='col-md-6'>
       <select id="gardu_induk" class="form-control">
        <option upt="" value="">--Pilih Gardu Induk / ULTG--</option>
        <?php if (!empty($list_gardu)) { ?>
         <?php foreach ($list_gardu as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($gardu_induk)) { ?>
           <?php $selected = $value['id'] == $gardu_induk ? 'selected' : '' ?>
          <?php } ?>
          <option upt="<?php echo $value['upt'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_upt'].' - '.$value['context'].' - '.$value['gardu'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kategori Alat
      </div>
      <div class='col-md-6'>
       <select id="kategori_alat" error="Kategori Alat" class="form-control required" 
               onchange="Alat.getNamaAlatList(this)">
        <option value="">Pilih Kategori</option>
        <?php if (!empty($list_kategori)) { ?>
         <?php foreach ($list_kategori as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($kategori_alat)) { ?>
           <?php $selected = $value['id'] == $kategori_alat ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       Status Alat
      </div>
      <div class='col-md-6'>
       <select id="status_alat" error="Status Alat" class="form-control required">
        <?php if (!empty($list_status)) { ?>
         <?php foreach ($list_status as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($status_alat)) { ?>
           <?php $selected = $value['id'] == $status_alat ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['status'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tanggal Kalibrasi Terakhir
      </div>
      <div class='col-md-6'>
       <input disabled type="text" value="<?php echo isset($tanggal_kalibrasi) ? $tanggal_kalibrasi != '0000-00-00' ? $tanggal_kalibrasi : '' : '' ?>" id="tgl_kalibrasi" class="form-control" />
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       List Alat
      </div>
      <div class='col-md-6'>
       <select id="alat" error="List Alat" class="form-control required" onchange="Alat.getAlatNama(this)">
        <option value="lain">Lain - lain</option>
        <?php // if ($baru == '') { ?>
         <?php if (!empty($list_nama_alat)) { ?>
          <?php foreach ($list_nama_alat as $value) { ?>
           <?php $selected = "" ?>
           <?php if (isset($alat)) { ?>
            <?php $selected = $value['id'] == $alat ? 'selected' : '' ?>
           <?php } ?>
           <option kategori="<?php echo $value['kategori_alat'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_alat'] ?></option>
          <?php } ?>
         <?php } ?>
        <?php // } ?>        
       </select>
      </div>     
     </div>
     <hr/>

     <?php $hide_alat = $baru == '' ? 'display-none' : ''; ?>
     <?php $required_alat = $baru == '' ? '' : "required"; ?>
     <?php if (isset($alat)) { ?>
      <?php $hide_alat = 'display-none'; ?>
      <?php $required_alat = "" ?>
     <?php } ?>
     <div class="row nama_alat <?php echo $hide_alat ?>">
      <div class='col-md-3'>
       Nama Alat
      </div>
      <div class='col-md-6'>
       <input type='text' name='' error="Nama Alat" id='nama_alat' error="" class='form-control <?php echo $required_alat ?>' value=''/>       
       <p class="label label-success" style="margin-top: 8px">Nama Alat Baru</p>
      </div>     
     </div>
     <br/>   

     <div class="row">
      <div class='col-md-3'>
       Merk Alat
      </div>
      <div class='col-md-6'>
       <input type='text' name='' id='merk' error="Merk Alat" class='form-control required' value='<?php echo isset($merk) ? $merk : '' ?>'/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tipe Alat
      </div>
      <div class='col-md-6'>
       <input type='text' name='' id='tipe_alat' error="Tipe Alat" class='form-control required' value='<?php echo isset($tipe) ? $tipe : '' ?>'/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Nomer Seri
      </div>
      <div class='col-md-6'>
       <input type='text' name='' id='nomer_seri' error="Nomer Seri" class='form-control required' value='<?php echo isset($nomer_seri) ? $nomer_seri : '' ?>'/>
      </div>     
     </div>
     <br/>            

     <div class="row">
      <div class='col-md-3'>
       Tahun Perolehan
      </div>
      <div class='col-md-6'>
       <select id="tahun" error="Tahun Perolehan" class="form-control required">       
        <?php if (!empty($list_tahun)) { ?>
         <?php foreach ($list_tahun as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($tahun)) { ?>
           <?php $selected = $value['id'] == $tahun ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tahun'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tahun Produksi
      </div>
      <div class='col-md-6'>
       <select id="tahun_produksi" error="Tahun Produksi" class="form-control required">       
        <?php if (!empty($list_tahun)) { ?>
         <?php foreach ($list_tahun as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($tahun_produksi)) { ?>
           <?php $selected = $value['id'] == $tahun_produksi ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tahun'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kelengkapan
      </div>
      <div class='col-md-6'>
       <input type='text' name='' id='kelengkapan' error="Kelengkapan Alat" class='form-control required' value='<?php echo isset($kelengkapan_alat) ? $kelengkapan_alat : '' ?>'/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tonase
      </div>
      <div class='col-md-6'>
       <input type='text' name='' id='tonase' error="Tonase Alat" class='form-control required text-right' value='<?php echo isset($tonase) ? $tonase : '0' ?>'/>
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       Nilai Perolehan
      </div>
      <div class='col-md-6'>
       <input type='text' name='' id='nilai_perolehan' error="Nilai Perolehan" class='form-control required text-right' value='<?php echo isset($nilai_perolehan) ? $nilai_perolehan : '0' ?>'/>
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-6'>
       <textarea id="keterangan" class="form-control required" error="Keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>     
     </div>
     <br/>

<!--     <div class='row manual_add <?php echo!isset($manual_book) ? '' : 'display-none' ?>'>
      <div class='col-md-3'>
       Manual Book (<b>.Pdf</b>)
      </div>
      <div class='col-md-6'>
       <div class="form-group manual_upload">
        <div class="input-group col-xs-12">
         <input id="filename" class="form-control file-upload-info" disabled="" placeholder="Upload File Pdf" type="text">
         <span class="input-group-append">
          <button class="file-upload-browse btn btn-warning" type="button" onclick="Alat.upload(this)">Import</button>
          <input type="file" style="display: none;" id="file" onchange="Alat.getFilename(this)"/>
         </span>
        </div>
       </div>
      </div>
     </div>
     <div class='row manual_detail <?php echo!isset($manual_book) ? 'display-none' : '' ?>'>
      <div class='col-md-3'>
       Manual Book (<b>.Pdf</b>)
      </div>
      <div class='col-md-6'>
       <a href="#" onclick="Alat.showManualBook(this, event)" class="label label-danger"><?php echo $manual_book ?></a>        
      </div>
      <div class='col-md-3 text-right'>
       <i class="mdi mdi-close mdi-24px hover" onclick="Alat.changeManual(this)"></i>
      </div>
     </div>
     <br/>-->

     <div class="row">
      <div class="col-md-3">
       Manual Book (<b>.Pdf</b>)
      </div>
      <div class="col-md-6">
       <div class="form-group">
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
         <div class="form-control" data-trigger="fileinput"> 
          <i class="glyphicon glyphicon-file fileinput-exists"></i> 
          <span class="fileinput-filename"><?php echo isset($manual_book) ? $manual_book : '' ?></span>
         </div> 
         <span class="input-group-addon btn btn-default btn-file"> 
          <span class="fileinput-new">Select file</span> 
          <span class="fileinput-exists">Change</span>
          <input id='file' type="file" name="..." onchange="Alat.getFilename(this, 'xls')"> 
         </span> 
         <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
        </div>
       </div>
      </div>
     </div>
     <hr/>

     <?php if (isset($list_foto)) { ?>
      <div class="row">
       <div class='col-md-3'>
        <label class="label label-warning">Detail Foto</label>
       </div>   
      </div>
      <br/>     

      <div class="row">
       <?php if (!empty($list_foto)) { ?>
        <?php foreach ($list_foto as $value) { ?>
         <div class='col-md-4 data_image' id="<?php echo $value['id'] ?>">
          <div class='col-12 text-right'>
           <i class="mdi mdi-close mdi-24px hover" onclick="Alat.removeFoto(this)"></i>
          </div>
          <div class='col-12 text-center'>           
           <img width="200" height="150" src="<?php echo $value['foto'] ?>"/>
          </div>         
          <br/>
          <div class='col-12 text-center'>
           <input style="margin-left: 16px;" type='text' name='' placeholder="Keterangan Foto" id='title' class='form-control' value='<?php echo $value['title'] ?>'/>
          </div>         
         </div>
        <?php } ?>
       <?php } ?>            
      </div>
      <br/>
      <hr/>
     <?php } ?>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Alat.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="Alat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div> 

  <div class='col-md-4 grid-margin'>
   <div class='white-box'>
    <div class='card-body'>
     <div class='row'>
      <div class='col-md-12'>
       <label class="label label-warning">Foto Alat</label>
      </div>
     </div>
     <hr/>
     <div class="row">
      <div class='col-md-12 text-center'>
       <div class='images'>
        <div class='pic' onclick="Alat.uploadImage(this, event)">
         <i class="mdi mdi-plus mdi-24px hover"></i>
        </div>
       </div>
      </div>
     </div>
    </div>
    <br/>
   </div>
  </div>
 </div>
</div>
