<!DOCTYPE html>
<html lang="en" dir="ltr">
 <head>
  <meta charset="utf-8">
  <title><?php echo $barcode; ?></title>

  <style>
   body {
    font-family: "Helvetica", sans-serif;
    font-size: 11px;
   }
   table {
    width: 100%;
   }
   .text-center {
    text-align: center;
   }
   .text-right {
    text-align: right;
   }
   .font-bold {
    font-weight: bold;
   }
   .mb-32px {
    margin-bottom: 32px;
   }
   .mr-8px {
    margin-right: 8px;
   }
   .ml-8px {
    margin-left: 8px;
   }
   .table-logo {
    width: 100%;
   }
   table.table-logo > tbody > tr > td {
    padding: 16px;
   }
   table.table-logo td {
    vertical-align: top;
   }
   .table-item {
    width: 100%;
    margin-top: 16px;
    border-collapse: collapse;
    font-size: 8px;
   }
   table.table-item th,  table.table-item td {
    border: 1px solid #333;
    padding: 4px 8px;
    border-collapse: collapse;
    vertical-align: top;
   }
   .media {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
   }
   .media-body {
    -ms-flex: 1;
    flex: 1;
   }

   .none-border{
    border:none;
   }
  </style>
 </head>
 <body>
  <table style="width: 100%; margin-top: 32px">
   <tbody>
    <tr>
     <?php if ($jenis == "qr") { ?>
      <td>
       <img width="150" height="150" src="<?php echo base_url() . 'files/qr_code/alat/' . $barcode . '.png' ?>"/>
      </td>
     <?php } else { ?>
      <td>
       <img width="150" height="30" src="<?php echo $barcode_bar ?>"/>
      </td>
     <?php } ?>
    </tr>
   </tbody>
  </table>
 </body>
</html>
