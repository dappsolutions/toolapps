<div class='container-fluid'>

 <div class="white-box">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-4'>
     <?php if ($this->session->userdata('hak_akses') != 'Superadmin') { ?>
      <button id="" class="btn btn-warning" onclick="Alat.add('baru')">Tambah</button>
      &nbsp;
     <?php } ?>     
     <button id="" class="btn btn-success" onclick="Alat.exportAlat()">Export Alat</button>
    </div>
    <div class='col-md-8'>
     <input type='text' name='' id='' onkeyup="Alat.searchBaru(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr class="bg-success text-white">
         <th class="font-12">No</th>
         <th class="font-12">UPT</th>
         <th class="font-12">Gardu Induk / ULTG</th>
         <th class="font-12">Kode Alat</th>
         <th class="font-12">Nomer Seri</th>
         <th class="font-12">Nama Alat</th>
         <th class="font-12">Merk Alat</th>
         <th class="font-12">Kategori Alat</th>
         <th class="font-12">Tipe Alat</th>
         <?php if ($this->session->userdata('hak_akses') == "Superadmin") { ?>
          <th class="font-12">Keputusan</th>
         <?php } else { ?>         
          <th class="font-12">Status Alat</th>
         <?php } ?>
         <th class="font-12">Tahun Perolehan</th>
         <th class="font-12">Keterangan</th>
         <th class="font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = $pagination['last_no'] + 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class="font-12"><?php echo $no++ ?></td>
           <td class="font-12"><?php echo $value['nama_upt'] ?></td>
           <td class="font-12"><?php echo $value['gardu'] ?></td>
           <td class="font-12"><?php echo '<b>' . $value['kode_alat'] . '</b>' ?></td>
           <td class="font-12"><?php echo $value['nomer_seri'] ?></td>
           <td class="font-12"><?php echo $value['nama_alat'] ?></td>
           <td class="font-12"><?php echo $value['merk'] ?></td>
           <td class="font-12"><?php echo $value['kategori'] ?></td>
           <td class="font-12"><?php echo $value['tipe'] ?></td>
           <td class="text-center">
            <?php if ($this->session->userdata('hak_akses') == "Superadmin") { ?>
             <label id="" class="label label-success font-10" 
                    onclick="Alat.approveAlatBaru('<?php echo $value['id'] ?>', 'APPROVE')">Approve</label>
             &nbsp;
             <label id="" class="label label-danger font-10" 
                    onclick="Alat.approveAlatBaru('<?php echo $value['id'] ?>', 'REJECT')">Reject</label>
             &nbsp;
             &nbsp;
            <?php } else { ?>
             <?php echo $value['approver_alat_baru'] == '0' ? '<label class="label label-warning font-10">Menunggu Approval Admin</label>' : '<label class="label label-success font-10">Approve<label>' ?>
            <?php } ?>
           </td>
           <td class="font-12"><?php echo $value['tahun_perolehan'] ?></td>
           <td class="font-12"><?php echo $value['keterangan'] ?></td>
           <td class="text-center">
            <label id="" class="label label-warning font-10" 
                   onclick="Alat.ubah('<?php echo $value['id'] ?>', 'baru')">Ubah</label>
            &nbsp;
            <label id="" class="label label-success font-10" 
                   onclick="Alat.detail('<?php echo $value['id'] ?>')">Detail</label>
            &nbsp;            
            <label id="" class="label label-danger font-10" 
                   onclick="Alat.delete('<?php echo $value['id'] ?>')">Hapus</label>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center" colspan="13">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>

