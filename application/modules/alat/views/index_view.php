<div class='container-fluid'>

 <div class="white-box">
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-4'>
     <?php if ($this->session->userdata('hak_akses') == 'Superadmin' || $this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
      <button id="" class="btn btn-warning" onclick="Alat.add()">Tambah</button>
      &nbsp;
     <?php } ?>
     <button id="" class="btn btn-success" onclick="Alat.exportAlat()">Export Alat</button>
    </div>
    <div class='col-md-8'>
     <input type='text' name='' id='' onkeyup="Alat.search(this, event)" class='form-control' value='' placeholder="Pencarian" />
    </div>
   </div>
   <br />
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br />
   <hr />

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <div class="sticky-table sticky-headers sticky-ltr-cells">
       <table class="table color-bordered-table primary-bordered-table">
        <thead>
         <tr class="sticky-row">
          <th class="font-12 bg-primary">No</th>
          <th class="font-12 bg-primary">UPT</th>
          <th class="font-12 bg-primary">Pemilik Alat</th>
          <th class="font-12 bg-primary">Kode Alat</th>
          <th class="font-12 bg-primary">Nomer Seri</th>
          <th class="font-12 bg-primary">Nama Alat</th>
          <th class="font-12 bg-primary">Merk Alat</th>
          <th class="font-12 bg-primary">Kategori Alat</th>
          <th class="font-12 bg-primary">Tipe Alat</th>
          <th class="font-12 bg-primary">Status Alat</th>
          <th class="font-12 bg-primary">Tahun Perolehan</th>
          <th class="font-12 bg-primary">Keterangan</th>
          <th class="font-12 bg-primary">Action</th>
         </tr>
        </thead>
        <tbody>
         <?php $no = $pagination['last_no'] + 1; ?>
         <?php if (!empty($content)) { ?>
          <?php foreach ($content as $value) { ?>
           <tr>
            <td class="font-12"><?php echo $no++ ?></td>
            <td class="font-12"><?php echo $value['nama_upt'] ?></td>
            <td class="font-12"><?php echo $value['context'].' - '.$value['gardu'] ?></td>
            <td class="font-12"><?php echo '<b>' . $value['kode_alat'] . '</b>' ?></td>
            <td class="font-12"><?php echo $value['nomer_seri'] ?></td>
            <td class="font-12"><?php echo $value['nama_alat'] ?></td>
            <td class="font-12"><?php echo $value['merk'] ?></td>
            <td class="font-12"><?php echo $value['kategori'] ?></td>
            <td class="font-12"><?php echo $value['tipe'] ?></td>
            <td class="font-12">
             <?php if ($value['status_alat'] != '') { ?>
              <?php echo $value['status_alat'] != 'Normal' ? '<label class="label label-danger font-12">' . $value['status_alat'] . '</label>' : '<label class="label label-success font-12">' . $value['status_alat'] . '<label>' ?>
              &nbsp;
             <?php } else { ?>
              <label class="label label-success font-12">Normal<label>
               <?php } ?>
               </td>
               <td class="font-12"><?php echo $value['tahun_perolehan'] ?></td>
               <td class="font-12"><?php echo $value['keterangan'] ?></td>
               <td class="text-center">
                <?php if ($this->session->userdata('hak_akses') == 'Superadmin' || $this->session->userdata('hak_akses') == 'REQUESTOR ANOMALI') { ?>
                 <i class="fa fa-pencil" onclick="Alat.ubah('<?php echo $value['id'] ?>')"></i>
                 &nbsp;
                <?php } ?>                
                <i class="fa fa-file-text" onclick="Alat.detail('<?php echo $value['id'] ?>')"></i>
                &nbsp;
                <?php if ($this->session->userdata('hak_akses') == 'Superadmin') { ?>
                 <i class="mdi mdi-delete mdi-18px" onclick="Alat.delete('<?php echo $value['id'] ?>')"></i>
                <?php } ?>
                &nbsp;
               </td>
               </tr>
              <?php } ?>
             <?php } else { ?>
              <tr>
               <td class="text-center" colspan="13">Tidak Ada Data Ditemukan</td>
              </tr>
             <?php } ?>
             </tbody>
             </table>
             </div>
             </div>
             </div>
             </div>
             <br />
             <div class="row">
              <div class="col-md-12 text-right">
               <span>Menampilkan data <?php echo ($no > 0 ? $pagination['last_no'] + 1 : 0) . ' - ' . ($no - 1) ?>
                dari <b><?php echo $pagination['total_rows'] ?></b> data.</span>
              </div>
             </div>
             <div class="row">
              <div class="col-md-12">
               <div class="pagination">
                <?php echo $pagination['links'] ?>
               </div>
              </div>
             </div>
             </div>
             </div>
             </div>
