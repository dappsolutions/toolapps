<input type="text" value="<?php echo $barcode ?>" id="barcode" class="form-control" />
<div class="container-fluid">
 <div class="row">
  <div class='col-md-3'>
   <div class='white-box'>
    <div class='card-body'>
     <div class='row'>
      <div class='col-md-12 text-center'>
       <img width="150" height="150" src="<?php echo base_url() . 'files/qr_code/alat/' . $barcode . '.png' ?>"/>
       <br/><br/><i class="fa fa-print fa-lg text-primary" onclick="Alat.printBarcode(this, 'qr')"></i>
      </div>
     </div>
    </div>
   </div>
   <div class='white-box'>
    <div class='card-body'>
     <div class='row'>
      <div class='col-md-12 text-center'>
       <img width="150" height="30" src="<?php echo $barcode_bar ?>"/>
       <br/><br/><i class="fa fa-print fa-lg text-primary" onclick="Alat.printBarcode(this, 'bar')"></i>
      </div>
     </div>
    </div>
   </div>
  </div>
  <div class='col-md-9'>
   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       UPT
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $nama_upt ?>
      </div>     
     </div>
     <br/>


     <div class="row">
      <div class='col-md-3'>
       Pemilik Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $context.' - '.$nama_gardu ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Tanggal Kalibrasi Terakhir
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $tanggal_kalibrasi ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kategori Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $kategori ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Nama Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $nama_alat ?>
      </div>     
     </div>
     <br/>      

     <div class="row">
      <div class='col-md-3'>
       Merk Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $merk ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tipe Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $tipe ?>
      </div>     
     </div>
     <br/>
     
     <div class="row">
      <div class='col-md-3'>
       Status Alat
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $status ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Nomer Seri
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $nomer_seri ?>
      </div>     
     </div>
     <br/>            

     <div class="row">
      <div class='col-md-3'>
       Tahun Perolehan
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $tahun_perolehan ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tahun Produksi
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $tahun_produksi_alat ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Kelengkapan
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $kelengkapan_alat ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tonase
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $tonase ?>
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       Nilai Perolehan
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $nilai_perolehan ?>
      </div>     
     </div>
     <br/>     

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-9 text-danger'>
       <?php echo $keterangan ?>
      </div>     
     </div>
     <br/>

     <div class='row'>
      <div class='col-md-3'>
       Manual Book (<b>.Pdf</b>)
      </div>
      <div class='col-md-9 text-danger'>
       <?php if ($manual_book != '') { ?>
        <a href="#" onclick="Alat.showManualBook(this, event)" class="label label-danger"><?php echo $manual_book ?></a>
       <?php } else { ?>
        Tidak Ada
       <?php } ?>
      </div>
     </div>
     <br/>

     <hr/>

     <div class="row">
      <div class='col-md-3'>
       <label class="label label-warning">Detail Foto</label>
      </div>   
     </div>
     <br/>

     <div class="row">
      <?php if (!empty($list_foto)) { ?>
       <?php foreach ($list_foto as $value) { ?>
        <div class='col-md-4'>       
         <div class='col-12 text-center'>
          <img width="200" height="150" src="<?php echo $value['foto'] ?>"/>
         </div>         
         <br/>
         <div class='col-12 text-center'>
          <label class="text-danger"><?php echo $value['title'] == '' ? 'Tidak Ada Keterangan' : $value['title'] ?></label>
         </div>         
        </div>
       <?php } ?>
      <?php } else { ?>            
       <div class="col-md-12">
        <p>Tidak Ada Foto</p>
       </div>
      <?php } ?>
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="Alat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
