<?php

class Servis extends MX_Controller {

    public $segment;
    public $limit;
    public $page;
    public $last_no;

    public function __construct() {
        parent::__construct();
        $this->limit = 10;
    }

    public function getModuleName() {
        return 'servis';
    }

    public function getHeaderJSandCSS() {
        $data = array(
            '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
            '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
            '<script src="' . base_url() . 'assets/js/controllers/servis.js"></script>'
        );

        return $data;
    }

    public function getTableName() {
        return 'servis_peralatan';
    }

    public function index() {
        $this->segment = 3;
        $this->page = $this->uri->segment($this->segment) ?
                $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;

        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data Servis";
        $data['title_content'] = 'Data Servis';
        $content = $this->getDataServis();
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function getTotalDataServis($keyword = '') {
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('a.kode_alat', $keyword),
                array('na.nama_alat', $keyword),
            );
        }

        $upt_id = $this->session->userdata('upt_id');
        $where = "u.deleted = 0 and a.approver_alat_baru != '-1'";
        if ($this->session->userdata('hak_akses') != "Superadmin") {
            $where = "u.deleted = 0 and a.approver_alat_baru != '-1' and a.upt = '" . $upt_id . "'";
        }
        $total = Modules::run('database/count_all', array(
                    'table' => $this->getTableName() . ' u',
                    'field' => array('u.*', 'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'u.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'like' => $like,
                    'is_or_like' => true,
                    'where' => $where
        ));

        return $total;
    }

    public function getDataServis($keyword = '') {
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('a.kode_alat', $keyword),
                array('na.nama_alat', $keyword),
            );
        }

        $upt_id = $this->session->userdata('upt_id');
        $where = "u.deleted = 0 and a.approver_alat_baru != '-1'";
        if ($this->session->userdata('hak_akses') != "Superadmin") {
            $where = "u.deleted = 0 and a.approver_alat_baru != '-1' and a.upt = '" . $upt_id . "'";
        }
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' u',
                    'field' => array('u.*', 'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'u.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'like' => $like,
                    'is_or_like' => true,
                    'limit' => $this->limit,
                    'offset' => $this->last_no,
                    'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }

        return array(
            'data' => $result,
            'total_rows' => $this->getTotalDataServis($keyword)
        );
    }

    public function getDetailDataServis($id) {
        $data = Modules::run('database/get', array(
                    'table' => $this->getTableName() . ' kr',
                    'field' => array('kr.*', 'a.kode_alat', 'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('alat a', 'kr.alat = a.id'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'where' => "kr.id = '" . $id . "'"
        ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
        return $data->row_array();
    }

    public function getListalat() {
        $upt_id = $this->session->userdata('upt_id');
        $where = "a.deleted = 0";

        if ($this->session->userdata('hak_akses') != "Superadmin") {
//   $where = "deleted = 0 and upt = '" . $upt_id . "'";
            $where = "(a.alat_baru = 0 and a.approver_alat_baru = 0 and a.upt = '" . $upt_id . "' and a.deleted = 0) "
                    . "or (a.alat_baru = 1 and a.approver_alat_baru = 1 and a.upt = '" . $upt_id . "' and a.deleted = 0)";
        }
        $data = $data = Modules::run('database/get', array(
                    'table' => 'alat a',
                    'field' => array('a.*', 
                        'sa.status', 
                        'na.nama_alat', 'ka.kategori'),
                    'join' => array(
                        array('(SELECT aad.alat, aad.status_alat, aa.validation 
      FROM anomali_alat_detail aad 
      JOIN anomali_alat aa ON aad.anomali_alat = aa.id
      WHERE aa.validation != 0
      GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'a.id = alat_anomali.alat and a.deleted = 0', 'left'),
                        array('status_alat sa', 'alat_anomali.status_alat = sa.id', 'left'),
                        array('nama_alat na', 'na.id = a.alat'),
                        array('kategori_alat ka', 'ka.id = na.kategori_alat'),
                    ),
                    'where' => $where
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }


//  echo '<pre>';
//  print_r($result);die;
        return $result;
    }

    public function add() {
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Tambah Servis";
        $data['title_content'] = 'Tambah Servis';
        $data['list_alat'] = $this->getListalat();
        echo Modules::run('template', $data);
    }

    public function ubah($id) {
        $data = $this->getDetailDataServis($id);
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Ubah Servis";
        $data['title_content'] = 'Ubah Servis';
        $data['list_alat'] = $this->getListalat();
        echo Modules::run('template', $data);
    }

    public function detail($id) {
        $data = $this->getDetailDataServis($id);
        $data['view_file'] = 'detail_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Detail Servis";
        $data['title_content'] = 'Detail Servis';
        echo Modules::run('template', $data);
    }

    public function getPostDataHeader($value) {
        $data['alat'] = $value->alat;
        $data['tanggal_servis'] = $value->tanggal;
        return $data;
    }

    public function simpan() {
        $data = json_decode($this->input->post('data'));
        $id = $this->input->post('id');
        $is_valid = false;
//  echo '<pre>';
//  print_r($data);die;

        $this->db->trans_begin();
        $message = '';
        try {
            $post = $this->getPostDataHeader($data);
            if ($id == '') {
                $id = Modules::run('database/_insert', $this->getTableName(), $post);
            } else {
                //update
                Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
            }
            $this->db->trans_commit();
            if ($message == '') {
                $is_valid = true;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
    }

    public function search($keyword) {
        $this->segment = 4;
        $this->page = $this->uri->segment($this->segment) ?
                $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;
        $keyword = urldecode($keyword);

        $data['keyword'] = $keyword;
        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data Servis";
        $data['title_content'] = 'Data Servis';
        $content = $this->getDataServis($keyword);
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function delete($id) {
        $is_valid = false;
        $this->db->trans_begin();
        try {
            Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid));
    }

}
