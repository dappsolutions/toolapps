<div class='container-fluid'>

 <div class="white-box">  
  <div class="card-body">
   <h4 class="card-title"><u><?php echo $title ?></u></h4>

   <div class='row'>
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="NamaAlat.add()">Tambah</button>
    </div>
    <div class='col-md-10'>
     <input type='text' name='' id='' onkeyup="NamaAlat.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>        
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <hr/>

   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr class="bg-success text-white">
         <th class="font-12">No</th>
         <th class="font-12">Kategori Alat</th>
         <th class="font-12">Nama Alat</th>
         <th class="font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class="font-12"><?php echo $no++ ?></td>
           <td class="font-12"><?php echo $value['kategori'] ?></td>
           <td class="font-12"><?php echo $value['nama_alat'] ?></td>
           <td class="text-center">
            <i class="fa fa-pencil" onclick="NamaAlat.ubah('<?php echo $value['id'] ?>')"></i>
            &nbsp;
            <i class="fa fa-file-text" onclick="NamaAlat.detail('<?php echo $value['id'] ?>')"></i>
            &nbsp;
            <i class="mdi mdi-delete mdi-18px" onclick="NamaAlat.delete('<?php echo $value['id'] ?>')"></i>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center" colspan="4">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>