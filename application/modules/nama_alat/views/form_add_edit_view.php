<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label >
     <hr/>
     
     <div class="row">
      <div class='col-md-3'>
       Kategori Alat
      </div>
      <div class='col-md-4'>
       <select id="kategori_alat" error="Kategori Alat" class="form-control required">       
        <?php if (!empty($list_kategori)) { ?>
         <?php foreach ($list_kategori as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($kategori_alat)) { ?>
           <?php $selected = $value['id'] == $kategori_alat ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>   
     
     <div class="row">
      <div class='col-md-3'>
       Nama Alat
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='nama_alat' class='form-control required' 
              value='<?php echo isset($nama_alat) ? $nama_alat : '' ?>' error="Nama Alat"/>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="NamaAlat.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="NamaAlat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
