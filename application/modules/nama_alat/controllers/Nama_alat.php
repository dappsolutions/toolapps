<?php

class Nama_alat extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'nama_alat';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/nama_alat.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'nama_alat';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Nama Alat";
  $data['title_content'] = 'Data Nama Alat';
  $content = $this->getDataNama();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataNama($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('u.tipe', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' u',
  'field' => array('u.*', 'ka.kategori'),
  'join' => array(
   array('kategori_alat ka', 'u.kategori_alat = ka.id')
  ),
  'like' => $like,
  'is_or_like' => true,
  'where' => "u.deleted = 0"
  ));

  return $total;
 }

 public function getDataNama($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('u.tipe', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' u',
  'field' => array('u.*', 'ka.kategori'),
  'join' => array(
   array('kategori_alat ka', 'u.kategori_alat = ka.id')
  ),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "u.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataNama($keyword)
  );
 }

 public function getDetailDataNama($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'field' => array('kr.*', 'ka.kategori'),
  'join' => array(
   array('kategori_alat ka', 'kr.kategori_alat = ka.id')
  ),
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListKategoriAlat() {
  $data = Modules::run('database/get', array(
  'table' => 'kategori_alat',
  'where' => array('deleted' => 0)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Nama Alat";
  $data['title_content'] = 'Tambah Nama Alat';
  $data['list_kategori'] = $this->getListKategoriAlat();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataNama($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Nama Alat";
  $data['title_content'] = 'Ubah Nama Alat';
  $data['list_kategori'] = $this->getListKategoriAlat();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataNama($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Nama Alat";
  $data['title_content'] = 'Detail Nama Alat';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['kategori_alat'] = $value->kategori_alat;
  $data['nama_alat'] = $value->nama_alat;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_upt = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_upt);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_upt, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Nama Alat";
  $data['title_content'] = 'Data Nama Alat';
  $content = $this->getDataNama($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
