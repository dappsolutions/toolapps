<div class="container-fluid">
 <div class="row">
  <div class='col-12'>

   <div class="white-box">
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Kategori Alat
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $kategori ?>
      </div>     
     </div>
     <br/>

     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-danger-baru" onclick="KategoriAlat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
