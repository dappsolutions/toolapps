<?php

class Anomali extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $upt_id;

 public function __construct() {
  parent::__construct();
  $this->limit = 40;

  $this->upt_id = array();
  $hak_akses = $this->session->userdata('hak_akses');

  $upt = $this->session->userdata('upt_id');

  if ($hak_akses != "Superadmin") {
   $this->upt_id = array('aa.upt' => $upt);
  }
 }

 public function getModuleName() {
  return 'anomali';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/anomali_tervalidasi.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/anomali.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'anomali_alat';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Anomali";
  $data['title_content'] = 'Data Anomali';
  $content = $this->getDataAnomali();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataAnomali($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('aa.no_anomali', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' aa',
  'field' => array('aa.*', 'u.username as validator'),
  'join' => array(
  array('user u', 'aa.validation = u.id', 'left')
  ),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => $this->upt_id
  ));

  return $total;
 }

 public function getDataAnomali($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('aa.no_anomali', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' aa',
  'field' => array('aa.*', 'u.username as validator'),
  'join' => array(
  array('user u', 'aa.validation = u.id', 'left')
  ),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => $this->upt_id
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataAnomali($keyword)
  );
 }

 public function getDetailDataAnomali($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListStatus() {
  $data = Modules::run('database/get', array(
  'table' => 'status_alat',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Anomali";
  $data['title_content'] = 'Tambah Anomali';
  $data['list_status'] = $this->getListStatus();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataAnomali($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Anomali";
  $data['title_content'] = 'Ubah Anomali';
  $data['list_status'] = $this->getListStatus();
  $data['detail_alat'] = $this->getDataDetailAlat($id);
  echo Modules::run('template', $data);
 }

 public function getDataDetailAlat($anomali) {
  $data = Modules::run('database/get', array(
  'table' => 'anomali_alat_detail aad',
  'field' => array('aad.*', 'a.nomer_seri', 'sa.status',
  'a.kode_alat', 'na.nama_alat'),
  'join' => array(
  array('alat a', 'aad.alat = a.id'),
  array('nama_alat na', 'a.alat = na.id'),
  array('status_alat sa', 'aad.status_alat = sa.id'),
  ),
  'where' => array('aad.anomali_alat' => $anomali)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataApproval($anomali) {
  $data = Modules::run('database/get', array(
  'table' => 'user_validation uv',
  'field' => array('uv.*', 'pg.nama as nama_pegawai', 'pg.nip', 'pv.hak_akses'),
  'join' => array(
  array('user u', 'uv.user = u.id'),
  array('priveledge pv', 'u.priveledge = pv.id'),
  array('pegawai pg', 'u.pegawai = pg.id'),
  ),
  'where' => array('uv.anomali_alat' => $anomali)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function detail($id) {
  $data = $this->getDetailDataAnomali($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Anomali";
  $data['title_content'] = 'Detail Anomali';
  $data['detail_alat'] = $this->getDataDetailAlat($id);
  $data['approval'] = $this->getDataApproval($id);
  $data['list_status'] = $this->getListStatus();
  echo Modules::run('template', $data);
 }

 public function showDataAlat() {
  $index_tr = $this->input->post('index_tr');
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $content = $this->getDataAlat(3000, 0);
  $data['detail_content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/seekPagination/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['index_tr'] = $index_tr;
  echo $this->load->view('detail_alat', $data, true);
 }

 public function seekPagination($offset) {
  $this->segment = 3;
  $offset = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $offset * $this->limit;

  $content = $this->getDataAlat($this->limit, $this->last_no);
  $data['detail_content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/seekPagination/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo $this->load->view('table_alat', $data, true);
 }

 public function getTotalDataAlat() {
  $upt = $this->session->userdata('upt_id');
  $like = array();
  $total = Modules::run('database/count_all', array(
  'table' => 'alat aa',
  'field' => array('aa.*', 'ka.kategori',
  't.tahun as tahun_perolehan', 'ut.upt as nama_upt'),
  'join' => array(
  array('nama_alat na', 'aa.alat = na.id and aa.deleted = 0'),
  array('kategori_alat ka', 'na.kategori_alat = ka.id and aa.deleted = 0'),
  array('tahun t', 'aa.tahun = t.id and aa.deleted = 0'),
  array('upt ut', 'aa.upt = ut.id and aa.deleted = 0'),
  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'aa.id = alat_anomali.alat and aa.deleted = 0', 'left'),
  ),
  'like' => $like,
  'is_or_like' => true,
  'where' => "aa.upt = '" . $upt . "' and (alat_anomali.status_alat = 1 or alat_anomali.status_alat is null)"
  ));

  return $total;
 }

 public function getDataAlat($limit = 1, $offset = 0) {
  $upt = $this->session->userdata('upt_id');

  $like = array();
  $data = Modules::run('database/get', array(
  'table' => 'alat aa',
  'field' => array('aa.*', 'ka.kategori',
  't.tahun as tahun_perolehan', 'ut.upt as nama_upt',
  'alat_anomali.status_alat AS status_alat', 'na.nama_alat'),
  'join' => array(
  array('nama_alat na', 'aa.alat = na.id and aa.deleted = 0'),
  array('kategori_alat ka', 'na.kategori_alat = ka.id and aa.deleted = 0'),
  array('tahun t', 'aa.tahun = t.id and aa.deleted = 0', 'left'),
  array('upt ut', 'aa.upt = ut.id and aa.deleted = 0'),
  array('(SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali', 'aa.id = alat_anomali.alat and aa.deleted = 0', 'left'),
  ),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $limit,
  'offset' => $offset,
  'where' => "aa.upt = '" . $upt . "' and (alat_anomali.status_alat = 1 or alat_anomali.status_alat is null)"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataAlat()
  );
 }

 public function getPostDataHeader($value) {
  $data['no_anomali'] = Modules::run('no_generator/generateNomerAnomali');
  $data['tgl_anomali'] = $value->tanggal_anomali;
  $data['upt'] = $this->session->userdata('upt_id');
  return $data;
 }

 public function getPostAnomaliAlatDetail($anomali, $value) {
  $data['anomali_alat'] = $anomali;
  $data['user'] = $this->session->userdata('user_id');
  $data['alat'] = $value->id_alat;
  $data['status_alat'] = $value->status;
  $data['keterangan_anomali'] = $value->keterangan_anomali;
  ;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $anomali = $id;
  $this->db->trans_begin();
  try {
   $post_anomali = $this->getPostDataHeader($data);
   if ($id == '') {
    $anomali = Modules::run('database/_insert', $this->getTableName(), $post_anomali);

    if (!empty($data->detail_item)) {
     foreach ($data->detail_item as $value) {
      $post_anomali_detail = $this->getPostAnomaliAlatDetail($anomali, $value);
      Modules::run('database/_insert', 'anomali_alat_detail', $post_anomali_detail);
     }
    }
   } else {
    //update
    unset($post_anomali['no_anomali']);
    Modules::run('database/_update', $this->getTableName(), $post_anomali, array('id' => $id));

    if (!empty($data->detail_item_ubah)) {
     foreach ($data->detail_item_ubah as $value) {
      if (isset($value->id_alat)) {
       $post_anomali_detail = $this->getPostAnomaliAlatDetail($id, $value);
       Modules::run('database/_update', 'anomali_alat_detail', $post_anomali_detail, array('id' => $value->id));
      }
     }
    }
    
    if (!empty($data->detail_item_hapus)) {
     foreach ($data->detail_item_hapus as $value) {
      Modules::run('database/_delete', "anomali_alat_detail", array('id' => $value->id));
     }
    }

    
    if (!empty($data->detail_item)) {
     foreach ($data->detail_item as $value) {
      if (!isset($value->id) || $value->id == '') {
       if (isset($value->id_alat)) {
        $post_anomali_detail = $this->getPostAnomaliAlatDetail($anomali, $value);
        Modules::run('database/_insert', 'anomali_alat_detail', $post_anomali_detail);
       }
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'anomali' => $anomali));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Anomali";
  $data['title_content'] = 'Data Anomali';
  $content = $this->getDataAnomali($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function ubahAnomali($id) {
  $data['list_status'] = $this->getListStatus();
//  $data['id'] = $id;
  echo $this->load->view('tr_detail_alat', $data, true);
 }

}
