<script src="<?php echo base_url() ?>assets/js/url.js"></script>
<script src="<?php echo base_url() ?>assets/js/controllers/pagination.js"></script>
<input type='hidden' name='' id='index_tr' class='form-control' value='<?php echo $index_tr ?>'/>

<div class="row">
 <div class="col-md-12">
  <input type="text" placeholder="Cari Data" value="" id="" class="form-control" onkeyup="Anomali.cariAlat(this, event)" />
 </div>
</div>
<hr/>
<div class='row' id="data_alat">
 <div class='col-md-12'>
  <div class='white-box'>
   <div class='card-body'>
    <h4><u>Pilih Alat</u></h4>
    <div class="table-responsive">
     <div class="" style="overflow: auto; height: 350px;">
      <table class="table color-bordered-table primary-bordered-table" id="tb_alat_detail">
       <thead>
        <tr class="bg-success text-white">
         <th class="">No</th>
         <th class="">UPT</th>
         <th class="">Kode Alat</th>
         <th class="">Nomer Seri</th>
         <th class="">Nama Alat</th>
         <th class="">Merk Alat</th>
         <th class="">Kategori Alat</th>
         <th class="">Tipe Alat</th>
         <th class="">Tahun Perolehan</th>
         <th class="">Keterangan</th>
         <th class="">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($detail_content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($detail_content as $value) { ?>
          <tr kode_alat="<?php echo $value['kode_alat'] ?>">
           <td class='font-12'><?php echo $no++ ?></td>
           <td class='font-12'><?php echo $value['nama_upt'] ?></td>
           <td class='font-12'><?php echo '<b>' . $value['kode_alat'] . '</b>' ?></td>
           <td class='font-12'><?php echo $value['nomer_seri'] ?></td>
           <td class='font-12'><?php echo $value['nama_alat'] ?></td>
           <td class='font-12'><?php echo $value['merk'] ?></td>
           <td class='font-12'><?php echo $value['kategori'] ?></td>
           <td class='font-12'><?php echo $value['tipe'] ?></td>
           <td class='font-12'><?php echo $value['keterangan'] ?></td>
           <td class='font-12'><?php echo $value['tahun_perolehan'] ?></td>
           <td class="text-center">
            <label id="" class="label label-warning font-10 hover" 
                   onclick="Anomali.pilihAlat(this, '<?php echo $value['id'] ?>')">Pilih</label>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center font-10" colspan="12">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
 <br/>
 <br/>
 <div class="col-md-12">
  <div class="pagination">
   <?php // echo $pagination['links'] ?>
  </div>
 </div>
</div>
