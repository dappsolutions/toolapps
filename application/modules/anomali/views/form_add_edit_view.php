<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <div class="row">
      <div class='col-md-3'>
       Tanggal Anomali
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='tanggal_anomali' class='form-control required' 
              value='<?php echo isset($tgl_anomali) ? $tgl_anomali : '' ?>' error="Tanggal Anomali"/>
      </div>     
     </div>
     <br/>     
    </div>
   </div>
  </div>
 </div>

 <div class='row'>
  <div class='col-md-9'>
   <div class='white-box'>
    <div class='card-body'>
     <div class="row">
      <div class='col-md-3'>
       <u>Anomali Alat</u>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-12'>
       <div class='table-responsive'>
        <?php if (!isset($detail_alat)) { ?>
         <table class="table color-bordered-table primary-bordered-table">
          <thead>
           <tr class="bg-success text-white">
            <th>Pilih Alat</th>
            <th>Kode Alat</th>
            <th>Status</th>
            <th>Keterangan</th>
            <th>&nbsp;</th>
           </tr>
          </thead>
          <tbody class="table_anomali">
           <tr id="">
            <td class='font-12'>
             <button id="" class="btn btn-info" onclick="Anomali.showDataAlat(this)">Pilih Alat</button>
            </td>
            <td class='font-12'>-</td>
            <td class='font-12'>
             <select id="status" error="Status" class="form-control required">
              <?php if (!empty($list_status)) { ?>
               <?php foreach ($list_status as $value) { ?>
                <option value="<?php echo $value['id'] ?>"><?php echo $value['status'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </td>
            <td>
             <textarea id ="keterangan_anomali" class="form-control required" error="Keterangan Anomali"></textarea>
            </td>
            <td class="text-center">
             <i class="mdi mdi-plus mdi-24px hover" onclick="Anomali.addDetail(this)"></i>
            </td>
           </tr>
          </tbody>
         </table>
        <?php } else { ?>
         <table class="table color-bordered-table primary-bordered-table">
          <thead>
           <tr class="bg-success text-white">
            <th>Pilih Alat</th>
            <th>Kode Alat</th>
            <th>Status</th>
            <th>Keterangan</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody class="table_anomali">
           <?php if (!empty($detail_alat)) { ?>
            <?php foreach ($detail_alat as $value) { ?>
             <tr id="<?php echo $value['id'] ?>">
              <td class='font-12'><?php echo '<b>' . $value['nama_alat'] . '</b>' . ' <label class="text-success">(' . $value['nomer_seri'] . ')</label>' ?></td>
              <td class='font-12'><b><?php echo $value['kode_alat'] ?></b></td>
              <td class='font-12'><?php echo $value['status'] == 'Rusak' ? '<label class="text-danger">Rusak</label>' : '<label class="text-success">Normal</label>' ?></td>
              <td class='font-12'><?php echo $value['keterangan_anomali'] ?></td>
              <td class='font-12'>
               <label class="label label-danger hover" onclick="Anomali.hapusAnomali(this)">Hapus</label>
               &nbsp;
               <label class="label label-success hover" onclick="Anomali.ubahAnomali(this)">Ubah</label>
              </td>
             </tr>
            <?php } ?>
            <tr id="">
             <td>
              <button id="" class="btn btn-info" onclick="Anomali.showDataAlat(this)">Pilih Alat</button>
             </td>
             <td>-</td>
             <td>
              <select id="status" error="Status" class="form-control required">
               <?php if (!empty($list_status)) { ?>
                <?php foreach ($list_status as $value) { ?>
                 <option value="<?php echo $value['id'] ?>"><?php echo $value['status'] ?></option>
                <?php } ?>
               <?php } ?>
              </select>
             </td>
             <td>
              <textarea id ="keterangan_anomali" class="form-control" error="Keterangan Anomali"></textarea>
             </td>
             <td class="text-center">
              <i class="mdi mdi-plus mdi-24px hover" onclick="Anomali.addDetail(this)"></i>
             </td>
            </tr>
           <?php } ?>
          </tbody>
         </table>
        <?php } ?>
       </div>
      </div>
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Anomali.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="Anomali.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>

  <div class="col-md-3">
   <div class="white-box">
    <div class="row">
     <div class="col-md-12">
      <u>Status Peralatan</u>
     </div>     
    </div>
    <hr/>
    
    <div class="row">
     <div class="col-md-12">
      <?php if (!empty($list_status)) { ?>
       <?php foreach ($list_status as $value) { ?>
        <div class="row">
         <div class="col-md-12">
          <label><?php echo $value['status'] ?></label>
         </div>
        </div>
        <br/>
        <div class="row">
         <div class="col-md-12 text-right">
          <label class="text-primary"><?php echo $value['keterangan'] == '' ? '-' : $value['keterangan'] ?></label>
         </div>
        </div>
       <?php } ?>
      <?php } ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
