
<td>
 <button id="" class="btn btn-info" onclick="Anomali.showDataAlat(this)">Pilih Alat</button>
</td>
<td>-</td>
<td>
 <select id="status" error="Status" class="form-control required">
  <?php if (!empty($list_status)) { ?>
   <?php foreach ($list_status as $value) { ?>
    <option value="<?php echo $value['id'] ?>"><?php echo $value['status'] ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>
<td>
 <textarea id ="keterangan_anomali" class="form-control required" error="Keterangan Anomali"></textarea>
</td>
<td class="text-center">
 <i class="mdi mdi-plus mdi-24px hover" onclick="Anomali.addDetail(this)"></i>
</td>