<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <div class="row">
      <div class='col-md-3'>
       No. Anomali
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $no_anomali ?>
      </div>     
     </div>
     <br/>
     <div class="row">
      <div class='col-md-3'>
       Tanggal Anomali
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo date('d F Y', strtotime($tgl_anomali)) ?>
      </div>     
     </div>
     <br/>     
    </div>
   </div>
  </div>
 </div>

 <br/>
 <div class='row'>
  <div class='col-md-9'>
   <div class='white-box'>
    <div class='card-body'>
     <div class="row">
      <div class='col-md-3'>
       <u>Anomali Alat</u>
      </div>
      <div class='col-md-9 text-right'>
       <button onclick="AnomaliTervalidasi.exportExcel('<?php echo $id ?>')" id="" class="btn btn-success">Export Excel</button>
       &nbsp;
       <button onclick="Template.showUpdateSystem(this, event)" id="" class="btn btn-danger">Export PDF <label class="label label-warning">PRO</label></button>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-12'>
       <div class='table-responsive'>
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr>
           <th>Pilih Alat</th>
           <th>Kode Alat</th>
           <th>Status</th>
           <th>Keterangan</th>
          </tr>
         </thead>
         <tbody class="table_anomali">
          <?php if (!empty($detail_alat)) { ?>
           <?php foreach ($detail_alat as $value) { ?>
            <tr>
             <td class='font-12'><?php echo '<b>' . $value['nama_alat'] . '</b>' . ' <label class="text-success">(' . $value['nomer_seri'] . ')</label>' ?></td>
             <td class='font-12'><b><?php echo $value['kode_alat'] ?></b></td>
             <td class='font-12'><?php echo $value['status'] == 'Rusak' ? '<label class="text-danger">Rusak</label>' : '<label class="text-success">Normal</label>' ?></td>
             <td class='font-12'><?php echo $value['keterangan_anomali'] ?></td>
            </tr>
           <?php } ?>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>
     </div>
     <br/>
     <hr/>

     <?php if (!empty($approval)) { ?>
      <div class='row'>
       <div class='col-md-5 text-center ' style="font-weight: bold;">
        <p>Divalidasi Oleh, <?php echo $approval[0]['hak_akses'] ?></p>
        <br/>
        <br/>
        <br/>
        <p><?php echo $approval[0]['nama_pegawai'] ?></p>
        <p><?php echo date('d F Y H:i:s', strtotime($approval[0]['date_validation'])) ?></p>
       </div>
      </div>
     <?php } ?>

     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn" onclick="Anomali.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>

  <div class="col-md-3">
   <div class="white-box">
    <div class="row">
     <div class="col-md-12">
      <u>Status Peralatan</u>
     </div>     
    </div>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <?php if (!empty($list_status)) { ?>
       <?php foreach ($list_status as $value) { ?>
        <div class="row">
         <div class="col-md-12">
          <label><?php echo $value['status'] ?></label>
         </div>
        </div>
        <br/>
        <div class="row">
         <div class="col-md-12 text-right">
          <label class="text-primary"><?php echo $value['keterangan'] == '' ? '-' : $value['keterangan'] ?></label>
         </div>
        </div>
       <?php } ?>
      <?php } ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
