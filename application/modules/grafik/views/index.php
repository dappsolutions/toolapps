
<div class="container-fluid">
    <div class="white-box">
        <div class="row">
            <div class="col-md-8">   
                <div class='card-body'>             
                    <h4>Grafik Alat</h4>
                    <br/>  
                    <div id="canvas_probis_setuju"></div>
                    <script>
                     $(function () {
                         console.log("Grafik Alat");
                         window.BarSetuju = Morris.Bar({
                             element: 'canvas_probis_setuju',
                             data: <?php echo '[{"jumlah":20,"alat":"Alat Rusak"}, {"jumlah_normal":10,"alat":"Alat Normal"}, {"jumlah_pengajuan":30,"alat":"Alat Keluar"}, {"jumlah_kembali":50,"alat":"Alat Kembali"}]' ?>,
                             xkey: ['alat'],
                             ykeys: ['jumlah', 'jumlah_normal', 'jumlah_pengajuan', 'jumlah_kembali'],
                             ymax: 100,
                             ymin: 0,
                             labels: ['Jumlah Rusak', 'Jumlah Normal', 'Jumlah Keluar', 'Jumlah Kembali'],
                             resize: true,
                             lineWidth: '3px',
                             redraw: true,
                             barColors: ["#e74a25", "#2ecc71", '#0283cc', '#ffb136']
                         });

                         $(window).resize(function () {
                             window.BarSetuju.redraw();
                         });
                     });
                    </script>
                </div>
            </div>  

            <div class="col-md-4">
                <i class="mdi mdi-settings mdi-18px hover"></i> UPT Malang<br/><hr/>
                <i class="mdi mdi-settings mdi-18px hover"></i> UPT Surabaya<br/><hr/>
                <i class="mdi mdi-settings mdi-18px hover"></i> UPT Gresik<br/>
            </div>
        </div>  
        <hr/>

        <div class="row">
            <div class="col-md-8">
                <label class='label label-danger'>&nbsp;</label> Alat Rusak<br/><br/>
                <label class='label label-success'>&nbsp;</label> Alat Normal<br/><br/>
                <label class='label label-primary'>&nbsp;</label> Alat Keluar<br/><br/>
                <label class='label label-warning'>&nbsp;</label> Alat Kembali<br/><br/>
            </div>
        </div>
    </div>
</div>