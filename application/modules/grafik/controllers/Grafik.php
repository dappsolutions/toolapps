<?php

class Grafik extends MX_Controller {

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
 }

 public function getModuleName() {
  return 'grafik';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/controllers/grafik.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Grafik";
  $data['title_content'] = 'Grafik';
  $data['data_alat_rusak'] = $this->getDataTotalItemRusak();
  $data['data_alat_normal'] = $this->getDataTotalItemNormal();
  $data['data_alat_not_validation'] = $this->getDataTotalPengajuanNotValidasi(); 
  $data['data_alat_valid'] = $this->getDataTotalPengajuanValidasi(); 
  
//  $data['data_kajian'] = $this->getDataKajian();

  echo Modules::run('template', $data);
 }

 public function getDataTotalItemRusak() {
  $data_alat = array();
  $total = 0;
  for ($i = 1; $i < 13; $i++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_fix = date('Y') . '-' . $i;
   $query = "SELECT count(*) as jumlah FROM alat a
LEFT JOIN (SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali ON a.id = alat_anomali.alat AND a.deleted = 0
WHERE alat_anomali.status_alat = 2 AND a.createddate LIKE '%" . $date_fix . "%'";
//echo $query;die;
//   echo $date.'<br/>';C
   $data = Modules::run('database/get_custom', $query)->row_array();

   $total += $data['jumlah'];
   $data_alat[] = $data['jumlah'];
  }

  return array(
  'data' => implode(',', $data_alat),
  'total' => $total
  );
 }

 public function getDataTotalItemNormal() {
  $data_alat = array();
  $total = 0;
  for ($i = 1; $i < 13; $i++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_fix = date('Y') . '-' . $i;
   $query = "SELECT count(*) as jumlah FROM alat a
LEFT JOIN (SELECT aad.alat, aad.status_alat, aa.validation 
FROM anomali_alat_detail aad 
JOIN anomali_alat aa ON aad.anomali_alat = aa.id
WHERE aa.validation != 0
GROUP BY aad.alat, aad.status_alat ) alat_anomali ON a.id = alat_anomali.alat AND a.deleted = 0
WHERE alat_anomali.status_alat = 1 AND a.createddate LIKE '%" . $date_fix . "%'";
//echo $query;die;
//   echo $date.'<br/>';C
   $data = Modules::run('database/get_custom', $query)->row_array();

   $total += $data['jumlah'];
   $data_alat[] = $data['jumlah'];
  }

  return array(
  'data' => implode(',', $data_alat),
  'total' => $total
  );
 }

 public function getDataTotalPengajuanNotValidasi() {
  $data_alat = array();
  $total = 0;
  for ($i = 1; $i < 13; $i++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_fix = date('Y') . '-' . $i;
   $query = "SELECT COUNT(*) AS jumlah FROM anomali_alat
WHERE validation = 0 AND createddate LIKE '%" . $date_fix . "%'";
//echo $query;die;
//   echo $date.'<br/>';C
   $data = Modules::run('database/get_custom', $query)->row_array();

   $total += $data['jumlah'];
   $data_alat[] = $data['jumlah'];
  }

  return array(
  'data' => implode(',', $data_alat),
  'total' => $total
  );
 }
 
 public function getDataTotalPengajuanValidasi() {
  $data_alat = array();
  $total = 0;
  for ($i = 1; $i < 13; $i++) {
   $i = $i < 10 ? '0' . $i : $i;
   $date_fix = date('Y') . '-' . $i;
   $query = "SELECT COUNT(*) AS jumlah FROM anomali_alat
WHERE validation != 0 AND createddate LIKE '%" . $date_fix . "%'";
//echo $query;die;
//   echo $date.'<br/>';C
   $data = Modules::run('database/get_custom', $query)->row_array();

   $total += $data['jumlah'];
   $data_alat[] = $data['jumlah'];
  }

  return array(
  'data' => implode(',', $data_alat),
  'total' => $total
  );
 }

}
