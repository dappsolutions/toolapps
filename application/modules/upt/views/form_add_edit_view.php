<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Nama UPT
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='upt' class='form-control required' 
              value='<?php echo isset($upt) ? $upt : '' ?>' error="Nama UPT"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Alamat
      </div>
      <div class='col-md-4'>
       <textarea id="alamat" class="form-control required" error="Alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
      </div>     
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="Upt.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="Upt.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
