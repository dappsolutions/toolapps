<div class="row">
 <div class="col-md-3">
  Pencarian
 </div>
 <div class="col-md-9">
  <input type="text" value="" placeholder="Kode Alat" id="" class="form-control" onkeyup="PinjamEksternal.cariAlat(this, event)"/>
 </div>
</div>
<br/>
<div class='row'>
 <div class='col-md-12'>
  <div class="table-responsive">
   <table class="table color-bordered-table primary-bordered-table" id='table_alat'>
    <thead>
     <tr class="bg-primary text-white">
      <th class="font-12">No</th>
      <th class="font-12">Kode Alat</th>
      <th class="font-12">Stok</th>
      <th class="font-12">Jumlah</th>
      <th class="font-12">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_alat)) { ?>
      <?php if (!empty($list_alat)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($list_alat as $value) { ?>
        <tr id='<?php echo $value['id'] ?>' upt='<?php echo $value['upt'] ?>'>
         <td class="font-12"><?php echo $no++ ?></td>
         <td class="font-12"><?php echo $value['nama_alat'].' - '.$value['kategori'].' - '.$value['kode_alat'].' - '.$value['nomer_seri'] ?></td> 
         <td class="font-12">
          <?php if ($value['stock'] > 0) { ?>
           <label class="label label-success"><?php echo $value['stock'] ?></label>
          <?php } else { ?> 
           <label class="label label-danger"><?php echo $value['stock'] ?></label>
          <?php } ?>
         </td>
         <td class="font-12">
          <?php if ($value['stock'] > 0) { ?>
           <input type="text" value="0" id="qty" class="form-control text-right" onkeyup="PinjamEksternal.checkValidInput(this, event)"/>
          <?php } ?>
         </td>
         <td class="font-12">
          <?php if ($value['stock'] > 0) { ?>
           <input type="checkbox" value="" id="check" class="check_input" />
          <?php } ?>
         </td>
        </tr>
       <?php } ?>
      <?php } else { ?>
       <tr>
        <td class="text-center font-12" colspan="6">Tidak Ada Data Ditemukan</td>
       </tr>
      <?php } ?>         
     <?php } ?>         
    </tbody>
   </table>
  </div>
 </div>      
</div>
<hr/>

<div class="row">
 <div class="col-md-12 text-right">
  <button class="btn btn-success" onclick="PinjamEksternal.prosesAmbilAlat()">Proses</button>
 </div>
</div>