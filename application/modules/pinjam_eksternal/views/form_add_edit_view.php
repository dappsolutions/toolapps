<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Tanggal Peminjaman
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='tanggal_pinjam' class='form-control required' 
              value='<?php echo isset($tanggal_pinjam) ? $tanggal_pinjam : '' ?>' error="Tanggal Pinjam"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4'>
       <textarea id='keterangan' class="form-control"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>     
     </div>
     <br/>


     <u>Data Alat</u>
     <hr/>

     <div class='row'>
      <div class='col-md-3'>
       UPT Pemilik alat
      </div>
      <div class='col-md-4'>
       <select class="form-control" error="UPT" id='upt' onchange="PinjamEksternal.getDataAlat(this)">
        <option value="">Pilih UPT</option>
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <option value="<?php echo $value['id'] ?>"><?php echo $value['upt'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     <br/>
     <br/>

     <div class='row'>
      <div class='col-md-3'>
       <label class="label label-success">Alat Dipinjam</label>
      </div>      
     </div>
     <br/>

     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table" id='data_alat'>
         <thead>
          <tr class="bg-primary text-white">
           <th class="font-12">No</th>
           <th class="font-12">Kode Alat</th>
           <th class="font-12">UPT Pemilik Alat</th>
           <th class="font-12">Jumlah</th>
           <th class="font-12">Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (isset($list_alat)) { ?>
           <?php if (!empty($list_alat)) { ?>
            <?php $no = 1; ?>
            <?php foreach ($list_alat as $value) { ?>
             <tr id='<?php echo $value['id'] ?>'>
              <td class="font-12"><?php echo $no++ ?></td>
              <td class="font-12"><?php echo $value['kode_alat'] ?></td>
              <td class="font-12"><?php echo $value['upt'] ?></td>
              <td class="font-12"><?php echo $value['qty'] ?></td>
              <td>
               <i class="mdi mdi-delete mdi-18px hover text-danger" onclick="PinjamEksternal.removeData(this)"></i>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td class="text-center font-12" colspan="6">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>         
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>      
     </div>
     <br/>
     <br/>

     <div class='row'>
      <div class='col-md-3'>
       <label class="label label-success">Surat Peminjaman (pdf, png, jpg) </label>
      </div>      
     </div>
     <br/>

     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table" id='data_surat'>
         <thead>
          <tr class="bg-primary text-white">
           <th class="font-12">UPT Pemilik Alat</th>
           <th class="font-12">Surat Pinjaman</th>
           <th class="font-12">Status Pinjaman</th>
           <th class="font-12">Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (isset($list_surat)) { ?>
           <?php foreach ($list_surat as $value) { ?>
            <tr id="<?php echo $value['id'] ?>">
             <td>
              <select class="form-control" error="UPT" id='upt_surat'>
               <option value="">Pilih UPT</option>
               <?php if (!empty($list_upt)) { ?>
                <?php foreach ($list_upt as $v_u) { ?>
                 <?php $selected = $v_u['id'] == $value['upt_id'] ? 'selected' : '' ?>
                 <option <?php echo $selected ?> value="<?php echo $v_u['id'] ?>"><?php echo $v_u['upt'] ?></option>
                <?php } ?>
               <?php } ?>
              </select>
             </td>
             <td>
              <div class="fileinput fileinput-new input-group" data-provides="fileinput">
               <div class="form-control" data-trigger="fileinput"> 
                <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                <span class="fileinput-filename"><?php echo isset($value['file']) ? $value['file'] : '' ?></span>
               </div> 
               <span class="input-group-addon btn btn-default btn-file"> 
                <span class="fileinput-new" onclick="PinjamEksternal.upload(this)">Select file</span> 
                <span class="fileinput-exists">Change</span>
                <input class="file" id='file' type="file" name="..." onchange="PinjamEksternal.getFilename(this, 'xls')"> 
               </span> 
               <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
              </div>
             </td>
             <td>
              <select class="form-control" error="Status Pinjaman" id='status_pinjaman'>
               <?php if (!empty($list_status)) { ?>
                <?php foreach ($list_status as $v_s) { ?>
                 <?php $selected = $v_s['id'] == $value['status_pinjaman'] ? 'selected' : '' ?>
                 <option <?php echo $selected ?> value="<?php echo $v_s['id'] ?>"><?php echo $v_s['status'] ?></option>
                <?php } ?>
               <?php } ?>
              </select>
             </td>
             <td>
              <i class="mdi mdi-delete mdi-24px hover text-danger" onclick="PinjamEksternal.removeDetail(this)"></i>
             </td>
            </tr>
           <?php } ?>
           <tr class="baru">
            <td>
             <select class="form-control" error="UPT" id='upt_surat'>
              <option value="">Pilih UPT</option>
              <?php if (!empty($list_upt)) { ?>
               <?php foreach ($list_upt as $value) { ?>
                <option value="<?php echo $value['id'] ?>"><?php echo $value['upt'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </td>
            <td>
             <div class="fileinput fileinput-new input-group" data-provides="fileinput">
              <div class="form-control" data-trigger="fileinput"> 
               <i class="glyphicon glyphicon-file fileinput-exists"></i> 
               <span class="fileinput-filename"><?php echo isset($manual_book) ? $manual_book : '' ?></span>
              </div> 
              <span class="input-group-addon btn btn-default btn-file"> 
               <span class="fileinput-new" onclick="PinjamEksternal.upload(this)">Select file</span> 
               <span class="fileinput-exists">Change</span>
               <input class="file" id='file' type="file" name="..." onchange="PinjamEksternal.getFilename(this, 'xls')"> 
              </span> 
              <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
             </div>
            </td>
            <td>
             <select class="form-control" error="Status Pinjaman" id='status_pinjaman'>
              <?php if (!empty($list_status)) { ?>
               <?php foreach ($list_status as $value) { ?>
                <option value="<?php echo $value['id'] ?>"><?php echo $value['status'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </td>
            <td>
             <i class="mdi mdi-plus-circle mdi-24px hover text-warning" onclick="PinjamEksternal.addDetail(this)"></i>
            </td>
           </tr>
          <?php } else { ?>
           <tr class="baru">
            <td>
             <select class="form-control" error="UPT" id='upt_surat'>
              <option value="">Pilih UPT</option>
              <?php if (!empty($list_upt)) { ?>
               <?php foreach ($list_upt as $value) { ?>
                <option value="<?php echo $value['id'] ?>"><?php echo $value['upt'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </td>
            <td>
             <div class="fileinput fileinput-new input-group" data-provides="fileinput">
              <div class="form-control" data-trigger="fileinput"> 
               <i class="glyphicon glyphicon-file fileinput-exists"></i> 
               <span class="fileinput-filename"><?php echo isset($manual_book) ? $manual_book : '' ?></span>
              </div> 
              <span class="input-group-addon btn btn-default btn-file"> 
               <span class="fileinput-new" onclick="PinjamEksternal.upload(this)">Select file</span> 
               <span class="fileinput-exists">Change</span>
               <input class="file" id='file' type="file" name="..." onchange="PinjamEksternal.getFilename(this, 'xls')"> 
              </span> 
              <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
             </div>
            </td>
            <td>
             <select class="form-control" error="Status Pinjaman" id='status_pinjaman'>
              <?php if (!empty($list_status)) { ?>
               <?php foreach ($list_status as $value) { ?>
                <option value="<?php echo $value['id'] ?>"><?php echo $value['status'] ?></option>
               <?php } ?>
              <?php } ?>
             </select>
            </td>
            <td>
             <i class="mdi mdi-plus-circle mdi-24px hover text-warning" onclick="PinjamEksternal.addDetail(this)"></i>
            </td>
           </tr>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>      
     </div>
     <br/>

     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="PinjamEksternal.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="PinjamEksternal.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
