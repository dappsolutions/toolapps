<div id="signature-pad" class="signature-pad">
 <div class="signature-pad--body">
  <canvas></canvas>
 </div>
 <div class="signature-pad--footer">
  <div class="description">Tanda Tangan </div>

  <div class="signature-pad--actions">
   <div>
    <button type="button" class="btn btn-warning" onclick="clearSign()">Clear</button>
    <button type="button" class="btn btn-primary" onclick="approveSign()">Approve</button>
   </div>
  </div>
 </div>
</div>

<script>
 var wrapper = document.getElementById("signature-pad");
 var canvas = wrapper.querySelector("canvas");

 var signaturePad = new SignaturePad(canvas, {
  // It's Necessary to use an opaque color when saving image as JPEG;
  // this option can be omitted if only saving as PNG or SVG
  backgroundColor: 'rgb(255, 255, 255)'
 });

 function  clearSign() {
  signaturePad.clear();
 }

 function approveSign() {
  if (signaturePad.isEmpty()) {
   toastr.warning("Tanda Tangan Harus Diisi");
  } else {
   var dataURL = signaturePad.toDataURL();

   $.ajax({
    type: 'POST',
    data: {
     signature: dataURL,
     pegawai: '<?php echo $pegawai ?>'
    },
    dataType: 'json',
    async: false,
    url: url.base_url(PinjamEksternal.module()) + "simpanTtd",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Pembuatan Tanda Tangan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      PinjamEksternal.execApproveAsman('<?php echo $id ?>', '<?php echo $status ?>')
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 }

 $(function () {
 });
</script>