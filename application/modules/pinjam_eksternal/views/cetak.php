<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title><?php echo $no_peminjaman; ?></title>?>" />

        <style>
            body {
                font-family: "Helvetica", sans-serif;
                font-size: 11px;
            }
            table {
                width: 100%;
            }
            .text-center {
                text-align: center;
            }
            .text-right {
                text-align: right;
            }
            .font-bold {
                font-weight: bold;
            }
            .mb-32px {
                margin-bottom: 32px;
            }
            .mr-8px {
                margin-right: 8px;
            }
            .ml-8px {
                margin-left: 8px;
            }
            .table-logo {
                width: 100%;
            }
            table.table-logo > tbody > tr > td {
                padding: 16px;
            }
            table.table-logo td {
                vertical-align: top;
            }
            .table-item {
                width: 100%;
                margin-top: 16px;
                border-collapse: collapse;
                font-size: 8px;
            }
            table.table-item th,  table.table-item td {
                border: 1px solid #333;
                padding: 4px 8px;
                border-collapse: collapse;
                vertical-align: top;
            }
            .media {
                display: -ms-flexbox;
                display: flex;
                -ms-flex-align: start;
                align-items: flex-start;
            }
            .media-body {
                -ms-flex: 1;
                flex: 1;
            }

            .none-border{
                border:none;
            }
        </style>
    </head>
    <body>
        <table class="table-logo">
            <tbody>
                <tr>
                    <td style="width: 50%">
                        <table style="margin-bottom: 8px;border-collapse: collapse;border:1px solid #000;">
                            <tr>
                                <td style="border:1px solid #000;padding: 16px;" width="40"><img src="<?php echo base_url() ?>assets/images/logo_pln_cetak.png" alt="" width="40"></td>
                                <td style="text-align: center;padding: 16px;border:1px solid #000;">
                                    <h2>PT PLN (PERSERO)</h2>
                                    <h2>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h2>
                                    <h3 style="font-weight: normal;">Jl. Suningrat No. 45, Taman - Sidoarjo, Jawa Timur 61257</h3>
                                    <h3 style="font-weight: normal;">Telp./email : (031) 7882113 / labminyakpln@gmail.com</h3>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;padding: 16px;">
                                    <h2>Form Pengajuan Peminjaman Peralatan</h2>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table-item" style="width: 320px;">   
            <tbody>
                <tr>
                    <td style="border:none;">No Peminjaman</td>     
                    <td style="border:none;">:</td>     
                    <td style="border:none;"><?php echo $no_peminjaman ?></td>
                </tr>
                <tr>
                    <td style="border:none;">Tanggal Pengajuan</td>
                    <td style="border:none;">:</td>     
                    <td style="border:none;"><?php echo $tanggal_pinjam ?></td>
                </tr>
                <tr>
                    <td style="border:none;">UPT Peminjam</td>     
                    <td style="border:none;">:</td>     
                    <td style="border:none;"><?php echo $upt_peminjam ?></td>
                </tr>
<!--                <tr>
                    <td style="border:none;">UPT Pemilik Alat</td>
                    <td style="border:none;">:</td>     
                    <td style="border:none;"><?php echo $keterangan ?></td>
                </tr>-->
            </tbody>
        </table>
        <br/>

        <table class="table-item" style="width: 100%;">   
            <tbody>
                <tr>
                    <td colspan="6" style="border:none;"><u><b>Daftar Peralatan</b></u></td>
                </tr>
                <tr>
                    <th style="border:none;text-align:center;border:1px solid #ccc;">No</th>
                    <th style="border:none;text-align:center;border:1px solid #ccc;">Kode Alat</th>
                    <th style="border:none;text-align:center;border:1px solid #ccc;">Merk</th>
                    <th style="border:none;text-align:center;border:1px solid #ccc;">Tipe</th>
                    <th style="border:none;text-align:center;border:1px solid #ccc;">No. Seri</th>
                    <th style="border:none;text-align:center;border:1px solid #ccc;">UPT Pemilik Alat</th>
                    <th style="border:none;text-align:center;border:1px solid #ccc;">Status Kepemilikan</th>                    
                    <!--<th style="border:none;text-align:center;border:1px solid #ccc;">Jumlah</th>-->
                    <th style="border:none;text-align:center;border:1px solid #ccc;">Status Peminjaman</th>
                </tr>
                <?php if (!empty($list_alat)) { ?>
                 <?php $no = 1; ?>
                 <?php foreach ($list_alat as $value) { ?>
                  <tr>
                      <td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo $no++ ?></td>
                      <td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo $value['kode_alat'] ?></td>
                      <td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo $value['merk'] ?></td>
                      <td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo $value['tipe'] ?></td>
                      <td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo $value['nomer_seri'] ?></td>                      
                      <td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo $value['upt'] ?></td>       
                      <td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo $value['status_alat'] ?></td>
                      <!--<td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo $value['qty'] ?></td>-->                                   
                      <td style="border:none;text-align:center;border:1px solid #ccc;"><?php echo 'Eksternal' ?></td> 
                  </tr>
                 <?php } ?>
                <?php } ?>
            </tbody>
        </table>
        <br/>


        <?php if (!empty($status_approve)) { ?>
         <table style="width: 100%; margin-top: 32px">
             <tbody>
                 <tr>
                     <?php foreach ($status_approve as $value) { ?>
                      <td class="text-center"><?php echo $value['tgl_approve'] ?></td>
                     <?php } ?>
                 </tr>
                 <tr>
                     <?php foreach ($status_approve as $value) { ?>
                      <td class="text-center">Menyetujui,</td>
                      <td class="text-center">&nbsp;</td>
                     <?php } ?>
                 </tr>
                 <tr>
                     <?php foreach ($status_approve as $value) { ?>
                      <td class="text-center" style="padding: 16px;">        
                          <?php if ($value['file'] != "") { ?>
                           <img width='30' height="30" src="<?php echo $value['file'] ?>">
                          <?php } ?>
                      </td>
                     <?php } ?>
                 </tr>
                 <tr>
                     <?php foreach ($status_approve as $value) { ?>
                      <td class="text-center"><?php echo $value['pegawai'] . ' ASMAN (' . $value['upt'] . ')' ?></td>
                     <?php } ?>
                 </tr>
             </tbody>
         </table>
        <?php } else { ?>
         -
        <?php } ?>
    </body>
</html>
