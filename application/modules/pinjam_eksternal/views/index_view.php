<div class='container-fluid'>
 <div class="row">
  <div class="col-md-12">
   <div class="white-box">  
    <div class="card-body">
     <div class="row">
      <div class="col-md-12">
       <div class="white-box">
        <label class="label label-primary font-12">Keterangan Status Pengajuan</label><br/><br/>
        <div class="table-responsive">
         <table class="table">
          <thead>
           <tr>
            <td>Status</td>
            <td>Ket.</td>
           </tr>
          </thead>
          <tbody>
           <tr>
            <td><label class="label label-warning font-10">DRAFT</label></td>
            <td><label class="label label-default font-10 text-dark">Status khusus pengajuan eksternal belum disetujui oleh Asman</label></td>
           </tr>
           <tr>
            <td><label class="label label-success font-10">COMPLETE</label></td>
            <td><label class="label label-default font-10 text-dark">Status sudah disetujui oleh Asman (Pengajuan Eksternal)</label></td>
           </tr>
           <tr>
            <td><label class="label label-primary font-10">BACK</label></td>
            <td><label class="label label-default font-10 text-dark">Status semua alat sudah dikembalikan</label></td>
           </tr>
           <tr>
            <td><label class="label label-warning font-10">TAKEN</label></td>
            <td><label class="label label-default font-10 text-dark">Status semua alat sudah diambil peminjam</label></td>
           </tr>
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>
     <br/>

     <h4 class="card-title"><u><?php echo $title ?></u></h4>

     <div class='row'>
      <div class='col-md-2'>
       <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR PEMINJAMAN ALAT') { ?>
        <button id="" class="btn btn-block btn-warning" onclick="PinjamEksternal.make()">Buat</button>
       <?php } ?>
      </div>
      <div class='col-md-10'>
       <input type='text' name='' id='' onkeyup="PinjamEksternal.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
      </div>     
     </div>        
     <br/>
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div>
     <hr/>

     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <div class="sticky-table sticky-headers sticky-ltr-cells">
         <table class="table color-bordered-table primary-bordered-table">
          <thead>
           <tr class="text-white sticky-row">
            <th class="font-12 bg-primary">No</th>
            <th class="font-12 bg-primary">No. Peminjaman</th>
            <th class="font-12 bg-primary">Tanggal Peminjaman</th>
            <th class="font-12 bg-primary">Peminjam</th>
            <th class="font-12 bg-primary">UPT Peminjam</th>
            <th class="font-12 bg-primary">Status</th>
            <th class="font-12 bg-primary">Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <tr>
              <td class="font-12"><?php echo $no++ ?></td>
              <td class="font-12"><b><?php echo $value['no_peminjaman'] ?></b></td>
              <td class="font-12"><?php echo $value['tanggal_pinjam'] ?></td>
              <td class="font-12"><?php echo $value['nama_pegawai'] ?></td>
              <td class="font-12"><?php echo $value['upt'] ?></td>
              <td class="font-12">
               <?php
               if ($value['status'] == 'DRAFT') {
                $status_color = 'label-warning';
               }

               if ($value['status'] == 'APPROVE' || $value['status'] == 'COMPLETE') {
                $status_color = 'label-success';
               }

               if ($value['status'] == 'REJECT') {
                $status_color = 'label-danger';
               }
               ?>
               <label class="label <?php echo $status_color ?>"><?php echo $value['status'] ?></label>
              </td>
              <td class="text-center">
               <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR PEMINJAMAN ALAT' && $value['status'] == 'DRAFT') { ?>
                <label id="" class="label label-warning font-10" 
                       onclick="PinjamEksternal.ubah('<?php echo $value['id'] ?>')">Ubah</label>
                &nbsp;
               <?php } ?>
               <label id="" class="label label-success font-10" 
                      onclick="PinjamEksternal.detail('<?php echo $value['id'] ?>')">Detail</label>
               &nbsp;
               <label id="" class="label label-primary font-10" 
                      onclick="PinjamEksternal.cetak('<?php echo $value['id'] ?>')">Cetak</label>            
               &nbsp;
               <?php if ($this->session->userdata('hak_akses') == 'REQUESTOR PEMINJAMAN ALAT' && $value['status'] == 'DRAFT') { ?>
                <label id="" class="label label-danger font-10" 
                       onclick="PinjamEksternal.delete('<?php echo $value['id'] ?>')">Hapus</label>
                &nbsp;
               <?php } ?>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td class="text-center font-12" colspan="8">Tidak Ada Data Ditemukan</td>
            </tr>
           <?php } ?>         
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>    
     <br/>
     <div class="row">
      <div class="col-md-12">
       <div class="pagination">
        <?php echo $pagination['links'] ?>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>