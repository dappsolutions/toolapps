<?php

class Validasi_anomali extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 40;
 }

 public function getModuleName() {
  return 'validasi_anomali';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/anomali_tervalidasi.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/validasi_anomali.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'anomali_alat';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Anomali";
  $data['title_content'] = 'Data Anomali';
  $content = $this->getDataAnomali();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataAnomali($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('aa.no_anomali', $keyword),
       array('ut.upt', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' aa',
              'field' => array('aa.*', 'u.username as validator', 'ut.upt as nama_upt'),
              'join' => array(
                  array('user u', 'aa.validation = u.id', 'left'),
                  array('upt ut', 'aa.upt = ut.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "aa.validation = 0"
  ));

  return $total;
 }

 public function getDataAnomali($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('aa.no_anomali', $keyword),
       array('ut.upt', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' aa',
              'field' => array('aa.*', 'u.username as validator', 'ut.upt as nama_upt'),
              'join' => array(
                  array('user u', 'aa.validation = u.id', 'left'),
                  array('upt ut', 'aa.upt = ut.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "aa.validation = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAnomali($keyword)
  );
 }

 public function getDetailDataAnomali($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListStatus() {
  $data = Modules::run('database/get', array(
              'table' => 'status_alat',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Anomali";
  $data['title_content'] = 'Tambah Anomali';
  $data['list_status'] = $this->getListStatus();
  echo Modules::run('template', $data);
 }

 public function getDataDetailAlat($anomali) {
  $data = Modules::run('database/get', array(
              'table' => 'anomali_alat_detail aad',
              'field' => array('aad.*', 'na.nama_alat', 'a.nomer_seri',
                  'sa.status', 'a.kode_alat'),
              'join' => array(
                  array('alat a', 'aad.alat = a.id'),
                  array('nama_alat na', 'a.alat = na.id'),
                  array('status_alat sa', 'aad.status_alat = sa.id'),
              ),
              'where' => array('aad.anomali_alat' => $anomali)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataAnomali($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Anomali";
  $data['title_content'] = 'Detail Anomali';
  $data['detail_alat'] = $this->getDataDetailAlat($id);
  echo Modules::run('template', $data);
 }

 public function showDataAlat() {
  $index_tr = $this->input->post('index_tr');
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $content = $this->getDataAlat($this->limit, $this->last_no);
  $data['detail_content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/seekPagination/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['index_tr'] = $index_tr;
  echo $this->load->view('detail_alat', $data, true);
 }

 public function seekPagination($offset) {
  $this->segment = 3;
  $offset = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $offset * $this->limit;

  $content = $this->getDataAlat($this->limit, $this->last_no);
  $data['detail_content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/seekPagination/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo $this->load->view('table_alat', $data, true);
 }

 public function getTotalDataAlat() {
  $like = array();
  $total = Modules::run('database/count_all', array(
              'table' => 'alat aa',
              'field' => array('aa.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt'),
              'join' => array(
                  array('kategori_alat ka', 'aa.kategori_alat = ka.id and aa.deleted = 0'),
                  array('tahun t', 'aa.tahun = t.id and aa.deleted = 0'),
                  array('upt ut', 'aa.upt = ut.id and aa.deleted = 0'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $this->upt_id
  ));

  return $total;
 }

 public function getDataAlat($limit = 1, $offset = 0) {
  $like = array();
  $data = Modules::run('database/get', array(
              'table' => 'alat aa',
              'field' => array('aa.*', 'ka.kategori',
                  't.tahun as tahun_perolehan', 'ut.upt as nama_upt'),
              'join' => array(
                  array('kategori_alat ka', 'aa.kategori_alat = ka.id and aa.deleted = 0'),
                  array('tahun t', 'aa.tahun = t.id and aa.deleted = 0'),
                  array('upt ut', 'aa.upt = ut.id and aa.deleted = 0'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $limit,
              'offset' => $offset,
              'where' => $this->upt_id
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAlat()
  );
 }

 public function getPostDataHeader($value) {
  $data['no_anomali'] = Modules::run('no_generator/generateNomerAnomali');
  $data['tgl_anomali'] = $value->tanggal_anomali;
  $data['upt'] = $this->session->userdata('upt_id');
  return $data;
 }

 public function getPostAnomaliAlatDetail($anomali, $value) {
  $data['anomali_alat'] = $anomali;
  $data['user'] = $this->session->userdata('user_id');
  $data['alat'] = $value->id_alat;
  $data['status_alat'] = $value->status;
  $data['keterangan_anomali'] = $value->keterangan_anomali;
  ;
  return $data;
 }

 public function approve($id) {
  $user = $this->session->userdata('user_id');

  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('validation' => $user), array('id' => $id));

   //insert user validation
   Modules::run('database/_insert', 'user_validation', array(
       'user' => $user,
       'anomali_alat' => $id,
       'date_validation' => date('Y-m-d H:i:s')
   ));

   $detail_item = $this->getDetailListItemAlat($id);
   if (!empty($detail_item)) {
    foreach ($detail_item as $value) {
     $post_update['status_alat'] = $value['status_alat'];
     Modules::run('database/_update', 'alat', $post_update, array('id' => $value['alat']));
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDetailListItemAlat($anomali) {
  $data = Modules::run('database/get', array(
              'table' => 'anomali_alat_detail aad',
              'field' => array('aad.*'),
              'where' => "aad.deleted = 0 and aad.anomali_alat = '" . $anomali . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Anomali";
  $data['title_content'] = 'Data Anomali';
  $content = $this->getDataAnomali($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

}
