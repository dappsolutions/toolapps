<div class='container-fluid'>

 <div class='row'>
  <div class='col-12'>
   <div class="white-box">  
    <div class="card-body">
     <h4 class="card-title"><u><?php echo $title ?></u></h4>

     <div class='row'>
      <div class='col-md-12'>
       <input type='text' name='' id='' onkeyup="ValidasiAnomali.search(this, event)" class='form-control' value='' placeholder="Pencarian"/>    
      </div>     
     </div>        
     <br/>
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div>
    </div>
   </div> 
  </div>
 </div>
 <br/>
 <div class='row'>
  <div class='col-12'>
   <div class='white-box'>
    <div class='card-body'>
     <div class='row'>
      <div class='col-md-12'>
       <div class="table-responsive">
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr class="bg-success text-white">
           <th class="font-12">No</th>
           <th class="font-12">No Anomali</th>
           <th class="font-12">UPT</th>
           <th class="font-12">Tanggal Anomali</th>
           <th class="font-12">Divalidasi</th>
           <th class="font-12">Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr>
             <td class="font-12"><?php echo $no++ ?></td>
             <td class="font-12"><?php echo '<b>'.$value['no_anomali'].'</b>' ?></td>
             <td class="font-12"><?php echo $value['nama_upt'] ?></td>
             <td class="font-12"><?php echo date('d F Y', strtotime($value['tgl_anomali'])) ?></td>
             <td class="font-12"><?php echo $value['validator'] == '' ? '<label class="label label-warning">Belum Divalidasi</label>' : '<label class="label label-success">Divalidasi oleh, '.$value['validator'].'</label>' ?></td>
             <td class="text-center">
              <?php if ($value['validation'] == 0) { ?>
               <label id="" class="label label-success font-10" 
                      onclick="ValidasiAnomali.approve('<?php echo $value['id'] ?>')">Approve</label>
                     <?php } ?>
              &nbsp;
              <label id="" class="label label-warning font-10" 
                     onclick="ValidasiAnomali.detail('<?php echo $value['id'] ?>')">Detail</label>
              &nbsp;
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td class="text-center" colspan="6">Tidak Ada Data Ditemukan</td>
           </tr>
          <?php } ?>         
         </tbody>
        </table>
       </div>
      </div>
     </div>    
     <br/>
     <div class="row">
      <div class="col-md-12">
       <div class="pagination">
        <?php echo $pagination['links'] ?>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>