<div class='col-md-12'>
 <div class='white-box'>
  <div class='card-body'>
   <h4><u>Pilih Alat</u></h4>
   <div class="table-responsive">
    <table class="table color-bordered-table primary-bordered-table">
     <thead>
      <tr>
       <th class="font-12">No</th>
       <th class="font-12">UPT</th>
       <th class="font-12">Kode Alat</th>
       <th class="font-12">Nomer Seri</th>
       <th class="font-12">Nama Alat</th>
       <th class="font-12">Merk Alat</th>
       <th class="font-12">Kategori Alat</th>
       <th class="font-12">Tipe Alat</th>
       <th class="font-12">Tahun Perolehan</th>
       <th class="font-12">Keterangan</th>
       <th class="font-12">Action</th>
      </tr>
     </thead>
     <tbody>
      <?php if (!empty($detail_content)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($detail_content as $value) { ?>
        <tr>
         <td class="font-12"><?php echo $no++ ?></td>
         <td class="font-12"><?php echo $value['nama_upt'] ?></td>
         <td class="font-12"><?php echo '<b>' . $value['barcode'] . '</b>' ?></td>
         <td class="font-12"><?php echo $value['nomer_seri'] ?></td>
         <td class="font-12"><?php echo $value['alat'] ?></td>
         <td class="font-12"><?php echo $value['merk'] ?></td>
         <td class="font-12"><?php echo $value['kategori'] ?></td>
         <td class="font-12"><?php echo $value['tipe'] ?></td>
         <td class="font-12"><?php echo $value['keterangan'] ?></td>
         <td class="font-12"><?php echo $value['tahun_perolehan'] ?></td>
         <td class="text-center">
          <label id="" class="label label-warning font-10 hover" 
                 onclick="Anomali.pilihAlat(this, '<?php echo $value['id'] ?>')">Pilih</label>
         </td>
        </tr>
       <?php } ?>
      <?php } else { ?>
       <tr>
        <td class="text-center font-12" colspan="12">Tidak Ada Data Ditemukan</td>
       </tr>
      <?php } ?>         
     </tbody>
    </table>
   </div>
  </div>
 </div>
</div>
<br/>
<br/>
<div class="col-md-12">
 <div class="pagination">
  <?php echo $pagination['links'] ?>
 </div>
</div>