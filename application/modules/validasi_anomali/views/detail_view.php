<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <div class="row">
      <div class='col-md-3'>
       No. Anomali
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $no_anomali ?>
      </div>     
     </div>
     <br/>
     <div class="row">
      <div class='col-md-3'>
       Tanggal Anomali
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo date('d F Y', strtotime($tgl_anomali)) ?>
      </div>     
     </div>
     <br/>     
    </div>
   </div>
  </div>
 </div>

 <br/>
 <div class='row'>
  <div class='col-12'>
   <div class='white-box'>
    <div class='card-body'>
     <div class="row">
      <div class='col-md-3'>
       <u>Anomali Alat</u>
      </div>
      <div class='col-md-9 text-right'>
       <button onclick="AnomaliTervalidasi.exportExcel('<?php echo $id ?>')" id="" class="btn btn-success">Export Excel</button>
       &nbsp;
              <button onclick="Template.showUpdateSystem(this, event)" id="" class="btn btn-danger">Export PDF <label class="badge badge-warning">PRO</label></button>
      </div>
     </div>
     <br/>

     <div class="row">
      <div class='col-md-12'>
       <div class='table-responsive'>
        <table class="table color-bordered-table primary-bordered-table">
         <thead>
          <tr class="bg-success text-white">
           <th class="font-12">Pilih Alat</th>
           <th class="font-12">Kode Alat</th>
           <th class="font-12">Status</th>
           <th class="font-12">Keterangan</th>
          </tr>
         </thead>
         <tbody class="table_anomali">
          <?php if (!empty($detail_alat)) { ?>
           <?php foreach ($detail_alat as $value) { ?>
            <tr>
             <td class="font-12"><?php echo '<b>'.$value['nama_alat'].'</b>' . ' <label class="text-success">(' . $value['nomer_seri'] . ')</label>' ?></td>
             <td class="font-12"><?php echo '<b>'.$value['kode_alat'].'</b>'?></td>
             <td class="font-12"><?php echo $value['status'] == 'Rusak' ? '<label class="text-danger">Rusak</label>' : '<label class="text-success">Normal</label>' ?></td>
             <td class="font-12"><?php echo $value['keterangan_anomali'] ?></td>
            </tr>
           <?php } ?>
          <?php } ?>
         </tbody>
        </table>
       </div>
      </div>
     </div>
     <br/>
     <hr/>
     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="ValidasiAnomali.approve('<?php echo isset($id) ? $id : '' ?>')">Approve</button>
       &nbsp;
       <button id="" class="btn" onclick="ValidasiAnomali.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
