<?php

class Ultg extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'ultg';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/ultg.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'gardu_induk';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data ULTG";
  $data['title_content'] = 'Data ULTG';
  $content = $this->getDataULTG();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataULTG($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.upt', $keyword),
       array('gi.gardu', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' gi',
              'field' => array('gi.*', 'u.upt as nama_upt'),
              'join' => array(
                  array('upt u', 'gi.upt = u.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "gi.deleted = 0 and gi.context = 'ULTG'"
  ));

  return $total;
 }

 public function getDataULTG($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('u.upt', $keyword),
       array('gi.gardu', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' gi',
              'field' => array('gi.*', 'u.upt as nama_upt', 
                  'kr_p.gardu as nama_parent', 'kr_p.context as ctx_parent'),
              'join' => array(
                  array('upt u', 'gi.upt = u.id'),
                  array($this->getTableName().' kr_p', 'kr_p.id = gi.parent', 'left'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "gi.deleted = 0 and gi.context = 'ULTG'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataULTG($keyword)
  );
 }

 public function getDetailDataULTG($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*', 'u.upt as nama_upt', 
                  'kr_p.gardu as nama_parent', 'kr_p.context as ctx_parent'),
              'join' => array(
                  array('upt u', 'kr.upt = u.id'),
                  array($this->getTableName().' kr_p', 'kr_p.id = kr.parent', 'left'),
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListParent() {
  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk gi',
              'field' => array('gi.*'),
              'where' => "gi.deleted = 0 and (gi.context = 'ULTG' or gi.context = 'PDKB')"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah ULTG";
  $data['title_content'] = 'Tambah ULTG';
  $data['list_upt'] = $this->getListUpt();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataULTG($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah ULTG";
  $data['title_content'] = 'Ubah ULTG';
  $data['list_upt'] = $this->getListUpt();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataULTG($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail ULTG";
  $data['title_content'] = 'Detail ULTG';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['upt'] = $value->upt;
  $data['gardu'] = $value->gardu;
  $data['alamat'] = $value->alamat;
  $data['context'] = 'ULTG';
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $gardu = $id;
  $this->db->trans_begin();
  try {
   $post_gardu = $this->getPostDataHeader($data);
   if ($id == '') {
    $gardu = Modules::run('database/_insert', $this->getTableName(), $post_gardu);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_gardu, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'gardu' => $gardu));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data ULTG";
  $data['title_content'] = 'Data ULTG';
  $content = $this->getDataULTG($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
