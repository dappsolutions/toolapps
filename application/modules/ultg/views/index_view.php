<div class='container-fluid'>
 <div class="white-box">
  <div class="card-body">
   <div class='row'>
    <div class='col-md-2'>
     <button id="" class="btn btn-block btn-warning" onclick="Ultg.add()">Tambah</button>
    </div>
    <div class='col-md-10'>
     <input type='text' name='' onkeyup="Ultg.search(this, event)" id='' class='form-control' value='' placeholder="Pencarian"/>    
    </div>     
   </div>
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <?php if (isset($keyword)) { ?>
      <?php if ($keyword != '') { ?>
       Cari Data : "<b><?php echo $keyword; ?></b>"
      <?php } ?>
     <?php } ?>
    </div>
   </div>
   <br/>
   <div class='row'>
    <div class='col-md-12'>
     <div class="table-responsive">
      <table class="table color-bordered-table primary-bordered-table">
       <thead>
        <tr>
         <th class="font-12">No</th>
         <th class="font-12">Nama UPT</th>
         <th class="font-12">ULTG</th>
         <th class="font-12">Alamat</th>
         <th class="font-12">Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td class="font-12"><?php echo $no++ ?></td>
           <td class="font-12"><?php echo $value['nama_upt'] ?></td>
           <td class="font-12"><?php echo $value['gardu'] ?></td>
           <td class="font-12"><?php echo $value['alamat'] ?></td>
           <td class="text-center">
            <label id="" class="label label-warning font-10" 
                    onclick="Ultg.ubah('<?php echo $value['id'] ?>')">Ubah</label>
            &nbsp;
            <label id="" class="label label-success font-10" 
                    onclick="Ultg.detail('<?php echo $value['id'] ?>')">Detail</label>
            &nbsp;
            <label id="" class="label label-danger font-10" 
                    onclick="Ultg.delete('<?php echo $value['id'] ?>')">Hapus</label>
            &nbsp;
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td class="text-center" colspan="6">Tidak Ada Data Ditemukan</td>
         </tr>
        <?php } ?>         
       </tbody>
      </table>
     </div>
    </div>
   </div>    
   <br/>
   <div class="row">
    <div class="col-md-12">
     <div class="pagination">
      <?php echo $pagination['links'] ?>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>