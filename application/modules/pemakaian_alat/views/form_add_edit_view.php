<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Kode Alat
      </div>
      <div class='col-md-4'>
       <select id="kode_alat" error="Kode Alat" class="form-control required">
        <?php if (!empty($list_alat)) { ?>
         <?php foreach ($list_alat as $value) { ?>
          <?php $selected = "" ?>
          <?php if (isset($alat)) { ?>
           <?php $selected = $value['id'] == $alat ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_alat'].' - '.$value['kategori'].' - '.$value['nomer_seri'].' - '.$value['kode_alat'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tanggal Pemakaian
      </div>
      <div class='col-md-4'>
       <input type='text' name='' id='tanggal_pemakaian' class='form-control required' 
              value='<?php echo isset($tgl_pemakaian) ? $tgl_pemakaian : '' ?>' error="Tanggal Pemakaian"/>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4'>
       <textarea class="form-control" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
      </div>     
     </div>
     <hr/>    
     <br/>

     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn btn-success" onclick="PemakaianAlat.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
       &nbsp;
       <button id="" class="btn" onclick="PemakaianAlat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
