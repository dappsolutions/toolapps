<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="container-fluid"> 
 <div class="row">
  <div class='col-12'>
   <div class="white-box">       
    <div class="card-body card-block">   
     <label class="label label-warning"><?php echo $title ?></label>
     <hr/>
     <div class="row">
      <div class='col-md-3'>
       Alat
      </div>
      <div class='col-md-4 text-danger'>
						<?php echo $nama_alat.' - '.$kategori.' - '.$nomer_seri.' - '.$kode_alat ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Tanggal Pemakaian
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $tgl_pemakaian ?>
      </div>     
     </div>
     <br/>

     <div class="row">
      <div class='col-md-3'>
       Keterangan
      </div>
      <div class='col-md-4 text-danger'>
       <?php echo $keterangan ?>       
      </div>     
     </div>
     <hr/>    
     <br/>

     <div class='row'>
      <div class='col-md-12 text-right'>
       <button id="" class="btn" onclick="PemakaianAlat.back()">Kembali</button>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
