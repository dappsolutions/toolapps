<?php

class Pemakaian_alat extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pemakaian_alat';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/select2.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pemakaian_alat.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pemakaian_alat';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pemakaian Alat";
  $data['title_content'] = 'Data Pemakaian Alat';
  $content = $this->getDataPemakaian();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPemakaian($keyword = '') {
  $upt_id = $this->session->userdata('upt_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('na.nama_alat', $keyword),
       array('a.kode_alat', $keyword),
   );
  }
  
  $where = "ska.deleted = 0";
  if ($this->session->userdata('hak_akses') != "Superadmin") {
   $where = "ska.deleted = 0 and a.upt = '" . $upt_id . "'";
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' ska',
              'field' => array('ska.*', 'na.nama_alat', 'a.kode_alat', 'ka.kategori', 'a.nomer_seri'),
              'join' => array(
				  array('alat a', 'ska.alat = a.id and a.deleted = 0'),
				  array('nama_alat na', 'na.id = a.alat'),
				  array('kategori_alat ka', 'ka.id = na.kategori_alat'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => $where
  ));

  return $total;
 }

 public function getDataPemakaian($keyword = '') {
  $upt_id = $this->session->userdata('upt_id');
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('na.nama_alat', $keyword),
       array('a.kode_alat', $keyword),
   );
  }

  $where = "ska.deleted = 0";
  if ($this->session->userdata('hak_akses') != "Superadmin") {
   $where = "ska.deleted = 0 and a.upt = '" . $upt_id . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' ska',
              'field' => array('ska.*', 'na.nama_alat', 'a.kode_alat', 'ka.kategori', 'a.nomer_seri'),
              'join' => array(
				  array('alat a', 'ska.alat = a.id and a.deleted = 0'),
				  array('nama_alat na', 'na.id = a.alat'),
				  array('kategori_alat ka', 'ka.id = na.kategori_alat'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPemakaian($keyword)
  );
 }

 public function getDetailDataPemakaian($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' ska',
			  'field' => array('ska.*', 'a.kode_alat', 
			  'na.nama_alat', 'ka.kategori', 'a.nomer_seri'),
              'join' => array(
				  array('alat a', 'ska.alat = a.id'),
				  array('nama_alat na', 'na.id = a.alat'),
				  array('kategori_alat ka', 'ka.id = na.kategori_alat'),
              ),
              'where' => "ska.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getDataAlat() {
  $upt_id = $this->session->userdata('upt_id');
  $where = "a.deleted = 0";

  if ($this->session->userdata('hak_akses') != "Superadmin") {
//   $where = "deleted = 0 and upt = '" . $upt_id . "'";
   $where = "(a.alat_baru = 0 and a.approver_alat_baru = 0 and a.upt = '" . $upt_id . "' and a.deleted = 0) "
           . "or (a.alat_baru = 1 and a.approver_alat_baru = 1 and a.upt = '" . $upt_id . "' and a.deleted = 0)";
  }
  $data = Modules::run('database/get', array(
			  'table' => 'alat a',
			  'field' => array('a.*', 'ka.kategori', 'na.nama_alat'),
			  'join' => array(
				array('nama_alat na', 'a.alat = na.id'),
				array('kategori_alat ka', 'ka.id = na.kategori_alat')
			  ),
			  'where' => $where,
			  'orderby'=> 'na.nama_alat'
  ));

//  echo '<pre>';
//  echo $this->db->last_query();die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pemakaian Alat";
  $data['title_content'] = 'Tambah Pemakaian Alat';
  $data['list_alat'] = $this->getDataAlat();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPemakaian($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pemakaian Alat";
  $data['title_content'] = 'Ubah Pemakaian Alat';
  $data['list_alat'] = $this->getDataAlat();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPemakaian($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pemakaian Alat";
  $data['title_content'] = 'Detail Pemakaian Alat';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['alat'] = $value->kode_alat;
  $data['tgl_pemakaian'] = $value->tgl_pemakaian;
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post_upt = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_upt);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_upt, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pemakaian Alat";
  $data['title_content'] = 'Data Pemakaian Alat';
  $content = $this->getDataPemakaian($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/sertifikasi_kalibrasi/';
  $config['allowed_types'] = 'pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showSertifikat() {
  $file = str_replace(' ', '_', $this->input->post('file'));
  $data['file'] = $file;
  echo $this->load->view('file_sertifikat', $data, true);
 }

}
