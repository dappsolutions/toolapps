<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class="row">
     <div class="col-md-12 text-center">
      <p><b>IMuamalah</b>versi 1.0 (Startup)</p>
      Dikembangkan oleh Greenhole Tech (CV. Greenhole Technology Solution)
      <br/>
      Email: support@greenholetch.com
      <br/>
      WA / Telp: 0813.2818.5987
      <br/>
      Website : <a href="http://www.greenholetech.com">www.greenholetech.com</a>
     </div>          
    </div> 
   </div>
  </div>
 </div>
</div>
